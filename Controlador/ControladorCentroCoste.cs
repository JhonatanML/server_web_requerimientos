﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controlador
{
    public class ControladorCentroCoste
    {        
        public static DataTable CargarComboCentroCosteC(string id_perfil, string id_usuario)
        {
            return MCentroCoste.CargarComboCentroCosteM(id_perfil, id_usuario);
        }
    }
}
