﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Utilidad;

namespace Controlador
{
    public class ControladorSucursal
    {
        public static BeanPaginate paginarBuscarSucursal(string codigo, string nombre, string page, string rows)
        {
            BeanPaginate paginateSucursalBean = new BeanPaginate();
            List<SucursalBean> sucursalBeanList = new List<SucursalBean>();
            DataTable dataTable = MSucursal.paginarBuscarSucursales(codigo, nombre, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    sucursalBeanList.Add(new SucursalBean()
                    {
                        codigo = row["Codigo"].ToString(),
                        nombre = row["Nombre"].ToString(),
                        id = row["ide"].ToString()
                    });
                paginateSucursalBean.lstResultadoSucursalBean = sucursalBeanList;
                paginateSucursalBean.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                paginateSucursalBean.totalRegistros = totalNumberOfItems;
            }
            return paginateSucursalBean;
        }

        public static SucursalBean infoSucursal(string id)
        {
            DataTable dataTable = MSucursal.infoSucursal(id);
            SucursalBean sucursalBean = new SucursalBean();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];
                sucursalBean.id = dataRow["IDE"].ToString();
                sucursalBean.codigo = dataRow["CODIGO"].ToString();
                sucursalBean.nombre = dataRow["NOMBRE"].ToString();
            }
            return sucursalBean;
        }

        public static void borrarSucursal(string id)
        {
            MSucursal.borrarSucursal(id);
        }

        public static string crearSucursal(SucursalBean bean)
        {
            string @string = Convert.ToString(MSucursal.crearSucursal(bean));
            if (@string == "0")
                throw new Exception("Sucursal Repetida");
            return @string;
        }

        public static string editarSucursal(SucursalBean bean)
        {
            string @string = Convert.ToString(MSucursal.editarSucursal(bean));
            if (@string == "0")
                throw new Exception("Sucursal Repetida");
            return @string;
        }
    }
}
