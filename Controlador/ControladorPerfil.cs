﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlador
{
    public class ControladorPerfil
    {
        public static DataTable Perfil_dt_Combo()
        {
            return MPerfil.Perfil_dt_Combo();
        }

        public static DataTable Combo_PerfilC(Int32 id_perfil)
        {
            return MPerfil.Combo_PerfilM(id_perfil);
        }
    }
}
