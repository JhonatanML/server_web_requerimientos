﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Utilidad;

namespace Controlador
{
    public class ControladorCategoria
    {
        public static BeanPaginate BuscarCategoriaC(string linea, string nom_categoria, string page, string rows)
        {
            BeanPaginate CategoriaResultado = new BeanPaginate();
            List<BeanCategoria> ListaCategoria = new List<BeanCategoria>();
            DataTable dataTable = MCategoria.BuscarCategoriaM(linea, nom_categoria, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaCategoria.Add(new BeanCategoria()
                    {
                        id_categoria = Convert.ToInt32(row["id_categoria"]),
                        linea = row["linea"].ToString(),
                        nom_categoria = row["nom_categoria"].ToString(),
                    });
                CategoriaResultado.lstResultadoBeanCategoria = ListaCategoria;
                CategoriaResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                CategoriaResultado.totalRegistros = totalNumberOfItems;
            }
            return CategoriaResultado;
        }

        public static void EliminarCategoriaC(string id_categoria)
        {
            MCategoria.EliminarCategoriaM(id_categoria);
        }

        public static string AgregarCategoriaC(BeanCategoria bean)
        {
            string @string = Convert.ToString(MCategoria.AgregarCategoriaM(bean));
            if (@string == "0")
                throw new Exception("¡La categoria ya existe en la base de datos!");
            return @string;
        }

        public static BeanCategoria ObtenerCategoriaC(string id_categoria)
        {
            DataTable dataTable = MCategoria.ObtenerCategoriaM(id_categoria);
            BeanCategoria bean = new BeanCategoria();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];

                bean.id_categoria = Convert.ToInt32(dataRow["id_categoria"]);
                bean.nom_categoria = dataRow["nom_categoria"].ToString();
                bean.linea = dataRow["linea"].ToString();
            }
            return bean;
        }

        public static string EditarCategoriaC(BeanCategoria bean)
        {
            string @string = Convert.ToString(MCategoria.EditarCategoriaM(bean));
            if (@string == "0")
                throw new Exception("¡La categoria ya existe en la base de datos!");
            return @string;
        }
    }
}
