﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Utilidad;

namespace Controlador
{
    public class ControladorReporte
    {
        public static DataTable Sucursales_dt_combo()
        {
            return MReporte.Sucursales_dt_combo();
        }
        public static DataTable Sucursales_dt_combo_bycodigo(Int32 id, Int32 idperfil)
        {
            return MReporte.Sucursales_dt_combo_bycodigo(id, idperfil);
        }

        public static BeanPaginate consultaReporteBusqueda(BeanBusqueda bean)
        {
            BeanPaginate resultado = new BeanPaginate();
            List<BeanReporte> list = new List<BeanReporte>();

            BeanReporte beanOutput;

            DataTable dt = MReporte.consultaReporteBusqueda(bean);

            int totalRegistros = 0;

            if (dt != null && dt.Rows.Count > 0)
            {
                totalRegistros = Int32.Parse(dt.Rows[0]["total"].ToString());

                foreach (DataRow row in dt.Rows)
                {
                    beanOutput = new BeanReporte();
                    beanOutput.usuario = (row["usuario"]) == DBNull.Value ? "" : Convert.ToString(row["usuario"]);
                    beanOutput.sucursal = (row["sucursal"]) == DBNull.Value ? "" : Convert.ToString(row["sucursal"]);
                    beanOutput.consultas = (row["consultas"]) == DBNull.Value ? "" : Convert.ToString(row["consultas"]);
                    beanOutput.encontrados = (row["Encontrados"]) == DBNull.Value ? "" : Convert.ToString(row["Encontrados"]);
                    beanOutput.afiliados = (row["Afiliados"]) == DBNull.Value ? "" : Convert.ToString(row["Afiliados"]);
                    beanOutput.noafiliados = (row["NoAfiliados"]) == DBNull.Value ? "" : Convert.ToString(row["NoAfiliados"]);
                    beanOutput.ide = (row["ide"]) == DBNull.Value ? "" : Convert.ToString(row["ide"]);
                    list.Add(beanOutput);
                }

                resultado.lstResultadoBeanReporte = list;
                resultado.totalPages = Utility.calculateNumberOfPages(totalRegistros, Int32.Parse(bean.filas));
                resultado.totalRegistros = totalRegistros;

            }
            return resultado;
        }

        public static DataTable reporte_BusquedasXLS(string xFechaini, string xFechaFin, string xSucursal, string xUsuario)
        {
            return MReporte.reporteBUSQUEDASxls(xFechaini, xFechaFin, xSucursal, xUsuario);
        }

        public static DataTable descarga_detallada(string xFechaini, string xFechaFin, string xSucursal, string xUsuario)
        {
            return MReporte.reporteDetDescarga(xFechaini, xFechaFin, xSucursal, xUsuario);
        }

        public static List<ReporteInfoBean> reporte_busquedasDET(string xcodigo, string xDesde, string sHasta)
        {
            //return MReporte.reporteBUSQUEDASdet(xcodigo, xDesde, sHasta);

            List<ReporteInfoBean> list = new List<ReporteInfoBean>();

            ReporteInfoBean beanOutput;

            DataTable dt = MReporte.reporteBUSQUEDASdet(xcodigo, xDesde, sHasta);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    beanOutput = new ReporteInfoBean();
                    beanOutput.nrodocto = (row["NRODOCTO"]) == DBNull.Value ? "" : Convert.ToString(row["NRODOCTO"]);
                    beanOutput.tipodocto = (row["TIPODOCTO"]) == DBNull.Value ? "" : Convert.ToString(row["TIPODOCTO"]);
                    beanOutput.cbase = (row["BASE"]) == DBNull.Value ? "" : Convert.ToString(row["BASE"]);
                    beanOutput.fecha = (row["FECHA"]) == DBNull.Value ? "" : Convert.ToString(row["FECHA"]);
                    beanOutput.encontrado = (row["ENCONTRADO"]) == DBNull.Value ? "" : Convert.ToString(row["ENCONTRADO"]);
                    beanOutput.afiliado = (row["AFILIADO"]) == DBNull.Value ? "" : Convert.ToString(row["AFILIADO"]);
                    beanOutput.codUsuario = (row["CODUSUARIO"]) == DBNull.Value ? "" : Convert.ToString(row["CODUSUARIO"]);
                    beanOutput.codSucursal = (row["CODSUCURSAL"]) == DBNull.Value ? "" : Convert.ToString(row["CODSUCURSAL"]);
                    list.Add(beanOutput);
                }

            }
            return list;
        }
    }
}
