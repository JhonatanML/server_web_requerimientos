﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Utilidad;

namespace Controlador
{
    public class ControladorLocal
    {
        public static BeanPaginate BuscarLocalC(string cod_local, string nom_local, string page, string rows, Int32 cod_empresa)
        {
            BeanPaginate LocalResultado = new BeanPaginate();
            List<BeanLocal> ListaLocal = new List<BeanLocal>();
            DataTable dataTable = MLocal.BuscarLocalM(cod_local, nom_local, page, rows, cod_empresa);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaLocal.Add(new BeanLocal()
                    {
                        cod_local = Convert.ToInt32(row["cod_local"]),
                        nom_local = row["nom_local"].ToString(),
                        telf_movil= Convert.ToInt32(row["telf_movil"]),
                        telf_fijo = Convert.ToInt32(row["telf_fijo"]),
                        direccion = row["direccion"].ToString(),
                        referencia = row["referencia"].ToString(),
                    });
                LocalResultado.lstResultadoBeanLocal = ListaLocal;
                LocalResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                LocalResultado.totalRegistros = totalNumberOfItems;
            }
            return LocalResultado;
        }

        public static string AgregarLocalC(BeanLocal bean)
        {
            string @string = Convert.ToString(MLocal.AgregarLocalM(bean));
            if (@string == "0")
                throw new Exception("Local ya registrado");
            return @string;
        }

        public static BeanLocal ObtenerLocalC(string cod_local)
        {
            DataTable dataTable = MLocal.ObtenerLocalM(cod_local);
            BeanLocal bean = new BeanLocal();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];
                bean.cod_local = Convert.ToInt32(dataRow["cod_local"]);
                bean.nom_local = dataRow["nom_local"].ToString();
                bean.direccion = dataRow["direccion"].ToString();
                bean.referencia = dataRow["referencia"].ToString();
                bean.telf_fijo = Convert.ToInt32(dataRow["telf_fijo"]);
                bean.telf_movil = Convert.ToInt32(dataRow["telf_movil"]);
            }
            return bean;
        }

        public static string EditarLocalC(BeanLocal bean)
        {
            string @string = Convert.ToString(MLocal.EditarLocalM(bean));
            if (@string == "0")
                throw new Exception("Local Repetido");
            return @string;
        }

        public static void EliminarLocalC(string cod_local)
        {
            MLocal.EliminarLocalM(cod_local);
        }
    }
}
