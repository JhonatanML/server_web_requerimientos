﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilidad;

namespace Controlador
{
    public class ControladorUsuario
    {
        // |UTILIZADO REQUERIMIENTOS SERVICIOS | UTILIZADO REQUERIMIENTOS SERVICIOS | UTILIZADO REQUERIMIENTOS SERVICIOS|
        public static BeanUsuario validarUsuario(String login, String clave)
        {
            BeanUsuario bean = new BeanUsuario();
            try
            {
                DataTable dt = MUsuario.validarUsuario(login, clave);

                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];

                    bean.id_usuario = row["id_usuario"] == DBNull.Value ? 0 : Convert.ToInt32(row["id_usuario"].ToString());
                    bean.usuario = row["usuario"].ToString();
                    bean.clave = row["clave"].ToString();
                    bean.nombre_completo = row["nombre_completo"].ToString();
                    bean.dni = row["dni"].ToString();
                    bean.email = row["email"].ToString();
                    bean.telf_movil = row["telf_movil"].ToString();
                    bean.telf_fijo = row["telf_fijo"].ToString();
                    bean.direccion = row["direccion"].ToString();
                    bean.distrito = row["distrito"].ToString();
                    bean.ciudad = row["ciudad"].ToString();
                    bean.habilitado = row["habilitado"] == DBNull.Value ? 0 : Convert.ToInt32(row["habilitado"].ToString());
                    bean.id_perfil = row["id_perfil"] == DBNull.Value ? 0 : Convert.ToInt32(row["id_perfil"].ToString());
                    bean.cod_empresa = row["cod_empresa"] == DBNull.Value ? 0 : Convert.ToInt32(row["cod_empresa"].ToString());
                    bean.estado_perfil = Convert.ToInt32(row["estado_perfil"].ToString());
                    bean.primeraSesion = row["sesion"] == DBNull.Value ? 0 : Convert.ToInt32(row["sesion"].ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            return bean;
        }

        public static List<BeanPermisos> permisosUsuario(Int32 idPerfil)
        {
            DataTable dt = MUsuario.permisosUsuario(idPerfil);
            List<BeanPermisos> lista = new List<BeanPermisos>();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    BeanPermisos bean = new BeanPermisos();
                    bean.idPermiso = Convert.ToInt32(row["IDPERMISO"]);
                    bean.NombreOpcion = row["OPCION"].ToString();
                    bean.Url = row["URL"].ToString();
                    bean.NombreCategoria = row["CATEGORIA"].ToString();
                    bean.Referencia = row["REFERENCIA"].ToString();
                    lista.Add(bean);
                }
            }
            return lista;
        }
        // |UTILIZADO REQUERIMIENTOS SERVICIOS | UTILIZADO REQUERIMIENTOS SERVICIOS | UTILIZADO REQUERIMIENTOS SERVICIOS|
      
        public static List<BeanPermisos> permisosUsuarioR(Int32 idPerfil)
        {
            DataTable dt = MUsuario.permisosUsuarioR(idPerfil);
            List<BeanPermisos> lista = new List<BeanPermisos>();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    BeanPermisos bean = new BeanPermisos();
                    bean.idPermiso = Convert.ToInt32(row["IDPERMISO"]);
                    bean.NombreOpcion = row["OPCION"].ToString();
                    bean.Url = row["URL"].ToString();
                    bean.NombreCategoria = row["CATEGORIA"].ToString();
                    bean.Referencia = row["REFERENCIA"].ToString();
                    lista.Add(bean);
                }
            }
            return lista;
        }        

        //AGREGADO RESERVA//AGREGADO RESERVA//AGREGADO RESERVA//AGREGADO RESERVA//AGREGADO RESERVA//AGREGADO RESERVA
        public static BeanUsuarioR validarUsuarioR(String login, String clave)
        {
            BeanUsuarioR bean = new BeanUsuarioR();
            try
            {
                DataTable dt = MUsuario.validarUsuarioR(login, clave);

                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];

                    bean.id_usuario = Convert.ToInt32(row["id_usuario"].ToString());
                    bean.nombres = row["nombres"].ToString();
                    bean.usuario = row["usuario"].ToString();
                    bean.clave = row["clave"].ToString();
                    bean.cod_empresa = Convert.ToInt32(row["cod_empresa"].ToString());
                    bean.id_perfil = Convert.ToInt32(row["id_perfil"].ToString());
                    bean.estado_perfil = Convert.ToInt32(row["estado_perfil"].ToString());
                    bean.estado = Convert.ToInt32(row["estado"].ToString());

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            return bean;
        }
        //AGREGADO RESERVA//AGREGADO RESERVA//AGREGADO RESERVA//AGREGADO RESERVA//AGREGADO RESERVA//AGREGADO RESERVA

        //public static BeanUsuario actualizarClave(String login, String oldClave, String newClave)
        //{

        //    BeanUsuario bean = new BeanUsuario();
        //    DataTable dt = MUsuario.actualizarClave(login, oldClave, newClave);

        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        DataRow row = dt.Rows[0];

        //        bean.id = Convert.ToInt32(row["IdUsuario"].ToString());
        //        bean.nombres = row["Nombres"].ToString();
        //        bean.login = row["Username"].ToString();
        //        bean.idPerfil = Convert.ToInt32(row["IdPerfil"].ToString());
        //        bean.estado = Convert.ToInt32(row["Estado"].ToString());
        //        bean.estado_perfil = Convert.ToInt32(row["estadoPerfil"].ToString());
        //        bean.primeraSesion = Convert.ToInt32(row["primeraSesion"].ToString());
        //    }

        //    return bean;
        //}

        public static void actualizarDatos(String login, String clave, String newClave)
        {
            MUsuario.actualizarDatos(login, clave, newClave);

        }

        public static DataTable operador_dt_combo()
        {
            return MUsuario.operador_dt_combo();
        }

        public static List<Combo> operador_List_combo()
        {

            List<Combo> list = new List<Combo>();
            Combo bean;
            DataTable dt = MUsuario.operador_dt_combo();


            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean = new Combo();
                    bean.Codigo = row["idusuario"].ToString();
                    bean.Nombre = row["nom_usuario"].ToString();
                    list.Add(bean);
                }


            }
            return list;
        }

        public static void usuario_Borrar(String codigo)
        {
            BeanUsuario bean = new BeanUsuario();
            DataTable dt = MUsuario.obtenerDatos(codigo);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    bean.habilitado = Convert.ToInt32(row["Estado"]);
                }
            }

            String estado = bean.habilitado.ToString();

            MUsuario.usuario_Borrar(codigo, estado);
        }
        public static void usuario_Actualizar(String codigo, String estado)
        {


            MUsuario.usuario_Borrar(codigo, estado);
        }

        public static void usuario_Nuevo(String login, String nombres, String perfil, String contrasena, String estado)
        {
            MUsuario.usuario_Nuevo(login, nombres, perfil, contrasena, estado);
        }



        //public static BeanUsuario obtenerDatos(String codigo)
        //{
        //    BeanUsuario bean = new BeanUsuario();
        //    DataTable dt = MUsuario.obtenerDatos(codigo);

        //    if (dt != null && dt.Rows.Count > 0)
        //    {

        //        foreach (DataRow row in dt.Rows)
        //        {

        //            bean.idPerfil = Convert.ToInt32(row["IdPerfil"]);
        //            bean.nombres = Convert.ToString(row["Nombres"]);
        //            bean.username = Convert.ToString(row["Username"]);
        //            bean.clave = Convert.ToString(row["Clave"]);
        //        }
        //    }
        //    return bean;
        //}

        public static void Usuario_actualizar(String codigo, String login, String nombres, String perfil, String clave)
        {
            MUsuario.Usuario_actualizar(codigo, login, nombres, perfil, clave);
        }

        //JM
        public static List<String> autoUsuario()
        {
            String nombres = "";
            List<String> lst = new List<String>();
            DataTable dt = MUsuario.autoUsuario();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    nombres = Convert.ToString(row["Nombres"]);
                    lst.Add(nombres);
                }
            }
            return lst;
        }

        //SAGA
        public static BeanPaginate paginarBuscarUsuarios(string codigo, string nombre, string xsuc, string page, string rows)
        {
            BeanPaginate paginateUsuarioBean = new BeanPaginate();
            List<UsuarioBean> usuarioBeanList = new List<UsuarioBean>();
            DataTable dataTable = MUsuario.paginarBuscarUsuarios(codigo, nombre, xsuc, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalRegistros = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    usuarioBeanList.Add(new UsuarioBean()
                    {
                        codigo = row["Codigo"].ToString(),
                        nombre = row["Nombre"].ToString(),
                        sucursal = row["sucursal"].ToString(),
                        id = row["ide"].ToString()
                    });
                paginateUsuarioBean.lstResultadoUsuarioBean = usuarioBeanList;
                paginateUsuarioBean.totalPages = Utility.calculateNumberOfPages(totalRegistros, int.Parse(rows));
                paginateUsuarioBean.totalRegistros = totalRegistros;
            }
            return paginateUsuarioBean;
        }

        public static string crearUsuario(UsuarioBean bean)
        {
            string @string = Convert.ToString(MUsuario.crearUsuario(bean));
            if (@string == "0")
                throw new Exception("Usuario Repetido");
            return @string;
        }

        public static string editarUsuario(UsuarioBean bean)
        {
            string @string = Convert.ToString(MUsuario.editarUsuario(bean));
            if (@string == "0")
                throw new Exception("Usuario Repetido");
            return @string;
        }

        public static UsuarioBean infoUsuario(string id)
        {
            DataTable dataTable = MUsuario.infoUsuario(id);
            UsuarioBean usuarioBean = new UsuarioBean();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];
                usuarioBean.id = dataRow["IDE"].ToString();
                usuarioBean.codigo = dataRow["CODIGO"].ToString();
                usuarioBean.nombre = dataRow["NOMBRE"].ToString();
                usuarioBean.clave = dataRow["CLAVE"].ToString();
                usuarioBean.codSucursal = dataRow["SUCURSAL"].ToString();
            }
            return usuarioBean;
        }

        public static void borrarUsuario(string id)
        {
            MUsuario.borrarUsuario(id);
        }

        public static BeanPaginate BuscarUsuarioC(string id_usuario, string nom_usuario, string usuario, string idPerfil, string id_perfil, string page, string rows)
        {
            BeanPaginate UsuarioResultado = new BeanPaginate();
            List<BeanUsuario> ListaUsuario = new List<BeanUsuario>();
            DataTable dataTable = MUsuario.BuscarUsuarioM(id_usuario, nom_usuario, usuario, idPerfil, id_perfil, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaUsuario.Add(new BeanUsuario()
                    {
                        id_usuario = row["id_usuario"] == DBNull.Value ? 0 : Convert.ToInt32(row["id_usuario"]),
                        nombre_completo = row["nombre_completo"] == DBNull.Value ? "" : Convert.ToString(row["nombre_completo"]),
                        dni = row["dni"] == DBNull.Value ? "" : Convert.ToString(row["dni"]),
                        email = row["email"] == DBNull.Value ? "" : Convert.ToString(row["email"]),
                        telf_movil = row["telf_movil"] == DBNull.Value ? "" : Convert.ToString(row["telf_movil"]),
                        telf_fijo = row["telf_fijo"] == DBNull.Value ? "" : Convert.ToString(row["telf_fijo"]),
                        direccion = row["direccion"] == DBNull.Value ? "" : Convert.ToString(row["direccion"]),
                        nom_perfil = row["nom_perfil"] == DBNull.Value ? "" : Convert.ToString(row["nom_perfil"]),
                        usuario = row["usuario"] == DBNull.Value ? "" : Convert.ToString(row["usuario"]),
                        clave = row["clave"] == DBNull.Value ? "" : Convert.ToString(row["clave"])
                    });
                UsuarioResultado.lstResultadoBeanUsuario = ListaUsuario;
                UsuarioResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                UsuarioResultado.totalRegistros = totalNumberOfItems;
            }
            return UsuarioResultado;
        }

        public static void EliminarUsuarioC(string id_usuario)
        {
            MUsuario.EliminarUsuarioM(id_usuario);
        }

        public static string AgregarUsuarioC(BeanUsuario bean)
        {
            string @string = Convert.ToString(MUsuario.AgregarUsuarioM(bean));
            if (@string == "0")
                throw new Exception("!El usuario ya existe en la base de datos!");
            return @string;
        }

        public static BeanUsuario ObtenerUsuarioC(string id_usuario)
        {
            DataTable dataTable = MUsuario.ObtenerUsuarioM(id_usuario);
            BeanUsuario bean = new BeanUsuario();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];

                bean.id_usuario = Convert.ToInt32(dataRow["id_usuario"]);
                bean.apellido_paterno = dataRow["apellido_paterno"].ToString();
                bean.apellido_materno = dataRow["apellido_materno"].ToString();
                bean.nombres = dataRow["nombres"].ToString();
                bean.dni = dataRow["dni"].ToString();
                bean.email = dataRow["email"].ToString();
                bean.telf_movil = dataRow["telf_movil"].ToString();
                bean.telf_fijo = dataRow["telf_fijo"].ToString();
                bean.direccion = dataRow["direccion"].ToString();
                bean.id_perfil = Convert.ToInt32(dataRow["id_perfil"]);
                bean.usuario = dataRow["usuario"].ToString();
                bean.clave = dataRow["clave"].ToString();
                bean.id_jefe = dataRow["id_jefe"] == DBNull.Value ? 0 : Convert.ToInt32(dataRow["id_jefe"]);
                bean.nombres_jefe = dataRow["nombres_jefe"] == DBNull.Value ? "" : Convert.ToString(dataRow["nombres_jefe"]);
                bean.idLineaNegocio = dataRow["id_linea_negocio"].ToString();
                bean.idCentroCosto = dataRow["id_centro_coste"].ToString();
            }
            return bean;
        }

        public static string EditarUsuarioC(BeanUsuario bean)
        {
            string @string = Convert.ToString(MUsuario.EditarUsuarioM(bean));
            if (@string == "0")
                throw new Exception("!El usuario ya existe en la base de datos!");
            return @string;
        }
    }
}
