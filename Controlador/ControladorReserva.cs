﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Utilidad;

namespace Controlador
{
    public class ControladorReserva
    {
        public static BeanPaginate ObtenerSalasC(Int32 cod_local)
        {
            BeanPaginate resultado = new BeanPaginate();
            List<BeanSala> list = new List<BeanSala>();

            BeanSala bean;
            DataTable dt = MReserva.ObtenerSalasM(cod_local);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new BeanSala();
                    bean.cod_sala = Convert.ToInt32(row["cod_sala"]);
                    bean.nom_sala = row["nom_sala"].ToString();
                    //bean.color_sala = row["color_sala"].ToString();
                    list.Add(bean);
                }
                resultado.lstResultadoBeanSala = list;
            }
            return resultado;
        }

        public static BeanPaginate ObtenerReservasC(Int32 id_usuario, Int32 cod_local, Int32 cod_filtro)
        {
            BeanPaginate resultado = new BeanPaginate();
            List<BeanReserva> list = new List<BeanReserva>();

            BeanReserva bean;
            DataTable dt = MReserva.ObtenerReservasM(id_usuario, cod_local, cod_filtro);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new BeanReserva();
                    bean.cod_reserva = Convert.ToInt32(row["cod_reserva"]);
                    bean.titulo_reserva = row["titulo_reserva"].ToString();
                    bean.fecha_reserva = row["fecha_reserva"].ToString();
                    bean.fecha_inicio_reserva = row["fecha_inicio_reserva"].ToString().Replace("/", "-");
                    bean.hora_inicio_reserva = row["hora_inicio_reserva"].ToString();
                    bean.hora_termino_reserva = row["hora_termino_reserva"].ToString();
                    bean.invitados = row["invitados"].ToString();
                    bean.cod_sala = Convert.ToInt32(row["cod_sala"]);
                    bean.id_usuario = Convert.ToInt32(row["id_usuario"]);
                    bean.color = row["color"].ToString();

                    list.Add(bean);
                }
                resultado.lstResultadoBeanReserva = list;
            }
            return resultado;
        }

        public static BeanPaginate ObtenerReporteReservasC(string fecha_inicio_reserva, string cod_local, string page, string rows)
        {
            BeanPaginate ReservaResultado = new BeanPaginate();
            List<BeanReserva> ListaReserva = new List<BeanReserva>();
            DataTable dataTable = MReserva.ObtenerReporteReservasM(fecha_inicio_reserva, cod_local, page, rows);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaReserva.Add(new BeanReserva()
                    {
                        cod_reserva = Convert.ToInt32(row["cod_reserva"]),
                        titulo_reserva = row["titulo_reserva"].ToString(),
                        fecha_reserva = row["fecha_reserva"].ToString(),
                        fecha_inicio_reserva = row["fecha_inicio_reserva"].ToString(),
                        hora_inicio_reserva = row["hora_inicio_reserva"].ToString(),
                        hora_termino_reserva = row["hora_termino_reserva"].ToString(),
                        invitados = row["invitados"].ToString(),
                        nom_sala = row["nom_sala"].ToString(),
                        nombre_completo = row["nombre_completo"].ToString(),
                        nom_local = row["nom_local"].ToString(),
                        detalle_reserva = row["detalle_reserva"].ToString(),
                        color = row["color"].ToString(),
                    });
                ReservaResultado.lstResultadoBeanReserva = ListaReserva;
                ReservaResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                ReservaResultado.totalRegistros = totalNumberOfItems;
            }
            return ReservaResultado;
        }

        public static DataTable Combo_LocalC(Int32 cod_empresa)
        {
            return MReserva.Combo_LocalM(cod_empresa);
        }

        public static DataTable Combo_SalaC(Int32 cod_local)
        {
            return MReserva.ObtenerSalasM(cod_local);
        }

        public static DataTable Combo_ColoresC()
        {
            return MReserva.Combo_ColoresM();
        }

        public static String RegistrarReservaC(BeanReserva bean)
        {
            Int32 valor = 0;
            try
            {
                DataTable dt = MReserva.RegistrarReservaM(bean);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach(DataRow dr in dt.Rows)
                    {
                        valor = Convert.ToInt32(dr["VALOR"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                //throw;
            }
            return valor.ToString();
           
        }

        public static BeanUsuarioSala ObtenerUsuarioSalaC(Int32 id_usuario, Int32 cod_sala)
        {
            DataTable dataTable = MReserva.ObtenerUsuarioSalaM(id_usuario, cod_sala);
            BeanUsuarioSala usuariosala = new BeanUsuarioSala();

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];
                usuariosala.nombre_completo = dataRow["NOMBRE_COMPLETO"].ToString();
                usuariosala.nom_sala = dataRow["NOM_SALA"].ToString();
            }
            return usuariosala;
        }

        public static BeanReserva ObtenerInfoReservaC(Int32 cod_reserva)
        {
            DataTable dataTable = MReserva.ObtenerInfoReservaM(cod_reserva);
            BeanReserva reserva = new BeanReserva();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];
                reserva.cod_reserva = Convert.ToInt32(dataRow["cod_reserva"]);
                reserva.titulo_reserva = dataRow["titulo_reserva"].ToString();
                reserva.fecha_reserva = dataRow["fecha_reserva"].ToString();
                reserva.fecha_inicio_reserva = dataRow["fecha_inicio_reserva"].ToString();
                reserva.hora_inicio_reserva = dataRow["hora_inicio_reserva"].ToString();
                reserva.hora_termino_reserva = dataRow["hora_termino_reserva"].ToString();
                reserva.cod_sala = Convert.ToInt32(dataRow["cod_sala"]);
                reserva.cod_color = Convert.ToInt32(dataRow["cod_color"]);
                reserva.invitados = dataRow["invitados"].ToString();
                reserva.detalle_reserva = dataRow["detalle_reserva"].ToString();
                reserva.id_usuario = Convert.ToInt32(dataRow["id_usuario"]);
                reserva.nombres = (dataRow["nombres"]).ToString();
            }
            return reserva;
        }
        public static BeanReserva ObtenerInfoDetalleReservaC(Int32 cod_reserva)
        {
            DataTable dataTable = MReserva.ObtenerInfoDetalleReservaM(cod_reserva);
            BeanReserva reserva = new BeanReserva();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];

                reserva.cod_reserva = Convert.ToInt32(dataRow["cod_reserva"]);
                reserva.titulo_reserva = dataRow["titulo_reserva"].ToString();
                reserva.fecha_reserva = dataRow["fecha_reserva"].ToString();
                reserva.fecha_inicio_reserva = dataRow["fecha_inicio_reserva"].ToString();
                reserva.hora_inicio_reserva = dataRow["hora_inicio_reserva"].ToString();
                reserva.hora_termino_reserva = dataRow["hora_termino_reserva"].ToString();
                reserva.invitados = dataRow["invitados"].ToString();
                reserva.nom_sala = dataRow["nom_sala"].ToString();
                reserva.nombre_completo = (dataRow["nombre_completo"]).ToString();
                reserva.nom_local = dataRow["nom_local"].ToString();
                reserva.detalle_reserva = dataRow["detalle_reserva"].ToString();
                reserva.color = dataRow["color"].ToString();
            }
            return reserva;
        }

        public static String EditarReservaC(BeanReserva bean)
        {
            String resultado = "";
            try
            {
                DataTable dt = MReserva.EditarReservaM(bean);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        resultado = dr["RESULTADO"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                //throw;
            }


            return resultado;
        }
    }
}
