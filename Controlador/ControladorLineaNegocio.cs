﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controlador
{
    public class ControladorLineaNegocio
    {
        public static DataTable CargarComboLineaNegocioC(string id_perfil, string id_usuario)
        {
            return MLineaNegocio.CargarComboLineaNegocioM(id_perfil, id_usuario);
        }

        public static List<BeanLineaNegocio> ObtenerLineaNegocioC(String id_perfil, String id_usuario)
        {
            List<BeanLineaNegocio> lst = new List<BeanLineaNegocio>();
            BeanLineaNegocio bean = null;
            DataTable dt = MLineaNegocio.CargarComboLineaNegocioM(id_perfil, id_usuario);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new BeanLineaNegocio();
                    bean.id_linea_negocio = row["id_linea_negocio"].ToString();
                    bean.nombre_linea_negocio = row["nombre_linea_negocio"].ToString();
                    lst.Add(bean);
                }
            }
            else{
                throw new Exception("ERROR ObtenerLineaNegocioC: No hay Lineas de Negocio en la tabla");
            }
            return lst;
        }
    }
}
