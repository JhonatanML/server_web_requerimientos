﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Utilidad;

namespace Controlador
{
    public class ControladorSala
    {
        public static BeanPaginate BuscarSalaC(string cod_sala, string nom_sala, string cod_local, string page, string rows, Int32 cod_empresa)
        {
            BeanPaginate SalaResultado = new BeanPaginate();
            List<BeanSala> ListaSala = new List<BeanSala>();
            DataTable dataTable = MSala.BuscarSalaM(cod_sala, nom_sala, cod_local, page, rows, cod_empresa);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaSala.Add(new BeanSala()
                    {
                        cod_sala = Convert.ToInt32(row["cod_sala"]),
                        nom_sala = row["nom_sala"].ToString(),
                        capacidad_personas = Convert.ToInt32(row["capacidad_personas"]),
                        descripcion = row["descripcion"].ToString(),
                        nom_local = row["nom_local"].ToString(),
                    });
                SalaResultado.lstResultadoBeanSala = ListaSala;
                SalaResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                SalaResultado.totalRegistros = totalNumberOfItems;
            }
            return SalaResultado;
        }

        public static string AgregarSalaC(BeanSala bean)
        {
            string @string = Convert.ToString(MSala.AgregarSalaM(bean));
            if (@string == "0")
                throw new Exception("Local ya registrado");
            return @string;
        }

        public static BeanSala ObtenerSalaC(string cod_sala)
        {
            DataTable dataTable = MSala.ObtenerSalaM(cod_sala);
            BeanSala bean = new BeanSala();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];
                bean.cod_sala = Convert.ToInt32(dataRow["cod_sala"]);
                bean.nom_sala = dataRow["nom_sala"].ToString();
                bean.capacidad_personas = Convert.ToInt32(dataRow["capacidad_personas"]);
                bean.cod_local = Convert.ToInt32(dataRow["cod_local"]);
                bean.descripcion = dataRow["descripcion"].ToString();                
            }
            return bean;
        }

        public static string EditarSalaC(BeanSala bean)
        {
            string @string = Convert.ToString(MSala.EditarSalaM(bean));
            if (@string == "0")
                throw new Exception("Sala Repetida");
            return @string;
        }

        public static void EliminarSalaC(string cod_sala)
        {
            MSala.EliminarSalaM(cod_sala);
        }
    }
}
