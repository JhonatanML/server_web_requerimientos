﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Utilidad;

namespace Controlador
{
    public class ControladorServicio
    {
        public static BeanPaginate Lista_ServicioC(string nom_serviciosT)
        {
            BeanPaginate ServicioResultado = new BeanPaginate();
            List<BeanServicio> ListaServicio = new List<BeanServicio>();

            DataTable dataTable = MServicio.Lista_ServicioM(nom_serviciosT);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaServicio.Add(new BeanServicio()
                    {
                        cod_servicios = row["cod_servicios"].ToString().Trim(),
                        nom_servicios = row["nom_servicios"].ToString().Trim(),
                        tipo_servicios = row["tipo"].ToString()
                    });
                ServicioResultado.lstResultadoBeanServicio = ListaServicio;
            }
            return ServicioResultado;
        }

        public static BeanPaginate Combo_ServicioC(string cod_subcategoria, string cod_categoria, string cod_servicio)
        {
            BeanPaginate ServicioResultado = new BeanPaginate();
            List<BeanServicio> ListaServicio = new List<BeanServicio>();

            DataTable dataTable = MServicio.Combo_ServicioM(cod_subcategoria, cod_categoria, cod_servicio);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaServicio.Add(new BeanServicio()
                    {
                        cod_servicios = row["ID_SERVICIO"].ToString(),
                        nom_servicios = row["NOM_SERVICIO"].ToString(),
                    });
                ServicioResultado.lstResultadoBeanServicio = ListaServicio;
            }
            return ServicioResultado;
        }

        public static BeanPaginate BuscarServicioC(string nom_servicio, string codigo_sap, string page, string rows)
        {
            BeanPaginate ServicioResultado = new BeanPaginate();
            List<BeanServicio> ListaServicio = new List<BeanServicio>();
            DataTable dataTable = MServicio.BuscarServicioM(nom_servicio, codigo_sap, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaServicio.Add(new BeanServicio()
                    {
                        id_servicio = row["id_servicio"] == DBNull.Value ? 0 : Convert.ToInt32(row["id_servicio"]),
                        cod_servicio = row["cod_servicio"] == DBNull.Value ? "" : Convert.ToString(row["cod_servicio"]),
                        codigo_sap = row["codigo_sap"] == DBNull.Value ? "" : Convert.ToString(row["codigo_sap"]),
                        nom_servicio = row["nom_servicio"] == DBNull.Value ? "" : Convert.ToString(row["nom_servicio"]),
                        detalles = row["detalles"] == DBNull.Value ? "" : Convert.ToString(row["detalles"]),
                        costo = row["costo"] == DBNull.Value ? "" : Convert.ToString(row["costo"]),
                        nom_categoria = row["nom_categoria"] == DBNull.Value ? "" : Convert.ToString(row["nom_categoria"]),
                        nom_subcategoria = row["nom_subcategoria"] == DBNull.Value ? "" : Convert.ToString(row["nom_subcategoria"]),
                    });
                ServicioResultado.lstResultadoBeanServicio = ListaServicio;
                ServicioResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                ServicioResultado.totalRegistros = totalNumberOfItems;
            }
            return ServicioResultado;
        }

        public static void EliminarServicioC(string id_servicio)
        {
            MServicio.EliminarServicioM(id_servicio);
        }

        public static string AgregarServicioC(BeanServicio bean)
        {
            string @string = Convert.ToString(MServicio.AgregarServicioM(bean));
            if (@string == "0")
                throw new Exception("¡El nombre del servicio ya esta registrado!");
            return @string;
        }

        public static BeanServicio ObtenerServicioC(string id_servicio)
        {
            DataTable dataTable = MServicio.ObtenerServicioM(id_servicio);
            BeanServicio bean = new BeanServicio();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];
                bean.id_servicio = Convert.ToInt32(dataRow["id_servicio"]);
                bean.cod_categoria = dataRow["cod_categoria"].ToString();
                bean.cod_subcategoria = dataRow["cod_subcategoria"].ToString();
                bean.cod_servicio = dataRow["cod_servicio"].ToString();
                bean.codigo_sap = dataRow["codigo_sap"].ToString();
                bean.nom_servicio = dataRow["nom_servicio"].ToString();
                bean.descripcion = dataRow["descripcion"].ToString();
                bean.precio = dataRow["precio"].ToString();
            }
            return bean;
        }

        public static string EditarServicioC(BeanServicio bean)
        {
            string @string = Convert.ToString(MServicio.EditarServicioM(bean));
            if (@string == "0")
                throw new Exception("¡El nombre del servicio ya esta registrado!");
            return @string;
        }
    }
}
