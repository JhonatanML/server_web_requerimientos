﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Utilidad;

namespace Controlador
{
    public class ControladorSubcategoria
    {
        public static BeanPaginate BuscarSubcategoriaC(string linea, string nom_subcategoria, string id_categoria, string page, string rows)
        {
            BeanPaginate SubcategoriaResultado = new BeanPaginate();
            List<BeanSubCategoria> ListaSubcategoria = new List<BeanSubCategoria>();
            DataTable dataTable = MSubcategoria.BuscarSubcategoriaM(linea, nom_subcategoria, id_categoria, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaSubcategoria.Add(new BeanSubCategoria()
                    {
                        id_subcategoria = Convert.ToInt32(row["id_subcategoria"]),
                        linea = row["linea"].ToString(),
                        nom_subcategoria = row["nom_subcategoria"].ToString(),
                        nom_categoria = row["nom_categoria"].ToString()
                    });
                SubcategoriaResultado.lstResultadoBeanSubcategoria = ListaSubcategoria;
                SubcategoriaResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                SubcategoriaResultado.totalRegistros = totalNumberOfItems;
            }
            return SubcategoriaResultado;
        }

        public static void EliminarSubcategoriaC(string id_subcategoria)
        {
            MSubcategoria.EliminarSubcategoriaM(id_subcategoria);
        }

        public static string AgregarSubcategoriaC(BeanSubCategoria bean)
        {
            string @string = Convert.ToString(MSubcategoria.AgregarSubcategoriaM(bean));
            if (@string == "0")
                throw new Exception("¡La subcategoria ya existe en la base de datos!");
            return @string;
        }

        public static BeanSubCategoria ObtenerSubcategoriaC(string id_subcategoria)
        {
            DataTable dataTable = MSubcategoria.ObtenerSubcategoriaM(id_subcategoria);
            BeanSubCategoria bean = new BeanSubCategoria();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];

                bean.id_subcategoria = Convert.ToInt32(dataRow["id_subcategoria"]);
                bean.id_categoria = Convert.ToInt32(dataRow["id_categoria"]);
                bean.nom_subcategoria = dataRow["nom_subcategoria"].ToString();
                bean.linea = dataRow["linea"].ToString();
            }
            return bean;
        }

        public static string EditarSubcategoriaC(BeanSubCategoria bean)
        {
            string @string = Convert.ToString(MSubcategoria.EditarSubcategoriaM(bean));
            if (@string == "0")
                throw new Exception("¡La subcategoria ya existe en la base de datos!");
            return @string;
        }
    }
}
