﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using Utilidad;

namespace Controlador
{
    public class ControladorRequerimiento
    {
        public static BeanPaginate ListarRequerimientosC(string visualizar, string id_requerimiento, string fecha_requerimiento, string cod_estado_requerimiento, Int32 id_usuario, Int32 id_perfil, string page, string rows)
        {
            BeanPaginate RequerimientoResultado = new BeanPaginate();
            List<BeanRequerimiento> ListaRequerimiento = new List<BeanRequerimiento>();
            DataTable dataTable = MRequerimiento.ListarRequerimientosM(visualizar, id_requerimiento, fecha_requerimiento, cod_estado_requerimiento, id_usuario, id_perfil, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaRequerimiento.Add(new BeanRequerimiento()
                    {
                        cod_requerimiento = Convert.ToInt32(row["COD_REQUERIMIENTO"]),
                        cod_estado_requerimiento = Convert.ToInt32(row["COD_ESTADO_REQUERIMIENTO"]),
                        fecha_requerimiento = row["FECHA_REQUERIMIENTO"].ToString(),
                        nom_categoria = row["NOM_CATEGORIA"].ToString(),
                        nom_subcategoria = row["NOM_SUBCATEGORIA"].ToString(),
                        nom_servicio = row["NOM_SERVICIO"].ToString(),
                        nom_prioridad = row["NOM_PRIORIDAD"].ToString(),
                        nombre_completo = row["NOMBRE_COMPLETO"].ToString(),
                        nom_estado_requerimiento = row["NOM_ESTADO_REQUERIMIENTO"].ToString(),
                    });
                RequerimientoResultado.lstResultadoBeanRequerimiento = ListaRequerimiento;
                RequerimientoResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                RequerimientoResultado.totalRegistros = totalNumberOfItems;
            }
            return RequerimientoResultado;
        }
        public static BeanPaginate ListarRequerimientosPendientesC(string cod_requerimiento, string fecha_requerimiento, string cod_prioridad, string page, string rows)
        {
            BeanPaginate RequerimientoResultado = new BeanPaginate();
            List<BeanRequerimiento> ListaRequerimiento = new List<BeanRequerimiento>();
            DataTable dataTable = MRequerimiento.ListarRequerimientosPendientesM(cod_requerimiento, fecha_requerimiento, cod_prioridad, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaRequerimiento.Add(new BeanRequerimiento()
                    {
                        cod_requerimiento = Convert.ToInt32(row["COD_REQUERIMIENTO"]),
                        fecha_requerimiento = row["FECHA_REQUERIMIENTO"].ToString(),
                        nom_categoria = row["NOM_CATEGORIA"].ToString(),
                        nom_subcategoria = row["NOM_SUBCATEGORIA"].ToString(),
                        nom_prioridad = row["NOM_PRIORIDAD"].ToString(),
                        nom_estado_requerimiento = row["NOM_ESTADO_REQUERIMIENTO"].ToString(),
                        nombre_completo = row["NOMBRE_COMPLETO"].ToString(),
                    });
                RequerimientoResultado.lstResultadoBeanRequerimiento = ListaRequerimiento;
                RequerimientoResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                RequerimientoResultado.totalRegistros = totalNumberOfItems;
            }
            return RequerimientoResultado;
        }

        public static BeanPaginate ListarRequerimientosC(string id_requerimiento, string fecha_requerimiento, string cod_estado_requerimiento, string page, string rows)
        {
            BeanPaginate RequerimientoResultado = new BeanPaginate();
            List<BeanRequerimiento> ListaRequerimiento = new List<BeanRequerimiento>();
            BeanRequerimiento bean = null;
            DataTable dt = MRequerimiento.ListarRequerimientosM(id_requerimiento, fecha_requerimiento, cod_estado_requerimiento, page, rows);
            if (dt != null && dt.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dt.Rows[0]["total"].ToString());
                foreach (DataRow row in dt.Rows)
                {
                    bean = new BeanRequerimiento();
                    bean.cod_requerimiento = Convert.ToInt32(row["COD_REQUERIMIENTO"]);
                    bean.fecha_requerimiento = row["FECHA_REQUERIMIENTO"].ToString();
                    bean.nom_prioridad = row["NOM_PRIORIDAD"].ToString();
                    bean.nom_estado_requerimiento = row["NOM_ESTADO_REQUERIMIENTO"].ToString();
                    bean.nombre_completo = row["NOMBRE_COMPLETO"].ToString();
                    bean.detalle = row["DETALLE"].ToString();

                    switch (bean.nom_estado_requerimiento)
                    {
                        case "PENDIENTE":
                            bean.rutaImg = "../../imagery/all/icons/1.png";
                            bean.tituloImg = "Aprobar";
                            break;
                        case "APROBADO":
                            bean.rutaImg = "../../imagery/all/icons/0.png";
                            bean.tituloImg = "Asignar";
                            break;
                        case "ASIGNADO":
                            bean.rutaImg = "../../imagery/all/icons/Baja.png";
                            bean.tituloImg = "Cerrar";
                            break;
                    }

                    ListaRequerimiento.Add(bean);
                }


                //foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                //    ListaRequerimiento.Add(new BeanRequerimiento()
                //    {
                //        cod_requerimiento = Convert.ToInt32(row["COD_REQUERIMIENTO"]),
                //        fecha_requerimiento = row["FECHA_REQUERIMIENTO"].ToString(),
                //        nom_prioridad = row["NOM_PRIORIDAD"].ToString(),
                //        nom_estado_requerimiento = row["NOM_ESTADO_REQUERIMIENTO"].ToString(),
                //        nombre_completo = row["NOMBRE_COMPLETO"].ToString(),
                //        detalle = row["DETALLE"].ToString(),

                //    });
                RequerimientoResultado.lstResultadoBeanRequerimiento = ListaRequerimiento;
                RequerimientoResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                RequerimientoResultado.totalRegistros = totalNumberOfItems;
            }
            return RequerimientoResultado;
        }

        public static BeanPaginate ListarRequerimientosAprobadosC(string cod_requerimiento, string fecha_requerimiento, string cod_prioridad, string page, string rows)
        {
            BeanPaginate RequerimientoResultado = new BeanPaginate();
            List<BeanRequerimiento> ListaRequerimiento = new List<BeanRequerimiento>();
            DataTable dataTable = MRequerimiento.ListarRequerimientosAprobadosM(cod_requerimiento, fecha_requerimiento, cod_prioridad, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaRequerimiento.Add(new BeanRequerimiento()
                    {
                        cod_requerimiento = Convert.ToInt32(row["COD_REQUERIMIENTO"]),
                        fecha_requerimiento = row["FECHA_REQUERIMIENTO"].ToString(),
                        nom_categoria = row["NOM_CATEGORIA"].ToString(),
                        nom_subcategoria = row["NOM_SUBCATEGORIA"].ToString(),
                        nom_prioridad = row["NOM_PRIORIDAD"].ToString(),
                        nom_estado_requerimiento = row["NOM_ESTADO_REQUERIMIENTO"].ToString(),
                        nombre_completo = row["NOMBRE_COMPLETO"].ToString(),
                    });
                RequerimientoResultado.lstResultadoBeanRequerimiento = ListaRequerimiento;
                RequerimientoResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                RequerimientoResultado.totalRegistros = totalNumberOfItems;
            }
            return RequerimientoResultado;
        }

        public static BeanPaginate ListarRequerimientosAsignadosC(string cod_requerimiento, string fecha_requerimiento, string cod_prioridad, string page, string rows)
        {
            BeanPaginate RequerimientoResultado = new BeanPaginate();
            List<BeanRequerimiento> ListaRequerimiento = new List<BeanRequerimiento>();
            DataTable dataTable = MRequerimiento.ListarRequerimientosAsignadosM(cod_requerimiento, fecha_requerimiento, cod_prioridad, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaRequerimiento.Add(new BeanRequerimiento()
                    {
                        cod_requerimiento = Convert.ToInt32(row["COD_REQUERIMIENTO"]),
                        fecha_requerimiento = row["FECHA_REQUERIMIENTO"].ToString(),
                        nom_categoria = row["NOM_CATEGORIA"].ToString(),
                        nom_subcategoria = row["NOM_SUBCATEGORIA"].ToString(),
                        nom_prioridad = row["NOM_PRIORIDAD"].ToString(),
                        nom_estado_requerimiento = row["NOM_ESTADO_REQUERIMIENTO"].ToString(),
                        nombre_completo = row["NOMBRE_COMPLETO"].ToString(),
                    });
                RequerimientoResultado.lstResultadoBeanRequerimiento = ListaRequerimiento;
                RequerimientoResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                RequerimientoResultado.totalRegistros = totalNumberOfItems;
            }
            return RequerimientoResultado;
        }

        public static DataTable Combo_EstadoRequerimientoC(string id_perfil)
        {
            return MRequerimiento.Combo_EstadoRequerimientoM(id_perfil);
        }

        public static DataTable Combo_PrioridadC()
        {
            return MRequerimiento.Combo_PrioridadM();
        }

        public static DataTable Combo_CategoriaC()
        {
            return MRequerimiento.Combo_CategoriaM();
        }

        public static DataTable List_ServicioC()
        {
            return MRequerimiento.Lista_ServiciosM();
        }

        public static List<BeanSubCategoria> Combo_SubCategoriaC(string cod_categoria)
        {
            //BeanPaginate SubCategoriaResultado = new BeanPaginate();
            List<BeanSubCategoria> ListaSubCategoria = new List<BeanSubCategoria>();

            DataTable dataTable = MRequerimiento.Combo_SubCategoriaM(cod_categoria);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaSubCategoria.Add(new BeanSubCategoria()
                    {
                        cod_subcategoria = Convert.ToInt32(row["id_subcategoria"]),
                        nom_subcategoria = row["nom_subcategoria"].ToString(),
                    });
                //SubCategoriaResultado.lstResultadoBeanSubCategoria = ListaSubCategoria;                
            }
            return ListaSubCategoria;
        }

        //public static List<BeanComentario> ListarComentarioC(string cod_requerimiento)
        //{
        //    //BeanPaginate SubCategoriaResultado = new BeanPaginate();
        //    List<BeanComentario> ListaComentario = new List<BeanComentario>();

        //    DataTable dataTable = MRequerimiento.ListarComentarioM(cod_requerimiento);

        //    if (dataTable != null && dataTable.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
        //            ListaComentario.Add(new BeanComentario()
        //            {
        //                comentarios = row["comentarios"].ToString(),
        //            });
        //        //SubCategoriaResultado.lstResultadoBeanSubCategoria = ListaSubCategoria;                
        //    }
        //    return ListaComentario;
        //}

        public static BeanPaginate ListarComentarioC(string cod_requerimiento)
        {
            BeanPaginate ComentarioResultado = new BeanPaginate();
            List<BeanComentario> ListaComentario = new List<BeanComentario>();

            DataTable dataTable = MRequerimiento.ListarComentarioM(cod_requerimiento);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaComentario.Add(new BeanComentario()
                    {
                        comentarios = row["comentarios"].ToString(),
                        fecha = row["fecha"].ToString(),
                        descripcionUsuario = row["descripcionUsuario"].ToString(),
                        idPerfil = row["idPerfil"].ToString()
                    });
                ComentarioResultado.lstResultadoBeanComentario = ListaComentario;
            }
            return ComentarioResultado;
        }

        public static String RegistrarRequerimientoC(BeanRequerimiento bean)
        {
            Int32 valor = 0;
            try
            {
                DataTable dt = MRequerimiento.RegistrarRequerimientoM(bean);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        valor = Convert.ToInt32(dr["VALOR"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.registrarLog("REGISTRAR REQ " + ex.Message);
                Utility.registrarLog("REGISTRAR REQ " + ex.StackTrace);
                //throw;
            }
            return valor.ToString();

        }

        public static BeanDetalleRequerimiento ObtenerDetalleRequerimientoC(Int32 id_usuario, Int32 cod_prioridad, Int32 id_perfil)
        {
            DataTable dataTable = MRequerimiento.ObtenerDetalleRequerimientoM(id_usuario, cod_prioridad, id_perfil);
            BeanDetalleRequerimiento detalles = new BeanDetalleRequerimiento();

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];
                detalles.nombre_completo = dataRow["NOMBRE_COMPLETO"].ToString();
                detalles.email = dataRow["EMAIL"].ToString();
                detalles.nom_prioridad = dataRow["NOM_PRIORIDAD"].ToString();
                detalles.nom_estado_requerimiento = dataRow["NOM_ESTADO_REQUERIMIENTO"].ToString();
                detalles.email_jefe = dataRow["EMAIL_JEFE"].ToString();
                detalles.id_perfil = dataRow["ID_PERFIL"].ToString();
            }
            return detalles;
        }

        public static BeanDetalleRequerimiento ObtenerDetalleRequerimientosC(Int32 cod_requerimiento)
        {
            DataTable dataTable = MRequerimiento.ObtenerDetalleRequerimientosM(cod_requerimiento);
            BeanDetalleRequerimiento detalles = new BeanDetalleRequerimiento();

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];

                detalles.cod_requerimiento = (dataRow["COD_REQUERIMIENTO"].ToString());
                detalles.nombre_completo = dataRow["NOMBRE_COMPLETO"].ToString();
                detalles.descripcion = dataRow["DESCRIPCION"].ToString();
                detalles.detalle = dataRow["DETALLE"].ToString();
                detalles.nom_estado_requerimiento = dataRow["NOM_ESTADO_REQUERIMIENTO"].ToString();
                detalles.fecha_requerimiento = dataRow["FECHA_REQUERIMIENTO"].ToString();
                detalles.nom_categoria = dataRow["NOM_CATEGORIA"].ToString();
                detalles.nom_subcategoria = dataRow["NOM_SUBCATEGORIA"].ToString();
                detalles.nom_prioridad = dataRow["NOM_PRIORIDAD"].ToString();

            }
            return detalles;
        }

        public static BeanDetalleRequerimiento ObtenerInfoDetallesRequerimientoC(Int32 cod_requerimiento)
        {
            DataTable dataTable = MRequerimiento.ObtenerInfoDetallesRequerimientoM(cod_requerimiento);
            BeanDetalleRequerimiento detalles = new BeanDetalleRequerimiento();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];

                detalles.cod_requerimiento = Convert.ToString(dataRow["cod_requerimiento"]);
                detalles.servicio_cliente = dataRow["servicio_cliente"].ToString();
                detalles.nom_estado_requerimiento = dataRow["nom_estado_requerimiento"].ToString();
                detalles.id_categoria = dataRow["id_categoria"] == DBNull.Value ? "-1" : Convert.ToString(dataRow["id_categoria"]);
                detalles.id_subcategoria = dataRow["id_subcategoria"] == DBNull.Value ? "-1" : Convert.ToString(dataRow["id_subcategoria"]);
                detalles.id_servicio = dataRow["id_servicio"] == DBNull.Value ? "-1" : Convert.ToString(dataRow["id_servicio"]);
                detalles.detalle = dataRow["detalle"] == DBNull.Value ? "" : Convert.ToString(dataRow["detalle"]);
                detalles.idUsuario = dataRow["id_usuario"] == DBNull.Value ? "" : Convert.ToString(dataRow["id_usuario"]);
                detalles.fecha_asignacion = dataRow["fecha_asignacion"] == DBNull.Value ? "" : Convert.ToString(dataRow["fecha_asignacion"]);
            }
            return detalles;
        }

        public static void AprobarRequerimientoC(Int32 cod_requerimiento)
        {
            MRequerimiento.AprobarRequerimientoM(cod_requerimiento);
        }

        public static void CerrarRequerimientoC(Int32 cod_requerimiento)
        {
            MRequerimiento.AprobarRequerimientoM(cod_requerimiento);
        }

        public static BeanRequerimiento ObtenerCategoriaSubcategoriaC(String cod_requerimiento)
        {
            BeanRequerimiento bean = new BeanRequerimiento();
            DataTable dt = MRequerimiento.ObtenerCategoriaSubcategoriaM(cod_requerimiento);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {

                    bean.cod_categoria = Convert.ToInt32(row["COD_CATEGORIA"]);
                    bean.nom_categoria = row["NOM_CATEGORIA"].ToString();
                    bean.cod_subcategoria = Convert.ToInt32(row["COD_SUBCATEGORIA"]);
                    bean.nom_subcategoria = row["NOM_SUBCATEGORIA"].ToString();
                }
            }
            return bean;
        }

        public static BeanCategoria ObtenerCategoriaCombo(String codigoServicio, String tipoServicio)
        {
            BeanCategoria bean = new BeanCategoria();
            DataTable dt = MRequerimiento.ObtenerCategoriaCombo(codigoServicio, tipoServicio);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean.codigoCategoria = row["COD_CATEGORIA"] == DBNull.Value ? "" : Convert.ToString(row["COD_CATEGORIA"]);
                    bean.nombreCategoria = row["NOM_CATEGORIA"] == DBNull.Value ? "" : Convert.ToString(row["NOM_CATEGORIA"]);
                }
            }
            return bean;
        }

        public static void GuardarRequerimiento(BeanDetalleRequerimiento bean)
        {
            MRequerimiento.GuardarRequerimiento(bean);
        }

        public static void AprobarRequerimiento(BeanDetalleRequerimiento bean)
        {
            MRequerimiento.AprobarRequerimiento(bean);
        }

        public static void CerrarRequerimiento(BeanDetalleRequerimiento bean)
        {
            //bean.serviciosAdicionales = bean.serviciosAdicionales + ',' + bean.servicioActual;
            String[] array = bean.serviciosAdicionales.Split(',');
            for (int i = 0; i < array.Length; i++)
            {
                bean.servicioCurrent = array[i].ToString();
                MRequerimiento.IngresarDetalleRequerimiento(bean);
            }
            MRequerimiento.CerrarRequerimiento(bean);
        }

        public static BeanServicio validarCodigoServicio(String codigoServicio, String codigoServicioActual)
        {
            BeanServicio bean = new BeanServicio();
            DataTable dt = MRequerimiento.validarCodigoServicio(codigoServicio, codigoServicioActual);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean.cod_servicios = row["ID_SERVICIO"] == DBNull.Value ? "" : row["ID_SERVICIO"].ToString();
                    bean.cod_sap = row["ID_SERVICIO_ACTUAL"] == DBNull.Value ? "" : row["ID_SERVICIO_ACTUAL"].ToString();

                }
            }
            return bean;
        }

        public static BeanDetalleRequerimiento ObtenerComentarioC(Int32 cod_requerimiento, Int32 cod_estado_requerimiento)
        {
            DataTable dataTable = MRequerimiento.ObtenerComentarioM(cod_requerimiento, cod_estado_requerimiento);
            BeanDetalleRequerimiento detalles = new BeanDetalleRequerimiento();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];

                detalles.cod_requerimiento = Convert.ToString(dataRow["cod_requerimiento"]);
                detalles.nom_estado_requerimiento = dataRow["nom_estado_requerimiento"].ToString();
                detalles.fecha_comentario = dataRow["fecha_comentario"].ToString();
                detalles.comentario = dataRow["comentario"].ToString();
            }
            return detalles;
        }

        public static DataSet ExportaReporteRequerimientosC(string fechaIN, string fechaFI, string id_linea_negocio, string id_centro_coste, string id_perfil, string id_usuario)
        {
            return MRequerimiento.ExportaReporteRequerimientosM(fechaIN, fechaFI, id_linea_negocio, id_centro_coste, id_perfil, id_usuario);
        }

        public static BeanPaginate ReporteRequerimientosC(string fechaIN, string fechaFI, string id_linea_negocio, string id_centro_coste, string id_perfil, string id_usuario, string page, string rows)
        {
            BeanPaginate RequerimientoResultado = new BeanPaginate();
            List<BeanRequerimiento> ListaRequerimiento = new List<BeanRequerimiento>();
            DataTable dataTable = MRequerimiento.ReporteRequerimientosM(fechaIN, fechaFI, id_linea_negocio, id_centro_coste, id_perfil, id_usuario, page, rows);
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                int totalNumberOfItems = int.Parse(dataTable.Rows[0]["total"].ToString());
                double precio_total = double.Parse(dataTable.Rows[0]["PrecioTotal"].ToString());
                foreach (DataRow row in (InternalDataCollectionBase)dataTable.Rows)
                    ListaRequerimiento.Add(new BeanRequerimiento()
                    {
                        cod_requerimiento = Convert.ToInt32(row["cod_requerimiento"]),

                        //id_servicio = row["id_servicio"].ToString(),
                        nombre_linea_negocio = row["nombre_linea_negocio"].ToString(),
                        nombre_centro_coste = row["nombre_centro_coste"].ToString(),
                        id_centro_coste = row["id_centro_coste"].ToString(),
                        //linea = row["linea"].ToString(),
                        codigo_sap = row["codigo_sap"].ToString(),
                        fecha_asignacion = row["fecha_ejecucion"].ToString(),
                        nom_servicio = row["nom_servicio"].ToString(),
                        //cantidad = row["cantidad"].ToString(),
                        //precio_unitario = row["precio_unitario"].ToString(),
                        precio_parcial = row["precio_parcial"].ToString()
                    });
                RequerimientoResultado.lstResultadoBeanRequerimiento = ListaRequerimiento;
                RequerimientoResultado.totalPages = Utility.calculateNumberOfPages(totalNumberOfItems, int.Parse(rows));
                RequerimientoResultado.totalRegistros = totalNumberOfItems;
                RequerimientoResultado.precio_total = precio_total;
            }
            return RequerimientoResultado;
        }

        public static String obtenerRutaFoto(String codigoRequerimiento, String opcion)
        {
            String filename = "";
            DataTable dt = new DataTable();
            dt = MRequerimiento.obtenerRutaFoto(codigoRequerimiento);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    switch (opcion)
                    {
                        case "1":
                            filename = row["NOMBRE_FOTO"] == DBNull.Value ? "" : Convert.ToString(row["NOMBRE_FOTO"]);
                            break;
                        case "2":
                            filename = row["NOMBRE_FOTO1"] == DBNull.Value ? "" : Convert.ToString(row["NOMBRE_FOTO1"]);
                            break;
                        case "3":
                            filename = row["NOMBRE_FOTO2"] == DBNull.Value ? "" : Convert.ToString(row["NOMBRE_FOTO2"]);
                            break;
                    }
                }
            }
            return filename;
        }

        public static List<BeanSupervisor> ObtenerSupervisores()
        {
            List<BeanSupervisor> lst = new List<BeanSupervisor>();
            BeanSupervisor bean;
            DataTable dt = MRequerimiento.ObtenerSupervisores();

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bean = new BeanSupervisor();
                    bean.codigo = row["CODIGO"] == DBNull.Value ? "" : Convert.ToString(row["CODIGO"]);
                    bean.nombre = row["DESCRIPCION"] == DBNull.Value ? "" : Convert.ToString(row["DESCRIPCION"]);
                    lst.Add(bean);
                }
            }
            return lst;
        }

        //public static void EnviarCorreo(String id_usuario, String cod_prioridad)
        //{
        //    BeanDetalleRequerimiento detalles = ControladorRequerimiento.ObtenerDetalleRequerimientoC(Convert.ToInt32(id_usuario), Convert.ToInt32(cod_prioridad), Convert.ToInt32(id_perfil));

        //    /*-------------------------MENSAJE DE CORREO----------------------*/

        //    //Creamos un nuevo Objeto de mensaje
        //    System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

        //    //Direccion de correo electronico a la que queremos enviar el mensaje
        //    //CORREO DEL USUARIO QUE REGISTRA EL REQUERIMIENTO DEL SERVICIO
        //    //Utility.registrarLog("EMAIL ENVIAR - " + detalles.email);
        //    mmsg.To.Add(detalles.email);

        //    //Nota: La propiedad To es una colección que permite enviar el mensaje a más de un destinatario

        //    //Asunto
        //    mmsg.Subject = "REQUERIMIENTO DE SERVICIOS" + "[ID REQUERIMIENTO: " + detalles.cod_requerimiento + "]";
        //    //mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

        //    //Direccion de correo electronico que queremos que reciba una copia del mensaje
        //    //CORREO DEL SUPERVISOR AL QUE SE LE ENVIA UNA COPIA DEL MENSAJE
        //    //Utility.registrarLog("EMAIL JEFE - " + detalles.email_jefe);
        //    mmsg.Bcc.Add(detalles.email_jefe); //Opcional
        //                                       //CORREO DEL PROVEEDOR AL QUE SE LE ENVIA UNA COPIA DEL MENSAJE
        //    String email_proveedor = ConfigurationManager.AppSettings["EMAIL_PROVEEDOR"].ToString();
        //    mmsg.Bcc.Add(email_proveedor); //Opcional

        //    //Cuerpo del Mensaje
        //    mmsg.Body = "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'/></head><body><table width='750' border='7px' align='center' cellpadding='0' cellspacing='0'><tr><td align='center' valign='top' bgcolor='#000000' style='background-color:#FFFFFF; font-size:13px; color:#000000;'><table width='100%' border='0' cellspacing='0' cellpadding='0' style='margin-top:-35px;'><tr><td align='center' valign='top'><br><br><br><br><br><div style='font-size:45px; color:#000000 ;'><u><b> MENSAJE INFORMATIVO </b></u></div><div style='font-size:30px; color:#000000;'><br> Sr.(a) " + detalles.nombre_completo + ", su requerimiento del servicio ha sido registrado correctamente.</div><div align='left'  style='font-size:17px; color:#34495E; padding-left:100px'><br><b><u>Datos sobre el requerimiento</u></b><br><br><b>Estado: </b>" + detalles.nom_estado_requerimiento + "<br><b>Fecha generada: </b>" + detalles.fecha_requerimiento + "<br><b>Línea: </b>" + detalles.id_servicio + "<br><b>Prioridad: </b>" + detalles.nom_prioridad + "<br><b>Detalle: </b>" + detalles.detalle + "</b></div><br><br><br><br><br><br></td></tr></table></td></tr><tr></tr></table></body></html>";
        //    mmsg.BodyEncoding = System.Text.Encoding.UTF8;
        //    mmsg.IsBodyHtml = true; //Si no queremos que se envíe como HTML

        //    //CORREO DEL SERVIDOR (EL MISMO DE LAS CREDENCIALES)
        //    //Correo electronico desde la que enviamos el mensaje
        //    String email_empresa = ConfigurationManager.AppSettings["EMAIL_EMPRESA"].ToString();
        //    mmsg.From = new System.Net.Mail.MailAddress(email_empresa);


        //    /*-------------------------CLIENTE DE CORREO----------------------*/

        //    //Creamos un objeto de cliente de correo
        //    System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();

        //    //Hay que crear las credenciales del correo emisor                
        //    String password_email_empresa = ConfigurationManager.AppSettings["PASSWORD_EMAIL_EMPRESA"].ToString();
        //    cliente.Credentials =
        //        new System.Net.NetworkCredential(email_empresa, password_email_empresa);

        //    //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail

        //    cliente.Port = 587;
        //    cliente.EnableSsl = true;


        //    cliente.Host = "smtp-mail.outlook.com"; //Para Gmail "smtp.gmail.com";
        //                                            //cliente.Host = "smtp.gmail.com";


        //    /*-------------------------ENVIO DE CORREO----------------------*/

        //    try
        //    {
        //        //Enviamos el mensaje      
        //        cliente.Send(mmsg);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Utility.registrarLog("Enviar correo :" + ex.Message);
        //        //Utility.registrarLog(ex.StackTrace);
        //    }

        public static Int32 EnviarComenarioC(String comentarios, String cod_requerimiento, String idUsuario)
        {
            Int32 valor = 0;
            try
            {
                DataTable dt = MRequerimiento.EnviarComentarioM(comentarios, cod_requerimiento, idUsuario);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        valor = Convert.ToInt32(dr["VALOR"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.registrarLog("REGISTRAR REQ " + ex.Message);
                Utility.registrarLog("REGISTRAR REQ " + ex.StackTrace);
                throw new Exception("Error en el servidor, vuelva a intentarlo mas tarde.");
            }
            return valor;

        }
    }
}

