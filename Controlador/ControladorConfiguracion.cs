﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controlador
{
    public class ControladorConfiguracion
    {
        public static DataTable configuracion_dt(String grupo)
        {
            return MConfiguracion.configuracion_dt(grupo);
        }
    }
}
