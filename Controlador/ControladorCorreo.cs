﻿using Modelo;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Controlador
{
    public class ControladorCorreo
    {
        public static void EnviarCorreoC(string email, string id_requerimiento, string email_jefe, string nombre_completo, string nom_estado_requerimiento, string fecha_generada, string linea, string nom_prioridad, string detalle, string id_perfil)
        {
            MCorreo.EnviarCorreoM(email, id_requerimiento, email_jefe, nombre_completo, nom_estado_requerimiento, fecha_generada, linea, nom_prioridad, detalle, id_perfil);
        }

        public static BeanCorreo ObtenerDetalleCorreoC(Int32 id_requerimiento, Int32 id_usuario)
        {
            DataTable dataTable = MCorreo.ObtenerDetalleCorreoM(id_requerimiento, id_usuario);
            BeanCorreo CorreoDetalle = new BeanCorreo();

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow dataRow = dataTable.Rows[0];
                CorreoDetalle.email = dataRow["email"].ToString();
                CorreoDetalle.id_requerimiento = Convert.ToInt32(dataRow["id_requerimiento"].ToString());
                CorreoDetalle.email_jefe = dataRow["email_jefe"].ToString();
                CorreoDetalle.nombre_completo = dataRow["nombre_completo"].ToString();
                CorreoDetalle.nom_estado_requerimiento = dataRow["nom_estado_requerimiento"].ToString();
                CorreoDetalle.fecha_requerimiento = dataRow["fecha_requerimiento"].ToString();
                CorreoDetalle.nom_servicio = dataRow["nom_servicio"].ToString();
                CorreoDetalle.nom_prioridad = dataRow["nom_prioridad"].ToString();
                CorreoDetalle.detalle = dataRow["detalle"].ToString();
                CorreoDetalle.id_perfil = dataRow["id_perfil"].ToString();
            }
            return CorreoDetalle;
        }
    }
}
