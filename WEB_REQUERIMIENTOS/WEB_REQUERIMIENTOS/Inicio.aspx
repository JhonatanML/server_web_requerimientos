﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Inicio.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Inicio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11" />
    <link rel="shortcut icon" href="images/icons/seterza.jpg"/>

    <title>Inicio</title>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="js/cz_main.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="cz-main cz-submain-form-backini">
        <div id="cz-other-box-main">
            <h1><span runat="server" id="sp_NombreUsuario"></span></h1>
        </div>
        <div id="cz-other-box-content">
            <div class="cz-other-box-center">
                <div class="cz-other-box-center-content">
                    <asp:Literal ID="MenuInicio" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
        </div>
    </form>
</body>
</html>
