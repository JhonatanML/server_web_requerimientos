﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Menu.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Menu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%=ConfigurationManager.AppSettings["APP_NAME"] %> | SETERZA S.A.C.</title>

    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11" />
    
    <link rel="shortcut icon" href="images/icons/LogoSeterza.ico"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="js/cz_main.js" type="text/javascript"></script>



</head>
<body>
    <form id="form1" runat="server" class="cz-main">
        <div class="cz-submain">
            <div id="cz-box-header2">
                <div id="cz-barr-top">
                    <div id="cz-control-top" class="cz-box-center-width">
                        <%--<div id="img_logo_top"></div>--%>
                        <div id="cz-control-options">
                            <div id="cz-control-user">
                                <div id="cz-control-user-avatar" class="cz-control-user-option"></div>
                                <div class="cz-control-user-option">   
                                    <asp:Label ID="lbNomUsuario" runat="server" Text=""></asp:Label>
                                </div>
                                <div id="cz-control-menu" class="cz-control-user-option">
                                    <div id="cz-control-menu-arrow"></div>
                                    <div id="cz-control-menu-options">
                                        <div class="cz-control-menu-option">
                                            <a href="inicio.aspx">Inicio</a>
                                        </div>
                                        <div class="cz-control-menu-option parent">
                                            <asp:LinkButton ID="opcSalir" runat="server" onclick="opcSalir_Click">Salir</asp:LinkButton></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="cz-barr-principal">
                    <div id="cz-principal-menu-options">
                        <div id="cz-principal-menu-option-inicio" class="cz-principal-menu-option">
                            <div class="cz-principal-menu-option-button">
                                <div class="cz-principal-menu-option-title link">
                                    <div class="cz-principal-menu-option-image"></div>
                                    <a href='inicio.aspx'>Inicio</a>
                                </div>
                            </div>
                        </div>
                        <asp:Literal ID="MenuTop" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div id="cz-util-hidden-top" class="cz-util-hidden cz-util-hidden-height cz-util-hidden-bottom">
                <div class="cz-util-hidden-arrow"></div>
            </div>

            <div id="cz-box-body2">
                <div id="cz-menu-lateral-left" class="cz-menu-lateral cz-menu-lateral-left cz-menu-lateral-hide">
                    <div id="cz-menu-lateral-options">
                        <asp:Literal ID="MenuLateral" runat="server"></asp:Literal>
                    </div>
                </div>

                <div id="cz-util-hidden-left" class="cz-util-hidden cz-util-hidden-width cz-util-hidden-right">
                    <div class="cz-util-hidden-arrow"></div>
                </div>

                <div id="cz-box-content">
                    <iframe name="centerFrame" id="centerFrame" width="100%" height="100%" scrolling="yes" frameborder="0"></iframe>
                </div>
            </div>
            <div id="cz-box-footer2">
                <p>SIEDEVS Developers - Derechos reservados. <%=ConfigurationManager.AppSettings["APP_VERSION"] %></p>
            </div>
        </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
        <div id="cz-background"></div>
    </form>
</body>
</html>

