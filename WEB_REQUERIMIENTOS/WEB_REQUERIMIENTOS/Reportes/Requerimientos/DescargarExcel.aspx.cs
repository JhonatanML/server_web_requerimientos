﻿using Controlador;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;
using Utilidad.excel;

namespace WEB_REQUERIMIENTOS.Reportes.Requerimientos
{
    public partial class DescargarExcel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {

                    ExcelFileSpreadsheet exportExcel;
                    DataSet ds = new DataSet();
                    DataTable dtConsultas = new DataTable();
                    DataTable dtPrecioTotal = new DataTable();

                    String fechaIN = Request["fini"].ToString();
                    String fechaFI = Request["ffin"].ToString();
                    String id_linea_negocio = Request["id_linea_negocio"].ToString();
                    String id_centro_coste = Request["id_centro_coste"].ToString();
                    String id_perfil = Request["id_perfil"].ToString();
                    String id_usuario = Request["id_usuario"].ToString();

                    ds = ControladorRequerimiento.ExportaReporteRequerimientosC(fechaIN, fechaFI, id_linea_negocio, id_centro_coste, id_perfil, id_usuario);
                    dtConsultas = ds.Tables[0];
                    dtPrecioTotal = ds.Tables[1];

                    string PrecioTotal = "0.0";
                    foreach (DataRow row in dtPrecioTotal.Rows) {
                        PrecioTotal = row["PrecioTotal"].ToString();

                    }

                    exportExcel = ExcelFileUtils.prepararExportacionExcel(dtConsultas, "REPORTE DE SERVICIOS REQUERIDOS", "REPORTE DE SERVICIOS REQUERIDOS");
                    ExcelFileUtils.ExportToExcel(exportExcel, "REPORTE DE SERVICIOS REQUERIDOS", true, PrecioTotal);
                }
                //}
            }
            catch (Exception ex)
            {
                //string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }
    }
}