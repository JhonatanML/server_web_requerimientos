﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Grid.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Reportes.Requerimientos.Grid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>

        $(document).ready(function () {

        });
    </script>
    <style>
        .calendar {
            background-position: right center;
            background-image: url(../../images/calendar.png);
            background-repeat: no-repeat;
            cursor: pointer;
            width: 165px;
        }

        .padd {
            border: 1px solid black;
            /*background-color: lightgrey;*/
            padding-top: 70px;
            padding-right: 200px;
            padding-bottom: 70px;
            padding-left: 250px;
        }

        .cajas {
            height: 20px;
        }

        .texto {
            font-size: 15px;
            margin-top: 5px;
            padding-right: 0px;
            margin-left: 45px;
            margin-right: 25px;
        }

        .combo {
            height: 30px;
            width: 190px;
        }

        .textarea {
            height: 100px;
            Width: 300px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="habilitados" runat="server" />
        <div id="divMyModal" class="modal fade">
            <div class="modal-dialog" id="divContenidoModal"></div>
        </div>
        <div id="divGridView" runat="server">
            <asp:GridView ID="grDocumento" GridLines="None" AlternatingRowStyle-CssClass="alt"
                runat="server" AutoGenerateColumns="False" CssClass="grilla table table-bordered table-striped" Style="width: 100%;">
                <Columns>

                    <asp:BoundField DataField="cod_requerimiento" HeaderText="RQ" ItemStyle-Width="100px" ControlStyle-CssClass="center" />
                    <asp:BoundField DataField="nombre_linea_negocio" HeaderText="Línea Negocio" />
                    <asp:BoundField DataField="nombre_centro_coste" HeaderText="Centro Coste" />
                    <asp:BoundField DataField="id_centro_coste" HeaderText="ID Centro Coste" />
                    <asp:BoundField DataField="codigo_sap" HeaderText="Codigo SAP" />
                    <asp:BoundField DataField="fecha_asignacion" HeaderText="Fecha Ejecución" />
                    <%--<asp:BoundField DataField="hora_asignacion" HeaderText="Hora Ejecución" />
                    <asp:BoundField DataField="id_servicio" HeaderText="ID Servicio" ItemStyle-Width="100px" ControlStyle-CssClass="center" />
                    <asp:BoundField DataField="linea" HeaderText="Línea" />--%>
                    <asp:BoundField DataField="nom_servicio" HeaderText="Servicio" />
                    <%--<asp:BoundField DataField="cantidad" HeaderText="Cantidad" />
                    <asp:BoundField DataField="precio_unitario" HeaderText="Precio Unitario" />--%>
                    <asp:BoundField DataField="precio_parcial" HeaderText="Precio" />

                </Columns>
            </asp:GridView>
        </div>
        <div id="divGridViewPagintator" class="paginator-table" runat="server">
            <div class="paginator-table-outer">
                <div class="paginator-table-inner">
                    <div class="paginator-data">
                        <div class="cz-page-ant">
                            <p class="pagina-direccion">
                                <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="Anterior"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-now">
                            <a>Página</a>
                            <p class="pagina-actual">
                                <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                            </p>
                            <p class="pagina-direccion">
                                <span class="pagina-data">de </span>
                                <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-des">
                            <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="Siguiente"></asp:Label>
                        </div>
                    </div>
                    <div class="paginator-data-search">
                        <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                        <p>
                            Buscando Resultados
                        </p>
                    </div>
                </div>
            </div>
            <div class="cz-TotalPaginas">Total de <span runat="server" id="spTotalReg" style="font-weight: bold; font-size: 13px;"></span> Registros</div>
            <div class="cz-table-expand">
                <div class="cz-table-expand-close-x">×</div>
            </div>
        </div>
        <div id="divTotal" class="form-group col-lg-12" style="margin-top: 20px;" runat="server">
            <%--<label for="Detalle" class="col-lg-1 texto">Detalles: </label>
                            <textarea id="Detalle" runat="server" class="form-control textarea" />--%>
            <input id="PrecioTotal" name="contador" runat="server" class="col-lg-1 form-control cajas cz-util-right" disabled="disabled" style="margin-right:60px;"/>            
            <a class="cz-util-right">S/. </a>
            <label for="PrecioTotal" class="col-lg-1 texto cz-util-right">Precio Total:</label>
            
        </div>
    </form>
    <input type="hidden" id="contador" name="contador" runat="server" />
    <style>
        #divGridView td {
            text-align: center;
        }
    </style>
</body>
</html>