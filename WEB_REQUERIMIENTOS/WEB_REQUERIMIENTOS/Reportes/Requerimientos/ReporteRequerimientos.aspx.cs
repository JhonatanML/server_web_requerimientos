﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Reportes.Requerimientos
{
    public partial class ReporteRequerimientos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                String id_perfil = usuario.id_perfil.ToString();
                String id_usuario = usuario.id_usuario.ToString();

                HidIdPerfil.Value = usuario.id_perfil.ToString();
                HidIdUsuario.Value = usuario.id_usuario.ToString();

                nFechaInicio.Value = getFechaActual();
                nFechaTermino.Value = getFechaActual();
                CargarComboLineaNegocio(id_perfil, id_usuario);
                CargarComboCentroCoste(id_perfil, id_usuario);
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        public static string getFechaActual()
        {
            string dia = DateTime.Now.Day.ToString().PadLeft(2, '0');
            string mes = DateTime.Now.Month.ToString().PadLeft(2, '0');
            string anio = DateTime.Now.Year.ToString().PadLeft(4, '0');

            return anio + "/" + mes + "/" + dia;
        }

        private void CargarComboLineaNegocio(string id_perfil, string id_usuario)
        {
            if (id_perfil == "2" || id_perfil == "3")
            {
                dllLineaNegocio.DataSource = ControladorLineaNegocio.CargarComboLineaNegocioC(id_perfil, id_usuario);
                dllLineaNegocio.DataValueField = "id_linea_negocio";
                dllLineaNegocio.DataTextField = "nombre_linea_negocio";
                dllLineaNegocio.DataBind();
                dllLineaNegocio.Enabled = false;
            }
            else if (id_perfil == "4" || id_perfil == "5")
            {
                dllLineaNegocio.DataSource = ControladorLineaNegocio.CargarComboLineaNegocioC(id_perfil, id_usuario);
                dllLineaNegocio.DataValueField = "id_linea_negocio";
                dllLineaNegocio.DataTextField = "nombre_linea_negocio";
                dllLineaNegocio.DataBind();
                dllLineaNegocio.Items.Insert(0, ".:TODOS:.");
                dllLineaNegocio.Items[0].Value = "-1";
            }
        }

        private void CargarComboCentroCoste(string id_perfil, string id_usuario)
        {
            if (id_perfil == "3")
            {
                dllCentroCoste.DataSource = ControladorCentroCoste.CargarComboCentroCosteC(id_perfil, id_usuario);
                dllCentroCoste.DataValueField = "id_centro_coste";
                dllCentroCoste.DataTextField = "nombre_centro_coste";
                dllCentroCoste.DataBind();
                dllCentroCoste.Enabled = false;
            }
            else if (id_perfil == "4" || id_perfil == "5" || id_perfil == "2")
            {
                dllCentroCoste.DataSource = ControladorCentroCoste.CargarComboCentroCosteC(id_perfil, id_usuario);
                dllCentroCoste.DataValueField = "id_centro_coste";
                dllCentroCoste.DataTextField = "nombre_centro_coste";
                dllCentroCoste.DataBind();
                dllCentroCoste.Items.Insert(0, ".:TODOS:.");
                dllCentroCoste.Items[0].Value = "-1";
            }
        }

        [WebMethod]
        public static string CargarComboSubCategoria(string cod_categoria)
        {
            try
            {
                List<BeanSubCategoria> lstBean = new List<BeanSubCategoria>();
                lstBean = ControladorRequerimiento.Combo_SubCategoriaC(cod_categoria);

                return Newtonsoft.Json.JsonConvert.SerializeObject(lstBean);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}