﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteRequerimientos.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Reportes.Requerimientos.ReporteRequerimientos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <link href="../../css/styleBootStrap.css" rel="stylesheet" />
    <script src="../../js/jquery.datetimepicker.js"></script>
    <script src="../../js/moment.js"></script>
    <script src="../../lib/moment.min.js"></script>
    <link href="../../css/jquery.datetimepicker.css" rel="stylesheet" />
    <script src="../../js/Siedevs.js"></script>
    <script>
        //var urldel = 'MantLocales.aspx/EliminarLocal';//WebMethod para eliminacion
        //var urlins = 'NuevoLocal.aspx'//pantalla de registros
        var urlbus = 'Grid.aspx';//grilla
        //var urlLN = 'ReporteRequerimientos.aspx/CargarComboLineaNegocio';
        //var urlCC = 'ReporteRequerimientos.aspx/CargarComboCentroCoste';
        //var urlsavN = 'MantLocales.aspx/AgregarLocal'; //WebMethod para insercion
        //var urlsavE = 'MantLocales.aspx/EditarLocal'; //WebMethod para edicion

        $(document).ready(function () {

            //EliminarLocal();//funcion encargada de la eliminacion
            //AgregarLocal();//funcion encargada de adiccionar registros
            //ModificarLocal();//funcion encargada en modificar registros
            BuscarReporteRequerimientos();//funcion encargada de mostrar la grilla y encargada de la paginacion de esta

            //ObtenerSubCategoriasC();
            //ObtenerSubCategoriasC();
            $('#buscar').trigger("click");
            $(document).keypress(function (e) {
                if (e.which == 13) {
                    //alert("EVENTO KEYPRESS");
                    $('#buscar').trigger("click");
                }
            });

            $('#nFechaInicio').datetimepicker({
                format: 'Y/m/d',
                lang: 'es',
                closeOnDateSelect: true,
                onShow: function (ct) {
                    this.setOptions({
                        maxDate: $('#nFechaTermino').val() ? $('#nFechaTermino').val() : false
                    })
                },
                timepicker: false

            });
            $('#nFechaTermino').datetimepicker({
                format: 'Y/m/d',
                lang: 'es',
                onShow: function (ct) {
                    this.setOptions({
                        minDate: $('#nFechaInicio').val() ? $('#nFechaInicio').val() : false
                    })
                },
                closeOnDateSelect: true,
                timepicker: false
            });

            var urlXLS = 'DescargarExcel.aspx';
            $('.xlsReg').click(function () {
                var cont = $('#contador').val();
                if (cont > 0) {
                    window.location.href = urlXLS + '?' + getParametrosXLS();
                }
                else {
                    addnotify("notify", "¡No hay datos que exportar!");
                }
            });

        });

        function getParametrosXLS() {
            var strData = new Object();
            strData.fini = $('#nFechaInicio').val();
            strData.ffin = $('#nFechaTermino').val();
            strData.id_linea_negocio = $('#dllLineaNegocio').val();
            strData.id_centro_coste = $('#dllCentroCoste').val();
            strData.id_perfil = $('#HidIdPerfil').val();
            strData.id_usuario = $('#HidIdUsuario').val();

            return 'fini=' + strData.fini + '&ffin=' + strData.ffin +
                   '&id_linea_negocio=' + strData.id_linea_negocio + '&id_centro_coste=' + strData.id_centro_coste +
                   '&id_perfil=' + strData.id_perfil + '&id_usuario=' + strData.id_usuario;
        }

        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.fechaIN = $('#nFechaInicio').val();
            strData.fechaFI = $('#nFechaTermino').val();
            strData.id_linea_negocio = $('#dllLineaNegocio').val();
            strData.id_centro_coste = $('#dllCentroCoste').val();
            strData.id_perfil = $('#HidIdPerfil').val();
            strData.id_usuario = $('#HidIdUsuario').val();

            strData.page = $('#hdnActualPage').val();
            strData.rows = $('#hdnShowRows').val();

            return strData;
        }

    </script>
    <style>
        .calendar {
            background-position: right center;
            background-image: url(../../images/calendar.png);
            background-repeat: no-repeat;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="borrarContenido">
        <form id="form1" runat="server">
            <input id="txtfecha" type="hidden" runat="server" />
            <input id="txtcliente" type="hidden" runat="server" />
            <asp:HiddenField ID="HidIdPerfil" runat="server" />
            <asp:HiddenField ID="HidIdUsuario" runat="server" />
            <div class="cz-submain cz-submain-form-background">
                <div id="cz-form-box">
                    <div class="cz-form-box-content">
                        <div id="cz-form-box-content-title" style="margin-top: -12px;">
                            <div id="cz-form-box-content-title-text">
                                <span class="glyphicon glyphicon glyphicon-stats btn-lg cz-form-box-content-title-icon" style="color: #666;" aria-hidden="true"></span>
                                <span class="font-size-20">Reporte de Requerimientos Cerrados</span>
                            </div>
                        </div>
                        <button id="cz-form-box-new" type="button" class="cz-form-content-input-button btn-md cz-form-box-content-button cz-util-right cz-u-expand-table" style="margin-top: 5px;" data-grid-id="divGridViewData">
                            <span class="glyphicon glyphicon-fullscreen" style="color: white;"></span>&nbsp;&nbsp;Ver Tabla
                        </button>
                        <button id="cz-form-box-deletes" type="button" class="cz-form-content-input-button btn-md cz-form-box-content-button cz-util-right xlsReg" style="margin-top: 5px;">
                            <span class="glyphicon glyphicon-download" style="color: white;"></span>&nbsp;&nbsp;Exportar
                        </button>
                        <%--<button id="cz-form-box-deletes" type="button" class="cz-form-content-input-button btn-md cz-form-box-content-button cz-util-right addUsuario" style="margin-top: 5px;">
                            <span class="glyphicon glyphicon-file" style="color: white;"></span>&nbsp;&nbsp;Nuevo
                        </button>
                        <button id="cz-form-box-delete" type="button" class="cz-form-content-input-button btn-md cz-form-box-content-button cz-util-right delReg" style="margin-top: 5px;">
                            <span class="glyphicon glyphicon-remove" style="color: white;"></span>&nbsp;&nbsp;Borrar
                        </button>--%>
                    </div>

                    <div class="cz-form-box-content">
                        <%--<div class="cz-form-content">
                            <p>Código: </p>
                            <input type="text" class="form-control" runat="server" id="CodigoRequerimiento" maxlength="10" style="height: 20px; width: 85%;" />
                        </div>--%>
                        <div class="cz-form-content">
                            <label for="nFechaInicio">Fecha de inicio:</label>
                            <input id="nFechaInicio" runat="server" type="text" class="form-control calendar" maxlength="10" style="height: 20px; width: 85%;" />
                            <%--<label for="txtFechaInicio">Fecha Inicio:</label>
                            <input id="txtFechaInicio" runat="server" type="text" class="form-control calendar" maxlength="10" style="height: 20px; width: 85%;" />--%>
                        </div>
                        <div class="cz-form-content">
                            <%--<label for="txtFechaTermino">Fecha Termino:</label>
                            <input id="txtFechaTermino" runat="server" type="text" class="form-control calendar" maxlength="10" style="height: 20px; width: 85%;" />--%>
                            <label for="nFechaTermino">Fecha de termino:</label>
                            <input id="nFechaTermino" runat="server" type="text" class="form-control calendar" maxlength="10" style="height: 20px; width: 85%;" />
                        </div>
                        <div class="cz-form-content" style="width: 250px;">
                            <p>Línea Negocio:</p>
                            <asp:DropDownList ID="dllLineaNegocio" runat="server" class="form-control"></asp:DropDownList>
                        </div>
                        <div class="cz-form-content" style="width: 250px;">
                            <p>Centro Coste:</p>
                            <asp:DropDownList ID="dllCentroCoste" runat="server" class="form-control"></asp:DropDownList>
                        </div>
                        <div class="cz-util-right cz-util-right-text col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <button id="buscar" type="button" class="cz-form-content-input-button btn-md cz-form-box-content-button cz-util-right" style="margin-top: 5px;">
                                <span class="glyphicon glyphicon-search" style="color: white;"></span>&nbsp;&nbsp;Buscar
                            </button>
                        </div>
                    </div>
                    <div class="cz-form-box-content">
                        <div class="form-grid-box">
                            <div class="form-grid-table-outer">
                                <div class="form-grid-table-inner">
                                    <div class="form-gridview-data" id="divGridViewData" runat="server"></div>
                                    <div class="form-gridview-error" id="divGridViewError" runat="server"></div>
                                    <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                        <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                        <p>Buscando Resultados</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="paginator-hidden-fields">
                        <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                        <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                    </div>
                    <div id="myModalCargando" class="modal hide fade in" style="display: block; background: transparent; border: none; -webkit-box-shadow: none;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
                    </div>
                    <div id="myModal" class="modal hide fade" style="display: block;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>
