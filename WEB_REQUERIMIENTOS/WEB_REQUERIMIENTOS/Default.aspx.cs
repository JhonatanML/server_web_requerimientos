﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace WEB_REQUERIMIENTOS
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String accion = Request["acc"];

            if (accion == null)
            { accion = ""; }


            if (accion.Equals("SES"))
            {
                this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Su sesion ha expirado, ingrese nuevamente.\", \"usernregister\");};</script>";
            }

            if (accion.Equals("EXT"))
            {
                this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Ha cerrado sesión satistactoriamente.\", \"usernregister\");};</script>";
            }
            if (accion.Equals("ERR"))
            {
                this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Ocurrio un Error, Revise Log Generado.\", \"usernregister\");};</script>";
            }
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                String login = this.txtUsuario.Text;
                String clave = this.txtClave.Text;
                if (login != "" && clave != "")
                {
                    BeanUsuario bean = ControladorUsuario.validarUsuario(login, clave);
                    ConfigBean config = new ConfigBean(); //ControladorGeneral.GetParametro("PAG");

                    if (bean.clave == null || bean.usuario == null)
                    {
                        this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Usuario/Contraseña incorrecta\", \"usernregister\");};</script>";
                    }

                    if (bean.usuario != null && bean.clave != null)
                    {
                        if (bean.estado_perfil == 1)
                        {
                            if (bean.habilitado == 0)
                            {
                                this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Usuario deshabilitado\", \"usernregister\");};</script>";


                            }
                            else if (bean.habilitado == 1)
                            {
                                if (bean.id_usuario != 0)
                                {
                                    Session[Configuracion.SESSION_OBJ_USUARIO] = bean;

                                    HttpContext.Current.Response.Redirect("Menu.aspx", false);
                                }
                            }
                        }
                        else
                        {
                            this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Perfil deshabilitado\", \"usernregister\");};</script>";
                        }
                    }
                }
                else if (login == "" || clave == "")
                {
                    this.txtmsg.Text = "<script>window.onload = function() {addnotify(\"alert\", \"Por favor completar todos los campos\", \"usernregister\");};</script>";
                }
            }
            catch (Exception ex)
            {
                Utility.registrarLog("< ERROR [Default.aspx.CS]:> " + ex.Message);
                HttpContext.Current.Response.Write(Utilidad.ExceptionUtils.getHtmlErrorPage(ex));
                HttpContext.Current.Response.End();
            }

        }
    }
}