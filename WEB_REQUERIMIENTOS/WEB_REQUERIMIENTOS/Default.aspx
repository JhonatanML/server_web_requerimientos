﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">

    <title><%=ConfigurationManager.AppSettings["APP_NAME"] %> | SETERZA S.A.C</title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10" />

    <%--<link rel="shortcut icon" href="images/icons/logoIncidencia.ico"/>--%>

    <!-- <link rel='stylesheet' href='http://codepen.io/assets/libs/fullpage/jquery-ui.css'> -->
    <link rel="shortcut icon" href="images/icons/LogoSeterza.ico"/>
    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/cz_main.js" type="text/javascript"></script>
    <script src="js/jsmodule.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="assets/css/style.css" media="screen" type="text/css" />

</head>

<body>

    <br />
    <p class="pd">SISTEMA WEB PARA</p>
    <p class="pd">REQUERIMIENTO DE SERVICIOS</p>
    <div class="login-card">
        <img src="images/logo/seterza.png" width="100%" alt="Seterza.png" />
        <%--<h1>Acceso</h1>--%>
        <br />
        <p>Ingrese su usuario y contraseña:</p>
        <form id="form1" runat="server" autocomplete="off">

            <%--AGREGADO--%>
            <asp:Literal ID="txtmsg" runat="server"></asp:Literal>

            <asp:TextBox ID="txtUsuario" placeholder="Usuario..." runat="server" MaxLength="50" CssClass="input"></asp:TextBox>
            <%--<input type="text" name="user" placeholder="Usuario...">--%>
            <asp:TextBox ID="txtClave" placeholder="Contraseña..." TextMode="Password" runat="server" MaxLength="50" CssClass="input"></asp:TextBox>
            <%--<input type="password" name="pass" placeholder="Contraseña...">--%>

            <asp:Button ID="btnIngresar" CssClass="login login-submit" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" />
            <%--<input type="submit" name="Ingresar" class="login login-submit" value="Ingresar">--%>
        </form>

        <!-- <div class="login-help">
    <a href="#">Register</a> • <a href="#">Forgot Password</a>
  </div> -->

        <div id="divMyModal" class="modal fade">
            <div class="modal-dialog" id="divContenidoModal"></div>
        </div>
    </div>
    <div id="cz-box-footer2">
        <p class="pd2">SIEDEVS Developers - Derechos reservados. <%=ConfigurationManager.AppSettings["APP_VERSION"] %></p>
    </div>

    <!-- <div id="error"><img src="https://dl.dropboxusercontent.com/u/23299152/Delete-icon.png" /> Your caps-lock is on.</div> -->

    <!-- <script src='http://codepen.io/assets/libs/fullpage/jquery_and_jqueryui.js'></script> -->


</body>

</html>
