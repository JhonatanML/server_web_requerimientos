﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace WEB_REQUERIMIENTOS
{
    public partial class Inicio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
            if (usuario != null)
            {
                try
                {
                    StringBuilder sbmenuinicio = new StringBuilder();
                    String OldcategoriaRef = "";

                    sp_NombreUsuario.InnerText = Session["lgn_nombre"] != null ? "BIENVENIDO " + (String)Session["lgn_nombre"] : "";
                    //List<BeanPermisos> lista = ControladorUsuario.permisosUsuario(Session["lgn_perfil"]!=null?(String)Session["lgn_perfil"]:"0");

                    List<BeanPermisos> lista = ControladorUsuario.permisosUsuario(usuario.id_perfil);
                    if (lista.Count > 0)
                    {
                        foreach (BeanPermisos bean in lista)
                        {
                            if (OldcategoriaRef != bean.Referencia)
                            {
                                if (OldcategoriaRef != "")
                                {
                                    sbmenuinicio.Append("</div></div>");
                                }

                                //Instanciando categoria de menu
                                sbmenuinicio.Append("<div class=\"cz-other-box-center-box\">");
                                sbmenuinicio.Append("<div class=\"cz-other-box-center-box-title\">" + bean.NombreCategoria + "</div>");
                                sbmenuinicio.Append("<div class=\"cz-other-box-center-box-options\">");

                                OldcategoriaRef = bean.Referencia;
                            }
                            //Instanciando opcion de menu
                            sbmenuinicio.Append("<div class=\"cz-other-box-center-box-option\"> <a href=\"" + bean.Url + "\">" + bean.NombreOpcion + "</a> </div>");

                        }

                        //Instanciando final de categoria de menu
                        sbmenuinicio.Append("</div></div>");
                    }


                    MenuInicio.Text = sbmenuinicio.ToString();
                }
                catch (Exception ex)
                {
                    string myScript = "parent.document.location.href = 'Default.aspx?acc=EXT';";
                    Utility.registrarLog("< ERROR [MENU.ASPX.CS]:> " + ex.Message);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
                }
            }
        }
    }
}