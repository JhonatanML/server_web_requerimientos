﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ActualizacionDatos.aspx.cs" Inherits="WEB_REQUERIMIENTOS.ActualizacionDatos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Example of Bootstrap 3 Modals Methods</title>
    <meta charset="utf-8" />


    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#myModal").modal({
                backdrop: 'static',
                keyboard: false
            });
        });

    </script>
    <style type="text/css">
        .centrado {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            -webkit-transform: translate(-50%, -50%);
            width: 400px;
            margin-top: 40%;
            z-index: 1050;
            background-color: #ffffff;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
        }

        .modal-header, .btnGuardar {
            background-color: #0154a0 !important;
            color: white !important;
        }

        .btnGuardar {
            padding: 10px;
            border-radius: 10px;
            box-shadow: none;
        }

        .caja-texto{
            height: 20px
        }
        /*.espacioAbajo {
            padding-bottom: 10px !important;
        }*/
    </style>


    <script>
        function getUsuarioClave() {
            var strData = new Object();
            strData.newClave = $('#txtNewClave').val();
            strData.repClave = $('#txtRepClave').val();
            return strData;
        }
    </script>

</head>
<body>

    <div id="myModal" class="centrado">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Cabecera -->
                <div class="modal-header">
                    <h4 class="modal-title">Ingrese una nueva contraseña</h4>
                </div>
                <!-- Contenido -->
                <form id="formPass" autocomplete="off">
                    <div class="modal-body">
                        <div>
                            
                            <div class="form-group">
                                <div class="col-lg-10 col-sm-10">
                                    <label for="txtPassword1">Nueva contraseña</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control input-sm caja-texto" id="txtNewClave" name="txtNewClave" />
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="col-lg-10 col-sm-10">
                                    <label for="txtPassword2">Repetir nueva contraseña</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control input-sm caja-texto" id="txtRepClave" name="txtRepClave" />
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="" style="background-color: white !important;">
                            <%--<button type="submit" class="btnGuardar">Guardar</button>--%>
                            <button id="btnActualizarClave" type="button" class="btn btn-primary cz-form-content-input-button btn-lg cz-form-box-content-button" style="margin-top: 15px; background-color: #0154a0 !important; font-size:12px;" >Confirmar</button>
                            <button id="btnCancelar" type="button" class="btn btn-default cz-form-content-input-button btn-lg cz-form-box-content-button cz-util-right" data-dismiss="modal" style="margin-top: 15px; background-color: #0154a0 !important; font-size:12px;">Cancelar</button>
                        </div>
                    </div>
                </form>
                <script src="js/validaCampos.js" type="text/javascript"></script>
            </div>
        </div>
    </div>

</body>
</html>
