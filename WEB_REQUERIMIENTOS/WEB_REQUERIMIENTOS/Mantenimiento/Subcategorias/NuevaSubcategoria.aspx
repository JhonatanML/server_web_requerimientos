﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevaSubcategoria.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Mantenimiento.Subcategorias.NuevaSubcategoria" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style>
        fieldset {
            border: 1.5px solid #1F497D;
            margin-bottom: 10px;
            margin-right: 10px;
        }

            fieldset legend {
                background-color: white;
                border-radius: 5px;
                font-size: 18px;
                margin-left: 15px;
                text-align: left;
                vertical-align: middle;
                width: 15%;
            }

        .cajas {
            height: 20px;
        }

        .texto {
            font-size: 15px;
            margin-top: 5px;
            padding-right: 0px;
            margin-left: 45px;
            margin-right: 25px;
        }

        .combo {
            height: 30px;
            width: 413px;
        }

        #dllPerfil {
            width: 188px;
            font-size: 15px;
        }

        .textarea {
            height: 100px;
        }
    </style>

    <script>

        function getSala() {
            var strData = new Object();

            
            strData.id_categoria = $('#dllCategoriaM').val();
            strData.nom_subcategoria = $('#txtSubcategoriaM').val();
            strData.linea = $('#txtLineaM').val();

            strData.id_subcategoria = $('#HidIdSubcategoria').val();

            return strData;
        }

    </script>

    <style>
        .modal-field {
            max-height: calc(100vh - 140px);
            overflow-y: auto;
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" class="form-horizontal" role="form">
        <asp:HiddenField ID="HidLocalEmpresa" runat="server" />
        <asp:HiddenField ID="HidIdSubcategoria" runat="server" />
        <asp:HiddenField ID="HidCodSala" runat="server" />
        <%--<asp:HiddenField ID="hdEstado" Value="True" runat="server" />--%>
        <!-- Titulo de ventana modal -->
        <div class="modal-header" style="border-bottom: 1px solid #7D7D7D!important;">
            <h3 id="myModalLabel" runat="server">Nueva Subcategoria</h3>
        </div>

        <div id="myModalContent" class="modal-body" style="background: #FFFFFF;">
            <fieldset style="padding-bottom: 10px;">
                <legend>Subcategoria
                </legend>

                <div class="container-fluid modal-field" style="padding: 0px !important">

                    <div class="form-group col-lg-12" style="margin-top: 20px;">
                        <label for="dllCategoriaM" class="col-lg-2 texto">Categoria:</label>
                        <asp:DropDownList ID="dllCategoriaM" runat="server" class="form-control col-lg-3 combo"></asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 20px;">
                        <label for="txtSubcategoriaM" class="col-lg-2 texto">Nombre de la Subcategoria:</label>
                        <input id="txtSubcategoriaM" runat="server" class="form-control col-lg-6 cajas" />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 20px;">
                        <label for="txtLineaM" class="col-lg-2 texto">Línea:</label>
                        <input id="txtLineaM" runat="server" class="form-control col-lg-1 cajas" />
                    </div>
                </div>
            </fieldset>

            <div class="modal-footer" style="padding: 5px 0px 0px 0px!important;">
                <div id="msg"></div>
                <button id="btnGuardar" type="button" class="btn btn-primary cz-form-content-input-button btn-mg cz-form-box-content-button">Guardar</button>
                <button id="btnCancelar" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button cz-util-right" data-dismiss="modal">Cancelar</button>
            </div>

        </div>
    </form>
</body>
</html>

