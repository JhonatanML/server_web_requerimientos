﻿using Controlador;
using Modelo.BEANS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Mantenimiento.Subcategorias
{
    public partial class NuevaSubcategoria : System.Web.UI.Page
    {
        string json = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CargarComboCategoria();

            try
            {

                json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                if (dataJSON == null)
                {
                    return;
                }
                else
                {
                    BeanSubCategoria bean = ControladorSubcategoria.ObtenerSubcategoriaC(dataJSON["id_subcategoria"]);

                    this.myModalLabel.InnerText = "Editar Subcategoria";
                    this.dllCategoriaM.SelectedValue = bean.id_categoria.ToString();
                    this.txtSubcategoriaM.Value = bean.nom_subcategoria;
                    this.txtLineaM.Value = bean.linea;

                    this.HidIdSubcategoria.Value = bean.id_subcategoria.ToString();
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        private void CargarComboCategoria()
        {
            dllCategoriaM.DataSource = ControladorRequerimiento.Combo_CategoriaC();
            dllCategoriaM.DataValueField = "cod_categoria";
            dllCategoriaM.DataTextField = "nom_categoria";
            dllCategoriaM.DataBind();
            dllCategoriaM.Items.Insert(0, ".:CATEGORIAS:.");
            dllCategoriaM.Items[0].Value = "-1";
        }
    }
}