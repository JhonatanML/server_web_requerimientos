﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Mantenimiento.Subcategorias
{
    public partial class MantSubcategorias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                CargarComboCategoria();
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        private void CargarComboCategoria()
        {
            dllCategoria.DataSource = ControladorRequerimiento.Combo_CategoriaC();
            dllCategoria.DataValueField = "cod_categoria";
            dllCategoria.DataTextField = "nom_categoria";
            dllCategoria.DataBind();
            dllCategoria.Items.Insert(0, ".:CATEGORIAS:.");
            dllCategoria.Items[0].Value = "-1";
        }

        [WebMethod]
        public static void EliminarSubcategoria(string id_subcategoria)
        {
            try
            {
                ControladorSubcategoria.EliminarSubcategoriaC(id_subcategoria);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static string AgregarSubcategoria(string id_categoria, string nom_subcategoria, string linea)
        {
            try
            {
                ControladorSubcategoria.AgregarSubcategoriaC(new BeanSubCategoria()
                {
                    id_categoria = Convert.ToInt32(id_categoria),
                    nom_subcategoria = nom_subcategoria,
                    linea = linea

                });
                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static string EditarSubcategoria(string id_categoria, string nom_subcategoria, string linea, string id_subcategoria)
        {
            try
            {
                ControladorSubcategoria.EditarSubcategoriaC(new BeanSubCategoria()
                {
                    id_categoria = Convert.ToInt32(id_categoria),
                    nom_subcategoria = nom_subcategoria,
                    linea = linea,
                    id_subcategoria = Convert.ToInt32(id_subcategoria)

                });
                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}