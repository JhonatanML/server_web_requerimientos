﻿using Controlador;
using Modelo.BEANS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace WEB_REQUERIMIENTOS.Mantenimiento.Usuarios
{
    public partial class NuevoUsuario : System.Web.UI.Page
    {
        string json = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
            Int32 id_perfil = usuario.id_perfil;
            Int32 id_admin_general = usuario.id_usuario;

            HidIdAdminGeneral.Value = id_admin_general.ToString();
            HidIdPerfil.Value = id_perfil.ToString();

            
            try
            {
                CargarComboPerfil(id_perfil);
                CargaCombos(Convert.ToString(id_perfil), Convert.ToString(usuario.id_usuario));
                json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                if (dataJSON == null)
                {
                    return;
                }
                else
                {
                    BeanUsuario bean = ControladorUsuario.ObtenerUsuarioC(dataJSON["id_usuario"]);

                    this.myModalLabel.InnerText = "Editar Usuario";
                    this.txtApellidoPaternoM.Value = bean.apellido_paterno;
                    this.txtApellidoMaternoM.Value = bean.apellido_materno;
                    this.txtNombresM.Value = bean.nombres;
                    this.txtDniM.Value = bean.dni;
                    this.txtEmailM.Value = bean.email;
                    this.txtTelfMovilM.Value = bean.telf_movil;
                    this.txtTelfFijoM.Value = bean.telf_fijo;
                    this.txtDireccion.Value = bean.direccion;
                    this.dllPerfilM.SelectedValue = bean.id_perfil.ToString();
                    this.txtUsuarioM.Value = bean.usuario;
                    this.txtClaveM.Value = bean.clave;
                    hdnIdLineaNegocio.Value = bean.idLineaNegocio;
                    hdnIdCentroCosto.Value = bean.idCentroCosto;
                    String idJefe = "";
                    if (bean.id_jefe == 0)
                    {
                        idJefe = "-1";
                    }
                    else
                    {
                        idJefe = Convert.ToString(bean.id_jefe);
                    }
                    HidIdJefe.Value = idJefe;
                    this.HidIdUsuario.Value = bean.id_usuario.ToString();
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        private void CargaCombos(String idPerfil, String id_usuario)
        {
            //COMBO DE LINEA DE NEGOCIO
            dllLineaNegocio.DataSource = ControladorLineaNegocio.CargarComboLineaNegocioC(idPerfil, id_usuario);
            dllLineaNegocio.DataValueField = "id_linea_negocio";
            dllLineaNegocio.DataTextField = "nombre_linea_negocio";
            dllLineaNegocio.DataBind();
            dllLineaNegocio.Items.Insert(0, ".:SELECCIONE:.");
            dllLineaNegocio.Items[0].Value = "-1";

            //COMBO CENTRO DE COSTO
            dllCentroCosto.DataSource = ControladorCentroCoste.CargarComboCentroCosteC(idPerfil, id_usuario);
            dllCentroCosto.DataValueField = "id_centro_coste";
            dllCentroCosto.DataTextField = "nombre_centro_coste";
            dllCentroCosto.DataBind();
            dllCentroCosto.Items.Insert(0, ".:SELECCIONE:.");
            dllCentroCosto.Items[0].Value = "-1";
        }

        private void CargarComboPerfil(Int32 id_perfil)
        {
            dllPerfilM.DataSource = ControladorPerfil.Combo_PerfilC(id_perfil);
            dllPerfilM.DataValueField = "id_perfil";
            dllPerfilM.DataTextField = "nom_perfil";
            dllPerfilM.DataBind();
            dllPerfilM.Items.Insert(0, ".:SELECCIONE:.");
            dllPerfilM.Items[0].Value = "-1";
        }

        [WebMethod]
        public static string ObtenerSupervisores()
        {
            try
            {
                List<BeanSupervisor> lstBean = new List<BeanSupervisor>();
                lstBean = ControladorRequerimiento.ObtenerSupervisores();

                return Newtonsoft.Json.JsonConvert.SerializeObject(lstBean);
            }
            catch (Exception ex)
            {
                Utility.registrarLog(ex.Message);
                return null;
            }
        }
    }
}