﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Grid.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Mantenimiento.Usuarios.Grid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>

        $(document).ready(function () {
            estadoCheck();
            var valor = localStorage.getItem("chbactivo");

            if (valor = 1 && valor != 2) {
                $('#grDocumento tr:first').each(function () {

                    //$(this).find("th:last").eq(0).html("Deshabilitar");
                });

                $('.chkbox ').each(function (i, v) {

                    //alert('i : ' + i);
                    var databooleana = $(this).hasClass("marcado");

                    //alert('databooleana: ' + databooleana);

                    if (databooleana == false) {
                        //alert('ingreso if = false');
                        //$('.iconImg').attr("src", "../../imagery/all/icons/eliminar.png");
                        $('.iconImg').attr("title", "Deshabilitar");

                    } else if (databooleana == true) {
                    }
                });

            } else if (valor = 2 && valor != 1) {
                $('#grDocumento tr:first').each(function () {
                    //$(this).find("th:last").eq(0).html("Habilitar");
                });
            }
        });

        //var estado = localStorage.getItem("chbactivo");
        //alert(estado);

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="habilitados" runat="server" />
        <div id="divMyModal" class="modal fade" style="width:60%; margin-left:-35%;">
            <div class="modal-dialog" id="divContenidoModal"></div>
        </div>
        <div id="divGridView" runat="server">
            <asp:GridView ID="grDocumento" GridLines="None" AlternatingRowStyle-CssClass="alt"
                runat="server" AutoGenerateColumns="False" CssClass="grilla table table-bordered table-striped" Style="width: 100%;">
                <Columns>
                    <asp:TemplateField ItemStyle-Width="40px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                        <HeaderTemplate>
                            <input id_usuario="ChkAll" name="chkSelectAll" value="<%# Eval("id_usuario") %>" type="checkbox">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <input id_usuario="<%# Eval("id_usuario") %>" value="<%# Eval("id_usuario") %>" type="checkbox">
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="id_usuario" HeaderText="ID" ControlStyle-CssClass="center" />
                    <asp:BoundField DataField="nombre_completo" HeaderText="Nombre Completo" />
                    <asp:BoundField DataField="dni" HeaderText="DNI" />
                    <asp:BoundField DataField="email" HeaderText="E-mail" />
                    <%--<asp:BoundField DataField="telf_movil" HeaderText="Telf. Móvil" />
                    <asp:BoundField DataField="telf_fijo" HeaderText="Telf. FIjo" />--%>
                    <asp:BoundField DataField="nom_perfil" HeaderText="Perfil" />
                    <%--<asp:BoundField DataField="usuario" HeaderText="Usuario" />
                    <asp:BoundField DataField="clave" HeaderText="Clave" />--%>



                    <asp:TemplateField HeaderText="Editar" ItemStyle-Width="40px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <a class="editItemReg" style="cursor: pointer;" data-toggle="modal"
                                id_usuario="<%# Eval("id_usuario") %>">
                                <img src="../../imagery/all/icons/modificar.png" border="0" title="Editar Usuario" /></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Estado" ItemStyle-Width="40px" HeaderStyle-CssClass="last"
                        ItemStyle-CssClass="last" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <a role="button" style="cursor: pointer;" data-toggle="modal" class="delItemReg form-icon"
                                id_usuario="<%# Eval("id_usuario") %>">
                                <img src="../../imagery/all/icons/eliminar.png" border="0" title="Eliminar Usuario" /></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="divGridViewPagintator" class="paginator-table" runat="server">
            <div class="paginator-table-outer">
                <div class="paginator-table-inner">
                    <div class="paginator-data">
                        <div class="cz-page-ant">
                            <p class="pagina-direccion">
                                <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="Anterior"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-now">
                            <a>Página</a>
                            <p class="pagina-actual">
                                <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                            </p>
                            <p class="pagina-direccion">
                                <span class="pagina-data">de </span>
                                <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-des">
                            <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="Siguiente"></asp:Label>
                        </div>
                    </div>
                    <div class="paginator-data-search">
                        <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                        <p>
                            Buscando Resultados
                        </p>
                    </div>
                </div>
            </div>
            <div class="cz-TotalPaginas">Total de <span runat="server" id="spTotalReg" style="font-weight: bold; font-size: 13px;"></span> Registros</div>
            <div class="cz-table-expand">
                <div class="cz-table-expand-close-x">×</div>
            </div>
        </div>
    </form>
    <input type="hidden" id="contador" name="contador" runat="server" />
</body>
</html>
