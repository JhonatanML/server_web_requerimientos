﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevoUsuario.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Mantenimiento.Usuarios.NuevoUsuario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="../../js/Siedevs.js"></script>
    <script>
        $(document).ready(function () {
            CombosModalUsuario();
        });
    </script>
    <style>

        fieldset {
            border: 1.5px solid #1F497D;
            margin-bottom: 10px;
            margin-right: 10px;
        }

            fieldset legend {
                background-color: white;
                border-radius: 5px;
                font-size: 18px;
                margin-left: 15px;
                text-align: left;
                vertical-align: middle;
                width: 15%;
            }

        .cajas {
            height: 20px;
        }

        .texto {
            font-size: 15px;
            margin-top: 5px;
            /*padding-right: 0px;*/
            /*margin-left: 45px;
            margin-right: 25px;*/
        }

        .combo {
            height: 35px;
            width: 190px;
        }

        #dllPerfil {
            width: 188px;
            font-size: 15px;
        }

        .textarea {
            height: 100px;
        }


    </style>

    <script>

        $(document).ready(function () {
            ModalUsuario();
        });

        function getSala() {
            var strData = new Object();
            strData.apellido_paterno = $('#txtApellidoPaternoM').val();
            strData.apellido_materno = $('#txtApellidoMaternoM').val();
            strData.nombres = $('#txtNombresM').val();
            strData.dni = $('#txtDniM').val();
            strData.email = $('#txtEmailM').val();
            strData.telf_movil = $('#txtTelfMovilM').val();
            strData.telf_fijo = $('#txtTelfFijoM').val();
            strData.direccion = $('#txtDireccion').val();
            strData.id_perfil = $('#dllPerfilM').val();
            strData.id_linea_negocio = $('#dllLineaNegocio').val();
            strData.id_centro_coste = $('#dllCentroCosto').val();;
            strData.usuario = $('#txtUsuarioM').val();
            strData.clave = $('#txtClaveM').val();
            strData.idSupervisor = $('#dllSupervisor').val();
            strData.id_usuario = $('#HidIdUsuario').val();


            return strData;
        }



    </script>

    <style>
        .modal-field {
            max-height: calc(100vh - 140px);
            overflow-y: auto;
            overflow-x: hidden;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server" class="form-horizontal" role="form">
        <asp:HiddenField ID="HidLocalEmpresa" runat="server" />
        <asp:HiddenField ID="HidIdUsuario" runat="server" Value="" />
        <asp:HiddenField ID="HidCodSala" runat="server" />
        <asp:HiddenField ID="HidIdAdminGeneral" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="HidIdPerfil" runat="server" />
        <input id="HidIdJefe" type="hidden" runat="server" />
        <input id="hdnIdLineaNegocio" type="hidden" runat="server" />
        <input id="hdnIdCentroCosto" type="hidden" runat="server" />
        <%--<asp:HiddenField ID="hdEstado" Value="True" runat="server" />--%>
        <!-- Titulo de ventana modal -->
        <div class="modal-header" style="border-bottom: 1px solid #7D7D7D!important;">
            <h3 id="myModalLabel" runat="server">Nuevo Usuario</h3>
        </div>


        <div id="myModalContent" class="modal-body" style="background: #FFFFFF;">
            <fieldset style="padding-bottom: 10px;">
                <legend>Usuario
                </legend>

                <div class="container-fluid modal-field" style="padding: 0px !important">

                    <div class="row col-lg-12">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="txtApellidoPaternoM" class="col-lg-3 texto">Apellido Paterno:</label>
                                <input id="txtApellidoPaternoM" runat="server" class="form-control col-lg-6 cajas" style="margin-top: 5px;" />
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="txtApellidoMaternoM" class="col-lg-3 texto">Apellido Materno:</label>
                                <input id="txtApellidoMaternoM" runat="server" class="form-control col-lg-6 cajas" style="margin-top: 5px;" />
                            </div>
                        </div>
                    </div>

                    <div class="row col-lg-12" style="margin-top: 20px;">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="txtNombresM" class="col-lg-1 texto">Nombres:</label>
                                <input id="txtNombresM" runat="server" class="form-control col-lg-8 cajas" style="margin-left: 15px;" />
                            </div>
                        </div>
                    </div>

                    <div class="row col-lg-12" style="margin-top: 20px;">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="txtDniM" class="col-lg-3 texto">DNI:</label>
                                <input id="txtDniM" runat="server" class="form-control col-lg-6 cajas" size="8" maxlength="8" />
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="txtEmailM" class="col-lg-3 texto">E-mail:</label>
                                <input id="txtEmailM" runat="server" class="form-control col-lg-6 cajas" />
                            </div>
                        </div>
                    </div>

                    <div class="row col-lg-12" style="margin-top: 20px;">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="txtDireccion" class="col-lg-1 texto">Dirección:</label>
                                <input id="txtDireccion" runat="server" class="form-control col-lg-8 cajas" style="margin-left: 15px;" />
                            </div>
                        </div>
                    </div>

                    <div class="row col-lg-12" style="margin-top: 20px;">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="txtTelfMovilM" class="col-lg-3 texto">Telf. Móvil:</label>
                                <input id="txtTelfMovilM" runat="server" class="form-control col-lg-6 cajas" />
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="txtTelfFijoM" class="col-lg-3 texto">Telf. Fijo:</label>
                                <input id="txtTelfFijoM" runat="server" class="form-control col-lg-6 cajas" />
                            </div>
                        </div>
                    </div>

                    <div class="row col-lg-12" style="margin-top: 20px;">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="dllPerfilM" class="col-lg-3 texto">Perfil:</label>
                                <asp:DropDownList ID="dllPerfilM" runat="server" class="form-control col-lg-6 combo"></asp:DropDownList>
                            </div>
                        </div>
                        <div id="cbxSupervisor" class="col-lg-5 hide" runat="server">
                            <div class="form-group">
                                <label for="dllSupervisor" class="col-lg-3 texto">Supervisor</label>
                                <asp:DropDownList ID="dllSupervisor" runat="server" class="form-control combo" Width="190px"></asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div id="cbxLineaCosto"  class="row col-lg-12" style="margin-top: 20px;">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="dllLineaNegocio" class="col-lg-3 texto">Linea de Negocio</label>
                                <asp:DropDownList ID="dllLineaNegocio" runat="server" class="form-control combo" Width="190px"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="dllCentroCosto" class="col-lg-3 texto">Centro de Costo</label>
                                <asp:DropDownList ID="dllCentroCosto" runat="server" class="form-control combo" Width="190px"></asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="row col-lg-12" style="margin-top: 20px;">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="txtUsuarioM" class="col-lg-3 texto">Usuario:</label>
                                <input id="txtUsuarioM" runat="server" class="form-control col-lg-6 cajas" />
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="txtClaveM" class="col-lg-3 texto">Clave:</label>
                                <input id="txtClaveM" runat="server" class="form-control col-lg-6 cajas" />
                            </div>
                        </div>
                    </div>

                </div>
            </fieldset>

            <div class="modal-footer" style="padding: 3px 0px 0px 0px!important;">
                <div id="msg"></div>
                <button id="btnGuardar" type="button" class="btn btn-primary cz-form-content-input-button btn-mg cz-form-box-content-button">Guardar</button>
                <button id="btnCancelar" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button cz-util-right" data-dismiss="modal">Cancelar</button>
            </div>

        </div>
    </form>
</body>
</html>

