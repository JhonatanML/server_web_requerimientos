﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace WEB_REQUERIMIENTOS.Mantenimiento.Usuarios
{
    public partial class MantUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                Int32 id_perfil = usuario.id_perfil;
                Int32 id_admin_general = usuario.id_usuario;

                HidIdAdminGeneral.Value = id_admin_general.ToString();
                HidIdPerfil.Value = id_perfil.ToString();

                CargarComboPerfil(id_perfil);
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        [WebMethod]
        public static void EliminarUsuario(string id_usuario)
        {
            try
            {
                ControladorUsuario.EliminarUsuarioC(id_usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static string AgregarUsuario(string apellido_paterno, string apellido_materno, string nombres, string dni,
                        string email, string telf_movil, string telf_fijo, string direccion, string id_perfil, string id_linea_negocio,
                        string id_centro_coste, string usuario, string clave, string idSupervisor)
        {
            try
            {
                BeanUsuario bean = new BeanUsuario();
                bean.apellido_paterno = apellido_paterno;
                bean.apellido_materno = apellido_materno;
                bean.nombres = nombres;
                bean.dni = dni;
                bean.email = email;
                bean.telf_movil = telf_movil;
                bean.telf_fijo = telf_fijo;
                bean.direccion = direccion;
                bean.id_perfil = Convert.ToInt32(id_perfil);
                bean.idLineaNegocio = id_linea_negocio;
                bean.idCentroCosto = id_centro_coste;
                bean.usuario = usuario;
                bean.clave = clave;
                bean.codigo_supervisor = idSupervisor;
                ControladorUsuario.AgregarUsuarioC(bean);
                return "{\"respuesta\": \"El usuario ha sido registrado correctamente.\"}";
                //return "ok";
            }
            catch (Exception ex)
            {
                Utility.registrarLog("ERROR AGREGAR USUARIO: " + ex.Message);
                return "{\"respuesta\": \"" + ex.Message + "\"}";
                //return "error";
            }
        }

        [WebMethod]
        public static string EditarUsuario(string id_usuario, string apellido_paterno, string apellido_materno, string nombres, 
          string dni, string email, string telf_movil, string telf_fijo, string direccion, string id_perfil, string id_linea_negocio,
          string id_centro_coste, string usuario, string clave, string idSupervisor)
        {
            try
            {
                BeanUsuario bean = new BeanUsuario();
                bean.id_usuario = Convert.ToInt32(id_usuario);
                bean.apellido_paterno = apellido_paterno;
                bean.apellido_materno = apellido_materno;
                bean.nombres = nombres;
                bean.dni = dni;
                bean.email = email;
                bean.telf_movil = telf_movil;
                bean.telf_fijo = telf_fijo;
                bean.direccion = direccion;
                bean.id_perfil = Convert.ToInt32(id_perfil);
                bean.idLineaNegocio = id_linea_negocio;
                bean.idCentroCosto = id_centro_coste;
                bean.usuario = usuario;
                bean.clave = clave;
                bean.codigo_supervisor = idSupervisor;

                ControladorUsuario.EditarUsuarioC(bean);
                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static String ComboLineaNegocioModal(String idPerfil, String idSupervisor)
        {
            try
            {
                List<BeanLineaNegocio> lista = new List<BeanLineaNegocio>();

                lista = ControladorLineaNegocio.ObtenerLineaNegocioC(idPerfil, idSupervisor);

                return Newtonsoft.Json.JsonConvert.SerializeObject(lista);
            }
            catch (Exception ex)
            {
                Utility.registrarLog("ERROR ComboLineaNegocioModal:" + ex.Message);
                return null;
            }
        }

        private void CargarComboPerfil(Int32 id_perfil)
        {
            dllPerfil.DataSource = ControladorPerfil.Combo_PerfilC(id_perfil);
            dllPerfil.DataValueField = "id_perfil";
            dllPerfil.DataTextField = "nom_perfil";
            dllPerfil.DataBind();
            dllPerfil.Items.Insert(0, ".:SELECCIONE:.");
            dllPerfil.Items[0].Value = "-1";
        }
    }
}