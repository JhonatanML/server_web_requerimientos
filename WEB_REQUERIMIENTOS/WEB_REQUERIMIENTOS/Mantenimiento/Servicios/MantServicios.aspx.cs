﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Mantenimiento.Servicios
{
    public partial class MantServicios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        [WebMethod]
        public static void EliminarServicio(string id_servicio)
        {
            try
            {
                ControladorServicio.EliminarServicioC(id_servicio);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static string AgregarServicio(string cod_categoria, string cod_subcategoria, string linea, string codigo_sap, string nom_servicio, string descripcion, string precio)
        {
            try
            {
                ControladorServicio.AgregarServicioC(new BeanServicio()
                {
                    cod_categoria = cod_categoria,
                    cod_subcategoria = cod_subcategoria,
                    linea = linea,
                    codigo_sap = codigo_sap,
                    nom_servicio = nom_servicio,
                    descripcion = descripcion,
                    precio = precio

                });
                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static string EditarServicio(string cod_categoria, string cod_subcategoria, string linea, string codigo_sap, string nom_servicio, string descripcion, string precio, string id_servicio)
        {
            try
            {
                ControladorServicio.EditarServicioC(new BeanServicio()
                {
                    cod_categoria = cod_categoria,
                    cod_subcategoria = cod_subcategoria,
                    linea = linea,
                    codigo_sap = codigo_sap,
                    nom_servicio = nom_servicio,
                    descripcion = descripcion,
                    precio = precio,
                    id_servicio = Convert.ToInt32(id_servicio)

                });
                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

    }
}