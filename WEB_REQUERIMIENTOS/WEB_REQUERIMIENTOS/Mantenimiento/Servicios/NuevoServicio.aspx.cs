﻿using Controlador;
using Modelo.BEANS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Mantenimiento.Servicios
{
    public partial class NuevoServicio : System.Web.UI.Page
    {
        string json = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CargarComboCategoria();

            try
            {

                json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                if (dataJSON == null)
                {
                    return;
                }
                else
                {
                    BeanServicio bean = ControladorServicio.ObtenerServicioC(dataJSON["id_servicio"]);
                    this.myModalLabel.InnerText = "Editar Servicio";
                    this.dllCategoria.SelectedValue = bean.cod_categoria;
                    this.dllSubCategoria.SelectedValue = bean.cod_subcategoria;
                    this.nLinea.Value = bean.cod_servicio;
                    this.nCodigoSap.Value = bean.codigo_sap;
                    this.nNombreServicio.Value = bean.nom_servicio;
                    this.nDescripcion.Value = bean.descripcion;
                    this.nPrecio.Value = bean.precio;

                    this.hidIdServicio.Value = bean.id_servicio.ToString();
                    hdnIdSubCategoria.Value = bean.cod_subcategoria;
                    //this.HidCodSala.Value = bean.cod_sala.ToString();

                    //this.dllNLocal.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        private void CargarComboCategoria()
        {
            dllCategoria.DataSource = ControladorRequerimiento.Combo_CategoriaC();
            dllCategoria.DataValueField = "cod_categoria";
            dllCategoria.DataTextField = "nom_categoria";
            dllCategoria.DataBind();
            dllCategoria.Items.Insert(0, ".:CATEGORIAS:.");
            dllCategoria.Items[0].Value = "-1";
        }

        [WebMethod]
        public static string CargarComboSubCategoria(string cod_categoria)
        {
            try
            {
                List<BeanSubCategoria> lstBean = new List<BeanSubCategoria>();
                lstBean = ControladorRequerimiento.Combo_SubCategoriaC(cod_categoria);

                return Newtonsoft.Json.JsonConvert.SerializeObject(lstBean);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}