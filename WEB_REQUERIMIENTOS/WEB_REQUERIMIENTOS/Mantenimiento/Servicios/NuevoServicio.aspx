﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NuevoServicio.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Mantenimiento.Servicios.NuevoServicio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <script type="text/javascript">
        $(document).ready(function () {
            var urlObSe = 'NuevoServicio.aspx/CargarComboSubCategoria';
            ObtenerSubCategoriasC();

            $('#nPrecio').keyup(function () {
                this.value = (this.value + '').replace(/[^0-9]/g, '');
            });
        });
    </script>
    <style>
        fieldset {
            border: 1.5px solid #1F497D;
            margin-bottom: 10px;
            margin-right: 10px;
        }

            fieldset legend {
                background-color: white;
                border-radius: 5px;
                font-size: 18px;
                margin-left: 15px;
                text-align: left;
                vertical-align: middle;
                width: 15%;
            }

        .cajas {
            height: 20px;
        }

        .texto {
            font-size: 15px;
            margin-top: 5px;
            padding-right: 0px;
            margin-left: 45px;
            margin-right: 25px;
        }

        .combo {
            height: 35px;
            width: 360px;
        }

        #dllPerfil {
            width: 188px;
            font-size: 15px;
        }

        .textarea {
            height: 100px;
        }

        textarea{
            resize: none;
        }
    </style>

    <script>

        function getSala() {
            var strData = new Object();

            strData.cod_categoria = $('#dllCategoria').val();
            strData.cod_subcategoria = $('#dllSubCategoria').val();
            strData.linea = $('#nLinea').val();
            strData.codigo_sap = $('#nCodigoSap').val();
            strData.nom_servicio = $('#nNombreServicio').val();
            strData.descripcion = $('#nDescripcion').val();
            strData.precio = $('#nPrecio').val();
            strData.id_servicio = $('#hidIdServicio').val();

            return strData;
        }

    </script>

    <style>
        .modal-field {
            max-height: calc(100vh - 140px);
            overflow-y: auto;
            overflow-x: hidden;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server" class="form-horizontal" role="form">
        <asp:HiddenField ID="hidIdServicio" Value="1" runat="server" />
        <input id="hdnIdSubCategoria" type="hidden" runat="server"/>
        <%--<asp:HiddenField ID="hdEstado" Value="True" runat="server" />--%>
        <!-- Titulo de ventana modal -->
        <div class="modal-header" style="border-bottom: 1px solid #7D7D7D!important;">
            <h3 id="myModalLabel" runat="server">Nuevo Servicio</h3>
        </div>


        <div id="myModalContent" class="modal-body" style="background: #FFFFFF;">
            <fieldset style="padding-bottom: 10px;">
                <legend>Servicio
                </legend>

                <div class="container-fluid modal-field" style="padding: 0px !important">

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="dllCategoria" class="col-lg-2 texto">Categoria:</label>
                        <asp:DropDownList ID="dllCategoria" runat="server" class="form-control col-lg-3 combo"></asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-12 divSubCategoria" style="margin-top: 10px;">
                        <label for="dllSubcategoria" class="col-lg-2 texto">Subcategoria:</label>
                        <asp:DropDownList ID="dllSubCategoria" runat="server" class="form-control col-lg-3 combo"></asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 20px;">
                        <label for="nLinea" class="col-lg-2 texto">Línea:</label>
                        <input id="nLinea" runat="server" class="form-control col-lg-1 cajas" />
                        <label for="nCodigoSap" class="col-lg-2 texto">Codigo SAP:</label>
                        <input id="nCodigoSap" runat="server" class="form-control col-lg-1 cajas" />
                    </div>

                    <%--<div class="form-group col-lg-12" style="margin-top:20px;">
                        <label for="nCodigoSap" class="col-lg-2 texto">Codigo SAP:</label>
                        <input id="nCodigoSap" runat="server" class="form-control col-lg-3 cajas" />
                    </div>--%>

                    <div class="form-group col-lg-12" style="margin-top: 20px;">
                        <label for="nNombreServicio" class="col-lg-2 texto">Servicio:</label>
                        <input id="nNombreServicio" runat="server" class="form-control col-lg-7 cajas" />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="nDescripcion" class="col-lg-2 texto">Descripción:</label>
                        <textarea id="nDescripcion" runat="server" class="form-control col-lg-7 textarea" style="height:100px;"/>
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 20px;">
                        <label for="nPrecio" class="col-lg-2 texto">Precio:</label>
                        <input id="nPrecio" runat="server" class="form-control col-lg-2 cajas" />
                    </div>
                </div>
            </fieldset>

            <div class="modal-footer" style="padding: 5px 0px 0px 0px!important;">
                <div id="msg"></div>
                <button id="btnGuardar" type="button" class="btn btn-primary cz-form-content-input-button btn-mg cz-form-box-content-button">Guardar</button>
                <button id="btnCancelar" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button cz-util-right" data-dismiss="modal">Cancelar</button>
            </div>

        </div>
    </form>
</body>
</html>

