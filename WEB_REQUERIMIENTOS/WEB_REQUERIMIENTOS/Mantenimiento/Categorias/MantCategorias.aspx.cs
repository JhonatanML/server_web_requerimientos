﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Mantenimiento.Categorias
{
    public partial class MantCategorias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }
        
        [WebMethod]
        public static void EliminarCategoria(string id_categoria)
        {
            try
            {
                ControladorCategoria.EliminarCategoriaC(id_categoria);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static string AgregarCategoria(string nom_categoria, string linea)
        {
            try
            {
                ControladorCategoria.AgregarCategoriaC(new BeanCategoria()
                {                    
                    nom_categoria = nom_categoria,
                    linea = linea

                });
                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static string EditarCategoria(string nom_categoria, string linea, string id_categoria)
        {
            try
            {
                ControladorCategoria.EditarCategoriaC(new BeanCategoria()
                {
                    nom_categoria = nom_categoria,
                    linea = linea,

                    id_categoria = Convert.ToInt32(id_categoria),

                });
                return "OK";
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}