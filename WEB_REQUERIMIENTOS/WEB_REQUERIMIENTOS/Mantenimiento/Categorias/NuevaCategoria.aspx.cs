﻿using Controlador;
using Modelo.BEANS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Mantenimiento.Categorias
{
    public partial class NuevaCategoria : System.Web.UI.Page
    {
        string json = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                if (dataJSON == null)
                {
                    return;
                }
                else
                {
                    BeanCategoria bean = ControladorCategoria.ObtenerCategoriaC(dataJSON["id_categoria"]);

                    this.myModalLabel.InnerText = "Editar Categoria";
                    this.txtCategoriaM.Value = bean.nom_categoria;
                    this.txtLineaM.Value = bean.linea;

                    this.HidIdCategoria.Value = bean.id_categoria.ToString();
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }
    }
}