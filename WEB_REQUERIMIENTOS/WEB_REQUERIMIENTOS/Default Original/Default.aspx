﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%=ConfigurationSettings.AppSettings["APP_NAME"] %> | SETERZA S.A.C</title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10" />
    
    <link rel="shortcut icon" href="images/icons/logoIncidencia.ico"/>

    <script src="js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="js/cz_main.js" type="text/javascript"></script>
    <script src="js/jsmodule.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    

    <link href="css/normalize.css" rel="stylesheet" />
    <link href="css/FormsEntel.css" rel="stylesheet" />
    <link href="css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <link href="css/Forms.css" type="text/css" rel="stylesheet"/>
    <link href="css/styleBootStrap.css" rel="stylesheet"/>


    <script>

    </script>
</head>
<body>
    <form id="form1" runat="server" class="cz-main" autocomplete="off">
        
        <div class="cz-submain cz-login-submain">
            <div id="cz-box-header" class="cz-login-box-header">
                <%--<div id="img_logo_top"></div>--%>
            </div>
            <div id="cz-box-body" class="cz-login-box-body">
                <div class="cz-box-part2">
                    <div id="cz-login-text" class="cz-util-center-box">
                        <p>SISTEMA WEB PARA <br/>
                           REQUERIMIENTO DE SERVICIOS
                        </p>
                    </div>
                </div>
                <div class="cz-box-part2">
                    <div id="cz-login-form" class="cz-util-center-box">
                        <%--<div class="cz-login-lat-left">
                            <div class="img"></div>
                        </div>--%>
                        
                        <div class="cz-login-lat-center">
                            <asp:Literal ID="txtmsg" runat="server"></asp:Literal>
                            <div id="cz-login-head">
                                <asp:Image id="Image1" ImageUrl="~/imagery/login/icons/users.png" runat="server" />
                                <div id="cz-login-title">Acceso</div>
                            </div>
                            <div id="cz-login-body">
                                <div id="cz-login-user">
                                    <label>Usuario</label>
                                    <div id="cz-login-input-user" class="cz-login-input">
                                        <asp:TextBox ID="txtUsuario" placeholder="Usuario" runat="server" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="cz-login-pass">
                                    <label>Contraseña</label>
                                    <div id="cz-login-input-pass" class="cz-login-input">
                                        <asp:TextBox ID="txtClave" placeholder="Contraseña" TextMode="Password" runat="server" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div id="cz-login-footer">
                                <asp:Button ID="btnIngresar" class="cz-util-slow" runat="server" Text="Ingresar" onclick="btnIngresar_Click"/>
                            </div>
                        </div>
                        <div class="cz-login-lat-right">
                            <%--<div class="img"></div>--%>
                        </div>
                    </div>
                </div>
                <div id="divMyModal" class="modal fade">
            <div class="modal-dialog" id="divContenidoModal"></div>
        </div>
            </div>
            <div id="cz-box-footer" class="cz-login-box-footer cz-util-center-text">
                <p>SIEDEVS Developers - Derechos reservados. <%=ConfigurationSettings.AppSettings["APP_VERSION"] %></p>
            </div>
        </div>

    </form>
    <%--<input type="hidden" id="hdnSesion" name="hdnSesion" value="0"  runat="server" />--%>

</body>
</html>
