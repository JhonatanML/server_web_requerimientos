﻿using Controlador;
using Modelo.BEANS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Requerimientos.Visualizar
{
    public partial class Grid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                Int32 id_usuario = usuario.id_usuario;
                Int32 id_perfil = usuario.id_perfil;

                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                List<BeanRequerimiento> lstBean = new List<BeanRequerimiento>();

                string visualizar = dataJSON["visualizar"].ToString();
                string id_requerimiento = dataJSON["id_requerimiento"].ToString();
                string fecha_requerimiento = dataJSON["fecha_requerimiento"].ToString();
                string cod_estado_requerimiento = dataJSON["cod_estado_requerimiento"].ToString();
                string page = dataJSON["page"].ToString();
                string rows = dataJSON["rows"].ToString();


                BeanPaginate paginate = ControladorRequerimiento.ListarRequerimientosC(visualizar, id_requerimiento, fecha_requerimiento, cod_estado_requerimiento, id_usuario, id_perfil, page, rows);
                Int32 cont = paginate.totalRegistros;
                contador.Value = cont.ToString();

                if ((Int32.Parse(page) > 0) && (Int32.Parse(page) <= paginate.totalPages))
                {
                    this.lbTpagina.Text = paginate.totalPages.ToString();
                    this.lbPagina.Text = page;
                    this.spTotalReg.InnerText = paginate.totalRegistros.ToString();
                    if (Int32.Parse(page) == 1)
                    { this.lbPaginaAnterior.CssClass = "pagina-disabled"; }

                    if (Int32.Parse(page) == paginate.totalPages)
                    { this.lbPaginaSiguiente.CssClass = "pagina-disabled"; }

                    lstBean = paginate.lstResultadoBeanRequerimiento;
                    grDocumento.DataSource = lstBean;
                    grDocumento.DataBind();

                }
                else
                {
                    this.divGridView.InnerHtml = "<div class='gridNoData'><img src='../../images/icons/grid/ico_grid_nodata.png' /><p>No se encontraron datos para mostrar</p></div>";
                    this.divGridViewPagintator.Visible = false;
                }
            }
            catch (Exception ex)
            {
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", "parent.document.location.href = '../../default.aspx?acc=EXT';", true);
            }
        }
    }
}