﻿using Controlador;
using Modelo.BEANS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Requerimientos.Visualizar
{
    public partial class DetallesRequirimiento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];

                Int32 cod_requerimiento = Convert.ToInt32(dataJSON["cod_requerimiento"]);
                Int32 cod_estado_requerimiento = Convert.ToInt32(dataJSON["cod_estado_requerimiento"]);

                if (dataJSON == null)
                {
                    return;
                }
                else
                {
                    BeanDetalleRequerimiento detalles = ControladorRequerimiento.ObtenerComentarioC(cod_requerimiento, cod_estado_requerimiento);

                    this.FechaObservacion.Value = detalles.fecha_comentario;
                    this.CodigoRequerimiento.Value = detalles.cod_requerimiento.ToString();
                    this.EstadoRequerimiento.Value = detalles.nom_estado_requerimiento;
                    this.Observacion.Value = detalles.comentario;

                    hdnCodRequerimiento.Value = detalles.cod_requerimiento;
                    switch (detalles.nom_estado_requerimiento)
                    {
                        case "PENDIENTE":
                            divDescargaFoto.Visible = false;
                            btnDescargar.Visible = false;
                            break;
                        case "ASIGNADO":
                            divDescargaFoto.Visible = false;
                            btnDescargar.Visible = false;
                            break;
                        case "APROBADO":
                            divDescargaFoto.Visible = false;
                            btnDescargar.Visible = false;
                            break;
                        case "CERRADO":
                            divDescargaFoto.Visible = true;
                            btnDescargar.Visible = true;
                            cargaFoto1.ImageUrl = "Descarga.ashx?codRequerimiento=" + detalles.cod_requerimiento + "&opcion=" + 1;
                            cargaFoto2.ImageUrl = "Descarga.ashx?codRequerimiento=" + detalles.cod_requerimiento + "&opcion=" + 2;
                            cargaFoto3.ImageUrl = "Descarga.ashx?codRequerimiento=" + detalles.cod_requerimiento + "&opcion=" + 3;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }
    }
}