﻿using Controlador;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Utilidad;

namespace WEB_REQUERIMIENTOS.Requerimientos.Visualizar
{
    /// <summary>
    /// Summary description for DescargaZipFotos
    /// </summary>
    public class DescargaZipFotos : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string json = new System.IO.StreamReader(context.Request.InputStream).ReadToEnd();
            Dictionary<string, string> JSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            String codigoRequerimiento = context.Request.QueryString["codRequerimiento"];

            String Folder = ControladorRequerimiento.obtenerRutaFoto(codigoRequerimiento, "1");
            String[] corteFolder = Folder.Split('_');
            String nombreZip = "\\" + codigoRequerimiento + "_" + corteFolder[2].Split('.')[0] + "\\";
            String nombreZip1 = codigoRequerimiento + "_" + corteFolder[2].Split('.')[0];

            String fileLocation = HttpContext.Current.Server.MapPath("~") + ConfigurationManager.AppSettings["rutaFoto"] + nombreZip;

            DownloadFileUtils.DesargarFotos(fileLocation, nombreZip1);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}