﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetallesRequeremiento.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Requerimientos.Visualizar.DetallesRequirimiento" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <script src="../../js/Siedevs.js"></script>

    <script>
        $(document).ready(function () {
            DescargarFoto();
        });
    </script>
    <style>
        fieldset {
            border: 1.5px solid #1F497D;
            margin-bottom: 10px;
            margin-right: 10px;
        }

            fieldset legend {
                background-color: white;
                border-radius: 5px;
                font-size: 18px;
                margin-left: 15px;
                text-align: left;
                vertical-align: middle;
                width: 15%;
            }

        .cajas {
            height: 20px;
        }

        .texto {
            font-size: 15px;
            margin-top: 5px;
            padding-right: 0px;
            margin-left: 45px;
            margin-right: 25px;
        }

        .combo {
            height: 30px;
            width: 160px;
        }

        .textarea {
            height: 100px;
        }

        #dllPerfil {
            width: 188px;
            font-size: 15px;
        }

        .calendar {
            background-position: right center;
            background-image: url(../../images/calendar.png);
            background-repeat: no-repeat;
            cursor: pointer;
        }
    </style>

    <style>
        .modal-field {
            max-height: calc(100vh - 140px);
            overflow-y: auto;
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" class="form-horizontal" role="form">
        <!-- Titulo de ventana modal -->
        <div class="modal-header" style="border-bottom: 1px solid #7D7D7D!important;">
            <h3 id="myModalLabel" runat="server">Requerimiento de Servicio</h3>
        </div>
        
        <input id="hdnCodRequerimiento" type="hidden" runat="server" />

        <div id="myModalContent" class="modal-body" style="background: #FFFFFF; padding-top: 0px;">
            <fieldset style="padding-bottom: 0px;">
                <legend style="width: 30%;">Detalles del Requerimiento</legend>

                <div class="container-fluid modal-field" style="padding: 0px !important">

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="FechaObservacion" class="col-lg-2 texto">Fecha Observación:</label>
                        <input id="FechaObservacion" runat="server" class="form-control col-lg-7 cajas" disabled="disabled" />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="CodigoRequerimiento" class="col-lg-2 texto">Codigo Requerimiento:</label>
                        <input id="CodigoRequerimiento" runat="server" class="form-control col-lg-7 cajas" disabled="disabled" />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="EstadoRequerimiento" class="col-lg-2 texto">Estado del requerimmiento:</label>
                        <input id="EstadoRequerimiento" runat="server" class="form-control col-lg-7 cajas" disabled="disabled" />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px; margin-bottom: 5px;">
                        <label for="Observacion" class="col-lg-2 texto">Observación del proveedor:</label>
                        <textarea id="Observacion" runat="server" class="form-control col-lg-7 textarea" disabled="disabled" />
                    </div>

                    <div id="divDescargaFoto" class="form-group col-lg-12" style="margin-top: 10px;" runat="server">
                        <label for="btnDescargar" class="col-lg-2 texto">Foto de Cierre: </label>
                        <asp:Image ID="cargaFoto1" runat="server" Width="150px" Height="120px"/>
                        <asp:Image ID="cargaFoto2" runat="server" Width="150px" Height="120px"/>
                        <asp:Image ID="cargaFoto3" runat="server" Width="150px" Height="120px"/>
                    </div>
                    <button id="btnDescargar" type="button" class="btn btn-default cz-form-content-input-button cz-util-right" style="margin-right:55px; margin-bottom: 5px;" runat="server">Descargar</button>
                </div>

            </fieldset>

            <div class="modal-footer" style="padding: 5px 0px 0px 0px!important;">
                <div id="msg"></div>
                <button id="btnCancelar" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button cz-util-right" data-dismiss="modal">Cerrar</button>
            </div>

        </div>
    </form>
</body>
</html>
