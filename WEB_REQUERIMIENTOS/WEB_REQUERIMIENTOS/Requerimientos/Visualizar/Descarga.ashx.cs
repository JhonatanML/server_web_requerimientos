﻿using Controlador;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace WEB_REQUERIMIENTOS.Requerimientos.Visualizar
{
    /// <summary>
    /// Descripción breve de Descarga
    /// </summary>
    public class Descarga : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string json = new System.IO.StreamReader(context.Request.InputStream).ReadToEnd();
            Dictionary<string, string> JSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            String codigoRequerimiento = context.Request.QueryString["codRequerimiento"];
            String opcion = context.Request.QueryString["opcion"];
            //String codigoRequerimiento = Convert.ToString(JSON["codRequerimiento"]);
            String filename = "";
            switch (opcion)
            {
                case "1":
                    filename = ControladorRequerimiento.obtenerRutaFoto(codigoRequerimiento, opcion);

                    break;
                case "2":
                    filename = ControladorRequerimiento.obtenerRutaFoto(codigoRequerimiento, opcion);
                    break;
                case "3":
                    filename = ControladorRequerimiento.obtenerRutaFoto(codigoRequerimiento, opcion);
                    break;
            }

            if (filename.Length > 0)
            {
                String[] nombreCarpeta = filename.Split('_');
                String folder = "\\" + nombreCarpeta[0] + "_" + nombreCarpeta[2].Split('.')[0] + "\\";
                String fileLocation = HttpContext.Current.Server.MapPath("~") + ConfigurationManager.AppSettings["rutaFoto"] + folder;
                String foto = fileLocation + filename;
                byte[] readBuffer = System.IO.File.ReadAllBytes(foto);
                //File file = fileLocation + 

                context.Response.AppendHeader("Content-Disposition", "attachment;filename=" + filename);
                context.Response.ContentType = "image/jpg";
                context.Response.OutputStream.Write(readBuffer, 0, readBuffer.Length);
                context.Response.Flush();
                context.Response.End();
                //context.Response.BinaryWrite(readBuffer);

            }
            else
            {
                String fileLocationError = HttpContext.Current.Server.MapPath("~") + ConfigurationManager.AppSettings["rutaErrorFoto"];
                byte[] readBuffer = System.IO.File.ReadAllBytes(fileLocationError);
                //File file = fileLocation + 

                context.Response.AppendHeader("Content-Disposition", "attachment;filename=ErrorFoto");
                context.Response.ContentType = "image/jpg";
                context.Response.OutputStream.Write(readBuffer, 0, readBuffer.Length);
                context.Response.Flush();
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}