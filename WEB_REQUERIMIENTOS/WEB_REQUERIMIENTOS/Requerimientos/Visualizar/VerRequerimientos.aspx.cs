﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace WEB_REQUERIMIENTOS.Requerimientos.Visualizar
{
    public partial class VerRequerimientos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime fecha = DateTime.Now;

                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                var id_perfil = (usuario.id_perfil).ToString();
                HidIdPerfil.Value = id_perfil;
                CargarComboEstadoRequerimiento(id_perfil);

                FechaGenerada.Value = getFechaActual();
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        public static string getFechaActual()
        {
            string dia = DateTime.Now.Day.ToString().PadLeft(2, '0');
            string mes = DateTime.Now.Month.ToString().PadLeft(2, '0');
            string anio = DateTime.Now.Year.ToString().PadLeft(4, '0');

            return anio + "/" + mes + "/" + dia;
        }

        private void CargarComboEstadoRequerimiento(string id_perfil)
        {
            dllEstado.DataSource = ControladorRequerimiento.Combo_EstadoRequerimientoC(id_perfil);
            dllEstado.DataValueField = "cod_estado_requerimiento";
            dllEstado.DataTextField = "nom_estado_requerimiento";
            dllEstado.DataBind();
            dllEstado.Items.Insert(0, ".:TODOS:.");
            dllEstado.Items[0].Value = "-1";
        }

        [WebMethod]
        public static String EnviarComentario(string comentario, string cod_requerimiento, string idUsuario)
        {
            try
            {
                Int32 respuesta = ControladorRequerimiento.EnviarComenarioC(comentario, cod_requerimiento, idUsuario);

                return "{\"respuesta\": \"Su comentario se ha enviado correctamente.\"}";

            }
            catch (Exception ex)
            {
                Utility.registrarLog("ERROR COMENTARIO: " + ex.Message);
                return "{\"respuesta\": \"" + ex.Message + "\"}";
            }
        }
    }
}