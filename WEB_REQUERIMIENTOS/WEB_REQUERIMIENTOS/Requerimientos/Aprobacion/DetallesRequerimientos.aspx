﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetallesRequerimientos.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Requerimientos.Aprobacion.DetallesRequerimientos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <script src="../../js/Siedevs.js"></script>
    <style>
        fieldset {
            border: 1.5px solid #1F497D;
            margin-bottom: 10px;
            margin-right: 10px;
        }

            fieldset legend {
                background-color: white;
                border-radius: 5px;
                font-size: 18px;
                margin-left: 15px;
                text-align: left;
                vertical-align: middle;
                width: 15%;
            }

        .cajas {
            height: 20px;
        }

        .texto {
            font-size: 15px;
            margin-top: 5px;
            padding-right: 0px;
            margin-left: 45px;
            margin-right: 25px;
        }

        .combo {
            height: 30px;
            width: 160px;
        }

        .textarea {
            height: 100px;
        }

        #dllPerfil {
            width: 188px;
            font-size: 15px;
        }

        .calendar {
            background-position: right center;
            background-image: url(../../images/calendar.png);
            background-repeat: no-repeat;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" class="form-horizontal" role="form">
        <!-- Titulo de ventana modal -->
        <div class="modal-header" style="border-bottom: 1px solid #7D7D7D!important;">
            <h3 id="myModalLabel" runat="server">Requerimiento de Servicio</h3>
        </div>


        <div id="myModalContent" class="modal-body" style="background: #FFFFFF;">
            <fieldset style="padding-bottom: 25px;">
                <legend>Detalles del Requerimiento</legend>

                <div class="container-fluid" style="padding: 0px !important">

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="NombreCompleto" class="col-lg-2 texto">Usuario:</label>
                        <input id="NombreCompleto" runat="server" class="form-control col-lg-7 cajas" readonly />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="FechaRequerimiento" class="col-lg-2 texto">Fecha de reserva:</label>
                        <input id="FechaRequerimiento" runat="server" class="form-control col-lg-7 cajas" readonly />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="FechaCierre" class="col-lg-2 texto">Fecha de cierre:</label>
                        <input id="FechaCierre" runat="server" class="form-control col-lg-7 cajas" readonly />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="Descripcion" class="col-lg-2 texto">Descripción:</label>
                        <textarea id="Descripcion" runat="server" class="form-control col-lg-7 textarea" readonly />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="Detalles" class="col-lg-2 texto">Detalle:</label>
                        <textarea id="Detalles" runat="server" class="form-control col-lg-7 textarea" readonly />
                    </div>
                </div>

            </fieldset>

            <div class="modal-footer" style="padding: 5px 0px 0px 0px!important;">
                <div id="msg"></div>
                <button id="btnCerrar" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button cz-util-right" data-dismiss="modal" runat="server">Cerrar</button>
            </div>

        </div>
    </form>
</body>
</html>
