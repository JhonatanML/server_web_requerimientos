﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModalComentarios.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Requerimientos.Aprobacion.ModalComentarios" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="../../js/Siedevs.js"></script>
    <style>
        fieldset {
            border: 1.5px solid #1F497D;
            margin-bottom: 10px;
            margin-right: 10px;
        }

            fieldset legend {
                background-color: white;
                border-radius: 5px;
                font-size: 18px;
                margin-left: 15px;
                text-align: left;
                vertical-align: middle;
                width: 15%;
            }

        .cajas {
            height: 20px;
        }

        .texto {
            font-size: 15px;
            margin-top: 5px;
            /*padding-right: 0px;*/
            /*margin-left: 45px;
            margin-right: 25px;*/
        }

        .combo {
            height: 35px;
            width: 190px;
        }

        #dllPerfil {
            width: 188px;
            font-size: 15px;
        }

        .textarea {
            height: 100px;
        }

        .modal-field {
            max-height: calc(100vh - 140px);
            overflow-y: auto;
            overflow-x: hidden;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server" class="form-horizontal" role="form">
        <asp:HiddenField ID="HidLocalEmpresa" runat="server" />
        <asp:HiddenField ID="HidIdUsuario" runat="server" Value="" />
        <asp:HiddenField ID="HidPerfil" runat="server" Value="" />
        <asp:HiddenField ID="HidCodSala" runat="server" />
        <asp:HiddenField ID="HidIdAdminGeneral" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="HidIdPerfil" runat="server" />
        <input id="HidIdJefe" type="hidden" runat="server" />
        <input id="hdnIdLineaNegocio" type="hidden" runat="server" />
        <input id="hdnIdCentroCosto" type="hidden" runat="server" />
        <%--<asp:HiddenField ID="hdEstado" Value="True" runat="server" />--%>
        <!-- Titulo de ventana modal -->
        <div class="modal-header" style="border-bottom: 1px solid #7D7D7D!important;">
            <h3 id="myModalLabel" runat="server">Lista de Comentarios</h3>
        </div>


        <div id="myModalContent" class="modal-body" style="background: #FFFFFF;">
            <fieldset style="padding-bottom: 10px;">
                <legend>Comentarios
                </legend>

                <div class="container-fluid modal-field" style="padding: 0px !important">

                    <div class="row col-lg-12">
                        <div class="col-lg-12">
                            <div class="form-group" id="cbxComentarios">
                                <%--<label for="txtaComentario1" class="col-lg-2 texto">Comentario:</label>
                                <textarea id="txtaComentario1" runat="server" class="form-control col-lg-7 textarea" />--%>
                            </div>
                        </div>
                    </div>                    
                    
                    <div class="row col-lg-12" style="margin-top: 20px;">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="txtaComentario" class="col-lg-2 texto">Comentario:</label>
                                <textarea id="txtaComentario" runat="server" class="form-control col-lg-7 textarea" />
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="modal-footer" style="padding: 3px 0px 0px 0px!important;">
                <div id="msg"></div>
                <button id="btnCancelar" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button cz-util-right" data-dismiss="modal">Cancelar</button>
                <button id="btnGuardar" type="button" class="btn btn-primary cz-form-content-input-button btn-mg cz-form-box-content-button">Enviar</button>
            </div>

        </div>
        <script>
            $('.modal-field').scrollTop($('.modal-field')[0].scrollHeight);
        </script>
    </form>
</body>
</html>
