﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AprobarRequerimiento.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Requerimientos.Aprobacion.AprobarRequerimiento" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%--<script src="../../js/Siedevs.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            var urlObS = 'AprobarRequerimiento.aspx/CargarComboSubCategoria';
            ObtenerSubCategorias();
            //ObtenerServicios();
            GuardarRequerimiento();
            AgregarServiciosAdicionales();
            AgregarTrabajadores();

            $("#miniModal").hide();
            $("#miniServicio").hide();

            var fecha = moment().format('YYYY/MM/DD');
            $('#fechaAsignacion').val(fecha);

            $('#fechaAsignacion').datetimepicker({
                format: 'Y/m/d H:i',
                minDate: fecha,
                lang: 'es',
                closeOnTimeSelect: true,
                allowTimes:
                   [
                        '00:00', '00:30',
                        '01:00', '01:30',
                        '02:00', '02:30',
                        '03:00', '03:30',
                        '04:00', '04:30',
                        '05:00', '05:30',
                        '06:00', '06:30',
                        '07:00', '07:30',
                        '08:00', '08:30',
                        '09:00', '09:30',
                        '10:00', '10:30',
                        '11:00', '11:30',
                        '12:00', '12:30',
                        '13:00', '13:30',
                        '14:00', '14:30',
                        '15:00', '15:30',
                        '16:00', '16:30',
                        '17:00', '17:30',
                        '18:00', '18:30',
                        '19:00', '19:30',
                        '20:00', '20:30',
                        '21:00', '21:30',
                        '22:00', '22:30',
                        '23:00', '23:30'
                   ]

            });

            //$('#horaAsignacion').datetimepicker({
            //    format: 'H:i',
            //    lang: 'es',
            //    allowTimes:
            //       [
            //            '00:00', '00:30',
            //            '01:00', '01:30',
            //            '02:00', '02:30',
            //            '03:00', '03:30',
            //            '04:00', '04:30',
            //            '05:00', '05:30',
            //            '06:00', '06:30',
            //            '07:00', '07:30',
            //            '08:00', '08:30',
            //            '09:00', '09:30',
            //            '10:00', '10:30',
            //            '11:00', '11:30',
            //            '12:00', '12:30',
            //            '13:00', '13:30',
            //            '14:00', '14:30',
            //            '15:00', '15:30',
            //            '16:00', '16:30',
            //            '17:00', '17:30',
            //            '18:00', '18:30',
            //            '19:00', '19:30',
            //            '20:00', '20:30',
            //            '21:00', '21:30',
            //            '22:00', '22:30',
            //            '23:00', '23:30'
            //       ],
            //    datepicker: false
            //});
        });

        $('#btnCerrarMiniModal').click(function () {
            $('#miniModal').hide();
        });

        $('#btnCerrarMiniServicio').click(function () {
            $('#miniServicio').hide();
        });

        $('#btnAsignarPersonal').click(function () {
        });

        $('#miniServicio').draggable({
            handle: ".modal-header"
        });

        $('#miniModal').draggable({
            handle: ".modal-header"
        });

        function getUsuario() {
            var strData = new Object();
            strData.codigoRequerimiento = $('#hdnCodigoRequerimiento').val();
            strData.servicioCliente = $('#ServicioClienteT').val(); //ServicioClienteT
            strData.estado = $('#NombreEstado').val(); //NombreEstado
            strData.categoria = $('#dllCategoria').val(); //dllCategoria
            strData.subCategoria = $('#dllSubCategoria').val(); //dllSubCategoria
            strData.servicio = $('#dllServicio').val(); //dllServicio
            strData.detalle = $('#Detalles').val(); //Detalles
            strData.observacion = $('#Observacion').val(); //Observacion
            strData.prioridad = $('#hdnPrioridad').val(); //Prioridad
            strData.codigoUsuario = $('#hdnUsuarioReq').val(); //Codigo Usuario del requerimiento
            strData.codigoUsuarioSesion = $('#hdnUsuarioSesion').val();
            return strData;
        }


    </script>

    <style>
        fieldset {
            border: 1.5px solid #1F497D;
            margin-bottom: 10px;
            margin-right: 10px;
        }

            fieldset legend {
                background-color: white;
                border-radius: 5px;
                font-size: 18px;
                margin-left: 15px;
                text-align: left;
                vertical-align: middle;
                width: 15%;
            }

        .cajas {
            height: 20px;
        }

        .texto {
            font-size: 15px;
            margin-top: 5px;
            padding-right: 0px;
            margin-left: 45px;
            margin-right: 25px;
        }

        .combo {
            height: 30px;
            width: 160px;
        }

        .textarea {
            height: 100px;
        }

        #dllPerfil {
            width: 188px;
            font-size: 15px;
        }

        .calendar {
            background-position: right center;
            background-image: url(../../images/calendar.png);
            background-repeat: no-repeat;
            cursor: pointer;
        }

        textarea {
            resize: none;
        }
    </style>

    <style>
        .modal-field {
            max-height: calc(100vh - 140px);
            overflow-y: auto;
            overflow-x: hidden;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" class="form-horizontal" role="form">
        <asp:HiddenField ID="IDCategoria" Value="1" runat="server" />
        <asp:HiddenField ID="IDSubCategoria" Value="1" runat="server" />
        <asp:HiddenField ID="IDServicio" Value="1" runat="server" />
        <input id="hdnCodigoRequerimiento" type="hidden" value="" runat="server" />
        <input id="hdnUsuario" type="hidden" value="" runat="server" />
        <input id="hdnCodigoServicioActual" type="hidden" runat="server" />
        <input id="hdnPrioridad" type="hidden" runat="server" />
        <input id="hdnUsuarioReq" type="hidden" runat="server" />
        <input id="hdnUsuarioSesion" type="hidden" runat="server" />
        <!-- Titulo de ventana modal -->
        <div class="modal-header" style="border-bottom: 1px solid #7D7D7D!important;">
            <h3 id="myModalLabel" runat="server">Requerimiento de Servicio</h3>
        </div>


        <div id="myModalContent" class="modal-body" style="background: #FFFFFF; overflow: auto;">
            <fieldset style="padding-bottom: 0px;">
                <legend style="width: 220px;">Detalles del Requerimiento</legend>

                <div class="container-fluid modal-field" style="padding: 0px !important">

                    <div class="form-group col-lg-12" style="margin-top: 0px;">
                        <label for="ServicioClienteT" class="col-lg-2 texto">Servicio Requerido:</label>
                        <textarea id="ServicioClienteT" runat="server" class="form-control col-lg-7 cajas textarea" disabled="disabled" />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="NombreEstado" class="col-lg-2 texto">Estado:</label>
                        <input id="NombreEstado" runat="server" class="form-control col-lg-1 cajas" disabled="disabled" style="font-size: 12px;" />

                        <div id="divFechaAsignacion" runat="server">
                            <label for="fechaAsignacion" class="col-lg-2 texto" style="margin-left: 8px;">Fecha de Ejecución: </label>
                            <input id="fechaAsignacion" runat="server" class="form-control col-lg-2 cajas calendar" />
                            <%--<label for="horaAsignacion" class="col-lg-1 texto" style="margin-left: 8px;">Hora de Ejecución: </label>
                            <input id="horaAsignacion" runat="server" class="form-control col-lg-1 cajas calendar" />--%>
                            <%--<label for="horaAsignacion" class="col-lg-1 texto" style="margin-left: 8px;">Hora de Ejecución: </label>
                            <input id="horaAsignacion" runat="server" class="form-control col-lg-1 cajas calendar" />--%>
                        </div>

                        <div id="divFecAsignacion" runat="server">
                            <label for="fecAsignacion" class="col-lg-1 texto" style="margin-left: 8px;">Fecha de Ejecución: </label>
                            <input id="fecAsignacion" runat="server" class="form-control col-lg-1 cajas" />
                            <label for="horaAsignacion" class="col-lg-1 texto" style="margin-left: 8px;">Hora de Ejecución: </label>
                            <input id="horaAsignacion" runat="server" class="form-control col-lg-1 cajas" />
                        </div>

                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="dllCategoria" class="col-lg-2 texto">Categorias: </label>
                        <asp:DropDownList ID="dllCategoria" runat="server" class="form-control combo" Width="475px"></asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-12 divSubCategoria" style="margin-top: 10px;">
                        <label for="dllSubCategoria" class="col-lg-2 texto">Subcategorias: </label>
                        <asp:DropDownList ID="dllSubCategoria" runat="server" class="form-control combo" Width="475px"></asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="dllServicio" class="col-lg-2 texto">Servicios: </label>
                        <asp:DropDownList ID="dllServicio" runat="server" class="form-control combo" Width="475px"></asp:DropDownList>
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px;">
                        <label for="Detalles" class="col-lg-2 texto">Detalle:</label>
                        <textarea id="Detalles" runat="server" class="form-control col-lg-7 textarea" disabled="disabled" />
                    </div>

                    <div class="form-group col-lg-12" style="margin-top: 10px; margin-bottom: 7px;">
                        <label for="Observacion" class="col-lg-2 texto">Observación:</label>
                        <textarea id="Observacion" runat="server" class="form-control col-lg-7 textarea" />
                    </div>

                    <div id="divSubirFoto" class="form-group col-lg-6" runat="server" style="margin-left: 35px;">
                        <label for="btnSubirFotoI" class="col-lg-2 texto" style="margin-left: 10px;">Subir Foto:</label>
                        <input id="btnSubirFotoI" type="file" style="margin-top: 5px; margin-left: 79px; margin-bottom: 20px;" multiple="multiple" />
                    </div>
                    <label id="mensajeFoto" runat="server" style="font-size: 11px; font-weight: 600; color: red">* Máxima cantidad de fotos: 3</label>

                    <button style="margin-left: 8px;" id="btnAgregar" data-toggle="modal" href="#miniServicio" type="button" class="btn btn-default cz-form-content-input-button btn-mg" runat="server" data-backdrop="" data-keyboard="false">
                        Agregar Servicios
                    </button>

                    <button style="margin-left: 74.5%; margin-bottom: 10px;" id="btnAsignarPersonal" data-toggle="modal" href="#miniModal" type="button" class="btn btn-default cz-form-content-input-button btn-mg" runat="server" data-backdrop="" data-keyboard="false">
                        Agregar Personal
                    </button>

                    <div id="miniModal" class="modal fade modal-dialog" role="dialog" style="z-index: 1600;" data-modal-parent="#divMyModal" runat="server">
                        <div class="modal-dialog">
                            <div class="modal-header" style="border-bottom: 1px solid #7D7D7D!important;">
                                <button id="btnCerrarMiniModal" type="button" class="close" aria-hidden="true">×</button>
                                <h3 id="miniModalTitle" runat="server">Personal</h3>
                            </div>
                            <!-- Modal content-->
                            <div class="modal-content modal-body">
                                <div class="modal-body">
                                    <div class="col-md-12 container-fluid" style="padding-left: 0px; padding-right: 0px;">
                                        <label for="txtTrabajador" class="col-md-2" style="padding-left: 0px; padding-right: 0px;">Agregar Trabajador</label>
                                        <input id="txtTrabajador" type="text" placeholder="Ingrese Trabajador" class="form-control col-md-5" style="height: 15px; margin-bottom: 10px;" />
                                        <button id="btnClearTrabajador" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button" style="font-size: smaller;" runat="server">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                        <button id="btnAddTrabajador" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button" style="font-size: smaller;" runat="server">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                        <textarea id="txaPersonal" class="form-control" style="height: 100px; width: 400px;" placeholder="Codigo 1, Codigo 2, Codigo 3" disabled="disabled"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="miniServicio" class="modal fade modal-dialog" role="dialog" style="z-index: 1600;" data-modal-parent="#divMyModal" runat="server">
                        <div class="modal-dialog">
                            <div class="modal-header" style="border-bottom: 1px solid #7D7D7D!important;">
                                <button id="btnCerrarMiniServicio" type="button" class="close" aria-hidden="true">×</button>
                                <h3 id="H1" runat="server">Servicios</h3>
                            </div>
                            <!-- Modal content-->
                            <div class="modal-content modal-body">
                                <div class="modal-body">
                                    <div class="col-md-12 container-fluid" style="padding-left: 0px; padding-right: 0px;">
                                        <label for="txtNuevosServicios" class="col-md-2" style="padding-left: 0px; padding-right: 0px;">Agregar Servicios</label>
                                        <input id="txtNuevosServicios" type="text" placeholder="Ingrese codigo de servicio" class="form-control col-md-5" style="height: 15px; margin-bottom: 10px;" />
                                        <button id="btnClear" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button" style="font-size: smaller;" runat="server">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                        <button id="btnAdd" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button" style="font-size: smaller;" runat="server">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                        <textarea id="txaServicio" class="form-control" style="height: 100px; width: 400px;" placeholder="Codigo 1, Codigo 2, Codigo 3" disabled="disabled"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </fieldset>

            <div class="modal-footer" style="padding: 5px 0px 0px 0px!important;">
                <div id="msg"></div>
                <button id="btnCerrar" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button cz-util-right" data-dismiss="modal" runat="server">Cerrar</button>
                <button id="btnGuardarModal" type="button" class="btn btn-default cz-form-content-input-button btn-mg cz-form-box-content-button cz-util-right" runat="server">Guardar</button>
            </div>
        </div>
    </form>
    <style>
        #miniModal {
            margin-top: 160px;
            margin-left: 140px;
        }

        #miniServicio {
            margin-top: 160px;
            margin-left: 140px;
        }

        #overlay {
            background: blue;
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            opacity: .2;
        }
    </style>
</body>
</html>
