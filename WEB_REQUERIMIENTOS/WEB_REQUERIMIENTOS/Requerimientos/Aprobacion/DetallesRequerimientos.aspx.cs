﻿using Controlador;
using Modelo.BEANS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Requerimientos.Aprobacion
{
    public partial class DetallesRequerimientos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];

                Int32 cod_requerimiento = Convert.ToInt32(dataJSON["cod_requerimiento"]);

                if (dataJSON == null)
                {
                    return;
                }
                else
                {
                    BeanDetalleRequerimiento detalles = ControladorRequerimiento.ObtenerInfoDetallesRequerimientoC(cod_requerimiento);
                    String fechacierre = "";
                    if (detalles.fecha_cierre == "")
                    {
                        fechacierre = "NO CERRADO";
                        this.NombreCompleto.Value = detalles.nombre_completo;
                        this.FechaRequerimiento.Value = detalles.fecha_requerimiento;
                        this.FechaCierre.Value = fechacierre;
                        this.Descripcion.Value = detalles.descripcion;
                        this.Detalles.Value = detalles.detalle;
                    }
                    else
                    {
                        this.NombreCompleto.Value = detalles.nombre_completo;
                        this.FechaRequerimiento.Value = detalles.fecha_requerimiento;
                        this.FechaCierre.Value = detalles.fecha_cierre;
                        this.Descripcion.Value = detalles.descripcion;
                        this.Detalles.Value = detalles.detalle;
                    }

                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }
    }
}