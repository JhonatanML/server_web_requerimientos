﻿using Controlador;
using Modelo.BEANS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Requerimientos.Aprobacion
{
    public partial class AprobarRequerimiento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
                Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];

                Int32 cod_requerimiento = Convert.ToInt32(dataJSON["cod_requerimiento"]);
                String estadoRequerimiento = dataJSON["nom_estado_requerimiento"].ToString();
                String nom_prioridad = dataJSON["nom_prioridad"].ToString();
                hdnUsuarioSesion.Value = usuario.id_usuario.ToString();
                if (dataJSON == null)
                {
                    return;
                }
                else
                {
                    BeanDetalleRequerimiento detalles = new BeanDetalleRequerimiento();
                    detalles = ControladorRequerimiento.ObtenerInfoDetallesRequerimientoC(cod_requerimiento);

                    this.NombreEstado.Value = detalles.nom_estado_requerimiento;
                    this.ServicioClienteT.Value = detalles.servicio_cliente;
                    //this.NombreEstado.Value = detalles.nom_estado_requerimiento;
                    this.dllCategoria.SelectedValue = detalles.id_categoria.ToString();
                    this.dllSubCategoria.SelectedValue = detalles.id_subcategoria.ToString();
                    this.dllServicio.SelectedValue = detalles.id_servicio.ToString();
                    this.Detalles.Value = detalles.detalle;

                    this.IDCategoria.Value = detalles.id_categoria.ToString();
                    this.IDSubCategoria.Value = detalles.id_subcategoria.ToString();
                    this.IDServicio.Value = detalles.id_servicio.ToString();

                    hdnCodigoRequerimiento.Value = Convert.ToString(cod_requerimiento);
                    hdnUsuario.Value = Convert.ToString(usuario.id_usuario);
                    hdnPrioridad.Value = nom_prioridad;
                    hdnUsuarioReq.Value = detalles.idUsuario;
                    switch (estadoRequerimiento)
                    {
                        case "PENDIENTE":
                            btnAsignarPersonal.Visible = false;
                            btnAgregar.Visible = false;
                            miniModal.Visible = false;
                            miniServicio.Visible = false;
                            btnAgregar.Visible = false;
                            divSubirFoto.Visible = false;
                            mensajeFoto.Visible = false;
                            divFechaAsignacion.Visible = false;
                            divFecAsignacion.Visible = false;
                            break;
                        case "ASIGNADO":
                            btnAsignarPersonal.Visible = false;
                            btnAgregar.Visible = true;
                            dllCategoria.Enabled = false;
                            dllSubCategoria.Enabled = false;
                            dllServicio.Enabled = false;
                            btnAgregar.Visible = true;
                            miniModal.Visible = false;
                            divSubirFoto.Visible = true;
                            mensajeFoto.Visible = true;
                            divFechaAsignacion.Visible = false;
                            divFecAsignacion.Visible = true;
                            fecAsignacion.Value = Convert.ToDateTime(detalles.fecha_asignacion).ToString("yyyy/MM/dd");
                            horaAsignacion.Value =  Convert.ToDateTime(detalles.fecha_asignacion).ToString("h:mm tt");
                            fecAsignacion.Disabled = true;
                            horaAsignacion.Disabled = true;
                            //horaAsignacion.Value = detalles.fecha_asignacion.Substring(10, 5) + ' ' + detalles.fecha_asignacion.Substring(19);
                            break;
                        case "APROBADO":
                            //miniModal.Visible = false;
                            btnAsignarPersonal.Visible = true;
                            dllCategoria.Enabled = false;
                            dllSubCategoria.Enabled = false;
                            dllServicio.Enabled = false;
                            btnAgregar.Visible = false;
                            btnAgregar.Visible = false;
                            miniServicio.Visible = false;
                            divSubirFoto.Visible = false;
                            mensajeFoto.Visible = false;
                            divFechaAsignacion.Visible = true;
                            divFecAsignacion.Visible = false;
                            //fechaAsignacion.Value = DateTime.Now.ToString("yyyy/MM/dd");
                            //horaAsignacion.Value = DateTime.Now.ToString("h:mm tt");
                            //DateTime.UtcNow
                            //miniModal.Visible = false;
                            break;
                    }
                }

                CargarComboCategoria();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        public static string getFechaActual()
        {
            string dia = DateTime.Now.Day.ToString().PadLeft(2, '0');
            string mes = DateTime.Now.Month.ToString().PadLeft(2, '0');
            string anio = DateTime.Now.Year.ToString().PadLeft(4, '0');

            return dia + "/" + mes + "/" + anio;
        }



        [WebMethod]
        private void CargarComboCategoria()
        {
            dllCategoria.DataSource = ControladorRequerimiento.Combo_CategoriaC();
            dllCategoria.DataValueField = "cod_categoria";
            dllCategoria.DataTextField = "nom_categoria";
            dllCategoria.DataBind();
            dllCategoria.Items.Insert(0, ".:CATEGORIAS:.");
            dllCategoria.Items[0].Value = "-1";
        }

        [WebMethod]
        public static string CargarComboSubCategoria(string cod_categoria)
        {
            try
            {
                List<BeanSubCategoria> lstBean = new List<BeanSubCategoria>();
                lstBean = ControladorRequerimiento.Combo_SubCategoriaC(cod_categoria);

                return Newtonsoft.Json.JsonConvert.SerializeObject(lstBean);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static string CargarComboServicio(string cod_subcategoria, string cod_categoria, string cod_servicio)
        {
            try
            {
                List<BeanServicio> lstBean = new List<BeanServicio>();
                BeanPaginate paginate = ControladorServicio.Combo_ServicioC(cod_subcategoria, cod_categoria, cod_servicio);

                lstBean = paginate.lstResultadoBeanServicio;

                return Newtonsoft.Json.JsonConvert.SerializeObject(lstBean);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}