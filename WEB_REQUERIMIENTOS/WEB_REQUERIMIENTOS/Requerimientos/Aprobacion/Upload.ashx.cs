﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace WEB_REQUERIMIENTOS.Requerimientos.Aprobacion
{
    /// <summary>
    /// Descripción breve de Upload
    /// </summary>
    public class Upload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            string filename = Path.GetFileName(request.Files["file"].FileName);
            String nombre = filename.Split('.')[0];
            String[] cortefolder = nombre.Split('_');
            String folder = "\\"+ cortefolder[0] + "_" + DateTime.Now.ToString("yyyyMMdd") + "\\";
            //String folder = "\\PRUEBA\\";
            String fileLocation = HttpContext.Current.Server.MapPath("~") + ConfigurationManager.AppSettings["rutaFoto"] + folder;            

            bool exists = System.IO.Directory.Exists(fileLocation);

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(fileLocation);
            }

            if (IsIE9(request))
            {
                byte[] buffer = new byte[request.ContentLength];


                using (BinaryReader br = new BinaryReader(request.Files["file"].InputStream))
                    br.Read(buffer, 0, buffer.Length);

                //string filename = Path.GetFileName(request.Files["file"].FileName);
                //String nombre = filename.Split('.')[0];
                String extension = filename.Split('.')[1];
                filename = nombre + DateTime.Now.ToString("yyyyMMdd") + '.' + extension;

                //string filename = cod_req + '_' + 
                //string cod_req = request.Files["cod_req"];

                if (!validarExtensionArchivo(Path.GetExtension(request.Files["file"].FileName)))
                {

                    context.Response.Write("{\"success\": false,\"error\":\"No es una extension soportada\"}");
                    context.Response.End();

                }

                //if (!validarTamanioArchivo(buffer.Length))
                //{

                //    context.Response.Write("{\"success\": false,\"error\":\"El tamaño maximo soportado es 400KB\"}");
                //    context.Response.End();

                //}


                File.WriteAllBytes(fileLocation + filename, buffer);

                context.Response.Write("{\"success\": true}");
                context.Response.End();
                context.Session["bufferImage"] = buffer;

            }
            else
            {
                byte[] buffer = new byte[request.ContentLength];
                using (BinaryReader br = new BinaryReader(request.InputStream))
                    br.Read(buffer, 0, buffer.Length);


                File.WriteAllBytes(fileLocation + request["file"], buffer);

                context.Response.Write("{\"success\":" + buffer + "}");
                context.Response.End();
                context.Session["bufferImage"] = buffer;

            }
            context.Response.Write("{\"success\": false, \"error\":\"Upload failed! Unexpected request\"}");
            context.Response.End();

        }

        private bool validarExtensionArchivo(string fileExtension)
        {
            bool respuesta = false;

            switch (fileExtension)
            {
                case ".jpg":
                case ".png":

                    respuesta = true;
                    break;

            }

            return respuesta;

        }

        // valida extension del archivo cargado y peso en MB del archivo
        private bool validarTamanioArchivo(int fileSize)
        {
            bool respuesta = false;

            // 1Mb 1048567bytes
            // 400KB aprox 409600

            if (fileSize <= 409600)
                respuesta = true;

            return respuesta;

        }

        private bool IsIE9(HttpRequest request)
        {
            return request["file"] == null;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}