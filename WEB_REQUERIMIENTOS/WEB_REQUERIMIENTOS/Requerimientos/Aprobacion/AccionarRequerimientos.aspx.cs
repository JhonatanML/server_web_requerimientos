﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace WEB_REQUERIMIENTOS.Requerimientos.Aprobacion
{
    public partial class AccionarRequerimientos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                var id_perfil = (usuario.id_perfil).ToString();

                CargarComboEstadoRequerimiento(id_perfil);
                FechaGenerada.Value = getFechaActual();
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        public static string getFechaActual()
        {
            string dia = DateTime.Now.Day.ToString().PadLeft(2, '0');
            string mes = DateTime.Now.Month.ToString().PadLeft(2, '0');
            string anio = DateTime.Now.Year.ToString().PadLeft(4, '0');

            return anio + "/" + mes + "/" + dia;
        }

        [WebMethod]
        private void CargarComboEstadoRequerimiento(string id_perfil)
        {
            dllEstadoRequerimiento.DataSource = ControladorRequerimiento.Combo_EstadoRequerimientoC(id_perfil);
            dllEstadoRequerimiento.DataValueField = "cod_estado_requerimiento";
            dllEstadoRequerimiento.DataTextField = "nom_estado_requerimiento";
            dllEstadoRequerimiento.DataBind();
            dllEstadoRequerimiento.Items.Insert(0, ".:TODOS:.");
            dllEstadoRequerimiento.Items[0].Value = "-1";
        }

        [WebMethod]
        public static void AprobarRequerimiento(Int32 cod_requerimiento)
        {
            try
            {
                ControladorRequerimiento.AprobarRequerimientoC(cod_requerimiento);

                BeanDetalleRequerimiento detalles = ControladorRequerimiento.ObtenerDetalleRequerimientosC(cod_requerimiento);

            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static void GuardarRequerimiento(String codigoRequerimiento, String servicioCliente, String estado,
            String categoria, String subCategoria, String servicio, String detalle, String observacion, String prioridad,
            String codigoUsuario, String codigoUsuarioSesion)
        {
            try
            {
                BeanDetalleRequerimiento bean = new BeanDetalleRequerimiento();
                bean.cod_requerimiento = codigoRequerimiento;
                bean.servicio_cliente = servicioCliente;
                bean.nom_estado_requerimiento = estado;
                bean.id_categoria = categoria;
                bean.id_subcategoria = subCategoria;
                bean.id_servicio = servicio;
                bean.detalle = detalle;
                bean.observacion = observacion;
                bean.cod_prioridad = prioridad;
                bean.idUsuario = codigoUsuario;
                bean.idUsuarioSesion = codigoUsuarioSesion;

                ControladorRequerimiento.GuardarRequerimiento(bean);

                //new Thread(() => ControladorRequerimiento.EnviarCorreo(Convert.ToString(bean.idUsuario), bean.cod_prioridad)).Start();

                BeanCorreo CorreoDetalle = ControladorCorreo.ObtenerDetalleCorreoC(Convert.ToInt32(codigoRequerimiento), Convert.ToInt32(codigoUsuario));

                String enviarCorreo = ConfigurationManager.AppSettings["ENVIAR_CORREO"].ToString();
                if (enviarCorreo == "1")
                {
                    new Thread(() => ControladorCorreo.EnviarCorreoC(CorreoDetalle.email, 
                        CorreoDetalle.id_requerimiento.ToString(), CorreoDetalle.email_jefe, 
                        CorreoDetalle.nombre_completo, CorreoDetalle.nom_estado_requerimiento, 
                        CorreoDetalle.fecha_requerimiento, CorreoDetalle.nom_servicio, 
                        CorreoDetalle.nom_prioridad, CorreoDetalle.detalle, 
                        CorreoDetalle.id_perfil)).Start();
                }

                //bool respuesta = ControladorRequerimiento.EnviarCorreo(Convert.ToString(bean.idUsuario), bean.cod_prioridad);
                //if (!respuesta)
                //{
                //    //"{\"success\": false, \"error\":\"Upload failed! Unexpected request\"}"
                //    return "{\"respuesta\": \"Error al enviar el correo \"}";
                //    //return "Error al enviar el correo.";
                //}
                //else
                //{
                //    return "{\"respuesta\": \"El requerimiento ha sido APROBADO \"}";
                //    //return "El requerimiento ha sido APROBADO.";
                //}
            }
            catch (Exception ex)
            {
                //Utility.registrarLog("Error al aprobar el requerimiento: " + ex.Message);
                //return "{\"respuesta\": \"Ocurrio un error en el servidor \"}";
                //return "Ocurrio un error en el servidor.";
            }
        }

        [WebMethod]
        public static void AprobarRequerimiento(String codigoRequerimiento, String servicioCliente, String estado,
            String categoria, String subCategoria, String servicio, String detalle, String observacion, String personal,
            String idUsuario, String codigoUsuario, String codigoUsuarioSesion, String fechaAsignacion)
        {
            try
            {
                BeanDetalleRequerimiento bean = new BeanDetalleRequerimiento();
                bean.cod_requerimiento = codigoRequerimiento;
                bean.servicio_cliente = servicioCliente;
                bean.nom_estado_requerimiento = estado;
                bean.id_categoria = categoria;
                bean.id_subcategoria = subCategoria;
                bean.id_servicio = servicio;
                bean.detalle = detalle;
                bean.observacion = observacion;
                bean.personal = personal;
                bean.idUsuario = idUsuario;
                bean.idUsuarioC = codigoUsuario;
                bean.idUsuarioSesion = codigoUsuarioSesion;
                bean.fecha_asignacion = fechaAsignacion;


                ControladorRequerimiento.AprobarRequerimiento(bean);

                BeanCorreo CorreoDetalle = ControladorCorreo.ObtenerDetalleCorreoC(Convert.ToInt32(codigoRequerimiento), Convert.ToInt32(codigoUsuario));

                String enviarCorreo = ConfigurationManager.AppSettings["ENVIAR_CORREO"].ToString();
                if (enviarCorreo == "1")
                {
                    new Thread(() => ControladorCorreo.EnviarCorreoC(CorreoDetalle.email, 
                        CorreoDetalle.id_requerimiento.ToString(), CorreoDetalle.email_jefe, 
                        CorreoDetalle.nombre_completo, CorreoDetalle.nom_estado_requerimiento, 
                        CorreoDetalle.fecha_requerimiento, CorreoDetalle.nom_servicio, 
                        CorreoDetalle.nom_prioridad, CorreoDetalle.detalle, 
                        CorreoDetalle.id_perfil)).Start();
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        [WebMethod]
        public static void CerrarRequerimiento(String codigoRequerimiento, String servicioCliente, String estado,
            String categoria, String subCategoria, String servicio, String detalle, String observacion, String serviciosAdicionales,
            String servicioActual, String foto1, String foto2, String foto3, String codigoUsuario, String codigoUsuarioSesion)
        {
            try
            {
                BeanDetalleRequerimiento bean = new BeanDetalleRequerimiento();
                bean.cod_requerimiento = codigoRequerimiento;
                bean.servicio_cliente = servicioCliente;
                bean.nom_estado_requerimiento = estado;
                bean.id_categoria = categoria;
                bean.id_subcategoria = subCategoria;
                bean.id_servicio = servicio;
                bean.detalle = detalle;
                bean.observacion = observacion;
                bean.serviciosAdicionales = serviciosAdicionales;
                bean.servicioActual = servicioActual;
                bean.foto1 = foto1;
                bean.foto2 = foto2;
                bean.foto3 = foto3;
                bean.idUsuarioC = codigoUsuario;
                bean.idUsuarioSesion = codigoUsuarioSesion;

                ControladorRequerimiento.CerrarRequerimiento(bean);

                BeanCorreo CorreoDetalle = ControladorCorreo.ObtenerDetalleCorreoC(Convert.ToInt32(codigoRequerimiento), Convert.ToInt32(codigoUsuario));

                String enviarCorreo = ConfigurationManager.AppSettings["ENVIAR_CORREO"].ToString();
                if (enviarCorreo == "1")
                {
                    new Thread(() => ControladorCorreo.EnviarCorreoC(CorreoDetalle.email, 
                        CorreoDetalle.id_requerimiento.ToString(), CorreoDetalle.email_jefe, 
                        CorreoDetalle.nombre_completo, CorreoDetalle.nom_estado_requerimiento, 
                        CorreoDetalle.fecha_requerimiento, CorreoDetalle.nom_servicio, 
                        CorreoDetalle.nom_prioridad, CorreoDetalle.detalle, 
                        CorreoDetalle.id_perfil)).Start();
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        [WebMethod]
        public static String validarCodigoServicio(String codigoServicio, String codigoServicioActual)
        {
            BeanServicio bean = new BeanServicio();
            try
            {
                bean = ControladorRequerimiento.validarCodigoServicio(codigoServicio, codigoServicioActual);
                return Newtonsoft.Json.JsonConvert.SerializeObject(bean);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static String EnviarComentario(string comentario, string cod_requerimiento, string idUsuario)
        {
            try
            {
                Int32 respuesta = ControladorRequerimiento.EnviarComenarioC(comentario, cod_requerimiento, idUsuario);

                return "{\"respuesta\": \"Su comentario se ha enviado correctamente.\"}";

            }
            catch (Exception ex)
            {
                Utility.registrarLog("ERROR COMENTARIO: " + ex.Message);
                return "{\"respuesta\": \"" + ex.Message + "\"}";
            }
        }
    }
}