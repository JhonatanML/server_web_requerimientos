﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Requerimientos.Aprobacion
{
    public partial class AprobarRequerimientos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                var id_perfil = (usuario.id_perfil).ToString();

                CargarComboEstadoRequerimiento(id_perfil);
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        [WebMethod]
        private void CargarComboEstadoRequerimiento(string id_perfil)
        {
            dllEstadoRequerimiento.DataSource = ControladorRequerimiento.Combo_EstadoRequerimientoC(id_perfil);
            dllEstadoRequerimiento.DataValueField = "cod_estado_requerimiento";
            dllEstadoRequerimiento.DataTextField = "nom_estado_requerimiento";
            dllEstadoRequerimiento.DataBind();
            dllEstadoRequerimiento.Items.Insert(0, ".:TODOS:.");
            dllEstadoRequerimiento.Items[0].Value = "-1";
        }

        [WebMethod]
        public static void AprobarRequerimiento(Int32 cod_requerimiento)
        {
            try
            {
                ControladorRequerimiento.AprobarRequerimientoC(cod_requerimiento);

                BeanDetalleRequerimiento detalles = ControladorRequerimiento.ObtenerDetalleRequerimientosC(cod_requerimiento);

                /*-------------------------MENSAJE DE CORREO----------------------*/

                //Creamos un nuevo Objeto de mensaje
                System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

                //Direccion de correo electronico a la que queremos enviar el mensaje
                //CORREO DEL USUARIO QUE REGISTRA EL REQUERIMIENTO DEL SERVICIO
                mmsg.To.Add("kevin_28_06@hotmail.com");

                //Nota: La propiedad To es una colección que permite enviar el mensaje a más de un destinatario

                //Asunto
                mmsg.Subject = "REQUERIMIENTO DE SERVICIOS";
                mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

                //Direccion de correo electronico que queremos que reciba una copia del mensaje
                //CORREO DEL SUPERVISOR AL QUE SE LE ENVIA UNA COPIA DEL MENSAJE
                mmsg.Bcc.Add("kevprimoz@gmail.com"); //Opcional
                //CORREO DEL PROVEEDOR AL QUE SE LE ENVIA UNA COPIA DEL MENSAJE
                mmsg.Bcc.Add("PROVEEDOR@hotmail.com"); //Opcional

                //Cuerpo del Mensaje
                mmsg.Body = "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'/></head><body><table width='750' border='7px' align='center' cellpadding='0' cellspacing='0'><tr><td align='center' valign='top' bgcolor='#000000' style='background-color:#FFFFFF; font-size:13px; color:#000000;'><table width='100%' border='0' cellspacing='0' cellpadding='0' style='margin-top:-35px;'><tr><td align='center' valign='top'><br><br><br><br><br><div style='font-size:45px; color:#000000 ;'><u><b> MENSAJE INFORMATIVO </b></u></div><div style='font-size:30px; color:#000000;'><br> Sr.(a) " + detalles.nombre_completo + ", su requerimiento del servicio ha sido 'APROBADO'.</div><div align='left'  style='font-size:17px; color:#34495E; padding-left:100px'><br><b>Detalles del requerimiento</b><br><br><b>Estado: </b>" + detalles.nom_estado_requerimiento + "<br><b>Fecha generada: </b>" + detalles.fecha_requerimiento + "<br><b>Descripción: </b>" + detalles.descripcion + "<br><b>Detalle: </b>" + detalles.detalle + "<br><b>Categoria </b>" + detalles.nom_categoria + "<br><b>Subcategoria </b>" + detalles.nom_subcategoria + "<br><b>Prioridad </b>" + detalles.nom_prioridad + "</b></div><br><br><br><br><br><br></td></tr></table></td></tr><tr></tr></table></body></html>";
                mmsg.BodyEncoding = System.Text.Encoding.UTF8;
                mmsg.IsBodyHtml = true; //Si no queremos que se envíe como HTML

                //CORREO DEL SERVIDOR (EL MISMO DE LAS CREDENCIALES)
                //Correo electronico desde la que enviamos el mensaje
                mmsg.From = new System.Net.Mail.MailAddress("leocraf@hotmail.com");


                /*-------------------------CLIENTE DE CORREO----------------------*/

                //Creamos un objeto de cliente de correo
                System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();

                //Hay que crear las credenciales del correo emisor
                cliente.Credentials =
                    new System.Net.NetworkCredential("leocraf@hotmail.com", "@Jp1596357159635");

                //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail

                cliente.Port = 587;
                cliente.EnableSsl = true;


                cliente.Host = "smtp-mail.outlook.com"; //Para Gmail "smtp.gmail.com";
                //cliente.Host = "smtp.gmail.com";


                /*-------------------------ENVIO DE CORREO----------------------*/

                try
                {
                    //Enviamos el mensaje      
                    cliente.Send(mmsg);
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    //Aquí gestionamos los errores al intentar enviar el correo
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}