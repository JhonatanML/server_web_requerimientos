﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Requerimientos.Aprobacion
{
    public partial class ModalComentarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //string json = new System.IO.StreamReader(Request.InputStream).ReadToEnd();
                //Dictionary<string, string> dataJSON = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                HidPerfil.Value = usuario.id_perfil.ToString();
                HidIdUsuario.Value = usuario.id_usuario.ToString();
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        [WebMethod]
        public static string CargarComentarios(string cod_requerimiento)
        {
            try
            {
                List<BeanComentario> lstBean = new List<BeanComentario>();
                BeanPaginate paginate = ControladorRequerimiento.ListarComentarioC(cod_requerimiento);

                lstBean = paginate.lstResultadoBeanComentario;

                return Newtonsoft.Json.JsonConvert.SerializeObject(lstBean);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}