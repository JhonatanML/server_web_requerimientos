﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccionarRequerimientos.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Requerimientos.Aprobacion.AccionarRequerimientos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <link href="../../css/styleBootStrap.css" rel="stylesheet" />
    <script src="../../js/jquery.datetimepicker.js"></script>
    <link href="../../css/jquery.datetimepicker.css" rel="stylesheet" />
    <script src="../../js/Siedevs.js"></script>
    <script src="../../scripts/code.jquery.com_ui_1.11.4_themes_smoothness/jquery-ui.js"></script>
    <link href="../../scripts/code.jquery.com_ui_1.11.4_themes_smoothness/jquery-ui.css" rel="stylesheet" />
    <script src="../../lib/moment.min.js"></script>

    <script>
        //var urldel = 'MantLocales.aspx/EliminarLocal';//WebMethod para eliminacion
        //var urlins = 'NuevoLocal.aspx'//pantalla de registros
        var urlbus = 'Grid.aspx';//grilla
        //var urlbusdet = 'DetallesRequerimientos.aspx'
        var urlObSe = 'AprobarRequerimiento.aspx/CargarComboServicio';
        var urlObS = 'AprobarRequerimiento.aspx/CargarComboSubCategoria';
        var urlAprM = 'AprobarRequerimiento.aspx';
        var urlAsiM = 'AsignarRequerimiento.aspx';
        var urlCerM = 'CerrarRequerimiento.aspx';
        var urlApr = 'AccionarRequerimientos.aspx/AprobarRequerimiento';
        var urlAsi = 'AccionarRequerimientos.aspx/AsignarRequerimiento';
        var urlCer = 'AccionarRequerimientos.aspx/CerrarRequerimiento';

        var urlReqAct = 'AccionarRequerimientos.aspx/GuardarRequerimiento'; //pendiente
        var urlReqAsignar = 'AccionarRequerimientos.aspx/AprobarRequerimiento'; //aprobado
        var urlReqCerrar = 'AccionarRequerimientos.aspx/CerrarRequerimiento'; //asignar

        var urlAddNewService = 'AccionarRequerimientos.aspx/validarCodigoServicio';

        var urlbuscom = 'ModalComentarios.aspx';
        var urlbuscome = 'ModalComentarios.aspx/CargarComentarios';
        var urlEnCom = 'AccionarRequerimientos.aspx/EnviarComentario';
        //var urlsavN = 'MantLocales.aspx/AgregarLocal'; //WebMethod para insercion
        //var urlsavE = 'MantLocales.aspx/EditarLocal'; //WebMethod para edicion

        $(document).ready(function () {

            //$('#FechaGenerada').val(hoy);
            $('#FechaGenerada').datetimepicker({
                format: 'Y/m/d',
                lang: 'es',
                closeOnDateSelect: true,
                timepicker: false
            });
            AccionarRequerimiento();
            ListarRequerimientosPendientes();//funcion encargada de mostrar la grilla y encargada de la paginacion de esta
            AgregarPersonal();
            //MostrarDetalleRequerimiento();

            MostrarComentarios();

            $('#buscar').trigger("click");
            //$(document).keypress(function (e) {
            //    if (e.which == 13) {
            //        //alert("EVENTO KEYPRESS");
            //        $('#buscar').trigger("click");
            //    }
            //});

        });

        function getParametros() { //funcion encargada de enviar los parametros a la grilla
            var strData = new Object();
            strData.id_requerimiento = $('#CodRequerimiento').val();
            strData.fecha_requerimiento = $('#FechaGenerada').val();
            strData.cod_estado_requerimiento = $('#dllEstadoRequerimiento').val();
            strData.page = $('#hdnActualPage').val();
            strData.rows = $('#hdnShowRows').val();

            return strData;
        }
    </script>
    <style>
        .calendar {
            background-position: right center;
            background-image: url(../../images/calendar.png);
            background-repeat: no-repeat;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="borrarContenido">
        <form id="form1" runat="server">
            <input id="txtfecha" type="hidden" runat="server" />
            <input id="txtcliente" type="hidden" runat="server" />
            <div class="cz-submain cz-submain-form-background">
                <div id="cz-form-box">
                    <div class="cz-form-box-content">
                        <div id="cz-form-box-content-title" style="margin-top: -12px;">
                            <div id="cz-form-box-content-title-text">
                                <span class="glyphicon glyphicon glyphicon-list-alt btn-lg cz-form-box-content-title-icon" style="color: #666;" aria-hidden="true"></span>
                                <span class="font-size-20">Atención de Requerimientos</span>
                            </div>
                        </div>
                        <%--<button id="cz-form-box-new" type="button" class="cz-form-content-input-button btn-md cz-form-box-content-button cz-util-right cz-u-expand-table" style="margin-top: 5px;" data-grid-id="divGridViewData">
                            <span class="glyphicon glyphicon-fullscreen" style="color: white;"></span>&nbsp;&nbsp;Ver Tabla
                        </button>--%>
                        <%--<button id="cz-form-box-deletes" type="button" class="cz-form-content-input-button btn-md cz-form-box-content-button cz-util-right addUsuario" style="margin-top: 5px;">
                            <span class="glyphicon glyphicon-file" style="color: white;"></span>&nbsp;&nbsp;Nuevo
                        </button>
                        <button id="cz-form-box-delete" type="button" class="cz-form-content-input-button btn-md cz-form-box-content-button cz-util-right delReg" style="margin-top: 5px;">
                            <span class="glyphicon glyphicon-remove" style="color: white;"></span>&nbsp;&nbsp;Borrar
                        </button>--%>
                    </div>

                    <div class="cz-form-box-content">
                        <div class="cz-form-content">
                            <p>ID: </p>
                            <input type="text" class="form-control" runat="server" id="CodRequerimiento" maxlength="10" style="height: 20px; width: 85%;" />
                        </div>
                        <div class="cz-form-content">
                            <label for="FechaGenerada">Fecha Generada:</label>
                            <input id="FechaGenerada" runat="server" type="text" class="form-control calendar" maxlength="10" style="height: 20px; width: 85%;" />
                        </div>
                        <div class="cz-form-content" style="width: 250px;">
                            <p>Estado:</p>
                            <asp:DropDownList ID="dllEstadoRequerimiento" runat="server" class="form-control"></asp:DropDownList>
                        </div>
                        <div class="cz-util-right cz-util-right-text col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <button id="buscar" type="button" class="cz-form-content-input-button btn-md cz-form-box-content-button cz-util-right" style="margin-top: 5px;">
                                <span class="glyphicon glyphicon-search" style="color: white;"></span>&nbsp;&nbsp;Buscar
                            </button>
                        </div>
                    </div>
                    <div class="cz-form-box-content">
                        <div class="form-grid-box">
                            <div class="form-grid-table-outer">
                                <div class="form-grid-table-inner">
                                    <div class="form-gridview-data" id="divGridViewData" runat="server"></div>
                                    <div class="form-gridview-error" id="divGridViewError" runat="server"></div>
                                    <div class="form-gridview-search" id="divGridViewSearch" runat="server">
                                        <img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />
                                        <p>Buscando Resultados</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="paginator-hidden-fields">
                        <asp:HiddenField ID="hdnActualPage" Value="1" runat="server" />
                        <asp:HiddenField ID="hdnShowRows" Value="10" runat="server" />
                    </div>
                    <div id="myModalCargando" class="modal hide fade in" style="display: block; background: transparent; border: none; -webkit-box-shadow: none;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
                    </div>
                    <div id="myModal" class="modal hide fade" style="display: block;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>
