﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Grid.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Requerimientos.Aprobacion.Grid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="habilitados" runat="server" />
        <div id="divMyModal" class="modal fade" style="width:1000px; margin-left:-550px;">
            <div class="modal-dialog" id="divContenidoModal"></div>
        </div>
        <div id="divModalComentario" class="modal fade" style="width:60%; margin-left:-35%;">
            <div class="modal-dialog" id="divContenidoModalComentario"></div>
        </div>
        <%--id="myModal--%>
        <div id="divModalSIE" class="modal fade"  role="dialog">
            <div class="modal-dialog" id="divContenidoSIE"></div>
        </div>
        <div id="divGridView" runat="server">
            <asp:GridView ID="grDocumento" GridLines="None" AlternatingRowStyle-CssClass="alt"
                runat="server" AutoGenerateColumns="False" CssClass="grilla table table-bordered table-striped" Style="width: 100%;">
                <Columns>
                    <asp:BoundField DataField="cod_requerimiento" HeaderText="Codigo" ItemStyle-Width="100px" ControlStyle-CssClass="center" />
                    <asp:BoundField DataField="fecha_requerimiento" HeaderText="Fecha Requerida" />
                    <%--<asp:BoundField DataField="nom_categoria" HeaderText="Categoria" />
                    <asp:BoundField DataField="nom_subcategoria" HeaderText="Subcategoria" />--%>
                    <asp:BoundField DataField="detalle" HeaderText="Detalle" />
                    <asp:BoundField DataField="nom_prioridad" HeaderText="Prioridad" />
                    <asp:BoundField DataField="nom_estado_requerimiento" HeaderText="Estado" />
                    <asp:BoundField DataField="nombre_completo" HeaderText="Usuario" />

                    <%--<asp:TemplateField HeaderText="Detalles" ItemStyle-Width="40px" HeaderStyle-CssClass="last" ItemStyle-CssClass="last" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <a class="MostrarDetalle form-icon" role="button" data-toggle="modal" style="cursor: pointer;" cod_requerimiento="<%# Eval("cod_requerimiento") %>">
                                <img src="../../imagery/all/icons/zoom.png" border="0" title="Detalle" />
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="ACCION" ItemStyle-Width="40px" HeaderStyle-CssClass="last"
                        ItemStyle-CssClass="last" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <a role="button" data-toggle="modal" class="delItemReg form-icon" style="cursor: pointer;" nom_prioridad="<%# Eval("nom_prioridad")%>" cod_requerimiento="<%# Eval("cod_requerimiento")%>" nom_estado_requerimiento="<%# Eval("nom_estado_requerimiento") %>">
                                <img src="<%# Eval("rutaImg")%>" border="0" title="<%# Eval("tituloImg")%>" width="14" /></a> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comentarios" ItemStyle-Width="40px" HeaderStyle-CssClass="last" ItemStyle-CssClass="last" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <a class="MostrarComentario form-icon" role="button" data-toggle="modal" style="cursor: pointer;" cod_requerimiento="<%# Eval("cod_requerimiento") %>" cod_estado_requerimiento="<%# Eval("cod_estado_requerimiento") %>">
                                <img src="../../imagery/all/icons/edit.png" border="0" title="Detalle" />
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="divGridViewPagintator" class="paginator-table" runat="server">
            <div class="paginator-table-outer">
                <div class="paginator-table-inner">
                    <div class="paginator-data">
                        <div class="cz-page-ant">
                            <p class="pagina-direccion">
                                <asp:Label ID="lbPaginaAnterior" CssClass="pagina-anterior" runat="server" Text="Anterior"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-now">
                            <a>Página</a>
                            <p class="pagina-actual">
                                <asp:TextBox ID="lbPagina" CssClass="pagina-editor" runat="server"></asp:TextBox>
                            </p>
                            <p class="pagina-direccion">
                                <span class="pagina-data">de </span>
                                <asp:Label ID="lbTpagina" class="pagina-maxima" runat="server" Text="1"></asp:Label>
                            </p>
                        </div>
                        <div class="cz-page-des">
                            <asp:Label ID="lbPaginaSiguiente" CssClass="pagina-siguiente" runat="server" Text="Siguiente"></asp:Label>
                        </div>
                    </div>
                    <div class="paginator-data-search">
                        <img src="../../images/icons/loader/ico_loader-arrow-grey.gif" />
                        <p>
                            Buscando Resultados
                        </p>
                    </div>
                </div>
            </div>
            <div class="cz-TotalPaginas">Total de <span runat="server" id="spTotalReg" style="font-weight: bold; font-size: 13px;"></span> Registros</div>
            <div class="cz-table-expand">
                <div class="cz-table-expand-close-x">×</div>
            </div>
        </div>
    </form>
    <input type="hidden" id="contador" name="contador" runat="server" />

    <style>
        #divGridView td{
            text-align:center;
        }
    </style>
</body>
</html>
