﻿using Common.Logger;
using Controlador;
using Modelo.BEANS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace WEB_REQUERIMIENTOS.Requerimientos
{
    public partial class RegistroRequerimiento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                hdnUsuario.Value = (usuario.id_usuario).ToString();
                hdnPerfilUsuario.Value = (usuario.id_perfil).ToString();

                FechaGenerada.Disabled = true;

                CargarComboCategoria();
                CargarComboPrioridad();
            }
            catch (Exception ex)
            {
                string myScript = "parent.document.location.href = '../../default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }

        }

        [WebMethod]
        private void CargarComboCategoria()
        {
            dllCategoria.DataSource = ControladorRequerimiento.Combo_CategoriaC();
            dllCategoria.DataValueField = "cod_categoria";
            dllCategoria.DataTextField = "nom_categoria";
            dllCategoria.DataBind();
            //dllCategoria.Items.Insert(0, ".:CATEGORIAS:.");
            //dllCategoria.Items[0].Value = "-1";
        }

        [WebMethod]
        private void CargarComboPrioridad()
        {
            dllPrioridad.DataSource = ControladorRequerimiento.Combo_PrioridadC();
            dllPrioridad.DataValueField = "cod_prioridad";
            dllPrioridad.DataTextField = "nom_prioridad";
            dllPrioridad.DataBind();
            dllPrioridad.Items.Insert(0, ".:PRIORIDADES:.");
            dllPrioridad.Items[0].Value = "-1";
        }

        [WebMethod]
        public static string CargarListaServicios(string nom_serviciosT)
        {
            try
            {
                List<BeanServicio> lstBean = new List<BeanServicio>();
                BeanPaginate paginate = ControladorServicio.Lista_ServicioC(nom_serviciosT);

                lstBean = paginate.lstResultadoBeanServicio;

                return Newtonsoft.Json.JsonConvert.SerializeObject(lstBean);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        //private void CargarListaServicios()
        //{
        //    listServicios.DataSource = ControladorRequerimiento.List_ServicioC();
        //    listServicios.DataValueField = "cod_servicios";
        //    listServicios.DataTextField = "nom_servicios";
        //    listServicios.DataBind();
        //}

        [WebMethod]
        public static string CargarComboSubCategoria(string cod_categoria)
        {
            try
            {
                List<BeanSubCategoria> lstBean = new List<BeanSubCategoria>();
                lstBean = ControladorRequerimiento.Combo_SubCategoriaC(cod_categoria);

                //lstBean = paginate.lstResultadoBeanSubCategoria;

                return Newtonsoft.Json.JsonConvert.SerializeObject(lstBean);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static string RegistrarRequerimiento(string fecha_requerimiento, String cod_categoria,
            String codigoServicio, String tipoServicio, String detalle, int cod_prioridad, int id_usuario, int id_perfil)
        {
            try
            {
                //fecha_requerimiento, cod_categoria, codigoServicio, tipoServicio, detalle, cod_prioridad, id_usuario
                BeanRequerimiento bean = new BeanRequerimiento();
                bean.fecha_requerimiento = fecha_requerimiento;
                bean.codigo_categoria = cod_categoria;
                bean.cod_servicio = codigoServicio;
                bean.tipoServicio = tipoServicio;
                bean.detalle = detalle;
                bean.cod_prioridad = cod_prioridad;
                bean.id_usuario = id_usuario;
                bean.id_perfil = id_perfil;
                //Utility.registrarLog("REGISTRA DETALLE json = " + JsonConvert.SerializeObject(bean));
                String id_requerimiento = ControladorRequerimiento.RegistrarRequerimientoC(bean);
                //Utility.registrarLog("REGISTRA DETALLE idusuario " + id_usuario + " PRIORIDAD " + cod_prioridad);
                BeanDetalleRequerimiento detalles = ControladorRequerimiento.ObtenerDetalleRequerimientoC(id_usuario, cod_prioridad, id_perfil);

                String enviarCorreo = ConfigurationManager.AppSettings["ENVIAR_CORREO"].ToString();
                if (enviarCorreo == "1")
                {
                    ControladorCorreo.EnviarCorreoC(detalles.email, id_requerimiento, detalles.email_jefe, detalles.nombre_completo, detalles.nom_estado_requerimiento, fecha_requerimiento, codigoServicio, detalles.nom_prioridad, detalle, detalles.id_perfil);
                }

                return "OK";
            }
            catch (Exception ex)
            {
                //Utility.registrarLog(ex.Message);
                //Utility.registrarLog(ex.StackTrace);
                throw new Exception("ERROR: " + ex.Message);
            }
        }

        [WebMethod]
        public static string ObtenerCategoria(string codigoServicio, string tipoServicio)
        {
            try
            {
                BeanCategoria bean = new BeanCategoria();
                bean = ControladorRequerimiento.ObtenerCategoriaCombo(codigoServicio, tipoServicio);
                return Newtonsoft.Json.JsonConvert.SerializeObject(bean);
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR: " + ex.Message);
            }
        }
    }
}