﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Requerimientos.Visualizar
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void EnviarCorreo(object sender, EventArgs e)
    {
        /*-------------------------MENSAJE DE CORREO----------------------*/

        //Creamos un nuevo Objeto de mensaje
        System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

        //Direccion de correo electronico a la que queremos enviar el mensaje
        //CORREO DEL USUARIO QUE REGISTRA EL REQUERIMIENTO DEL SERVICIO
        mmsg.To.Add("kevin_28_06@hotmail.com");

        //Nota: La propiedad To es una colección que permite enviar el mensaje a más de un destinatario

        //Asunto
        mmsg.Subject = "Asunto del correo";
        mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

        //Direccion de correo electronico que queremos que reciba una copia del mensaje
        //CORREO DEL SUPERVISOR AL QUE SE LE ENVIA UNA COPIA DEL MENSAJE
        mmsg.Bcc.Add("SUPERVISOR@hotmail.com"); //Opcional
        //CORREO DEL PROVEEDOR AL QUE SE LE ENVIA UNA COPIA DEL MENSAJE
        mmsg.Bcc.Add("PROVEEDOR@hotmail.com"); //Opcional

        //Cuerpo del Mensaje
        mmsg.Body = "Texto del contenio del mensaje de correo";
        mmsg.BodyEncoding = System.Text.Encoding.UTF8;
        mmsg.IsBodyHtml = false; //Si no queremos que se envíe como HTML
        
        //CORREO DEL SERVIDOR (EL MISMO DE LAS CREDENCIALES)
        //Correo electronico desde la que enviamos el mensaje
        mmsg.From = new System.Net.Mail.MailAddress("leocraf@hotmail.com");


        /*-------------------------CLIENTE DE CORREO----------------------*/

        //Creamos un objeto de cliente de correo
        System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();

        //Hay que crear las credenciales del correo emisor
        cliente.Credentials =
            new System.Net.NetworkCredential("leocraf@hotmail.com", "@Jp1596357159635");

        //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail
        
        cliente.Port = 587;
        cliente.EnableSsl = true;
        

        cliente.Host = "smtp-mail.outlook.com"; //Para Gmail "smtp.gmail.com";
        cliente.Host = "smtp.gmail.com";


            /*-------------------------ENVIO DE CORREO----------------------*/

            try
        {
            //Enviamos el mensaje      
            cliente.Send(mmsg);
        }
        catch (System.Net.Mail.SmtpException ex)
        {
            //Aquí gestionamos los errores al intentar enviar el correo
        }
    }
    }
}