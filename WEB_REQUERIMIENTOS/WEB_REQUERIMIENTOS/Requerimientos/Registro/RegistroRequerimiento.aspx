﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegistroRequerimiento.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Requerimientos.RegistroRequerimiento" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../js/jsmodule.js" type="text/javascript"></script>
    <script src="../../js/Validacion.js" type="text/javascript"></script>
    <link href="../../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../../js/cz_main.js" type="text/javascript"></script>
    <link href="../../css/styleBootStrap.css" rel="stylesheet" />
    <script src="../../js/jquery.datetimepicker.js"></script>
    <link href="../../css/jquery.datetimepicker.css" rel="stylesheet" />
    <script src="../../lib/moment.min.js"></script>
    <script src="../../js/Siedevs.js"></script>
    <script>
        //var urldel = 'MantLocales.aspx/EliminarLocal';//WebMethod para eliminacion
        //var urlins = 'RegistroRequerimiento.aspx/RegistrarRequerimiento'; //WebMethod para edicion
        //var urlbus = 'Grid.aspx';//grilla
        var urlsavN = 'RegistroRequerimiento.aspx/RegistrarRequerimiento'; //WebMethod para registrar
        //var urlsavE = 'MantLocales.aspx/EditarLocal'; //WebMethod para edicion
        var urlObS = 'RegistroRequerimiento.aspx/CargarComboSubCategoria'; //WebMethod para edicion
        var urlObLS = 'RegistroRequerimiento.aspx/CargarListaServicios'; //WebMethod para edicion

        $(document).ready(function () {

            var fecha = moment().format('YYYY/MM/DD');
            $('#FechaGenerada').val(fecha);

            $('#FechaGenerada').datetimepicker({
                format: 'Y/m/d',
                lang: 'es',
                closeOnDateSelect: true,
                timepicker: false
            });

            //EliminarLocal();//funcion encargada de la eliminacion
            //AgregarLocal();//funcion encargada de adiccionar registros
            //ModificarLocal();//funcion encargada en modificar registros
            //busRegCon();//funcion encargada de mostrar la grilla y encargada de la paginacion de esta
            RegistroRequerimiento();//funcion encargada de registrar requerimiento
            //ObtenerSubCategoria();//funcion encargada de obtener las subcategorias de acuerdo a su categoria
            ObtenerListaServicios();

        });

    </script>
    <style>
        .calendar {
            background-position: right center;
            background-image: url(../../images/calendar.png);
            background-repeat: no-repeat;
            cursor: pointer;
            width: 165px;
        }

        .padd {
            /*border: 1px solid black;*/
            /*background-color: lightgrey;*/
            padding-top: 50px;
            padding-right: 100px;
            /*padding-bottom: 70px;*/
            padding-left: 100px;
        }

        .cajas {
            height: 20px;
        }

        .texto {
            font-size: 15px;
            margin-top: 5px;
            /*padding-right: 0px;*/
            /*margin-left: 45px;
            margin-right: 25px;*/
        }

        .combo {
            height: 30px;
            width: 190px;
        }

        .textarea {
            height: 100px;
            Width: 300px;
        }

        #listServicios {
            height: 145px;
            width: 432px;
        }

        #Detalle{
            height: 100px;
            width: 265px;
        }

    </style>
</head>
<body>
    <div class="borrarContenido">
        <form id="form1" runat="server">
            <div id="divMyModal" class="modal fade">
                <div class="" id="divContenidoModal"></div>
            </div>
            <asp:HiddenField ID="hdnUsuario" runat="server" />
            <asp:HiddenField ID="hdnPerfilUsuario" runat="server" />
            <asp:HiddenField ID="hdnFechaRequerimiento" runat="server" />
            <div class="cz-submain cz-submain-form-background">
                <div id="cz-form-box">
                    <div class="cz-form-box-content">
                        <div id="cz-form-box-content-title" style="margin-top: -12px;">
                            <div id="cz-form-box-content-title-text">
                                <span class="glyphicon glyphicon glyphicon-pencil btn-lg cz-form-box-content-title-icon" style="color: #666;" aria-hidden="true"></span>
                                <span class="font-size-20">Registro de Requerimiento de Servicios</span>
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-box-content padd">

                        <div class="row col-lg-12">
                            <div class="col-lg-7">
                                <label for="dllCategoria" class="col-lg-2 texto" style="padding-left: 0px; padding-right: 0px;">Categoria: </label>
                                <asp:DropDownList ID="dllCategoria" runat="server" class="form-control combo" Enabled="false" Width="430px"></asp:DropDownList>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="FechaGenerada" class="col-lg-3 texto" style="padding-left: 0px; padding-right: 15px">Fecha: </label>
                                    <input id="FechaGenerada" runat="server" type="text" class="form-control calendar cajas" />
                                </div>
                            </div>
                        </div>

                        <div class="row col-lg-12" style="margin-top: 20px;">
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <label for="NombreServicio" class="col-lg-2 texto" style="padding-left: 0px; padding-right: 0px;">Servicio:</label>
                                    <input id="NombreServicio" runat="server" class="form-control col-lg-8 cajas" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="dllPrioridad" class="col-lg-3 texto" style="padding-left: 0px;">Prioridad: </label>
                                    <asp:DropDownList ID="dllPrioridad" runat="server" class="form-control combo"></asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row col-lg-12" style="margin-top: 20px;">
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <label for="listServicios" class="col-lg-1 texto" style="margin-right: 20px;"></label>
                                    <asp:ListBox ID="listServicios" runat="server" SelectionMode="Single" class="form-control"></asp:ListBox>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="Detalle" class="col-lg-2 texto" style="padding-left:0px; margin-bottom:10px;">Detalles: </label>
                                    <textarea id="Detalle" runat="server" class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="row col-lg-11" style="margin-top:30px;">
                            <div class="form-group">
                                <button id="btnRegistrar" type="button" class="btn btn-primary cz-form-content-input-button btn-mg cz-form-box-content-button cz-util-right" style="">Registrar</button>
                            </div>
                        </div>

                        <div class="form-group col-lg-12 hide" style="margin-top: 20px;">
                            <label for="dllSubCategoria" class="col-lg-1 texto">Sub Categoria: </label>
                            <asp:DropDownList ID="dllSubCategoria" runat="server" class="form-control combo"></asp:DropDownList>
                        </div>


                       <%-- <div class="form-group col-lg-12" style="margin-top: 20px;">
                            <button id="btnRegistrar" type="button" class="btn btn-primary cz-form-content-input-button btn-mg" style="margin-left: 550px;">Registrar</button>
                        </div>--%>

                    </div>
                </div>
            </div>
             <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true" style="margin-left:-300px;">
        </div>
        </form>
    </div>
</body>
</html>
