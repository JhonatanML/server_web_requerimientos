﻿function alertHtml(tipo, error, titulo) {
    var strHtml = '';
    if (tipo == "delConfirm") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H2">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">' + titulo + '</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="btnDelNo form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">NO</button>'
        strHtml = strHtml + '<button class="btnDelSi form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">SI</button></div>';
    }
    else if (tipo == "delConfirmFalta") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">  Seleccione por lo menos un item.</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "error") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Error</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "cargando") {
        strHtml = '<div class="loading" id="loading"></div>';
        strHtml = strHtml + '<div style="text-align: center;"><span style="color:white;font-size: 16px;">';
        strHtml = strHtml + titulo;
        strHtml = strHtml + '</span></div>';
    }
    else if (tipo == "delConfirmCUSTOM") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H2">Confirmación</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">' + titulo + '</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">NO</button>'
        strHtml = strHtml + '<button class="btnDelSi form-button cz-form-content-input-button" aria-hidden="true">SI</button></div>';
    }
    else if (tipo == "errorCarga") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">' + titulo + '</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "alertValidacion") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;margin-top: 6px!important;margin-left: 7px!important;">' + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    } else if (tipo == "alertDetalleTrabajo") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Detalle de Trabajo</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<p style="text-align: justify;"><strong>Trabajo Realizado:</strong><br/>' + titulo + '</p>';
        strHtml = strHtml + '<p style="text-align: justify;"><strong>Detalle:</strong><br/>' + error + '</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    return strHtml;

}

//$('.chkbox').change(function () { alert("CAJA MARCADA"); });


$(document).on("click", ".chkbox", function () {
    ////alert("CAJA MARCADA");

    $(this).addClass('marcado');
});

function estadoCheck() {

    if ($('#check').is(':checked')) {
        localStorage.setItem("chbactivo", 1);
    } else {
        localStorage.setItem("chbactivo", 2);
    }

}


//SD ACTUALIZAR ESTADO
//SD

//---
/*Elimina Registros*/
function deleteReg() {


    $('.delReg').click(function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover los items seleccionados?"));

            $('#myModal').modal('show');
            $('.btnDelNo').click(function (e) {
                busqueda();
            }
            );

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = new Object();
                strData.codigo = strReg;
                strData.estado = $('#habilitados').val();

                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        addnotify("notify", "Usuarios removidos Correctamente.", "registeruser");
                        busqueda();
                        //$('#buscar').trigger("click");

                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                        $('#myModal').modal('show');
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });












    //SD

    $('.delItemReg').live('click', function (e) {
        var strReg = $(this).attr('cod')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover este usuario?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {

            var strData = new Object();
            strData.codigo = strReg;
            strData.estado = $('#habilitados').val();

            if ($('#habilitados').val() == 1) {

                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        addnotify("notify", "Usuario deshabilitado", "registeruser");
                        busqueda();
                        //$('#buscar').trigger("click");
                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                        $('#myModal').modal('show');
                    }
                });
            }
            else if ($('#habilitados').val() == 2) {
                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        addnotify("notify", "Usuario habilitado", "registeruser");
                        busqueda();
                        //$('#buscar').trigger("click");
                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                        $('#myModal').modal('show');
                    }
                });
            }
        });


    });

    function busqueda() {
        var strData = getParametros();
        strData.pagina = $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });
    }

}

//EDITAR USUARIO

function editUsuario() {
    $('.editUser').click(function (e) {

        var strReg = $(this).attr('cod');
        var strData = new Object();
        strData.codigo = strReg;
        //alert(strData);

        $.ajax({
            type: 'POST',
            url: urleditUser,
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnEditarUsuario').click(function (e) {

                    var validateItems = true;
                    $('#txtLogin').attr('style', '');
                    $('#txtNombre').attr('style', '');
                    $('#dllPerfil').attr('style', '');
                    $('#txtContrasena').attr('style', '');
                    //$('#taTrabajosRea').attr('style', '');
                    //$('#taDetalle').attr('style', '');
                    if ($('#txtLogin').val() == '') {
                        $('#txtLogin').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese login del Usuario", "registeruser");
                        validateItems = false;
                    } else if ($('#txtNombre').val() == '') {
                        $('#txtNombre').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese nombres del Usuario", "registeruser");
                        validateItems = false;
                    }
                    else if ($('#dllPerfil').val() == '-1') {
                        $('#dllPerfil').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese un perfil del Usuario", "registeruser");
                        validateItems = false;
                    }
                        //
                    else if ($('#txtContrasena').val() == '') {
                        $('#txtContrasena').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese contraseña del Usuario", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        //addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");

                        $.ajax({
                            type: 'POST',
                            url: urleditUserDatos,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getUsuario()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnEditarUsuario').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Usuario Actualizado", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('##btnEditarUsuario').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}


/*Agrega Registros*/
function addReg() {
    $('.addReg').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#myModal").html(data);
                $('#myModal').modal('show');

                $('#saveReg').click(function (e) {
                    e.preventDefault();

                    var validateItems = true;

                    $('.requerid').each(function () {
                        $(this).parent().find('span').remove();
                        if ($(this).val() == "") {

                            $(this).after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
                            validateItems = false;
                        }
                    });

                    if (!validateItems) {
                        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
                    }
                    else {
                        $('#divError').attr('class', "alert fade");
                        var hModal = $("#myModal").height();

                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getData()),
                            success: function (data) {
                                $('#myModal').modal('hide');
                                addnotify("notify", "Registrado Correctamente.", "registeruser");
                                busqueda()
                                //$('#buscar').trigger("click");

                            },
                            error: function (xhr, status, error) {
                                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                            }
                        });

                    }
                });

            },
            error: function (xhr, status, error) {

                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');

            }
        });

    });
    function busqueda() {
        var strData = getParametros();
        strData.pagina = $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });
    }


}

function addRegObjetivo() {
    var validateItems = true;

    $('.requerid').each(function () {
        $(this).attr('style', '');
        if ($(this).val() == "") {

            $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            //after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
            validateItems = false;
        }
    });

    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    }
    else {
        $.ajax({
            type: 'POST',
            url: urlsavN,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            data: JSON.stringify(getDatosAdd()),
            beforeSend: function () {
                $("#myModalCargando").html(alertHtml('cargando', '', ''));
                $('#myModalCargando').modal({ backdrop: 'static', keyboard: false, show: true });
            },
            success: function (data) {

                addnotify("notify", "Registrado Correctamente.", "registeruser");
                desHabilitarTxt();
                ocultarBotones();
                $('#btnEditar').removeClass('hide').addClass('show');
                $('#myModalCargando').modal('hide');
            },
            error: function (xhr, status, error) {
                $('#myModalCargando').modal('hide');
                addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
            }
        });
    }
    //            });
    //        } else {
    //            addnotify("alert", "Debe seleccionar PERFIL por favor.", "registeruser");
    //        }
    //    } else {
    //        addnotify("alert", "Debe seleccionar AÑO por favor.", "registeruser");
    //    }
    //} else {
    //    addnotify("alert", "Debe seleccionar MES por favor.", "registeruser");
    //}
}

function addIncidenciaMant() {
    if ($('#txtHoraInicio').val() == '') {
        $('#txtHoraInicio').focus();
        addnotify("notify", "Ingrese Hora de Inicio", "id");
        return;
    }
    if ($('#txtHoraFin').val() == '') {
        $('#txtHoraFin').focus();
        addnotify("notify", "Ingrese Hora Fin", "id");
        return;
    }

    var validateItems = true;

    $('.requerid').each(function () {
        $(this).attr('style', '');
        $(this).parent().find('span').remove();
        if ($(this).val() == "") {
            $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            $(this).after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
            validateItems = false;
        }
    });
    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    } else {
        var flagCheckTecnologia = false;
        $("#checkListTecnologia li.active").each(function (idx, li) {
            flagCheckTecnologia = true;
        });
        if (!flagCheckTecnologia) {

            $('#divContenidoModal').html(alertHtml('delConfirm', "", "No ha seleccionado ninguna Técnologia, ¿Desea continuar?"));
            $('#divMyModal').modal('show');

            $('.btnDelSi').click(function (e) {
                registrarIncidencia();
            });
        } else {
            var flagCheckClientes = false;
            $("#divListClientes li.list-group-item").each(function (idx, li) {
                flagCheckClientes = true;
            });
            if (!flagCheckClientes) {

                $('#divContenidoModal').html(alertHtml('delConfirm', "", "No ha seleccionado ningún cliente, ¿Desea continuar?"));
                $('#divMyModal').modal('show');

                $('.btnDelSi').click(function (e) {
                    registrarIncidencia();
                });
            } else {
                registrarIncidencia();
            }
        }
    }
}

function addIncidenciaNave() {
    if ($('#txtHoraInicio').val() == '') {
        $('#txtHoraInicio').focus();
        addnotify("notify", "Ingrese Fecha & Hora de Inicio", "id");
        return;
    }
    if ($('#txtHoraFin').val() == '') {
        $('#txtHoraFin').focus();
        addnotify("notify", "Ingrese Fecha & Hora Fin", "id");
        return;
    }

    var validateItems = true;

    $('.requerid').each(function () {
        $(this).attr('style', '');
        $(this).parent().find('span').remove();
        if ($(this).val() == "") {
            $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            $(this).after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
            validateItems = false;
        }
    });
    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    } else {
        var flagCheckTecnologia = false;
        $("#checkListTecnologia li.active").each(function (idx, li) {
            flagCheckTecnologia = true;
        });
        if (!flagCheckTecnologia) {

            $('#divContenidoModal').html(alertHtml('delConfirm', "", "No ha seleccionado ninguna Técnologia, ¿Desea continuar?"));
            $('#divMyModal').modal('show');

            $('.btnDelSi').click(function (e) {
                registrarIncidencia();
            });
        } else {
            var flagCheckClientes = false;
            $("#divListClientes li.list-group-item").each(function (idx, li) {
                flagCheckClientes = true;
            });
            if (!flagCheckClientes) {

                $('#divContenidoModal').html(alertHtml('delConfirm', "", "No ha seleccionado ningún cliente, ¿Desea continuar?"));
                $('#divMyModal').modal('show');

                $('.btnDelSi').click(function (e) {
                    registrarIncidencia();
                });
            } else {
                registrarIncidencia();
            }
        }
    }
}

function addIncidenciaDiaria() {
    var validateItems = true;

    $('.requerid').each(function () {
        $(this).attr('style', '');
        $(this).parent().find('span').remove();
        if ($(this).val() == "") {
            $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            $(this).after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
            validateItems = false;
        }
    });
    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    } else {
        registrarIncidencia();

    }
}

function addIncidenciaErrorApp() {
    if ($('#txtFechaRep').val() == '') {
        $('#txtFechaRep').focus();
        addnotify("notify", "Ingrese Fecha de Reporte", "id");
        return;
    }
    if ($('#txtHoraRep').val() == '') {
        $('#txtHoraRep').focus();
        addnotify("notify", "Ingrese Hora de Reporte", "id");
        return;
    }

    var validateItems = true;

    $('.requerid').each(function () {
        $(this).attr('style', '');
        $(this).parent().find('span').remove();
        if ($(this).val() == "") {
            $(this).attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            $(this).after("<span style='COLOR: #b94a48;FONT-SIZE: 10PX;FONT-WEIGHT: BOLD;'>CAMPO OBLIGATORIO</span>");
            validateItems = false;
        }
    });
    if (!validateItems) {
        addnotify("notify", "Ingresar los campos obligatorios", "registeruser");
    } else {
        registrarIncidencia();

    }
}

function registrarIncidencia() {
    $.ajax({
        type: 'POST',
        url: urlsavN,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: false,
        data: JSON.stringify(getParametros()),
        beforeSend: function () {
            $("#divContenidoModal").html(alertHtml('cargando', '', ''));
            $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });
        },
        success: function (data) {
            addnotify("notify", "Registrado Correctamente.", "registeruser");
            document.location.href = "../../Reporte/BuscadorIncidencia/Incidencia.aspx";
        },
        error: function (xhr, status, error) {
            $('#myModalCargando').modal('hide');
            addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
        }
    });
}
function addClienteIncidencia() {
    $('.addRegClienteInc').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlAgregarCliente,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardaCliIn').click(function (e) {
                    e.preventDefault();
                    var validateItems = false;
                    $("#divDesSelec li.active").each(function (idx, li) {
                        validateItems = true;
                    })
                    if (!validateItems) {
                        addnotify("notify", "No hay ningun cliente Agregado.", "id");
                    }
                    else {
                        var idCheck = '';
                        $("#divDesSelec li.active").each(function (idx, li) {
                            idCheck = idCheck + $(li).attr('id') + '?' + $(li).text() + '|';
                        })
                        var strData = new Object();
                        strData.idsClientes = idCheck;
                        $.ajax({
                            type: 'POST',
                            url: urlClientesSession,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(strData),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                $('#btnGuardaCliIn').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Cliente(s) Agregado(s) correctamente.", "registeruser");
                                $('#divListClientes').html(data);
                                $('#divMyModal').modal('hide');
                            },
                            error: function (xhr, status, error) {
                                $('#msg').hide();
                                $('#btnGuardaCliIn').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('show');

            }
        });

    });
}

//SD

function addNewUsuario() {
    $('.addUsuario').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlnewUser,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardarUsuario').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#txtLogin').attr('style', '');
                    $('#txtNombre').attr('style', '');
                    $('#dllPerfil').attr('style', '');
                    $('#txtContrasena').attr('style', '');
                    if ($('#txtLogin').val() == '') {
                        $('#txtLogin').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese login del Usuario", "registeruser");
                        validateItems = false;
                    } else if ($('#txtNombre').val() == '') {
                        $('#txtNombre').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese nombres del Usuario", "registeruser");
                        validateItems = false;
                    }
                    else if ($('#dllPerfil').val() == '-1') {
                        $('#dllPerfil').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese un perfil del Usuario", "registeruser");
                        validateItems = false;
                    }
                    else if ($('#txtContrasena').val() == '') {
                        $('#txtContrasena').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese contraseña del Usuario", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlins,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getUsuario()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardaTrabajoInc').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Usuario agregado correctamente.", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardaTrabajoInc').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}


function addTrabajoIncidencia_Old() {
    $('.addRegTrabajoInc').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlMostrarTrabajo,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardaTrabajoInc').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#taTrabajosRea').attr('style', '');
                    $('#taDetalle').attr('style', '');
                    if ($('#taTrabajosRea').val() == '') {
                        $('#taTrabajosRea').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese trabajos realizados", "registeruser");
                        validateItems = false;
                    } else if ($('#taDetalle').val() == '') {
                        $('#taDetalle').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese detalles", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlGuardarTrabajo,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getTrabajo()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                $('#btnLimpiar').hide();
                                $('#btnGuardaTrabajoInc').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");
                                $('#divRegistroSeg').html(data);
                                $('#divMyModal').modal('hide');
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                $('#btnLimpiar').show();
                                $('#btnGuardaTrabajoInc').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

function updRegObjetivo() {
    $.ajax({
        type: 'POST',
        url: urlsavU,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        cache: false,
        data: JSON.stringify(getDatosUpd()),
        beforeSend: function () {
            $("#myModalCargando").html(alertHtml('cargando', '', ''));
            $('#myModalCargando').modal({ backdrop: 'static', keyboard: false, show: true });
        },
        success: function (data) {
            $('#myModalCargando').modal('hide');
            desHabilitarTxt();
            ocultarBotones();
            $('#btnEditar').removeClass('hide').addClass('show');
            addnotify("notify", "Actualizado Correctamente.", "registeruser");
        },
        error: function (xhr, status, error) {
            $('#myModalCargando').modal('hide');
            addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
        }
    });
}

function updRegObjetivo_OLD() {
    $('.updReg').live('click', function (e) {
        // var strReg = $(this).attr('cod')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Está Ud. Seguro de Actualizar los Registros?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {
            $.ajax({
                type: 'POST',
                url: urlsavU,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(getDatosUpd()),
                beforeSend: function () {
                    $("#myModalCargando").html(alertHtml('cargando', '', ''));
                    $('#myModalCargando').modal({ backdrop: 'static', keyboard: false, show: true });
                },
                success: function (data) {
                    $('#myModalCargando').modal('hide');
                    desHabilitarTxt();
                    ocultarBotones();
                    $('#btnEditar').removeClass('hide').addClass('show');
                    addnotify("notify", "Actualizado Correctamente.", "registeruser");
                },
                error: function (xhr, status, error) {
                    $('#myModalCargando').modal('hide');
                    addnotify("notify", jQuery.parseJSON(xhr.responseText).Message, "registeruser");
                }
            });
        });
    });
}

function modReg() {

    $('.editItemReg').live('click', function (e) {

        var strReg = $(this).attr('cod')
        var strData = new Object();
        strData.codigo = strReg;

        $.ajax({
            type: 'POST',
            url: urlins,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#myModal").html(data);
                $('#myModal').modal('show');

                $('#saveReg').click(function (e) {
                    e.preventDefault();

                    var validateItems = true;
                    $('.requerid').each(function () {
                        $(this).parent().find('span').remove();
                        if ($(this).val() == "") {
                            $(this).after("<span style='color:#b94a48'>*</span>");
                            validateItems = false;
                        }
                    });
                    if (!validateItems) {
                        $("#mensajeError").text("Ingresar los campos obligatorios");
                        $('#divError').attr('class', "alert fade in");
                        $("#tituloMensajeError").text('ADVERTENCIA');
                    }
                    else {
                        $('#divError').attr('class', "alert fade");

                        var hModal = $("#myModal").height();

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getData()),
                            success: function (data) {
                                addnotify("notify", "Actualizado Correctamente.", "registeruser");
                                //$('#divError').attr('class', "alert alert-success");
                                //$("#mensajeError").text("Usuario registrado exitosamente");
                                //$("#tituloMensajeError").text('OK');
                                $('#myModal').modal('hide');
                                busqueda();
                                //$('#buscar').trigger("click");

                            },
                            error: function (xhr, status, error) {
                                $('#divError').attr('class', "alert alert-error");
                                $("#tituloMensajeError").text('ERROR');
                                $("#mensajeError").text(jQuery.parseJSON(xhr.responseText).Message);


                            }
                        });

                    }
                });

            },
            error: function (xhr, status, error) {

                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');

            }
        });

    });
    function busqueda() {
        var strData = getParametros();
        strData.pagina = $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });
    }
}


/*Busqueda*/
function busReg() {

    $("input[id$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
                     .attr('checked', $("input[id$='ChkAll']")
                     .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[id$='ChkAll']").attr("checked", false);
        }
    });



    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}


function busRegReporte() {

    $("input[id$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
                     .attr('checked', $("input[id$='ChkAll']")
                     .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[id$='ChkAll']").attr("checked", false);
        }
    });

    //Buscador
    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function buscarUsuario() {
    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);

            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });

    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function busObjetivos() {
    $('#busqueda').click(function (e) {
        var strData = getParametros();
        if ($('#dllMes').val() != '-1') {
            if ($('#dllAnio').val() != '-1') {
                if ($('#dllPerfil').val() != '-1') {
                    $.ajax({
                        type: 'POST',
                        url: urlBus,
                        contentType: "application/json; charset=utf-8",
                        async: true,
                        cache: false,
                        data: JSON.stringify(strData),
                        beforeSend: function () {
                            $('#divResultado').html('<center><span class="glyphicon glyphicon-hourglass" style="color:#FF6702;" aria-hidden="true"></span><span class="sr-only">Load:</span> Espere un Momento Por Favor</center>');
                            $("#myModalCargando").html(alertHtml('cargando', '', ''));
                            $('#myModalCargando').modal({ backdrop: 'static', keyboard: false, show: true });
                        },
                        success: function (data) {
                            $('#myModalCargando').modal('hide');
                            $('#divResultado').html(data);
                            if ($('#flagEncontro').val() == '1') {
                                desHabilitarTxt();
                                ocultarBotones();
                                $('#btnEditar').removeClass('hide').addClass('show');
                            } else {
                                habilitarTxt();
                                ocultarBotones();
                                $('#btnGuardar').removeClass('hide').addClass('show');
                            }
                        },
                        error: function (xhr, status, error) {
                            $('.form-gridview-search').hide();
                            $('.form-gridview-data').html();
                            $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                            $("#myModal").html(alertHtml('error', xhr.responseText));

                            $('#divError').click(function (e) {
                                $('#myModal').modal('show');
                            });
                        }
                    });
                } else {
                    addnotify("alert", "Debe seleccionar PERFIL por favor.", "registeruser");
                }
            } else {
                addnotify("alert", "Debe seleccionar AÑO por favor.", "registeruser");
            }
        } else {
            addnotify("alert", "Debe seleccionar MES por favor.", "registeruser");
        }
    });
}

function detReg(control, urlD) {
    $(control).live('click', function (e) {
        if (!$(this).parent().hasClass("nofoto") && !$(this).parent().hasClass("nogps")) {
            var strReg = $(this).attr('cod');
            var strData = new Object();
            strData.codigo = strReg
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: urlD,
                data: JSON.stringify(strData),
                beforeSend: function () {
                    $("#myModal").html('');
                },
                success: function (data) {
                    //console.log("return data");
                    $("#myModal").html(data);
                    var modali = 0;
                    $("#myModalContent div table").each(function () {
                        modali = 1;
                    });

                    if (modali == 0 && $this.hasClass("detReg")) {
                        $("#myModalContent div").addClass("cz-util-center-text").html("<p>No se registro ningun detalle.<p>");
                        $("#cz-form-modal-exportar").hide();
                    }

                    //$('#myModal').on('shown', function () {
                    //    google.maps.event.trigger(map, "resize");
                    //});

                    $('#myModal').modal('show');

                },
                error: function (xhr, status, error) {

                    $("#myModal").html(alertHtml('error', xhr.responseText));
                    $('#myModal').modal('show');

                }
            });
        }
    });
}

function detRegCustom(control, urlD) {
    $(control).live('click', function (e) {
        if (!$(this).parent().hasClass("nofoto") && !$(this).parent().hasClass("nogps")) {
            var strReg = $(this).attr('cod');
            var strData = new Object();
            strData.codigo = strReg
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: urlD,
                data: JSON.stringify(strData),
                beforeSend: function () {
                    $("#myModal2").html('');
                },
                success: function (data) {
                    //console.log("return data");
                    $("#myModal2").html(data);
                    var modali = 0;
                    $("#myModalContent div table").each(function () {
                        modali = 1;
                    });

                    if (modali == 0 && $this.hasClass("detReg")) {
                        $("#myModalContent div").addClass("cz-util-center-text").html("<p>No se registro ningun detalle.<p>");
                        $("#cz-form-modal-exportar").hide();
                    }

                    //$('#myModal2').on('shown', function () {
                    //    google.maps.event.trigger(map, "resize");
                    //});

                    $('#myModal2').modal('show');

                },
                error: function (xhr, status, error) {

                    $("#myModal2").html(alertHtml('error', xhr.responseText));
                    $('#myModal2').modal('show');

                }
            });
        }
    });
}


function detReg2(control, urlD) {



    $(control).live('click', function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll") {
                strReg = strReg + this.value + "|";
            }
        });

        if (strReg != "") {
            var strData = new Object();
            strData.codigo = strReg;
            $.ajax({
                type: 'POST',
                url: urlD,
                data: JSON.stringify(strData),
                beforeSend: function () {
                    $("#myModal").html('');
                },
                success: function (data) {
                    $("#myModal").html(data);

                    $('#myModal').on('shown', function () {
                        google.maps.event.trigger(map, "resize");
                    });

                    $('#myModal').modal('show');

                },
                error: function (xhr, status, error) {

                    $("#myModal").html(alertHtml('error', xhr.responseText));
                    $('#myModal').modal('show');

                }
            });
        } else {
            $("#myModal").html(alertHtml('erroCheck'));
            $('#myModal').modal('show');
        }
    });
}

function exportarReg(control, urlXLS, grilla) {
    $(control).live('click', function (e) {
        var filas = getNumRows(grilla);
        if (filas > 0) {
            window.location.href = urlXLS + '?' + getParametrosXLS();
        }
        else {
            $('#myModal').html(alertHtml('alertValidacion', "No existe data para exportar"));
            $('#myModal').modal('show');

        }
    });
}

function exportarRegDet(control, urlXLS, grilla, parameters) {
    $(control).live('click', function (e) {
        var filas = getNumRows(grilla);
        if (filas > 0) {
            window.location.href = urlXLS + '?' + parameters;
        }
        else {
            $('#myModal').html(alertHtml('alertValidacion', "No existe data para exportar"));
            $('#myModal').modal('show');
        }
    });
}

function getNumRows(grilla) {
    var numRows = "";
    numRows = $(grilla).find('tr').length;
    return numRows;
}

function cargacomboA(Padre, Hijo, pag) //funcion encargada de manejar combos anidados sin la necesidad de realizar algun postback
{
    console.log("cargacomboA");
    $(document).ready(function () {
        //elegido=$(Padre).val();
        elegido = Padre;

        $.post(pag, { elegido: elegido }, function (data) {
            $(Hijo).html(data);
        });
        console.log("a-" + Hijo);
        $(Hijo).change();
    });
}

/*CARGA*/
function accionesCarga() {


    $('.btnError').live('click', function (e) {
        e.preventDefault();

        tit = $(this).attr("idC");
        $("#myModal").html(alertHtml("errorCarga", $(this).parent().find('.errmsg').html(), tit));
        $('#myModal').modal('show');

    });


    $('#buscar').live("click", function (e) {
        e.preventDefault();
        $('.form-box').show();
        $('#divGridViewData').html('');
        $('.divUploadNew').hide();

    });


    var bex = -1;
    var bey = 0;
    var beerror = 0;

    $('.boton-ejecutar').live("click", function (e) {
        e.preventDefault();
        bex = -1;
        bey = 0;
        beerror = 0;

        $.ajax({
            type: "POST",
            url: "CargaDatos.aspx/ejecutarArchivoDTS",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            beforeSend: function () {
                $("#myModal").html('<div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> ×</button> <h3 id="myModalLabel"> Cargando</h3> </div> <div id="myModalContent" class="modal-body"> <div class="cz-util-center-text"><p>Ejecutando...</br>Espere un momento por favor...</p></div> </div> <div class="modal-footer"> </div>');
                $('#myModal').modal('show');
            },
            success: function (msg) {
                $('#myModal').modal('hide');
                $('#file-uploader').find('.qq-upload-list li').remove();
                $('.boton-ejecutar').hide();
                $('#divGridViewData').html(msg.d);
                $('.form-box').hide();
                $('.divUploadNew').show();
                $("#myModal").html("");
                $("#divGridViewData table tr").each(function () {
                    bex++;
                    if (bex < $("#divGridViewData table tr").length) {
                        if (bex != 0) {
                            while (bey < $(this).find("td").length) {
                                console.log("fila y columna:" + bex + "-" + bey);
                                if (bey == 1) {
                                    var detalle_error = $(this).find(":nth-child(" + (bey + 1) + ") .errmsg").html();
                                    if (detalle_error.length > 0) {
                                        beerror++;
                                        $("#myModal").html($("#myModal").html() + "<br>" + $(this).find(":nth-child(" + (bey) + ")").html() + ": 1");
                                    }
                                }
                                bey++;
                            }
                            bey = 0;
                        }
                    }
                });

                //if (beerror > 0) {
                //    $("#myModal").html('<title> </title> <form method="post" action="repDet.aspx" id="form1"> </div> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> ×</button> <h3 id="myModalLabel"> Detalle</h3> </div> <div id="myModalContent" class="modal-body"> <div class="cz-util-center-text"><p>Se ah encontrado errores de carga en las filas: ' + $("#myModal").html() + '</p></div> </div> <div class="modal-footer"> </div> </form> ');
                //    $('#myModal').modal('show');
                //}
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml("error", jQuery.parseJSON(xhr.responseText).Message));
                $('#myModal').modal('show');
            }
        });
    });



    $('.boton-borrar').live("click", function (e) {
        e.preventDefault();
        var listFiles = $('#file-uploader').find('.qq-upload-list li');

        var idclass = $(this).attr("idclass");
        var filename = $(this).attr("filename");
        var error = $(this).attr("error");

        var name = filename.substring(0, filename.lastIndexOf('.'));
        var ext = filename.substr(filename.lastIndexOf('.') + 1);

        var jsonFileName = name;
        var jsonFileExt = ext;

        listFiles.find("#fri-" + idclass).remove();
        if (error == "si")
        { listFiles.find("#fbd-" + idclass).append("<div class='file-load-icon' id='fld-" + idclass + "'><img src='../images/icons/loader/ico_loader_red.gif' /></div>"); }
        else
        { listFiles.find("#fbd-" + idclass).append("<div class='file-load-icon' id='fld-" + idclass + "'><img src='../images/icons/loader/ico_loader_lightorange.gif' /></div>"); }

        var strData = new Object();
        strData.filename = jsonFileName;
        strData.fileext = jsonFileExt;

        $.ajax({
            type: "POST",
            url: "CargaDatos.aspx/borrarArchivo",
            data: JSON.stringify(strData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            cache: false,
            success: function (msg) {
                $('.cz-form-step-examinar').show();
                listFiles.find("#fld-" + idclass).parent().parent().parent().parent().remove();
                if (listFiles.length > 1) {
                    $('.boton-ejecutar').show();
                }
                else {
                    $('.boton-ejecutar').hide();
                }
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                listFiles.find("#fld-" + idclass).remove();
                listFiles.find("#fbd-" + idclass).addClass("file-red");
                listFiles.find("#fbd-" + idclass).append("<div class='file-result-icon'><img src='../images/icons/carga/ico_carga_error.png' /></div>");
                listFiles.find("#fbx-" + idclass).append("<div class='file-result-text file-red-font' id='frs-" + idclass + "'>" + err.Message + "</div>");
            }
        });
    });
}

function cargaSup(control) {
    var strperfil = $('#MddlPerfil option:selected').val();
    if (strperfil == 'V') {
        $('.filsup').show();
    }
    else {
        $('.filsup').hide();
    }
}

function cargaSup2() {
    var strperfil = $('#vddlPerfil option:selected').val();
    if (strperfil == 'V') {
        $('.filsup2').show();
    }
    else {
        $('.filsup2').hide();
    }
}
//personalizacion


