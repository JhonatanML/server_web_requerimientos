﻿function alertHtml(tipo, error, titulo) {
    var strHtml = '';
    if (tipo == "delConfirm") {
        strHtml = '<div class="modal-header" style="">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H2">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white;">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">' + titulo + '</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5;">';
        strHtml = strHtml + '<button class="btnDelNo form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">NO</button>'
        strHtml = strHtml + '<button class="btnDelSi form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">SI</button></div>';
    }
    else if (tipo == "delConfirmFalta") {
        strHtml = '<div class="modal-header" style="">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white;">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important; align:center;">  Seleccione por lo menos un item.</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5;">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "error") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Error</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "cargando") {
        strHtml = '<div class="loading" id="loading"></div>';
        strHtml = strHtml + '<div style="text-align: center;"><span style="color:white;font-size: 16px;">';
        strHtml = strHtml + titulo;
        strHtml = strHtml + '</span></div>';
    }
    else if (tipo == "delConfirmCUSTOM") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H2">Confirmación</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;line-height: 32px;margin-left: 10px!important;">' + titulo + '</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true" style="margin-right: 10px;">NO</button>'
        strHtml = strHtml + '<button class="btnDelSi form-button cz-form-content-input-button" aria-hidden="true">SI</button></div>';
    }
    else if (tipo == "errorCarga") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">' + titulo + '</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    else if (tipo == "alertValidacion") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Alerta</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<img src="../../images/alert/ico_alert.png" style="float: left;height: 32px;"/> <p style="float: left;margin-top: 6px!important;margin-left: 7px!important;">' + error + '</div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    } else if (tipo == "alertDetalleTrabajo") {
        strHtml = '<div class="modal-header">';
        strHtml = strHtml + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
        strHtml = strHtml + '<h3 id="H1">Detalle de Trabajo</h3></div>';
        strHtml = strHtml + '<div class="modal-body" style="background:white">';
        strHtml = strHtml + '<p style="text-align: justify;"><strong>Trabajo Realizado:</strong><br/>' + titulo + '</p>';
        strHtml = strHtml + '<p style="text-align: justify;"><strong>Detalle:</strong><br/>' + error + '</p></div>';
        strHtml = strHtml + '<div class="modal-footer" style="background:#e5e5e5">';
        strHtml = strHtml + '<button class="form-button cz-form-content-input-button" data-dismiss="modal" aria-hidden="true">CERRAR</button></div>';
    }
    return strHtml;

}

function GridReporteBusqueda() {
    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.pagina = "1";
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);

            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });

    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.pagina = parseInt(strData.pagina) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.pagina);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function exportarReg(control, urlXLS, grilla) {
    $(control).live('click', function (e) {
        var filas = getNumRows(grilla);
        if (filas > 0) {
            window.location.href = urlXLS + '?' + getParametrosXLS();
        }
        else {
            $('#myModal').html(alertHtml('alertValidacion', "No existe data para exportar"));
            $('#myModal').modal('show');

        }
    });
}

function exportarRegDet(control, urlXLS, grilla, parameters) {
    $(control).live('click', function (e) {
        var filas = getNumRows(grilla);
        if (filas > 0) {
            window.location.href = urlXLS + '?' + parameters;
        }
        else {
            alert('No existe data para exportar');
        }
    });
}



function detRepBusqueda() {

    $('.detReg').live('click', function (e) {
        //alert('gg');
        var codigo = $(this).attr('cod');
        var xDesde = $('#hdnFecIni').val();
        var sHasta = $('#hdnFecFin').val();
        var xusuario = $(this).attr('usu');
        var xcantidad = $(this).attr('cant');
        var xsucursal = $(this).attr('suc');

        var strData = new Object();
        strData.codigo = codigo;
        strData.fdesde = xDesde;
        strData.fhasta = sHasta;
        strData.xusuario = xusuario;
        strData.xcantidad = xcantidad;
        strData.xsucursal = xsucursal;

        $.ajax({
            type: 'POST',
            url: urlModalDet,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $("#myModal").html('');
            },
            success: function (data) {
                $("#myModal").html(data);

                $('#myModal').on('shown', function () {

                    //google.maps.event.trigger(map, "resize");


                });

                $('#myModal').modal('show');

            },
            error: function (xhr, status, error) {

                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');

            }
        });

    });
}

function addReg() {
    $('.addUsuario').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardarUsuario').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#nCodigo').attr('style', '');
                    $('#nDescripcion').attr('style', '');
                    $('#nClave').attr('style', '');
                    $('#dllNSucursal').attr('style', '');
                    if ($('#nCodigo').val() == '') {
                        $('#nCodigo').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese un código", "registeruser");
                        validateItems = false;
                    } else if ($('#nDescripcion').val() == '') {
                        $('#nDescripcion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese una Descripción", "registeruser");
                        validateItems = false;
                    }
                    else if ($('#nClave').val() == '') {
                        $('#nClave').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese Clave", "registeruser");
                        validateItems = false;
                    }
                    else if ($('#dllNSucursal').val() == '') {
                        $('#txtContrasena').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese Sucursal", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getUsuario()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardarUsuario').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Usuario creado correctamente.", "registeruser");
                                //$('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardarUsuario').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

function modReg() {
    $('.editItemReg').live('click', function (e) {
        var strReg = $(this).attr('cod');
        var strData = new Object();
        strData.codigo = strReg;
        //alert(strData);

        $.ajax({
            type: 'POST',
            url: urlins,
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardarUsuario').click(function (e) {

                    var validateItems = true;
                    $('#nCodigo').attr('style', '');
                    $('#nDescripcion').attr('style', '');
                    $('#nClave').attr('style', '');
                    $('#dllNSucursal').attr('style', '');
                    if ($('#nCodigo').val() == '') {
                        $('#nCodigo').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese un código", "registeruser");
                        validateItems = false;
                    } else if ($('#nDescripcion').val() == '') {
                        $('#nDescripcion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese una Descripción", "registeruser");
                        validateItems = false;
                    }
                    else if ($('#nClave').val() == '') {
                        $('#nClave').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese Clave", "registeruser");
                        validateItems = false;
                    }
                    else if ($('#dllNSucursal').val() == '') {
                        $('#txtContrasena').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese Sucursal", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        //addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getUsuario()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardarUsuario').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Usuario Actualizado", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('##btnGuardarUsuario').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

function deleteUser() {

    $('.delReg').click(function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover los items seleccionados?"));
            $('#myModal').modal('show');

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = jQuery.parseJSON('{ "codigos": "' + strReg + '" }');


                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#buscar').trigger("click");

                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });

    $('.delItemReg').live('click', function (e) {



        var strReg = $(this).attr('cod')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover el item seleccionado?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {


            var strData = jQuery.parseJSON('{ "codigos": "' + strReg + '" }');


            $.ajax({
                type: 'POST',
                url: urldel,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");

                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                }
            });
        });

    });


}

//SUCURSAL

function addRegSuc() {
    $('.addUsuario').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardarUsuario').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#nCodigo').attr('style', '');
                    $('#nDescripcion').attr('style', '');
                    if ($('#nCodigo').val() == '') {
                        $('#nCodigo').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese un código", "registeruser");
                        validateItems = false;
                    } else if ($('#nDescripcion').val() == '') {
                        $('#nDescripcion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese una Descripción", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getUsuario()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardarUsuario').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Sucursal creada correctamente.", "registeruser");
                                //$('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardarUsuario').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

function modRegSuc() {
    $('.editItemReg').live('click', function (e) {
        var strReg = $(this).attr('cod');
        var strData = new Object();
        strData.codigo = strReg;
        //alert(strData);

        $.ajax({
            type: 'POST',
            url: urlins,
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardarUsuario').click(function (e) {

                    var validateItems = true;
                    $('#nCodigo').attr('style', '');
                    $('#nDescripcion').attr('style', '');
                    if ($('#nCodigo').val() == '') {
                        $('#nCodigo').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese un código", "registeruser");
                        validateItems = false;
                    } else if ($('#nDescripcion').val() == '') {
                        $('#nDescripcion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese una Descripción", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        //addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getUsuario()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardarUsuario').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Sucursal Actualizada", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('##btnGuardarUsuario').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

function descargaDetReg(control, urlXLS, urlXLSfind) {
    $(control).live('click', function (e) {
        $.ajax({
            type: 'GET',
            url: urlXLS + '?' + getParametrosXLS(),
            //contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            //dataType: "json",
            async: true,
            cache: false,
            //data: getParametrosXLS(),
            beforeSend: function () {
                //alert("GG");<div id="carga" class="cz-util-center-text"></div> 
                $("#myModal").html('<div class="modal-header"> <h3 id="myModalLabel">Procesando</h3> </div> <div id="myModalContent" class="modal-body cz-util-center-text"><p>Procesando...</br>Espere un momento por favor...</p></div> <div class="modal-footer"> </div>');
                $('#myModal').modal({ backdrop: 'static', show: true });
            },
            success: function (data) {
                $('#myModal').modal('hide');
                var obj = JSON.parse(data);
                if (obj.status == "ok") {
                    window.location.href = urlXLSfind + '?f=' + obj.f;
                } else {
                    addnotify("notify", "Error al generar archivo: " + obj.f, "registeruser");
                }
            },
            error: function (xhr, status, error) {
                $('#myModal').modal('hide');
                addnotify("notify", "Se ah Generado un Error en el Servidor ", "registeruser");
            }
        });
        //window.location.href = urlXLS + '?' + getParametrosXLS();
    });
}


// | MODULO RESERVA || MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA |
// | MODULO RESERVA || MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA | MODULO RESERVA |

/*ELEGIR RESERVAS*/
function ElegirLocal() {

    $('#elegir').click(function () {

        $.ajax({
            type: 'POST',
            url: urleleg,
            success: function (data) {
                $("#myModal").html(data);
                $('#myModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnElegirLocal').click(function (e) {
                    e.preventDefault();
                    if ($('#dllLocal').val() != -1) {
                        $.ajax({
                            type: 'POST',
                            url: urlbus,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getParametros()),
                            beforeSend: function () {
                                $('.form-gridview-data').html('');
                                $('.form-gridview-error').html('');
                                $('.form-gridview-search').show();
                            },
                            success: function (data) {
                                //console.log('data_: ' + data);
                                $('#myModal').modal('hide');
                                $('.form-gridview-search').hide();
                                $('.form-gridview-error').html('');
                                $('.form-gridview-data').html(data);
                            },
                            error: function (xhr, status, error) {
                                $('.form-gridview-search').hide();
                                $('.form-gridview-data').html();
                                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                                $("#myModal").html(alertHtml('error', xhr.responseText));

                                $('#divError').click(function (e) {
                                    $('#myModal').modal('show');
                                });
                            }
                        });
                    } else {
                        addnotify("notify", "¡Elija un local!");
                    }
                    //$('#myModal').modal('hide');
                });
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('hide');
            }
        });
    });
}

/*BUSQUEDA RESERVAS*/
function BuscarReserva() {

    $('#buscar').click(function (e) {
        //cargando la grilla
        if ($('#dllLocal').val() != -1) {
            var strData = getParametros();
            $.ajax({
                type: 'POST',
                url: urlbus,
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                beforeSend: function () {
                    $('.form-gridview-data').html('');
                    $('.form-gridview-error').html('');
                    $('.form-gridview-search').show();
                },
                success: function (data) {
                    //console.log('data_: ' + data);
                    $('.form-gridview-search').hide();
                    $('.form-gridview-error').html('');
                    $('.form-gridview-data').html(data);
                    $('#hdnActualPage').val(strData.pagina);
                },
                error: function (xhr, status, error) {
                    $('.form-gridview-search').hide();
                    $('.form-gridview-data').html();
                    $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                    $("#myModal").html(alertHtml('error', xhr.responseText));

                    $('#divError').click(function (e) {
                        $('#myModal').modal('show');
                    });
                }
            });
        } else {

            addnotify("notify", "¡Elija un local!");
        }

    });
}

/*REGISTRO RESERVAS*/
function RegistrarReserva(fecha_reserva, hora_reserva) {

    var fecha_reserva = fecha_reserva;
    var hora_reserva = hora_reserva;

    var strData = getLocal();
    strData.fecha_reserva = fecha_reserva;
    strData.hora_reserva = hora_reserva;
    $.ajax({
        type: 'POST',
        url: urlins,
        async: true,
        cache: false,
        data: JSON.stringify(strData),
        success: function (data) {
            $("#divContenidoModal").html(data);

            $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });
            $('#divMyModal').on('shown', function () {
                $('body').css('overflow', 'hidden');
            }).on('hidden', function () {
                $('body').css('overflow', 'auto');
            });

            $('#btnGuardarReserva').click(function (e) {
                e.preventDefault();
                var validateItems = true;
                $('#nTituloEvento').attr('style', '');
                $('#nFechaInicio').attr('style', '');
                $('#nHoraInicio').attr('style', '');
                $('#nHoraTermino').attr('style', '');
                $('#nInvitados').attr('style', '');
                $('#nDetalleReserva').attr('style', '');

                if ($('#nTituloEvento').val() == '') {
                    $('#nTituloEvento').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese un título para el evento!");
                    validateItems = false;
                } else if ($('#nFechaInicio').val() == '') {
                    $('#nFechaInicio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese la fecha de inicio!");
                    validateItems = false;
                } else if ($('#nHoraInicio').val() == '') {
                    $('#nHoraInicio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese la hora de inicio!");
                    validateItems = false;
                } else if ($('#nHoraTermino').val() == '') {
                    $('#nHoraTermino').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese la hora de termino!");
                    validateItems = false;
                } else if ($('#nInvitados').val() == '') {
                    $('#nInvitados').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "Ingrese el E-mail de los invitados");
                    validateItems = false;
                } else if ($('#nDetalleReserva').val() == '') {
                    $('#nDetalleReserva').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese el detalle del evento!");
                    validateItems = false;
                }

                if (validateItems) {
                    $.ajax({
                        type: 'POST',
                        url: urlsavN,
                        contentType: "application/json; charset=utf-8",
                        async: true,
                        cache: false,
                        data: JSON.stringify(getReserva()),
                        beforeSend: function () {
                            $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                            //$('#btnLimpiar').hide();
                            $('#btnGuardarReserva').hide();
                            $('#btnCancelar').hide();
                        },
                        success: function (data) {

                            if ((data.d) == '1') {
                                addnotify("notify", "¡La reserva ha sido registrada correctamente!");
                            } else {
                                addnotify("notify", "¡Hay un cruce de horario con una sala ya reservada!");
                            }
                            //$('#divContenidoModal').html(data);
                            $('#divMyModal').modal('hide');
                            $('body').css('overflow', 'auto');
                            $('#buscar').trigger("click");
                        },
                        error: function (xhr, status, error) {
                            $('#msg').html('');
                            //$('#btnLimpiar').show();
                            $('#btnGuardarReserva').show();
                            $('#btnCancelar').show();
                            addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.");
                        }
                    });
                }
            });

        },
        error: function (xhr, status, error) {
            $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
            $('#divMyModal').modal('hide');
            $('body').css('overflow', 'auto');

        }
    });
}

/*EDICION RESERVAS*/
function EditarReserva(cod_reserva) {

    var cod_reserva = cod_reserva;

    var strData = getLocal();
    strData.cod_reserva = cod_reserva;

    $.ajax({
        type: 'POST',
        url: urledit,
        async: true,
        cache: false,
        data: JSON.stringify(strData),
        success: function (data) {
            $("#divContenidoModal").html(data);
            $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });
            $('#divMyModal').on('shown', function () {
                $('body').css('overflow', 'hidden');
            }).on('hidden', function () {
                $('body').css('overflow', 'auto');
            });

            $('#btnEditarReserva').click(function (e) {
                e.preventDefault();
                var validateItems = true;

                $('#nTituloEvento').attr('style', '');
                $('#nFechaInicio').attr('style', '');
                $('#nHoraInicio').attr('style', '');
                $('#nHoraTermino').attr('style', '');
                $('#nInvitados').attr('style', '');
                $('#nDetalleReserva').attr('style', '');

                if ($('#nTituloEvento').val() == '') {
                    $('#nTituloEvento').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese un título para el evento!");
                    validateItems = false;
                } else if ($('#nFechaInicio').val() == '') {
                    $('#nFechaInicio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese la fecha de inicio!");
                    validateItems = false;
                } else if ($('#nHoraInicio').val() == '') {
                    $('#nHoraInicio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese la hora de inicio!");
                    validateItems = false;
                } else if ($('#nHoraTermino').val() == '') {
                    $('#nHoraTermino').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese la hora de termino!");
                    validateItems = false;
                } else if ($('#nInvitados').val() == '') {
                    $('#nInvitados').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "Ingrese el E-mail de los invitados");
                    validateItems = false;
                } else if ($('#nDetalleReserva').val() == '') {
                    $('#nDetalleReserva').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese el detalle del evento!");
                    validateItems = false;
                }

                if (validateItems) {
                    $.ajax({
                        type: 'POST',
                        url: urlsavE,
                        contentType: "application/json; charset=utf-8",
                        async: true,
                        cache: false,
                        data: JSON.stringify(getReserva()),
                        beforeSend: function () {
                            $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                            //$('#btnLimpiar').hide();
                            $('#btnEditarReserva').hide();
                            $('#btnCancelar').hide();
                        },
                        success: function (data) {
                            addnotify("notify", data.d);
                            //$('#divContenidoModal').html(data);
                            $('#divMyModal').modal('hide');
                            $('body').css('overflow', 'auto');
                            $('#buscar').trigger("click");
                        },
                        error: function (xhr, status, error) {
                            $('#msg').html('');
                            //$('#btnLimpiar').show();
                            $('#btnEditarReserva').show();
                            $('#btnCancelar').show();
                            addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.");
                        }
                    });
                }
            });

        },
        error: function (xhr, status, error) {
            $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
            $('#divMyModal').modal('hide');
            $('body').css('overflow', 'auto');

        }
    });
}

/*BUSQUEDA RESERVAS (GRID)*/
function BuscarRegistroReservas() {

    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.page = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

//MOSTRAR DETALLE RESERVA
function MostrarDetalleReserva() {
    $('.MostrarDetalle').live('click', function (e) {
        var strReg = $(this).attr('cod_reserva');
        var strData = new Object();
        strData.cod_reserva = strReg;
        //alert(strData);

        $.ajax({
            type: 'POST',
            url: urlbusdet,
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

// | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO |
// | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO | MODULO MANTENIMIENTO |

//BUSQUEDA LOCALES (GRID)
function busRegCon() {

    $("input[cod_local$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
                     .attr('checked', $("input[cod_local$='ChkAll']")
                     .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[cod_local$='ChkAll']").attr("checked", false);
        }
    });

    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.page = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

//AGREGAR LOCAL
function AgregarLocal() {
    $('.addUsuario').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardarLocal').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#nNombreLocal').attr('style', '');
                    $('#nDireccion').attr('style', '');
                    $('#nReferencia').attr('style', '');
                    $('#nTelfFijo').attr('style', '');
                    $('#nTelfMovil').attr('style', '');

                    if ($('#nNombreLocal').val() == '') {
                        $('#nNombreLocal').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese nombre del Local", "registeruser");
                        validateItems = false;
                    } else if ($('#nDireccion').val() == '') {
                        $('#nDireccion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese dirección del local", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getLocal()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardarLocal').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Local creada correctamente.", "registeruser");
                                //$('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardarLocal').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//MODIFICAR LOCAL
function ModificarLocal() {
    $('.editItemReg').live('click', function (e) {
        var strReg = $(this).attr('cod_local');
        var strData = new Object();
        strData.cod_local = strReg;
        //alert(strData);

        $.ajax({
            type: 'POST',
            url: urlins,
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardarLocal').click(function (e) {

                    var validateItems = true;

                    $('#nNombreLocal').attr('style', '');
                    $('#nDireccion').attr('style', '');
                    $('#nReferencia').attr('style', '');
                    $('#nTelfFijo').attr('style', '');
                    $('#nTelfMovil').attr('style', '');

                    if ($('#nNombreLocal').val() == '') {
                        $('#nNombreLocal').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese nombre del Local", "registeruser");
                        validateItems = false;
                    } else if ($('#nDireccion').val() == '') {
                        $('#nDireccion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese dirección del local", "registeruser");
                        validateItems = false;
                    }

                    if (validateItems) {
                        //addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getLocal()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardarLocal').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Local Actualizado", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardarLocal').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//function estadoCheck() {

//    if ($('#check').is(':checked')) {
//        localStorage.setItem("chbactivo", 1);
//    } else {
//        localStorage.setItem("chbactivo", 2);
//    }

//}

//ELIMINAR LOCAL
function EliminarLocal() {

    $('.delReg').click(function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover los items seleccionados?"));
            $('#myModal').modal('show');

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = jQuery.parseJSON('{ "cod_local": "' + strReg + '" }');


                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#buscar').trigger("click");

                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });

    $('.delItemReg').live('click', function (e) {



        var strReg = $(this).attr('cod_local')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover el item seleccionado?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {


            var strData = jQuery.parseJSON('{ "cod_local": "' + strReg + '" }');


            $.ajax({
                type: 'POST',
                url: urldel,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");

                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                }
            });
        });

    });


}

//BUSQUEDA SALAS (GRID)
function BuscarRegistroSala() {

    $("input[cod_sala$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
                     .attr('checked', $("input[cod_sala$='ChkAll']")
                     .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[cod_sala$='ChkAll']").attr("checked", false);
        }
    });

    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.page = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

//AGREGAR SALA
function AgregarSala() {
    $('.addUsuario').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardar').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#nNombreLocal').attr('style', '');
                    $('#nDireccion').attr('style', '');
                    $('#nReferencia').attr('style', '');
                    $('#nTelfFijo').attr('style', '');
                    $('#nTelfMovil').attr('style', '');

                    if ($('#nNombreLocal').val() == '') {
                        $('#nNombreLocal').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese nombre del Local", "registeruser");
                        validateItems = false;
                    } else if ($('#nDireccion').val() == '') {
                        $('#nDireccion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese dirección del local", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getSala()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardar').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Local creada correctamente.", "registeruser");
                                //$('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardar').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//MODIFICAR SALA
function ModificarSala() {
    $('.editItemReg').live('click', function (e) {
        var strReg = $(this).attr('cod_sala');
        var strData = new Object();
        strData.cod_sala = strReg;

        $.ajax({
            type: 'POST',
            url: urlins,
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardar').click(function (e) {

                    var validateItems = true;

                    $('#nNombreLocal').attr('style', '');
                    $('#nDireccion').attr('style', '');
                    $('#nReferencia').attr('style', '');
                    $('#nTelfFijo').attr('style', '');
                    $('#nTelfMovil').attr('style', '');

                    if ($('#nNombreLocal').val() == '') {
                        $('#nNombreLocal').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese nombre del Local", "registeruser");
                        validateItems = false;
                    } else if ($('#nDireccion').val() == '') {
                        $('#nDireccion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese dirección del local", "registeruser");
                        validateItems = false;
                    }

                    if (validateItems) {
                        //addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getSala()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardar').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Sala Actualizada", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardar').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//ELIMINAR SALA
function EliminarSala() {

    $('.delReg').click(function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover los items seleccionados?"));
            $('#myModal').modal('show');

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = jQuery.parseJSON('{ "cod_sala": "' + strReg + '" }');


                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#buscar').trigger("click");

                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });

    $('.delItemReg').live('click', function (e) {



        var strReg = $(this).attr('cod_sala')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover el item seleccionado?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {


            var strData = jQuery.parseJSON('{ "cod_sala": "' + strReg + '" }');


            $.ajax({
                type: 'POST',
                url: urldel,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");

                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                }
            });
        });

    });


}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//BUSQUEDA REQUERIMIENTOS (GRID)
function ListarRequerimientos() {

    $("input[cod_requerimiento$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
                     .attr('checked', $("input[cod_requerimiento$='ChkAll']")
                     .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[cod_requerimiento$='ChkAll']").attr("checked", false);
        }
    });

    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.page = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

//REGISTRO DE REQUERIMIENTO
function RegistroRequerimiento() {

    $('#btnRegistrar').click(function (e) {
        e.preventDefault();

        var validateItems = true;
        $('#listServicios').attr('style', '');
        $('#Detalle').attr('style', '');
        $('#dllPrioridad').attr('style', '');
        //        alert($('#listServicios').val());
        if ($('#listServicios').val() == null) {
            $('#listServicios').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            addnotify("notify", "Elija un servicio", "registeruser");
            validateItems = false;
        } else if ($('#Detalle').val() == '') {
            $('#Detalle').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            addnotify("notify", "Ingrese el detalle del requerimiento", "registeruser");
            validateItems = false;
        } else if ($('#dllPrioridad').val() == '-1') {
            $('#dllPrioridad').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
            addnotify("notify", "Elija una prioridad", "registeruser");
            validateItems = false;
        }

        var strData = new Object();
        strData.fecha_requerimiento = $('#FechaGenerada').val();
        strData.cod_categoria = $('#dllCategoria').val();
        var servicio = $('#listServicios').val();
        var cadenaServicio = servicio.split("|");
        strData.codigoServicio = cadenaServicio[0];
        strData.tipoServicio = cadenaServicio[1];

        //strData.cod_subcategoria = $('#dllSubCategoria').val();
        //strData.descripcion = $('#Descripcion').val();
        strData.detalle = $('#Detalle').val();
        strData.cod_prioridad = $('#dllPrioridad').val();
        strData.id_perfil = $('#hdnPerfilUsuario').val();
        strData.id_usuario = $('#hdnUsuario').val();

        if (validateItems) {
            $.ajax({
                type: 'POST',
                url: urlsavN,
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                beforeSend: function () {

                    $("#myModal").html('<div class="modal-header"><h3 id="myModalLabel">Procesando</h3> </div> <div id="myModalContent" class="modal-body"> <div id="carga" class="cz-util-center-text"></div> </div> <div class="modal-footer"> </div>');

                    $('#myModal').modal({ backdrop: 'static', show: true });

                    $('#carga').html('<p>Espere un momento por favor...</p>');
                },
                success: function (data) {
                    $('#NombreServicio').val('');
                    $('#listServicios').val('');
                    $('#dllCategoria').val('');
                    $('#Detalle').val('');
                    $('#dllPrioridad').val('-1');
                    //if ((data.d) == '1') {
                    addnotify("notify", "¡Su requerimiento ha sido registrado correctamente!");
                    //}
                    //$('#btnRegistrar').prop("disabled", false);
                    $('#myModal').modal('hide');
                    $('body').css('overflow', 'auto');
                },
                error: function (xhr, status, error) {
                    $('#msg').html('');
                    //$('#btnLimpiar').show();
                    $('#btnRegistrar').show();
                    $('#btnCancelar').show();
                    addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.");
                }
            });
        }
    });
}

//MOSTRAR DETALLE REQUERIMIENTO
function MostrarDetalleRequerimiento() {
    $('.MostrarDetalle').live('click', function (e) {
        var strReg = $(this).attr('cod_requerimiento');
        var strRega = $(this).attr('cod_estado_requerimiento');
        var strData = new Object();
        strData.cod_requerimiento = strReg;
        strData.cod_estado_requerimiento = strRega;
        //alert(strData);
        if (strData.cod_estado_requerimiento == 1) {
            addnotify("notify", "¡El requerimiento aún esta pendiente!");
        } else {
            $.ajax({
                type: 'POST',
                url: urlbusdet,
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $("#divContenidoModal").html(data);
                    $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                },
                error: function (xhr, status, error) {
                    $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                    $('#divMyModal').modal('hide');
                }
            });
        }
    });
}

function AprobarRequerimiento() {

    $('.delItemReg').live('click', function (e) {

        var strReg = $(this).attr('cod_requerimiento')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea aprobar el requerimiento?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {


            var strData = jQuery.parseJSON('{ "cod_requerimiento": "' + strReg + '" }');


            $.ajax({
                type: 'POST',
                url: urlApr,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");

                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                }
            });
        });

    });
}

function ListarRequerimientosPendientes() {

    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.page = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function CerrarRequerimiento() {

    $('.delItemReg').live('click', function (e) {

        var strReg = $(this).attr('cod_requerimiento')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea cerrar el requerimiento?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {


            var strData = jQuery.parseJSON('{ "cod_requerimiento": "' + strReg + '" }');


            $.ajax({
                type: 'POST',
                url: urlCer,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");

                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                }
            });
        });

    });
}

//OBTIENE LOS SERVICIOS DE ACUERDO A LA CATEGORIA Y SUBCATEGORIA DEL REQUERIMIENTO
function ObtenerServiciosDisponibles() {
    $(document).on("click", ".btnEditarUsuario", function (e) {
        var strReg = $(this).attr('cod_requerimiento');
        var strDatas = new Object();
        strDatas.cod_requerimiento = strReg;
        //alert(strReg);

        $.ajax({
            type: 'POST',
            url: urlObServ,
            async: true,
            cache: false,
            data: JSON.stringify(strDatas),
            success: function (data) {
                $('#myModal').html(data);
                $('#myModal').modal('show');
            },
            error: function (xhr, status, error) {
                alert('xhr: ' + JSON.stringify(xhr) + ' status: ' + JSON.stringify(status) + ' error: ' + JSON.stringify(error));
                //$("#myModal").html(alertHtml('error', xhr.responseText));
                //$('#myModal').modal('show');

            }
        });

    });

}

//OBTIENE LAS CATEGORIAS Y SUBCATEGORIAS DE ACUERDO A LO QUE TIPEA
function ObtenerListaServicios() {

    $("#NombreServicio").keyup(function () {

        var nom_serviciosT = $('#NombreServicio').val();

        var strDat = new Object();
        strDat.nom_serviciosT = nom_serviciosT;

        $.ajax({
            type: 'POST',
            url: urlObLS,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strDat),
            beforeSend: function () {
                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                //$('#btnLimpiar').hide();
                //$('#btnGuardar').hide();
                //$('#btnCancelar').hide();
            },
            success: function (msg) {
                $("#listServicios").html(null);
                var datos = $.parseJSON(msg.d);

                $(datos).each(function () {
                    var option = $(document.createElement('option'));

                    option.text(this.nom_servicios);
                    option.val(this.cod_servicios + '|' + this.tipo_servicios);
                    $("#listServicios").append(option);

                });
            },
            error: function (xhr, status, error) {
                $('#msg').html('');
                //$('#btnLimpiar').show();
                //$('#btnGuardar').show();
                //$('#btnCancelar').show();
                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
            }
        });
    });


    $('#listServicios').change(function () {
        var strData = new Object();
        strData.nombreOption = $(this).find("option:selected").text();
        //alert(thisvalue);
        $('#NombreServicio').val('');
        $('#NombreServicio').val(strData.nombreOption);

        var cadenaCodigo = $(this).find("option:selected").val();
        var codigo = cadenaCodigo.split("|");
        strData.codigoServicio = codigo[0];
        strData.tipoServicio = codigo[1];

        $.ajax({
            type: 'POST',
            url: 'RegistroRequerimiento.aspx/ObtenerCategoria',
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (msg) {
                var datos = $.parseJSON(msg.d);
                $('#dllCategoria>option:selected').val(datos.codigoCategoria);
                $('#dllCategoria>option:selected').text(datos.nombreCategoria);
            },
            error: function (xhr, status, error) {
                $('#msg').html('');
                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
            }
        });
    });

}


function AccionarRequerimiento() {

    $('.delItemReg').live('click', function (e) {
        //$('.delItemReg').click(function(e){
        var obj = new Object();
        obj.cod_requerimiento = $(this).attr('cod_requerimiento');
        obj.nom_estado_requerimiento = $(this).attr('nom_estado_requerimiento');
        var nombrePrioridad = $(this).attr('nom_prioridad');
        switch (nombrePrioridad) {
            case "ALTA": obj.nom_prioridad = "1"; break;
            case "MEDIA": obj.nom_prioridad = "2"; break;
            case "BAJA": obj.nom_prioridad = "3"; break;
        }
        $.ajax({
            type: 'POST',
            url: urlAprM,
            async: true,
            cache: false,
            data: JSON.stringify(obj),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });
            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });
    });
}

function ObtenerSubCategorias() {

    var cod_categoria = $('#dllCategoria').val();
    var strDat = new Object();
    strDat.cod_categoria = cod_categoria;
    $.ajax({
        type: 'POST',
        url: urlObS, //'AprobarRequerimiento.aspx/CargarComboSubCategoria'
        contentType: "application/json; charset=utf-8",
        async: true,
        cache: false,
        data: JSON.stringify(strDat),
        success: function (msg) {
            $("#dllSubCategoria").html(null);
            var option = $(document.createElement('option'));

            option.text(".:SELECCIONE:.");
            option.val("-1");
            $("#dllSubCategoria").append(option);

            var datos = $.parseJSON(msg.d);
            if (datos.length > 0) {
                $(datos).each(function () {
                    var option = $(document.createElement('option'));

                    option.text(this.nom_subcategoria);
                    option.val(this.cod_subcategoria);

                    $("#dllSubCategoria").append(option);
                });
                if ($('#IDSubCategoria').val() != "-1") {
                    var subcategoria = $('#IDSubCategoria').val();
                    $('#dllSubCategoria option[value=' + subcategoria + ']').attr("selected", "selected");
                    ObtenerServicios();
                } else {
                    var option = $(document.createElement('option'));

                    option.text(".:SELECCIONE:.");
                    option.val("-1");
                    $("#dllServicio").append(option);
                }
            } else {
                $(".divSubCategoria").hide();
                ObtenerServicios();
            }
            //ObtenerServicios();
        },
        error: function (xhr, status, error) {
            addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
        }
    });

    $("#dllCategoria").change(function () {
        var strDat = new Object();
        var tieneSub = false;
        cod_categoria = $('#dllCategoria').val();
        strDat.cod_categoria = cod_categoria;
        $.ajax({
            type: 'POST',
            url: urlObS, //'AprobarRequerimiento.aspx/CargarComboSubCategoria'
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strDat),
            success: function (msg) {
                $("#dllSubCategoria").html(null);
                //$("#dllServicio").html(null);
                var datos = $.parseJSON(msg.d);
                var option = $(document.createElement('option'));

                option.text(".:SELECCIONE:.");
                option.val("-1");
                $("#dllSubCategoria").append(option);

                if (datos.length > 0) {
                    $(datos).each(function () {
                        var option = $(document.createElement('option'));

                        option.text(this.nom_subcategoria);
                        option.val(this.cod_subcategoria);

                        $("#dllSubCategoria").append(option);
                        $(".divSubCategoria").show();
                        tieneSub = true;
                    });
                } else {
                    $(".divSubCategoria").hide();
                }

                if (tieneSub) {
                    $("#dllServicio").html(null);
                    var option = $(document.createElement('option'));
                    option.text(".:SELECCIONE:.");
                    option.val("-1");
                    $("#dllServicio").append(option);
                } else {
                    ObtenerServiciosModal();//OBTIENE EL SERVICIO
                }
            }
        });
    });

    $("#dllSubCategoria").change(function () {
        var strDat = new Object();
        strDat.cod_categoria = $('#dllCategoria').val();
        strDat.cod_subcategoria = $('#dllSubCategoria').val();
        strDat.cod_servicio = '-1';
        if (strDat.cod_subcategoria == '-1') {
            $("#dllServicio").html(null);
            var option = $(document.createElement('option'));
            option.text(".:SELECCIONE:.");
            option.val("-1");
            $("#dllServicio").append(option);
        } else {
            $.ajax({
                type: 'POST',
                url: urlObSe,
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strDat),
                success: function (msg) {
                    var datos = $.parseJSON(msg.d);
                    $("#dllServicio").html(null);
                    $(datos).each(function () {
                        var option = $(document.createElement('option'));
                        option.text(this.nom_servicios);
                        option.val(this.cod_servicios);
                        $("#dllServicio").append(option);
                    });
                },
                error: function (xhr, status, error) {
                    addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                }
            });
        }
    });
}

//OBTENCION DE SERVICIOS DE ACUERDO A SU CATEGORIA O SUBCATEGORIA (DROPDOWNLIST)
function ObtenerServicios() {
    var cod_categoria = $('#IDCategoria').val();
    var cod_subcategoria;
    if ($('#IDSubCategoria').val() == '') {
        cod_subcategoria = "-1";
    } else {
        cod_subcategoria = $('#IDSubCategoria').val()
    }
    var cod_servicio = $('#IDServicio').val();
    var strDat = new Object();
    strDat.cod_subcategoria = cod_subcategoria;
    strDat.cod_categoria = cod_categoria;
    strDat.cod_servicio = cod_servicio;
    $("#dllServicio").html(null);
    if (cod_servicio == '-1') {
        var option = $(document.createElement('option'));
        option.text(".:SELECCIONE:.");
        option.val("-1");
        $("#dllServicio").append(option);
    }
    $.ajax({
        type: 'POST',
        url: urlObSe,
        contentType: "application/json; charset=utf-8",
        async: true,
        cache: false,
        data: JSON.stringify(strDat),
        success: function (msg) {
            var datos = $.parseJSON(msg.d);
            $(datos).each(function () {
                var option = $(document.createElement('option'));
                option.text(this.nom_servicios);
                option.val(this.cod_servicios);
                $("#dllServicio").append(option);
            });
            var servicio = $('#IDServicio').val();
            $('#dllServicio option[value=' + servicio + ']').attr("selected", "selected");
        },
        error: function (xhr, status, error) {
            addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
        }
    });
}

function ObtenerServiciosModal() {
    var cod_categoria = $('#dllCategoria').val();
    var cod_subcategoria;
    if ($('#dllSubCategoria').val() == '') {
        cod_subcategoria = "-1";
    } else if ($('#dllSubCategoria').val() == null) {
        cod_subcategoria = '-1';
    } else {
        cod_subcategoria = $('#dllSubCategoria').val();
    }
    var cod_servicio = '-1';
    var strDat = new Object();
    strDat.cod_subcategoria = cod_subcategoria;
    strDat.cod_categoria = cod_categoria;
    strDat.cod_servicio = cod_servicio;

    $("#dllServicio").html(null);
    if (cod_servicio == '-1') {
        var option = $(document.createElement('option'));
        option.text(".:SELECCIONE:.");
        option.val("-1");
        $("#dllServicio").append(option);
    }
    $.ajax({
        type: 'POST',
        url: urlObSe,
        contentType: "application/json; charset=utf-8",
        async: true,
        cache: false,
        data: JSON.stringify(strDat),
        success: function (msg) {
            var datos = $.parseJSON(msg.d);
            $(datos).each(function () {
                var option = $(document.createElement('option'));
                option.text(this.nom_servicios);
                option.val(this.cod_servicios);
                $("#dllServicio").append(option);
            });
        },
        error: function (xhr, status, error) {
            addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
        }
    });

    $("#dllSubCategoria").change(function () {
        var strDat = new Object();
        strDat.cod_categoria = $('#dllCategoria').val();
        strDat.cod_subcategoria = $('#dllSubCategoria').val();
        strDat.cod_servicio = '-1';
        if (strDat.cod_subcategoria == '-1') {
            $("#dllServicio").html(null);
            var option = $(document.createElement('option'));
            option.text(".:SELECCIONE:.");
            option.val("-1");
            $("#dllServicio").append(option);
        } else {
            $.ajax({
                type: 'POST',
                url: urlObSe,
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strDat),
                success: function (msg) {
                    var datos = $.parseJSON(msg.d);
                    $("#dllServicio").html(null);
                    $(datos).each(function () {
                        var option = $(document.createElement('option'));
                        option.text(this.nom_servicios);
                        option.val(this.cod_servicios);
                        $("#dllServicio").append(option);
                    });
                },
                error: function (xhr, status, error) {
                    addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                }
            });
        }
    });
}

function GuardarRequerimiento() {

    function receivedText() {
        var b64Data = fr.result.split(',');
        var contentType = 'image/png';
        var byteCharacters = atob(b64Data[1]);
        localStorage.setItem('imagen', atob(b64Data[1]));
        $("#btnSubirFotoI").data("byteCharacters", byteCharacters);
        var byteNumbers = Array.prototype.map.call(byteCharacters, charCodeFromCharacter);
        var uint8Data = new Uint8Array(byteNumbers);
        var blob = b64toBlob(b64Data[1], contentType);
        //var blobUrl = URL.createObjectURL(blob);

    }

    function charCodeFromCharacter(c) {
        return c.charCodeAt(0);
    }

    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 1024;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = Array.prototype.map.call(slice, charCodeFromCharacter);
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        for (var i = 0; i < byteArray.length; i++) {

        }
    }

    $('#btnGuardarModal').click(function () {
        //alert("prueba");
        var validateItems = true;

        var strData = getUsuario();
        //strData.codigoRequerimiento = $('#hdnCodigoRequerimiento').val();
        //strData.servicioCliente = $('#ServicioClienteT').val(); //ServicioClienteT
        //strData.estado = $('#NombreEstado').val(); //NombreEstado
        //strData.categoria = $('#dllCategoria').val(); //dllCategoria
        //strData.subCategoria = $('#dllSubCategoria').val(); //dllSubCategoria
        //strData.servicio = $('#dllServicio').val(); //dllServicio
        //strData.detalle = $('#Detalles').val(); //Detalles
        //strData.observacion = $('#Observacion').val(); //Observacion
        //strData.prioridad = $('#hdnPrioridad').val(); //Prioridad
        //strData.codigoUsuario = $('#hdnUsuarioReq').val(); //Codigo Usuario del requerimiento

        switch (strData.estado) {
            case "PENDIENTE":
                $('#dllCategoria').attr('style', 'width: 475px !important;');
                $('#Observacion').attr('style', '');
                $('#dllSubCategoria').attr('style', 'width: 475px !important;');
                $('#dllServicio').attr('style', 'width: 475px !important;');

                if ($('#dllCategoria').val() == '-1') {
                    $('#dllCategoria').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6); width: 475px !important;')
                    addnotify("notify", "¡Seleccione una Categoria!", "registeruser");
                    validateItems = false;
                }
                else if ($('#dllServicio').val() == '-1') {
                    $('#dllServicio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6); width: 475px !important;')
                    addnotify("notify", "¡Seleccione un Servicio!", "registeruser");
                    validateItems = false;
                }
                else if ($('#Observacion').val() == '') {
                    $('#Observacion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese una Observación!", "registeruser");
                    validateItems = false;
                }

                if (validateItems) {
                    $.ajax({
                        type: 'POST',
                        url: urlReqAct,
                        contentType: "application/json; charset=utf-8",
                        async: true,
                        cache: false,
                        data: JSON.stringify(strData),
                        success: function (msg) {
                            $('#buscar').trigger("click");
                            addnotify("notify", "El requerimiento ha sido APROBADO. ", "registeruser");
                            $('#divMyModal').removeClass('modal-open');
                            $('.modal-backdrop').remove();
                        },
                        error: function (xhr, status, error) {
                            addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                        }
                    });
                }
                break;

            case "ASIGNADO":

                $('#Observacion').attr('style', '');
                if ($('#Observacion').val() == '') {
                    $('#Observacion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese una Observación!", "registeruser");
                    validateItems = false;
                }
                else if ($('#btnSubirFotoI').val() == '') {
                    addnotify("notify", "¡Por favor subir una Foto!", "registeruser");
                    validateItems = false;
                }

                if (validateItems) {
                    var serviciosAdicionales = $('#txaServicio').val();
                    var servicioActual = $('#hdnCodigoServicioActual').val();
                    var inputFileImage = document.getElementById("btnSubirFotoI");
                    var file1 = inputFileImage.files[0];
                    var file2 = inputFileImage.files[1];
                    var file3 = inputFileImage.files[2];

                    strData.serviciosAdicionales = serviciosAdicionales;
                    strData.servicioActual = servicioActual;

                    var resultadoFoto1 = "";
                    var resultadoFoto2 = "";
                    var resultadoFoto3 = "";

                    if (file1 != null && file1.name != null && file1.name.length > 0) {
                        var data = new FormData();
                        var extension = file1.name;
                        corte = extension.split('.')[1];
                        data.append("file", file1, strData.codigoRequerimiento + '_1_' + '.' + corte);
                        fr = new FileReader();
                        fr.onload = receivedText
                        fr.readAsDataURL(file1);

                        var d = new Date();

                        var month = d.getMonth() + 1;
                        var day = d.getDate();
                        var dia;
                        if (day.toString().length == 1) {
                            dia = '0' + day.toString();
                        }
                        else {
                            dia = day;
                        }

                        var output = d.getFullYear().toString() + month.toString() + dia;
                        strData.foto1 = strData.codigoRequerimiento + '_1_' + output + '.' + corte;

                        $.ajax({
                            type: 'POST',
                            url: 'Upload.ashx',
                            contentType: false,
                            processData: false,
                            cache: false,
                            data: data,
                            success: function (data) {
                                obj = $.parseJSON(data);
                                if (obj.success) {
                                    console.log("Subido correctamente");
                                    var strDataFile = new Object();
                                    strDataFile.icono = "1";
                                    try {
                                        strDataFile.urlicon = file1.name;
                                    } catch (err) {
                                        strDataFile.urlicon = "";
                                    }
                                    resultadoFoto1 = "OK";
                                    if (file2 != null && file2.name != null && file2.name.length > 0) {
                                        var data = new FormData();
                                        var extension = file2.name;
                                        corte = extension.split('.')[1];
                                        data.append("file", file2, strData.codigoRequerimiento + '_2_' + '.' + corte);
                                        fr = new FileReader();
                                        fr.onload = receivedText
                                        fr.readAsDataURL(file2);

                                        var d = new Date();

                                        var month = d.getMonth() + 1;
                                        var day = d.getDate();
                                        var dia;
                                        if (day.toString().length == 1) {
                                            dia = '0' + day.toString();
                                        }
                                        else {
                                            dia = day;
                                        }

                                        var output = d.getFullYear().toString() + month.toString() + dia;
                                        strData.foto2 = strData.codigoRequerimiento + '_2_' + output + '.' + corte;

                                        $.ajax({
                                            type: 'POST',
                                            url: 'Upload.ashx',
                                            contentType: false,
                                            processData: false,
                                            cache: false,
                                            data: data,
                                            success: function (data) {
                                                obj = $.parseJSON(data);
                                                if (obj.success) {
                                                    console.log("Subido correctamente");
                                                    var strDataFile = new Object();
                                                    strDataFile.icono = "1";
                                                    try {
                                                        strDataFile.urlicon = file2.name;
                                                    } catch (err) {
                                                        strDataFile.urlicon = "";
                                                    }
                                                    resultadoFoto2 = "OK";
                                                    if (file3 != null && file3.name != null && file3.name.length > 0) {
                                                        var data = new FormData();
                                                        var extension = file3.name;
                                                        corte = extension.split('.')[1];
                                                        data.append("file", file3, strData.codigoRequerimiento + '_3_' + '.' + corte);
                                                        fr = new FileReader();
                                                        fr.onload = receivedText
                                                        fr.readAsDataURL(file3);

                                                        var d = new Date();

                                                        var month = d.getMonth() + 1;
                                                        var day = d.getDate();
                                                        var dia;
                                                        if (day.toString().length == 1) {
                                                            dia = '0' + day.toString();
                                                        }
                                                        else {
                                                            dia = day;
                                                        }

                                                        var output = d.getFullYear().toString() + month.toString() + dia;
                                                        strData.foto3 = strData.codigoRequerimiento + '_3_' + output + '.' + corte;

                                                        $.ajax({
                                                            type: 'POST',
                                                            url: 'Upload.ashx',
                                                            contentType: false,
                                                            processData: false,
                                                            cache: false,
                                                            data: data,
                                                            success: function (data) {
                                                                obj = $.parseJSON(data);
                                                                if (obj.success) {
                                                                    console.log("Subido correctamente");
                                                                    var strDataFile = new Object();
                                                                    strDataFile.icono = "1";
                                                                    try {
                                                                        strDataFile.urlicon = file3.name;
                                                                    } catch (err) {
                                                                        strDataFile.urlicon = "";
                                                                    }
                                                                    resultadoFoto3 = "OK";
                                                                    if (resultadoFoto1 == "OK" || resultadoFoto2 == "OK" || resultadoFoto3 == "OK") {
                                                                        $.ajax({
                                                                            type: 'POST',
                                                                            url: urlReqCerrar,
                                                                            contentType: "application/json; charset=utf-8",
                                                                            async: true,
                                                                            cache: false,
                                                                            data: JSON.stringify(strData),
                                                                            success: function (msg) {
                                                                                $('#buscar').trigger("click");
                                                                                addnotify("notify", "El requerimiento ha sido CERRADO", "registeruser");
                                                                                $('#divMyModal').removeClass('modal-open');
                                                                                $('.modal-backdrop').remove();
                                                                            },
                                                                            error: function (xhr, status, error) {
                                                                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                                else {
                                                                    addnotify("notify", obj.error, "registeruser");
                                                                }
                                                            },
                                                            error: function (xhr, status, error) {
                                                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        strData.foto3 = "";
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: urlReqCerrar,
                                                            contentType: "application/json; charset=utf-8",
                                                            async: true,
                                                            cache: false,
                                                            data: JSON.stringify(strData),
                                                            success: function (msg) {
                                                                $('#buscar').trigger("click");
                                                                addnotify("notify", "El requerimiento ha sido CERRADO", "registeruser");
                                                                $('#divMyModal').removeClass('modal-open');
                                                                $('.modal-backdrop').remove();
                                                            },
                                                            error: function (xhr, status, error) {
                                                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                                                            }
                                                        });
                                                    }
                                                }
                                                else {
                                                    addnotify("notify", obj.error, "registeruser");
                                                }
                                            },
                                            error: function (xhr, status, error) {
                                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                                            }
                                        });
                                    } else {
                                        strData.foto2 = "";
                                        strData.foto3 = "";
                                        $.ajax({
                                            type: 'POST',
                                            url: urlReqCerrar,
                                            contentType: "application/json; charset=utf-8",
                                            async: true,
                                            cache: false,
                                            data: JSON.stringify(strData),
                                            success: function (msg) {
                                                $('#buscar').trigger("click");
                                                addnotify("notify", "El requerimiento ha sido CERRADO", "registeruser");
                                                $('#divMyModal').removeClass('modal-open');
                                                $('.modal-backdrop').remove();
                                            },
                                            error: function (xhr, status, error) {
                                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                                            }
                                        });
                                    }
                                }
                                else {
                                    addnotify("notify", obj.error, "registeruser");
                                }
                            },
                            error: function (xhr, status, error) {
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    } else {
                        addnotify("notify", "Cargue una foto para cerrar el requerimiento", "registeruser");
                    }
                }
                break;

            case "APROBADO":
                $('#Observacion').attr('style', '');
                $('#fechaAsignacion').attr('style', '');
                $('#txtTrabajador').attr('style', 'height: 15px !important; margin-bottom: 10px !important;');
                if ($('#Observacion').val() == '') {
                    $('#Observacion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese una Observación!", "registeruser");
                    validateItems = false;
                }
                else if ($('#fechaAsignacion').val() == '') {
                    $('#fechaAsignacion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    addnotify("notify", "¡Ingrese la fecha y hora de la ejecución!", "registeruser");
                    validateItems = false;
                }
                else if ( $('#txtTrabajador').val() == '' || $('#txaPersonal').val() == '' ) {
                    $('#txtTrabajador').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6); height: 15px !important; margin-bottom: 10px !important;')
                    addnotify("notify", "¡Agregue un Trabajador!", "registeruser");
                    validateItems = false;
                }

                if (validateItems) {
                    strData.personal = $('#txaPersonal').val();
                    strData.idUsuario = $('#hdnUsuario').val();
                    strData.fechaAsignacion = $('#fechaAsignacion').val();
                    $.ajax({
                        type: 'POST',
                        url: urlReqAsignar,
                        contentType: "application/json; charset=utf-8",
                        async: true,
                        cache: false,
                        data: JSON.stringify(strData),
                        success: function (msg) {
                            $('#buscar').trigger("click");
                            addnotify("notify", "Se ha ASIGNADO recursos al requerimiento.", "registeruser");
                            $('#divMyModal').removeClass('modal-open');
                            $('.modal-backdrop').remove();
                        },
                        error: function (xhr, status, error) {
                            addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                        }
                    });
                }
                break;
        }
    });

    //function ValidarFoto() {

    //}
}

function AgregarPersonal() {
    $('#btnAgregarCaja').click(function () {
        var element = document.createElement("input");
        element.setAttribute("type", "text");
        //element.className("form-control");

        var foo = document.getElementById("fooBar");

        foo.appendChild(element);
    });

}

function BuscarReporteRequerimientos() {

    $('#buscar').click(function (e) {
        //cargando la grilla
        var strDat = getParametros();
        strDat.page = 1;
        $('#hdnActualPage').val();
        if (strDat.fechaIN != "" && strDat.fechaFI != "") {
            $.ajax({
                type: 'POST',
                url: urlbus,
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strDat),
                beforeSend: function () {
                    $('.form-gridview-data').html('');
                    $('.form-gridview-error').html('');
                    $('.form-gridview-search').show();
                },
                success: function (data) {
                    //console.log('data_: ' + data);
                    $('.form-gridview-search').hide();
                    $('.form-gridview-error').html('');
                    $('.form-gridview-data').html(data);
                    $('#hdnActualPage').val(strDat.page);
                },
                error: function (xhr, status, error) {
                    $('.form-gridview-search').hide();
                    $('.form-gridview-data').html();
                    $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                    $("#myModal").html(alertHtml('error', xhr.responseText));

                    $('#divError').click(function (e) {
                        $('#myModal').modal('show');
                    });
                }
            });
        } else if (strDat.fechaIN == "" && strDat.fechaFI == "") {
            addnotify("notify", "¡Ingrese la fecha de inicio y termino!");
        } else if (strDat.fechaIN == "" && strDat.fechaFI != "") {
            addnotify("notify", "¡Ingrese la fecha de inicio!");
        } else if (strDat.fechaIN != "" && strDat.fechaFI == "") {
            addnotify("notify", "¡Ingrese la fecha de termino!");
        }


    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function AgregarServiciosAdicionales() {
    var cantidadServicios = 0;

    $('#btnClear').click(function () {
        cantidadServicios = 0;
        $("#txaServicio").attr("value", "");
        //document.getElementById("txaServicio").value = "";
        //$('#txaServicio').val('');
        //$('#txaServicio').text('');
    });

    $('#btnAdd').click(function () {
        var strData = new Object();
        var codigoServicio = $('#txtNuevosServicios').val();
        var codigoServicioActual = $('#dllServicio').val();
        var resultado = $('#txaServicio').val().indexOf(codigoServicio) != -1;
        strData.codigoServicio = codigoServicio;
        strData.codigoServicioActual = codigoServicioActual;

        if (resultado == false) {
            $.ajax({
                type: 'POST',
                url: urlAddNewService,
                contentType: "application/json; charset=utf-8",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (msg) {
                    var datos = $.parseJSON(msg.d);
                    if (datos.cod_servicios != '') {
                        if (codigoServicio == datos.cod_sap) {
                            addnotify("notify", "Se ingreso el código actual del servicio", "registeruser");
                        } else {
                            if (cantidadServicios == 0) {
                                var servicio = $('#txtNuevosServicios').val();
                                $('#txaServicio').val(servicio);
                                cantidadServicios++;
                            } else {
                                var servicio = $('#txtNuevosServicios').val();
                                var textArea = $('#txaServicio').val();
                                var cadena = textArea + ',' + servicio;
                                $('#txaServicio').val(cadena);
                            }
                            $('#hdnCodigoServicioActual').val(datos.cod_sap);
                        }
                    } else {
                        addnotify("notify", "Código invalido", "registeruser");
                    }
                },
                error: function (xhr, status, error) {
                    addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                }
            });
        } else {
            addnotify("notify", "El código se repite en la lista", "registeruser");
        }
    });
}

function AgregarTrabajadores() {
    var cantidadServicios = 0;

    $('#btnClearTrabajador').click(function () {
        cantidadServicios = 0;
        $("#txaPersonal").attr("value", "");
    });

    $('#btnAddTrabajador').click(function () {

        $('#txtTrabajador').attr('style', 'height: 15px !important; margin-bottom: 10px !important;');

        var trabajador = $('#txtTrabajador').val();
        var resultado = $('#txaPersonal').val().indexOf(trabajador) != -1;
        if (resultado == false) {
            if (cantidadServicios == 0) {
                var trabajadorNew = $('#txtTrabajador').val();
                $('#txaPersonal').val(trabajadorNew);
                cantidadServicios++;
            } else {
                var trabajadorNew = $('#txtTrabajador').val();
                var textArea = $('#txaPersonal').val();
                var cadena = textArea + ', ' + trabajadorNew;
                $('#txaPersonal').val(cadena);
            }
        } else {
            addnotify("notify", "El trabajdor ya esta en la lista", "registeruser");
        }
    });
}


// PRIMO
function BuscarRegistroServicio() {

    $("input[id_servicio$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
                     .attr('checked', $("input[id_servicio$='ChkAll']")
                     .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[id_servicio$='ChkAll']").attr("checked", false);
        }
    });

    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.page = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

function EliminarServicio() {

    $('.delReg').click(function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover los items seleccionados?"));
            $('#myModal').modal('show');

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = jQuery.parseJSON('{ "id_servicio": "' + strReg + '" }');


                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#buscar').trigger("click");

                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });

    $('.delItemReg').live('click', function (e) {



        var strReg = $(this).attr('id_servicio')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover el item seleccionado?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {


            var strData = jQuery.parseJSON('{ "id_servicio": "' + strReg + '" }');


            $.ajax({
                type: 'POST',
                url: urldel,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");

                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                }
            });
        });

    });


}

//REGISRAR SERVICIO
function AgregarServicio() {
    $('.addUsuario').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardar').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#dllCategoria').attr('style', '');
                    $('#dllSubcategoria').attr('style', '');
                    $('#nLinea').attr('style', '');
                    $('#nCodigoSap').attr('style', '');
                    $('#nNombreServicio').attr('style', '');
                    $('#nDescripcion').attr('style', '');
                    $('#nPrecio').attr('style', '');

                    if ($('#dllCategoria').val() == '-1') {
                        $('#dllCategoria').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija una categoria!", "registeruser");
                        validateItems = false;
                    } else if ($('#dllSubcategoria').val() == '-1') {
                        $('#dllSubcategoria').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija una subcategoria!", "registeruser");
                        validateItems = false;
                    } else if ($('#nLinea').val() == '') {
                        $('#nLinea').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese la línea del servicio!", "registeruser");
                        validateItems = false;
                    } else if ($('#nCodigoSap').val() == '') {
                        $('#nCodigoSap').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el codigo SAP del servicio!", "registeruser");
                        validateItems = false;
                    } else if ($('#nNombreServicio').val() == '') {
                        $('#nNombreServicio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el nombre del servicio!", "registeruser");
                        validateItems = false;
                    } else if ($('#nDescripcion').val() == '') {
                        $('#nDescripcion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese una descripción del servicio!", "registeruser");
                        validateItems = false;
                    } else if ($('#nPrecio').val() == '') {
                        $('#nPrecio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingre el precio del servicio!", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getSala()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardar').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Su servicio ha sido registrado correctamente.", "registeruser");
                                //$('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardar').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//OBTENER CATEGORIA Y SUBCATEGORIAS PARA EL REGISTRO DE SERVICIOS

function ObtenerSubCategoriasC() {

    var cod_categoria = $('#dllCategoria').val();
    var strDat = new Object();
    strDat.cod_categoria = cod_categoria;
    $.ajax({
        type: 'POST',
        url: urlObS, //'AprobarRequerimiento.aspx/CargarComboSubCategoria'
        contentType: "application/json; charset=utf-8",
        async: true,
        cache: false,
        data: JSON.stringify(strDat),
        success: function (msg) {
            $("#dllSubCategoria").html(null);
            var datos = $.parseJSON(msg.d);

            var option = $(document.createElement('option'));

            option.text(".:SELECCIONE:.");
            option.val("-1");
            $("#dllSubCategoria").append(option);
            if (datos.length > 0) {
                $(datos).each(function () {
                    var option = $(document.createElement('option'));

                    option.text(this.nom_subcategoria);
                    option.val(this.cod_subcategoria);

                    $("#dllSubCategoria").append(option);
                });
                var subcategoria = $('#hdnIdSubCategoria').val();
                $('#dllSubCategoria option[value=' + subcategoria + ']').attr("selected", "selected");

            } else {
                $(".divSubCategoria").hide();

            }
            //ObtenerServicios();
        },
        error: function (xhr, status, error) {
            addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
        }
    });

    $("#dllCategoria").change(function () {
        var strDat = new Object();
        var tieneSub = false;
        cod_categoria = $('#dllCategoria').val();
        strDat.cod_categoria = cod_categoria;
        $.ajax({
            type: 'POST',
            url: urlObS, //'AprobarRequerimiento.aspx/CargarComboSubCategoria'
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strDat),
            success: function (msg) {
                $("#dllSubCategoria").html(null);
                //$("#dllServicio").html(null);
                var datos = $.parseJSON(msg.d);
                var option = $(document.createElement('option'));

                option.text(".:SELECCIONE:.");
                option.val("-1");
                $("#dllSubCategoria").append(option);

                if (datos.length > 0) {
                    $(datos).each(function () {
                        var option = $(document.createElement('option'));

                        option.text(this.nom_subcategoria);
                        option.val(this.cod_subcategoria);

                        $("#dllSubCategoria").append(option);
                        $(".divSubCategoria").show();
                        tieneSub = true;
                    });
                } else {
                    $(".divSubCategoria").hide();
                }

                //if (tieneSub) {
                //    $("#dllServicio").html(null);
                //    var option = $(document.createElement('option'));
                //    option.text(".:SELECCIONE:.");
                //    option.val("-1");
                //    $("#dllServicio").append(option);
                //} else {
                //    ObtenerServiciosModal();//OBTIENE EL SERVICIO
                //}
            }
        });
    });
}

//MODIFICAR SERVICIO
function ModificarServicio() {
    $('.editItemReg').live('click', function (e) {
        var strReg = $(this).attr('id_servicio');
        var strData = new Object();
        strData.id_servicio = strReg;

        $.ajax({
            type: 'POST',
            url: urlins,
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardar').click(function (e) {

                    var validateItems = true;
                    $('#dllCategoria').attr('style', '');
                    $('#dllSubcategoria').attr('style', '');
                    $('#nLinea').attr('style', '');
                    $('#nCodigoSap').attr('style', '');
                    $('#nNombreServicio').attr('style', '');
                    $('#nDescripcion').attr('style', '');
                    $('#nPrecio').attr('style', '');

                    if ($('#dllCategoria').val() == '-1') {
                        $('#dllCategoria').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija una categoria!", "registeruser");
                        validateItems = false;
                    } else if ($('#dllSubcategoria').val() == '-1') {
                        $('#dllSubcategoria').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija una subcategoria!", "registeruser");
                        validateItems = false;
                    } else if ($('#nLinea').val() == '') {
                        $('#nLinea').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese la línea del servicio!", "registeruser");
                        validateItems = false;
                    } else if ($('#nCodigoSap').val() == '') {
                        $('#nCodigoSap').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el codigo SAP del servicio!", "registeruser");
                        validateItems = false;
                    } else if ($('#nNombreServicio').val() == '') {
                        $('#nNombreServicio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el nombre del servicio!", "registeruser");
                        validateItems = false;
                    } else if ($('#nDescripcion').val() == '') {
                        $('#nDescripcion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese una descripción del servicio!", "registeruser");
                        validateItems = false;
                    } else if ($('#nPrecio').val() == '') {
                        $('#nPrecio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingre el precio del servicio!", "registeruser");
                        validateItems = false;
                    }

                    if (validateItems) {
                        //addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getSala()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardar').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Servicio Actualizado", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardar').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//BUSQUEDA SUBCATEGORIA (GRID)
function BuscarRegistroSubcategoria() {

    $("input[id_subcategoria$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
                     .attr('checked', $("input[id_subcategoria$='ChkAll']")
                     .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[id_subcategoria$='ChkAll']").attr("checked", false);
        }
    });

    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.page = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

//ELIMINAR SUBCATEGORIA
function EliminarSubcategoria() {

    $('.delReg').click(function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover los items seleccionados?"));
            $('#myModal').modal('show');

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = jQuery.parseJSON('{ "id_subcategoria": "' + strReg + '" }');


                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#buscar').trigger("click");

                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });

    $('.delItemReg').live('click', function (e) {



        var strReg = $(this).attr('id_subcategoria')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover el item seleccionado?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {


            var strData = jQuery.parseJSON('{ "id_subcategoria": "' + strReg + '" }');


            $.ajax({
                type: 'POST',
                url: urldel,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");

                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                }
            });
        });

    });
}

//AGREGAR SUBCATEGORIA
function AgregarSubcategoria() {
    $('.addUsuario').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardar').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#dllCategoriaM').attr('style', '');
                    $('#txtSubcategoriaM').attr('style', '');
                    $('#txtLineaM').attr('style', '');

                    if ($('#dllCategoriaM').val() == '-1') {
                        $('#dllCategoriaM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija una categoria!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtSubcategoriaM').val() == '') {
                        $('#txtSubcategoriaM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "Ingrese el nombre de la subcategoria!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtLineaM').val() == '') {
                        $('#txtLineaM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese la línea de la subcategoria!", "registeruser");
                        validateItems = false;
                    }
                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getSala()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardar').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Subcategoria creada correctamente.", "registeruser");
                                //$('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardar').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//MODIFICAR SUBCATEGORIA
function ModificarSubcategoria() {
    $('.editItemReg').live('click', function (e) {
        var strReg = $(this).attr('id_subcategoria');
        var strData = new Object();
        strData.id_subcategoria = strReg;

        $.ajax({
            type: 'POST',
            url: urlins,
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardar').click(function (e) {

                    var validateItems = true;

                    $('#dllCategoria').attr('style', '');

                    if ($('#nNombreLocal').val() == '-1') {
                        $('#nNombreLocal').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el nombre de la subcategoria!", "registeruser");
                        validateItems = false;
                    } else if ($('#nDireccion').val() == '') {
                        $('#nDireccion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese dirección del local!", "registeruser");
                        validateItems = false;
                    }

                    if (validateItems) {
                        //addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getSala()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardar').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Subcategoria Actualizada", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardar').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//BUSQUEDA CATEGORIA (GRID)
function BuscarRegistroCategoria() {

    $("input[id_categoria$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
                     .attr('checked', $("input[id_categoria$='ChkAll']")
                     .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[id_categoria$='ChkAll']").attr("checked", false);
        }
    });

    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.page = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

//ELIMINAR CATEGORIA
function EliminarCategoria() {

    $('.delReg').click(function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover los items seleccionados?"));
            $('#myModal').modal('show');

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = jQuery.parseJSON('{ "id_categoria": "' + strReg + '" }');


                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#buscar').trigger("click");

                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });

    $('.delItemReg').live('click', function (e) {



        var strReg = $(this).attr('id_categoria')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover el item seleccionado?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {


            var strData = jQuery.parseJSON('{ "id_categoria": "' + strReg + '" }');


            $.ajax({
                type: 'POST',
                url: urldel,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");

                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                }
            });
        });

    });
}

//AGREGAR CATEGORIA
function AgregarCategoria() {
    $('.addUsuario').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardar').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;

                    $('#txtCategoriaM').attr('style', '');
                    $('#txtLineaM').attr('style', '');

                    if ($('#txtCategoriaM').val() == '') {
                        $('#txtCategoriaM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el nombre de la categoria!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtLineaM').val() == '') {
                        $('#txtLineaM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese la línea de la categoria!", "registeruser");
                        validateItems = false;
                    }

                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getSala()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardar').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Categoria creada correctamente.", "registeruser");
                                //$('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardar').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//MODIFICAR SUBCATEGORIA
function ModificarCategoria() {
    $('.editItemReg').live('click', function (e) {
        var strReg = $(this).attr('id_categoria');
        var strData = new Object();
        strData.id_categoria = strReg;

        $.ajax({
            type: 'POST',
            url: urlins,
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardar').click(function (e) {

                    var validateItems = true;

                    $('#dllCategoriaM').attr('style', '');
                    $('#txtLineaM').attr('style', '');
                    $('#txtSubcategoriaM').attr('style', '');

                    if ($('#dllCategoriaM').val() == '-1') {
                        $('#dllCategoriaM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija una categoria!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtLineaM').val() == '') {
                        $('#txtLineaM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese la línea de la subcategoria!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtSubcategoriaM').val() == '') {
                        $('#txtSubcategoriaM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el nombre de la subcategoria!", "registeruser");
                        validateItems = false;
                    }

                    if (validateItems) {
                        //addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getSala()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardar').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Categoria Actualizada", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardar').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//BUSCAR REGISTRO USUARIOS
function BuscarRegistroUsuario() {

    $("input[id_usuario$='ChkAll']").live('click', function (e) {
        $(".grilla INPUT[type='checkbox']")
                     .attr('checked', $("input[id_usuario$='ChkAll']")
                     .is(':checked'));
    });

    $(".grilla INPUT[type='checkbox']").live('click', function (e) {
        if (!$(this)[0].checked) {
            $("input[id_usuario$='ChkAll']").attr("checked", false);
        }
    });

    $('#buscar').click(function (e) {
        //cargando la grilla
        var strData = getParametros();
        strData.page = 1;
        $('#hdnActualPage').val();
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $('.form-gridview-data').html('');
                $('.form-gridview-error').html('');
                $('.form-gridview-search').show();
            },
            success: function (data) {
                //console.log('data_: ' + data);
                $('.form-gridview-search').hide();
                $('.form-gridview-error').html('');
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
            },
            error: function (xhr, status, error) {
                $('.form-gridview-search').hide();
                $('.form-gridview-data').html();
                $('.form-gridview-error').html("<p><strong>ERROR: </strong> Se ha producido un error en el codigo. Para mayor detalle </p><a id='divError'>haga click aqui</a>");
                $("#myModal").html(alertHtml('error', xhr.responseText));

                $('#divError').click(function (e) {
                    $('#myModal').modal('show');
                });
            }
        });

    });
    $('.pagina-anterior').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) - 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });

    $('.pagina-siguiente').live('click', function (e) {

        var strData = getParametros();
        strData.page = parseInt(strData.page) + 1;
        $.ajax({
            type: 'POST',
            url: urlbus,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            beforeSend: function () {
                $(".paginator-data").hide();
                $(".paginator-data-search").show();
            },
            success: function (data) {
                $('.form-gridview-data').html(data);
                $('#hdnActualPage').val(strData.page);
                //Si la tabla esta expandido, auto expandirse al cambiar de pagina.
                if ($(".form-grid-table-outer").hasClass("cz-table-expand-table")) {
                    $(".cz-table-expand").click();
                }
            },
            error: function (xhr, status, error) {
                $("#myModal").html(alertHtml('error', xhr.responseText));
                $('#myModal').modal('show');
            }
        });

    });
}

//ELIMINAR USUARIO
function EliminarUsuario() {

    $('.delReg').click(function (e) {

        var strReg = "";

        $('#divGridView input:checked').each(function () {
            if (this.name != "chkSelectAll")
            { strReg = strReg + this.value + "|"; }
        });

        if (strReg != "") {
            $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover los items seleccionados?"));
            $('#myModal').modal('show');

            $('.btnDelSi').click(function (e) {

                var strReg = "";

                $('#divGridView input:checked').each(function () {
                    if (this.name != "chkSelectAll")
                    { strReg = strReg + this.value + "|"; }
                });
                var strData = jQuery.parseJSON('{ "id_usuario": "' + strReg + '" }');


                $.ajax({
                    type: 'POST',
                    url: urldel,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (data) {
                        $('#buscar').trigger("click");

                    },
                    error: function (xhr, status, error) {
                        $("#myModal").html(alertHtml('error', xhr.responseText));
                    }
                });
            });

        }
        else {
            $('#myModal').html(alertHtml('delConfirmFalta'));
            $('#myModal').modal('show');
        }


    });

    $('.delItemReg').live('click', function (e) {



        var strReg = $(this).attr('id_usuario')
        $('#myModal').html(alertHtml('delConfirm', "", "¿Esta seguro que desea remover el item seleccionado?"));
        $('#myModal').modal('show');

        $('.btnDelSi').click(function (e) {


            var strData = jQuery.parseJSON('{ "id_usuario": "' + strReg + '" }');


            $.ajax({
                type: 'POST',
                url: urldel,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify(strData),
                success: function (data) {
                    $('#buscar').trigger("click");

                },
                error: function (xhr, status, error) {
                    $("#myModal").html(alertHtml('error', xhr.responseText));
                }
            });
        });

    });
}

//AGREGAR USUARIO
function AgregarUsuario() {
    $('.addUsuario').click(function (e) {
        $.ajax({
            type: 'POST',
            url: urlins,
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                $('#btnGuardar').click(function (e) {
                    e.preventDefault();
                    var validateItems = true;
                    $('#txtApellidoPaternoM').attr('style', '');
                    $('#txtApellidoMaternoM').attr('style', '');
                    $('#txtNombresM').attr('style', '');
                    $('#txtDniM').attr('style', '');
                    $('#txtEmailM').attr('style', '');
                    //$('#txtTelfMovilM').attr('style', '');
                    //$('#txtTelfFijoM').attr('style', '');
                    //$('#txtDireccion').attr('style', '');
                    $('#dllPerfilM').attr('style', '');
                    $('#txtUsuarioM').attr('style', '');
                    $('#txtClaveM').attr('style', '');
                    if ($('#dllPerfilM').val() == '3') {
                        $('#dllSupervisor').attr('style', '');
                    }

                    if ($('#txtApellidoPaternoM').val() == '') {
                        $('#txtApellidoPaternoM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el apellido paterno!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtApellidoMaternoM').val() == '') {
                        $('#txtApellidoMaternoM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el apellido materno!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtNombresM').val() == '') {
                        $('#txtNombresM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese los nombres!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtDniM').val() == '') {
                        $('#txtDniM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el DNI!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtEmailM').val() == '') {
                        $('#txtEmailM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el correo electrónico!", "registeruser");
                        validateItems = false;
                    }
                    //else if ($('#txtDireccion').val() == '') {
                    //    $('#txtDireccion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    //    addnotify("notify", "¡Ingrese la dirección de domicilio!", "registeruser");
                    //    validateItems = false;
                    //} else if ($('#txtTelfMovilM').val() == '') {
                    //    $('#txtTelfMovilM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    //    addnotify("notify", "¡Ingrese el número de teléfono celular!", "registeruser");
                    //    validateItems = false;
                    //} else if ($('#txtTelfFijoM').val() == '') {
                    //    $('#txtTelfFijoM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    //    addnotify("notify", "¡Ingrese el número de teléfono fijo!", "registeruser");
                    //    validateItems = false;
                    //}
                    else if ($('#dllPerfilM').val() == '-1') {
                        $('#dllPerfilM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija un perfil para el usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtUsuarioM').val() == '') {
                        $('#txtUsuarioM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingre un nombre de usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtClaveM').val() == '') {
                        $('#txtClaveM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese la clave para el usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#dllPerfilM').val() == '3' && $('#dllSupervisor').val() == '-1') {
                        $('#dllSupervisor').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija un supervisor para el Usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#dllPerfilM').val() == '3' && $('#dllCentroCosto').val() == '-1') {
                        $('#dllCentroCosto').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija un centro de costo para el Usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#dllPerfilM').val() == '2' && $('#dllLineaNegocio').val() == '-1') {
                        $('#dllLineaNegocio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija una línea de negocio para el Usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#dllPerfilM').val() == '2' && $('#dllCentroCosto').val() == '-1') {
                        $('#dllCentroCosto').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija un centro de costo para el Usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#dllPerfilM').val() == '5' && $('#dllLineaNegocio').val() == '-1') {
                        $('#dllLineaNegocio').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija una línea de negocio para el Usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#dllPerfilM').val() == '5' && $('#dllCentroCosto').val() == '-1') {
                        $('#dllCentroCosto').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija un centro de costo para el Usuario!", "registeruser");
                        validateItems = false;
                    }

                    if (validateItems) {
                        $.ajax({
                            type: 'POST',
                            url: urlsavN,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getSala()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardar').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {

                                var datos = $.parseJSON(data.d);

                                addnotify("notify", datos.respuesta, "registeruser");
                                //$('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");

                                //var datos = $.parseJSON(msg.d);
                                //if (datos == "ok") {
                                //    addnotify("notify", "Usuario registrado correctamente.", "registeruser");
                                //    //$('#divContenidoModal').html(data);
                                //    $('#divMyModal').modal('hide');
                                //    $('#buscar').trigger("click");
                                //} else {
                                //    addnotify("notify", "El DNI del usuario ya existe", "registeruser");
                                //    //$('#divContenidoModal').html(data);
                                //    $('#divMyModal').modal('hide');
                                //    $('#buscar').trigger("click");
                                //}

                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardar').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

//MODIFICAR USUARIO
function ModificarUsuario() {
    $('.editItemReg').live('click', function (e) {
        var strReg = $(this).attr('id_usuario');
        var strData = new Object();
        strData.id_usuario = strReg;
        $.ajax({
            type: 'POST',
            url: urlins,
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (data) {
                $("#divContenidoModal").html(data);
                $('#divMyModal').modal({ backdrop: 'static', keyboard: false, show: true });

                var idLineaNegocio = $('#hdnIdLineaNegocio').val();
                var idCentroCosto = $('#hdnIdCentroCosto').val();

                $('#dllLineaNegocio option[value=' + idLineaNegocio + ']').attr("selected", "selected");
                $('#dllCentroCosto option[value=' + idCentroCosto + ']').attr("selected", "selected");


                //$('#dllPerfilM').change(function () {
                //    var idPerfil = $('#dllPerfilM').val();
                //    switch (idPerfil) {
                //        case '-1':
                //            $('#dllLineaNegocio').attr('disabled', true);
                //            $('#dllCentroCosto').attr('disabled', true);
                //            break;
                //        case '2':
                //            $('#dllLineaNegocio').attr('disabled', false);
                //            $('#dllCentroCosto').attr('disabled', false);
                //            break;
                //        case '3':
                //            $('#dllLineaNegocio').attr('disabled', true);
                //            $('#dllCentroCosto').attr('disabled', true);
                //            break;
                //    }
                //});

                var perfil = $("#dllPerfilM").val();
                switch (perfil) {
                    case '-1':
                        $('#dllLineaNegocio').attr('disabled', true);
                        $('#dllCentroCosto').attr('disabled', true);
                        break;
                    case '1':
                        $('#cbxLineaCosto').hide();
                        $('#dllLineaNegocio').attr('disabled', true);
                        $('#dllCentroCosto').attr('disabled', true);
                        $('#dllLineaNegocio').hide();
                        $('#dllCentroCosto').hide();
                        break;
                    case '2':
                        $('#dllLineaNegocio').attr('disabled', false);
                        $('#dllCentroCosto').attr('disabled', false);
                        break;
                    case '3':
                        $('#dllLineaNegocio').attr('disabled', true);
                        $('#dllCentroCosto').attr('disabled', true);
                        break;
                    case '4':
                        $('#dllLineaNegocio').attr('disabled', true);
                        $('#dllCentroCosto').attr('disabled', true);
                        $('#cbxLineaCosto').hide();
                        break;
                    case '5':
                        $('#dllLineaNegocio').attr('disabled', false);
                        $('#dllCentroCosto').attr('disabled', false);
                        break;
                }


                var id_perfil_admin = $('#HidIdPerfil').val();
                var id_admin = $('#HidIdAdminGeneral').val();
                switch (perfil) {
                    case "-1": $('#cbxSupervisor').hide();
                        break;
                    case "1": $('#cbxSupervisor').hide();
                        break;
                    case "2": $('#cbxSupervisor').hide();
                        $('#cbxSupervisor').hide();

                        break;
                    case "3":
                        $('#cbxSupervisor').show();
                        var strData = new Object();
                        //strData.codigoPerfil = perfil;
                        //strData.id_perfil_admin = id_perfil_admin;
                        //strData.id_admin = id_admin;
                        $.ajax({
                            type: 'POST',
                            url: 'NuevoUsuario.aspx/ObtenerSupervisores',
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(strData),
                            success: function (msg) {
                                $("#dllSupervisor").html(null);
                                var option = $(document.createElement('option'));
                                option.text(".:SELECCIONE:.");
                                option.val("-1");
                                $("#dllSupervisor").append(option);
                                var datos = $.parseJSON(msg.d);
                                $(datos).each(function () {
                                    var option = $(document.createElement('option'));

                                    option.text(this.nombre);
                                    option.val(this.codigo);

                                    $("#dllSupervisor").append(option);
                                });
                                var id_jefe = $('#HidIdJefe').val();
                                $('#dllSupervisor option[value=' + id_jefe + ']').attr("selected", "selected");
                            },
                            error: function (xhr, status, error) {
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                        break;
                    case "4": $('#cbxSupervisor').hide();
                        break;
                }



                $('#btnGuardar').click(function (e) {

                    var validateItems = true;

                    $('#txtApellidoPaternoM').attr('style', '');
                    $('#txtApellidoMaternoM').attr('style', '');
                    $('#txtNombresM').attr('style', '');
                    $('#txtDniM').attr('style', '');
                    $('#txtEmailM').attr('style', '');
                    //$('#txtTelfMovilM').attr('style', '');
                    //$('#txtTelfFijoM').attr('style', '');
                    //$('#txtDireccion').attr('style', '');
                    $('#dllPerfilM').attr('style', '');
                    $('#txtUsuarioM').attr('style', '');
                    $('#txtClaveM').attr('style', '');

                    if ($('#txtApellidoPaternoM').val() == '-1') {
                        $('#txtApellidoPaternoM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el apellido paterno!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtApellidoMaternoM').val() == '') {
                        $('#txtApellidoMaternoM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el apellido materno!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtNombresM').val() == '') {
                        $('#txtNombresM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese los nombres!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtDniM').val() == '') {
                        $('#txtDniM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el DNI!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtEmailM').val() == '') {
                        $('#txtEmailM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese el correo electrónico!", "registeruser");
                        validateItems = false;
                    }
                    //else if ($('#txtTelfMovilM').val() == '') {
                    //    $('#txtTelfMovilM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    //    addnotify("notify", "¡Ingrese el número de teléfono celular!", "registeruser");
                    //    validateItems = false;
                    //} else if ($('#txtTelfFijoM').val() == '') {
                    //    $('#txtTelfFijoM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    //    addnotify("notify", "¡Ingrese el número de teléfono fijo!", "registeruser");
                    //    validateItems = false;
                    //} else if ($('#txtDireccion').val() == '') {
                    //    $('#txtDireccion').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                    //    addnotify("notify", "¡Ingrese la dirección de domicilio!", "registeruser");
                    //    validateItems = false;
                    //}
                    else if ($('#dllPerfilM').val() == '-1') {
                        $('#dllPerfilM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija un perfil para el usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtUsuarioM').val() == '') {
                        $('#txtUsuarioM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingre un nombre de usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#txtClaveM').val() == '') {
                        $('#txtClaveM').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Ingrese la clave para el usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#dllPerfilM').val() == '3' && $('#dllSupervisor').val() == '-1') {
                        $('#dllSupervisor').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija un supervisor para el Usuario!", "registeruser");
                        validateItems = false;
                    } else if ($('#dllPerfilM').val() == '2' && $('#dllAdminGeneral').val() == '-1') {
                        $('#dllAdminGeneral').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                        addnotify("notify", "¡Elija un administrador general para el Supervisor!", "registeruser");
                        validateItems = false;
                    }

                    if (validateItems) {
                        //addnotify("notify", "Trabajo Agregado correctamente.", "registeruser");

                        $.ajax({
                            type: 'POST',
                            url: urlsavE,
                            contentType: "application/json; charset=utf-8",
                            async: true,
                            cache: false,
                            data: JSON.stringify(getSala()),
                            beforeSend: function () {
                                $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                //$('#btnLimpiar').hide();
                                $('#btnGuardar').hide();
                                $('#btnCancelar').hide();
                            },
                            success: function (data) {
                                addnotify("notify", "Usuario actualizado correctamente", "registeruser");
                                $('#divContenidoModal').html(data);
                                $('#divMyModal').modal('hide');
                                $('#buscar').trigger("click");
                            },
                            error: function (xhr, status, error) {
                                $('#msg').html('');
                                //$('#btnLimpiar').show();
                                $('#btnGuardar').show();
                                $('#btnCancelar').show();
                                addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                            }
                        });
                    }
                });

            },
            error: function (xhr, status, error) {
                $("#divContenidoModal").html(alertHtml('error', xhr.responseText));
                $('#divMyModal').modal('hide');

            }
        });

    });
}

function DescargarFoto() {

    $('#btnDescargar').click(function () {
        var strData = new Object();
        var codRequerimiento = $('#hdnCodRequerimiento').val();

        window.location.href = 'DescargaZipFotos.ashx?codRequerimiento=' + codRequerimiento;
    });
}

function ModalUsuario() {
    $('#txtDniM').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    //txtTelfMovilM
    $('#txtTelfMovilM').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('#txtTelfFijoM').keyup(function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $('#dllPerfilM').change(function () {
        var usuarioActual = $('#HidIdUsuario').val();
        var perfil = $("#dllPerfilM").val();
        var id_perfil_admin = $('#HidIdPerfil').val();
        var id_admin = $('#HidIdAdminGeneral').val();
        switch (perfil) {
            case "-1":
                $('#cbxSupervisor').hide();
                break;
            case "1":
                $('#cbxSupervisor').hide();
                break;
            case "2":
                $('#cbxSupervisor').hide();

                break;
            case "3":
                $('#cbxSupervisor').show();
                var strData = new Object();
                strData.codigoPerfil = perfil;
                strData.id_perfil_admin = id_perfil_admin;
                strData.id_admin = id_admin;
                $.ajax({
                    type: 'POST',
                    url: 'NuevoUsuario.aspx/ObtenerSupervisores',
                    contentType: "application/json; charset=utf-8",
                    async: true,
                    cache: false,
                    data: JSON.stringify(strData),
                    success: function (msg) {
                        $("#dllSupervisor").html(null);
                        var option = $(document.createElement('option'));
                        option.text(".:SELECCIONE:.");
                        option.val("-1");
                        $("#dllSupervisor").append(option);
                        var datos = $.parseJSON(msg.d);
                        $(datos).each(function () {
                            var codigo = this.codigo;
                            if (usuarioActual != codigo)
                            {
                                var option = $(document.createElement('option'));

                                option.text(this.nombre);
                                option.val(this.codigo);


                                $("#dllSupervisor").append(option);
                            }
                        });
                        //var subcategoria = $('#IDSubCategoria').val();
                        //$('#dllSubCategoria option[value=' + subcategoria + ']').attr("selected", "selected");
                    },
                    error: function (xhr, status, error) {
                        addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                    }
                });
                break;
            case "4":
                $('#cbxSupervisor').hide();
                break;
            case "5":
                $('#cbxSupervisor').hide();
                break;
        }
    });
}

function ObtenerLineaNegocio() {

    var strData = new Object();
    id_perfil = $('#HidIdPerfil').val();
    id_usuario = $("#HidIdUsuario").val();
    strData.id_perfil = id_perfil;
    strData.id_usuario = id_usuario;

    $.ajax({
        type: 'POST',
        url: urlLN, //'AprobarRequerimiento.aspx/CargarComboSubCategoria'
        contentType: "application/json; charset=utf-8",
        async: true,
        cache: false,
        data: JSON.stringify(strData),
        success: function (msg) {
            $("#dllLineaNegocio").html(null);
            var datos = $.parseJSON(msg.d);
            var option = $(document.createElement('option'));

            if ($('#HidIdPerfil').val() == 3 || $('#HidIdPerfil').val() == 2) {
                $(datos).each(function () {
                    var option = $(document.createElement('option'));

                    option.text(this.nombre_linea_negocio);
                    option.val(this.id_linea_negocio);

                    $("#dllLineaNegocio").append(option);
                    $('#dllLineaNegocio').attr('disabled', true);
                    //$(".divSubCategoria").show();
                    //tieneSub = true;
                });
            }
            else {
                option.text(".:TODOS:.");
                option.val("-1");
                $("#dllLineaNegocio").append(option);

                if (datos.length > 0) {
                    $(datos).each(function () {
                        var option = $(document.createElement('option'));

                        option.text(this.nombre_linea_negocio);
                        option.val(this.id_linea_negocio);

                        $("#dllLineaNegocio").append(option);
                        //$(".divSubCategoria").show();
                        //tieneSub = true;
                    });
                }
            }
            //else {
            //    $(".divSubCategoria").hide();
            //}
        }
    });
}

function ObtenerCentroCoste() {

    var strDat = new Object();
    id_perfil = $('#HidIdPerfil').val();
    id_usuario = $("#HidIdUsuario").val();

    strDat.id_perfil = id_perfil;
    strDat.id_usuario = id_usuario;

    $.ajax({
        type: 'POST',
        url: urlCC,
        contentType: "application/json; charset=utf-8",
        async: true,
        cache: false,
        data: JSON.stringify(strDat),
        success: function (msg) {
            $("#dllCentroCoste").html(null);
            //$("#dllServicio").html(null);
            var datos = $.parseJSON(msg.d);
            var option = $(document.createElement('option'));

            if ($('#HidIdPerfil').val() == 3) {
                $(datos).each(function () {
                    var option = $(document.createElement('option'));

                    option.text(this.nombre_centro_coste);
                    option.val(this.id_centro_coste);

                    $("#dllCentroCoste").append(option);
                    $('#dllCentroCoste').attr('disabled', true);
                    //$(".divSubCategoria").show();
                    //tieneSub = true;
                });
            }
            else {
                option.text(".:TODOS:.");
                option.val("-1");
                $("#dllCentroCoste").append(option);

                if (datos.length > 0) {
                    $(datos).each(function () {
                        var option = $(document.createElement('option'));

                        option.text(this.nombre_centro_coste);
                        option.val(this.id_centro_coste);

                        $("#dllCentroCoste").append(option);
                    });
                }
            }
        }
    });
}

function CombosModalUsuario() {
    if ($('#dllPerfilM').val() == '-1') {
        $('#cbxLineaCosto').hide();
        $('#dllLineaNegocio').attr('disabled', true);
        $('#dllCentroCosto').attr('disabled', true);
        $('#dllLineaNegocio').hide();
        $('#dllCentroCosto').hide();
    }

    $('#dllPerfilM').change(function () {
        var idPerfil = $('#dllPerfilM').val();
        switch (idPerfil) {
            case '-1':
                $('#cbxLineaCosto').hide();
                $('#dllLineaNegocio').attr('disabled', true);
                $('#dllCentroCosto').attr('disabled', true);
                $('#dllLineaNegocio').hide();
                $('#dllCentroCosto').hide();

                break;
            case '1':
                $('#cbxLineaCosto').hide();
                $('#dllLineaNegocio').attr('disabled', true);
                $('#dllCentroCosto').attr('disabled', true);
                $('#dllLineaNegocio').hide();
                $('#dllCentroCosto').hide();
                break;
            case '2':
                $('#cbxLineaCosto').show();
                $('#dllLineaNegocio').show();
                $('#dllCentroCosto').show();
                $('#dllLineaNegocio').attr('disabled', false);
                $('#dllCentroCosto').attr('disabled', false);
                break;
            case '3':
                $('#cbxLineaCosto').show();
                $('#dllLineaNegocio').show();
                $('#dllCentroCosto').show();
                $('#dllLineaNegocio').attr('disabled', true);
                $('#dllCentroCosto').attr('disabled', true);
                break;
            case '4':
                $('#dllLineaNegocio').attr('disabled', true);
                $('#dllCentroCosto').attr('disabled', true);
                $('#cbxLineaCosto').hide();
                break;
            case '5':
                $('#cbxLineaCosto').show();
                $('#dllLineaNegocio').show();
                $('#dllCentroCosto').show();
                $('#dllLineaNegocio').attr('disabled', false);
                $('#dllCentroCosto').attr('disabled', false);
                break;
        }
    });

    $('#dllSupervisor').change(function () {
        var idSupervisor = $('#dllSupervisor').val();
        strData = new Object();
        strData.idPerfil = '2';
        strData.idSupervisor = idSupervisor;
        $.ajax({
            type: 'POST',
            url: urlComboLineaNegocio,
            contentType: "application/json; charset=utf-8",
            async: true,
            cache: false,
            data: JSON.stringify(strData),
            success: function (msg) {
                $("#dllLineaNegocio").html(null);
                var option = $(document.createElement('option'));
                option.text(".:SELECCIONE:.");
                option.val("-1");
                $("#dllLineaNegocio").append(option);
                var idLineaNegocio;
                //$("#dllServicio").html(null);
                var datos = $.parseJSON(msg.d);
                if (datos != null) {
                    $(datos).each(function () {
                        var option = $(document.createElement('option'));

                        option.text(this.nombre_linea_negocio);
                        option.val(this.id_linea_negocio);
                        idLineaNegocio = this.id_linea_negocio;
                        $("#dllLineaNegocio").append(option);
                    });
                }
                $('#dllLineaNegocio').attr('disabled', true);
                $('#dllCentroCosto').attr('disabled', false);
                $('#dllLineaNegocio option[value=' + idLineaNegocio + ']').attr("selected", "selected");
            }
        });
    });
}

//MOSTRAR COMENTARIOS
function MostrarComentarios() {
    $('.MostrarComentario').live('click', function (e) {
        var strR = $(this).attr('cod_requerimiento');
        var strRc = $(this).attr('cod_estado_requerimiento');
        var strDa = new Object();
        strDa.cod_requerimiento = strR;
        //alert(strData);
        if (strRc == 4) {
            addnotify("notify", "¡El requerimiento ha sido cerrado!");
        }
        else {
            $("#divContenidoModalComentario").html('');
            $.ajax({
                type: 'POST',
                url: urlbuscom,
                async: true,
                cache: false,
                data: JSON.stringify(),
                beforeSend: function () {
                    $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                    //$('#btnLimpiar').hide();
                    //$('#btnGuardar').hide();
                    //$('#btnCancelar').hide();
                },
                success: function (data) {
                    $("#divContenidoModalComentario").html(data);
                    $('#divModalComentario').modal({ backdrop: 'static', keyboard: false, show: true });

                    $.ajax({
                        type: 'POST',
                        url: urlbuscome,
                        contentType: "application/json; charset=utf-8",
                        async: true,
                        cache: false,
                        data: JSON.stringify(strDa),
                        success: function (msg) {

                            var datos = $.parseJSON(msg.d);

                            $(datos).each(function () {
                                var option = $(document.createElement("textarea"));
                                var title = $(document.createElement("input"));
                                var descripcion = $(document.createElement("input"));

                                var miTitle = $(document.createElement("input"));
                                var miDescripcion = $(document.createElement("input"));

                                var idPerfil = this.idPerfil;
                                var miID = $('#HidPerfil').val();
                                option.text(this.comentarios);

                                $("#cbxComentarios input").attr('disabled', true);
                                if(miID == this.idPerfil){
                                    
                                    miTitle.val(this.fecha);
                                    miTitle.addClass("fechaComentario2 form-control col-lg-2");
                                    miDescripcion.val(this.descripcionUsuario);
                                    miDescripcion.addClass("descripcion2 form-control col-lg-5");
                                    //INPUT FECHA
                                    //$("#cbxComentarios input").addClass("form-control col-lg-2");
                                   
                                    $("#cbxComentarios").append(miTitle);
                                    $("#cbxComentarios input.fechaComentario2").attr('style', 'padding-bottom:0px; width:135px; height:20px; background-color: #f7f7f7;');
                                    
                                    $("#cbxComentarios").append(miDescripcion);
                                    $("#cbxComentarios input.descripcion2").attr('style', 'padding-bottom:0px; height:20px; background-color: #f7f7f7; font-weight:600; margin-left:28%;');
                                    $("#cbxComentarios input.descripcion2").attr('disabled', true);
                                    $("#cbxComentarios input.fechaComentario2").attr('disabled', true);
                                    $("#cbxComentarios").append(option);

                                    $("#cbxComentarios textarea").addClass("form-control col-lg-11");
                                    $("#cbxComentarios textarea").attr('disabled', true);
                                    $("#cbxComentarios textarea").attr('style', 'margin-bottom: 15px; resize: none;');
                                } else {
                                    title.val(this.fecha);
                                    title.addClass("fechaComentario form-control col-lg-2");
                                    descripcion.val(this.descripcionUsuario);
                                    descripcion.addClass("descripcion form-control col-lg-5");

                                    $("#cbxComentarios").append(descripcion);
                                    $("#cbxComentarios input.descripcion").attr('style', 'padding-bottom:0px; height:20px; background-color: #f7f7f7; font-weight:600;');
                                    $("#cbxComentarios").append(title);
                                    $("#cbxComentarios input.fechaComentario").attr('style', 'padding-bottom:0px; width:135px; height:20px; background-color: #f7f7f7; margin-left:28%;');
                                    $("#cbxComentarios input.descripcion").attr('disabled', true);
                                    $("#cbxComentarios input.fechaComentario").attr('disabled', true);
                                    $("#cbxComentarios").append(option);
                                    //$("textarea").addClass("form-control col-lg-11 textareas");

                                    $("#cbxComentarios textarea").addClass("form-control col-lg-11");
                                    $("#cbxComentarios textarea").attr('disabled', true);
                                    $("#cbxComentarios textarea").attr('style', 'margin-bottom: 15px; resize: none;');
                                }
                                
                            });

                            $('#txtaComentario').attr('disabled', false);
                            $('.modal-field').scrollTop($('.modal-field')[0].scrollHeight);

                            $('#btnGuardar').click(function (e) {
                                e.preventDefault();
                                var strDataa = new Object();
                                strDataa.comentario = $('#txtaComentario').val();
                                strDataa.cod_requerimiento = strR;
                                strDataa.idUsuario = $('#HidIdUsuario').val();
                                var validateItems = true;
                                $('#txtaComentario').attr('style', '');

                                if ($('#txtaComentario').val() == '') {
                                    $('#txtaComentario').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
                                    addnotify("notify", "¡Ingrese un comentario!", "registeruser");
                                    validateItems = false;
                                }

                                //$(".modal-field").animate({ scrollTop: $('.modal-field')[0].scrollHeight });

                                if (validateItems) {
                                    $.ajax({
                                        type: 'POST',
                                        url: urlEnCom,
                                        contentType: "application/json; charset=utf-8",
                                        async: true,
                                        cache: false,
                                        data: JSON.stringify(strDataa),
                                        beforeSend: function () {
                                            $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
                                            //$('#btnLimpiar').hide();
                                            $('#btnGuardar').hide();
                                            $('#btnCancelar').hide();
                                        },
                                        success: function (data) {

                                            var datos = $.parseJSON(data.d);

                                            addnotify("notify", datos.respuesta, "registeruser");
                                            //$('#divContenidoModal').html(data);
                                            $('#divModalComentario').modal('hide');
                                            $('#buscar').trigger("click");

                                        },
                                        error: function (xhr, status, error) {
                                            $('#msg').html('');
                                            //$('#btnLimpiar').show();
                                            $('#btnGuardar').show();
                                            $('#btnCancelar').show();
                                            addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                                        }
                                    });
                                }
                            });
                        },
                        error: function (xhr, status, error) {
                            $('#msg').html('');
                            //$('#btnLimpiar').show();
                            //$('#btnGuardar').show();
                            //$('#btnCancelar').show();
                            addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
                        }
                    });
                },
                error: function (xhr, status, error) {
                    $("#divContenidoModalComentario").html(alertHtml('error', xhr.responseText));
                    $('#divMyModalComentario').modal('hide');
                }
            });
        }
    });
}

//ENVIAR COMENTARIO
//function EnviarComentario() {
//    $('btnGuardar').click(function (e) {
//        e.preventDefault();
//        var strR = $(this).attr('cod_requerimiento');
//        var strDataa = new Object();
//        strDataa.comentario = $('#txtaComentario').val();
//        strDataa.cod_requerimiento = strR;

//        var validateItems = true;
//        $('#txtaComentario').attr('style', '');

//        if ($('#txtaComentario').val() == '') {
//            $('#txtaComentario').attr('style', 'border-color: #b94a48;outline: 0;box-shadow: inset 0 1px 1px rgba(0,0,0,-6.925),0 0 8px rgba(91,67,68,.6);')
//            addnotify("notify", "¡Ingrese un comentario!", "registeruser");
//            validateItems = false;
//        }

//        if (validateItems) {
//            $.ajax({
//                type: 'POST',
//                url: urlEnCom,
//                contentType: "application/json; charset=utf-8",
//                async: true,
//                cache: false,
//                data: JSON.stringify(strDataa),
//                beforeSend: function () {
//                    $('#msg').html('<center><img src="../../images/icons/loader/ico_loader-arrow-orange.gif" />Espere un momento por Favor...</center>');
//                    //$('#btnLimpiar').hide();
//                    $('#btnGuardar').hide();
//                    $('#btnCancelar').hide();
//                },
//                success: function (data) {

//                    var datos = $.parseJSON(data.d);

//                    addnotify("notify", datos.respuesta, "registeruser");
//                    //$('#divContenidoModal').html(data);
//                    $('#divMyModal').modal('hide');
//                    $('#buscar').trigger("click");

//                },
//                error: function (xhr, status, error) {
//                    $('#msg').html('');
//                    //$('#btnLimpiar').show();
//                    $('#btnGuardar').show();
//                    $('#btnCancelar').show();
//                    addnotify("notify", "Ocurrio un Error, Vuelve intentarlo mas tarde.", "registeruser");
//                }
//            });
//        }
//    });
//}