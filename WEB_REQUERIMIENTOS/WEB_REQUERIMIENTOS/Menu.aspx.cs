﻿using Controlador;
using Modelo.BEANS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidad;

namespace WEB_REQUERIMIENTOS
{
    public partial class Menu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                StringBuilder sbmenutop = new StringBuilder();
                StringBuilder sbmenulateral = new StringBuilder();
                String OldcategoriaRef = "";

                BeanUsuario usuario = (BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO];
                List<BeanPermisos> lista = ControladorUsuario.permisosUsuario(usuario.id_perfil);
                if (lista.Count > 0)
                {
                    foreach (BeanPermisos bean in lista)
                    {
                        if (OldcategoriaRef != bean.Referencia)
                        {
                            if (OldcategoriaRef != "")
                            {
                                sbmenutop.Append("</div></div>");
                                sbmenulateral.Append("</div></div>");
                            }

                            //Instanciando categoria de menu
                            sbmenutop.Append("<div id=\"cz-principal-menu-option-" + bean.Referencia + "\" class=\"cz-principal-menu-option\">");
                            sbmenutop.Append("<div class=\"cz-principal-menu-option-button\">");
                            sbmenutop.Append("<div class=\"cz-principal-menu-option-title\">");
                            sbmenutop.Append("<div class=\"cz-principal-menu-option-image\"></div>");
                            sbmenutop.Append("<a>" + bean.NombreCategoria + "</a>");
                            sbmenutop.Append("</div>");
                            sbmenutop.Append("</div>");
                            sbmenutop.Append("<div class=\"cz-principal-menu-suboptions\">");

                            sbmenulateral.Append("<div class=\"cz-menu-lateral-option\">");
                            sbmenulateral.Append("<div id=\"cz-menu-lateral-option-" + bean.Referencia + "\" class=\"cz-menu-lateral-title\">");
                            sbmenulateral.Append("<div class=\"cz-menu-lateral-title-icon\"></div>");
                            sbmenulateral.Append("<div class=\"cz-menu-lateral-title-text\">" + bean.NombreCategoria + "</div>");
                            sbmenulateral.Append("<div class=\"cz-menu-lateral-title-status\">+</div>");
                            sbmenulateral.Append("</div>");
                            sbmenulateral.Append("<div class=\"cz-menu-lateral-suboptions\">");

                            OldcategoriaRef = bean.Referencia;
                        }
                        //Instanciando opcion de menu
                        sbmenutop.Append("<div class=\"cz-principal-menu-suboption\"><a href='" + bean.Url + "'>" + bean.NombreOpcion + "</a></div>");

                        sbmenulateral.Append("<div class=\"cz-menu-lateral-suboption\"><a href='" + bean.Url + "'>" + bean.NombreOpcion + "</a></div>");

                    }

                    //Instanciando final de categoria de menu
                    sbmenutop.Append("</div></div>");
                    sbmenulateral.Append("</div></div>");
                }


                MenuTop.Text = sbmenutop.ToString();
                MenuLateral.Text = sbmenulateral.ToString();

                if (Session[Configuracion.SESSION_OBJ_USUARIO] != null)
                { this.lbNomUsuario.Text = ((BeanUsuario)Session[Configuracion.SESSION_OBJ_USUARIO]).nombre_completo; }
                else
                { Response.Redirect("Default.aspx?acc=SES"); }

                Page.ClientScript.RegisterStartupScript(this.GetType(), "initialize", "$('#centerFrame').attr('src', 'Inicio.aspx');", true);
            }
            catch (Exception ex)
            {
                Utility.registrarLog("< ERROR [MENU.ASPX.CS]:> " + ex.Message);
                string myScript = "parent.document.location.href = 'Default.aspx?acc=EXT';";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
                //string myScript = "parent.document.location.href = '../../Default.aspx?acc=ERR';";
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, true);
            }
        }

        protected void opcInicio_Click(object sender, EventArgs e)
        {
            Response.Redirect("Menu.aspx");
        }

        protected void opcSalir_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("Default.aspx?acc=EXT");
        }
    }
}