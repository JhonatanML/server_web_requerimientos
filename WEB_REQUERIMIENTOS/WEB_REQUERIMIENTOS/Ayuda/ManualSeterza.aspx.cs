﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEB_REQUERIMIENTOS.Ayuda
{
    public partial class ManualSeterza : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Response.AddHeader("content-disposition", "attachment; filename=" + filename + ".pdf");
            //Response.End();
            String fileLocation = HttpContext.Current.Server.MapPath("~") + ConfigurationManager.AppSettings["ManualSeterza"];
            String filename = "Manual_Usuario_Seterza";
            Response.ContentType = "application/pdf";
            Response.WriteFile(fileLocation + filename + ".pdf");
            Response.AddHeader("content-disposition", "inline;filename=" + filename + ".pdf");
            Response.End();
        }
    }
}