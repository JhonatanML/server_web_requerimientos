﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AyudaAdminGeneral.aspx.cs" Inherits="WEB_REQUERIMIENTOS.Ayuda.AyudaAdminGeneral" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/jsmodule.js" type="text/javascript"></script>
    <script src="../js/Validacion.js" type="text/javascript"></script>
    <link href="../css/cz_main.ashx" type="text/css" rel="stylesheet" />
    <script src="../js/cz_main.js" type="text/javascript"></script>
    <link href="../css/styleBootStrap.css" rel="stylesheet" />
    <script src="../js/jquery.datetimepicker.js"></script>
    <link href="../css/jquery.datetimepicker.css" rel="stylesheet" />
    <script src="../js/Siedevs.js"></script>
</head>
<body>
    <div class="borrarContenido">
        <form id="form1" runat="server">
            <div class="cz-submain cz-submain-form-background">
                <div id="cz-form-box">
                    <div class="cz-form-box-content">
                        <div id="cz-form-box-content-title" style="margin-top: -12px;">
                            <div id="cz-form-box-content-title-text">
                                <span class="glyphicon glyphicon glyphicon-user btn-lg cz-form-box-content-title-icon" style="color: #666;" aria-hidden="true"></span>
                                <span class="font-size-20">Tutorial Administrador Empresa</span>
                            </div>
                        </div>
                    </div>

                    <div class="cz-form-box-content">
                        <div id="B" style="text-align: center;">
                            <iframe width="960" height="460" src="https://www.youtube.com/embed/ecYJRPwf92c?autoplay=1&theme=light&showinfo=0&rel=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div id="myModalCargando" class="modal hide fade in" style="display: block; background: transparent; border: none; -webkit-box-shadow: none;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
                    </div>
                    <div id="myModal" class="modal hide fade" style="display: block;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>
