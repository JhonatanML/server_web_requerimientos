﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Threading;
using System.Configuration;
using System.Web;
using Common.Logger;


namespace Utilidad
{
    public class Utility
    {
        public static void registrarLog(String msg)
        {
            string LOG_ACTIVO = ConfigurationManager.AppSettings["LOG_ACTIVO"];

            if (LOG_ACTIVO == "1")
            {
                string ruta = ConfigurationManager.AppSettings["RUTALOG"];
                Log.Write(HttpContext.Current.Server.MapPath("~") + ruta, msg);
            }
        }
        public static int calculateNumberOfPages(int totalNumberOfItems, int pageSize)
        {
            Int32 result = totalNumberOfItems % pageSize;
            if (result == 0)
                return totalNumberOfItems / pageSize;
            else
                return totalNumberOfItems / pageSize + 1;
        }

        public static string FormatDateTime(string inputDate, int tipo)
        {
            String resultado = inputDate;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("fr-FR");
            DateTime dt = DateTime.Parse(inputDate);

            if (tipo == 1)
            { resultado = dt.ToShortDateString(); }

            if (tipo == 2)
            { resultado = dt.ToShortTimeString(); }

            return resultado;
        }

        public static DateTime stringToDateTime(String strFecha)
        {
            char[] splitter = { '/' };
            String[] arrfecha = strFecha.Split(splitter);
            String dia = arrfecha[0];
            String mes = arrfecha[1];
            String anho = arrfecha[2];

            DateTime fecha = new DateTime(Convert.ToInt32(anho), Convert.ToInt32(mes), Convert.ToInt32(dia));

            return fecha;
        }


        public static string fechaSQL(string fecha)
        {
            char[] delim = { '/' };

            string[] arrFecha = fecha.Split(delim);

            return arrFecha[2] + arrFecha[1] + arrFecha[0];
        }

        public static bool esFechaValida(string fecha)
        {
            bool esValido = false;
            char[] delim = { '/' };
            string[] arrFecha;
            string anio;
            string mes;
            string dia;
            DateTime fecDateTime;

            try
            {
                arrFecha = fecha.Split(delim);
                anio = arrFecha[0];
                mes = arrFecha[1];
                dia = arrFecha[2];

                fecDateTime = new DateTime(int.Parse(anio), int.Parse(mes), int.Parse(dia));
                esValido = true;
            }
            catch
            {

            }

            return esValido;
        }

        public static string remplazarCS(string cadena)
        {
            cadena = cadena.Replace("&", "&amp;");
            cadena = cadena.Replace("" + (char)34 + "", "&quot;");
            cadena = cadena.Replace("$", "$$");
            cadena = cadena.Replace("<", "&lt;");
            cadena = cadena.Replace(">", "&gt;");
            cadena = cadena.Replace("'", "&#39;");
            cadena = cadena.Replace("¿", "&#191;");
            cadena = cadena.Replace("?", "&#63;");
            cadena = cadena.Replace("ÿ", "&#255;");

            return cadena;
        }

        public static Boolean isValid(int day, int month, int year)
        {
            if (!(month >= 1 && month <= 12)
            || !(day >= 1 && day <= 31)
            || !(year >= 1000 && year <= 9999)
            || (day == 31 &&
            (month == 4 /*april*/ || month == 6 /*june*/ ||
            month == 9 /*sept*/ || month == 11 /*nov*/ )) ||
            (month == 2 /*feb*/ && day > 29))
            {
                return false;
            }
            if (month == 2 && day == 29)
            { // if a year is divisible by 4 it is a leap year UNLESS it is also
                // divisible by 100 AND is not divisible by 400
                if (year % 4 > 0
                || ((year % 100 == 0) && (year % 400 > 0)))
                {
                    return false;
                }
            }
            return true;
        }

        public static Boolean validarFECHA(String fecha)
        {
            Boolean flag = true;
            try
            {
                string[] arrElementosFecha = fecha.Split('/');
                string day = arrElementosFecha[0];
                string month = arrElementosFecha[1];
                string year = arrElementosFecha[2];

                if (day != null && !day.Equals("") && month != null && !month.Equals("") && year != null && !year.Equals(""))
                    flag = isValid(int.Parse(day), int.Parse(month), int.Parse(year));
                else
                    flag = false;
            }
            catch (Exception e)
            {

                flag = false;
            }
            return flag;
        }

        public static Boolean validarHORA(String horapara)
        {
            Boolean flag = true;
            try
            {
                string[] arrElementosFecha = horapara.Split(':');
                int hora = int.Parse(arrElementosFecha[0]);
                int minuto = int.Parse(arrElementosFecha[1]);


                if (hora >= 0 && hora < 24 && minuto >= 0 && minuto < 60)
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception e)
            {

                flag = false;
            }
            return flag;
        }
        public static string linea()
        { return "-------------------<br/>"; }
        public static String numeroMes_a_NombreMes(Int32 numeroMes)
        {
            String nombreMes = "";
            switch (numeroMes)
            {
                case 1:
                    nombreMes = "Enero";
                    break;
                case 2:
                    nombreMes = "Febrero";
                    break;
                case 3:
                    nombreMes = "Marzo";
                    break;
                case 4:
                    nombreMes = "Abril";
                    break;
                case 5:
                    nombreMes = "Mayo";
                    break;
                case 6:
                    nombreMes = "Junio";
                    break;
                case 7:
                    nombreMes = "Julio";
                    break;
                case 8:
                    nombreMes = "Agosto";
                    break;
                case 9:
                    nombreMes = "Septiembre";
                    break;
                case 10:
                    nombreMes = "Octubre";
                    break;
                case 11:
                    nombreMes = "Noviembre";
                    break;
                case 12:
                    nombreMes = "Diciembre";
                    break;
                default:
                    nombreMes = "Mes no válido";
                    break;
            }
            return nombreMes.ToUpper();
        }

        public static string NombreMes(int numeroMes)
        {
            DateTimeFormatInfo dtinfo = new CultureInfo("es-ES", false).DateTimeFormat;
            return dtinfo.GetMonthName(numeroMes);
        }

        public static string PrimerLetraMayus(string value)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value);
        }
        public static string fechaActual(string formato)
        {
            return (DateTime.Now).ToString(formato);
        }
    }
}
