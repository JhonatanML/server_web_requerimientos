﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Web;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using OfficeOpenXml.Drawing;
using System.Globalization;
using System.Threading;
using Utilidad.excel;
using System.Configuration;


namespace Utilidad
{
    public class ExcelFileUtils
    {
        public static void ExportToExcel(ExcelFileSpreadsheet spreadsheet, String filename,bool showtotal,String total)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                int contWorksheet = 1;
                int initalCol = 2;
                int initialRow = 2;

                // Ingresamos los datos para las propiedades del archivo Excel
                p.Workbook.Properties.Author = spreadsheet.propertyAuthor;
                p.Workbook.Properties.Title = spreadsheet.propertyTitle;

                foreach (ExcelFileWorksheet worksheet in spreadsheet.worksheets)
                {   // Creamos un Worksheet
                    p.Workbook.Worksheets.Add(worksheet.sheetName);

                    ExcelWorksheet ws = p.Workbook.Worksheets[contWorksheet];
                    ws.Cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                    ws.Cells.Style.Font.Size = 10;
                    ws.Cells.Style.Font.Name = "Arial";

                    int colIndex = initalCol;
                    int rowIndex = initialRow;
                    int contHeader = 1;
                    int indexHeader = 0;
                    int contRow = 0;
                    DataTable dt = worksheet.dtSource;

                    if (!worksheet.sheetTitle.Equals(""))
                    {
                        // Unimos celdas para poder colocar el titulo de la grilla a mostrar
                        ws.Cells[colIndex, rowIndex].Value = worksheet.sheetTitle;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Merge = true;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.Font.Size = 12;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.Font.Bold = true;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rowIndex = rowIndex + 2;
                    }

                    // Creamos las cabeceras de la grilla
                    foreach (DataColumn dc in dt.Columns)
                    {
                        var cell = ws.Cells[rowIndex, colIndex];
                        cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                        var font = cell.Style.Font;
                        font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        font.Bold = true;

                        var border = cell.Style.Border;
                        border.Top.Style = ExcelBorderStyle.Thin;
                        border.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));


                        if (contHeader == 1)
                        {
                            border.Left.Style = ExcelBorderStyle.Thin;
                            border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        }

                        if (contHeader < dt.Columns.Count)
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(System.Drawing.Color.White);
                        }
                        else
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        }

                        // Colocamos el nombre de la celda en la cabecera
                        cell.Value = worksheet.columnHeader[indexHeader];

                        double cellSize = ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width;
                        double proposedCellSize = cell.Value.ToString().Trim().Length * 1.3;
                        if (cellSize <= proposedCellSize)
                        {
                            ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize;
                        }

                        colIndex++;
                        contHeader++;
                        indexHeader++;

                    }

                    contRow = 1;

                    // Agregamos el contenido del DataTable en las celdas de la grilla
                    foreach (DataRow dr in dt.Rows)
                    {
                        colIndex = initalCol;
                        rowIndex++;
                        int contCol = 1;
                        int cellIndex = 0;

                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];

                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;

                            if (rowIndex % 2 == 0)
                            {
                                fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ECEEE7"));
                            }

                            String strValue = "";
                            if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.TEXT))
                            {
                                cell.Value = dr[dc.ColumnName].ToString();
                                strValue = cell.Value.ToString();
                            }
                            else
                            {
                                if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.NUMERIC))
                                {
                                    cell.Value = Convert.ToInt32(dr[dc.ColumnName]);
                                    strValue = cell.Value.ToString();
                                }
                                else
                                {
                                    if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.FLOAT))
                                    {
                                        cell.Value = Convert.ToDecimal(dr[dc.ColumnName]);
                                        strValue = cell.Value.ToString();
                                    }
                                    else
                                    {
                                        if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DOUBLE))
                                        {
                                            cell.Value = Convert.ToDouble(dr[dc.ColumnName]);
                                            strValue = cell.Value.ToString();
                                        }
                                        else
                                        {
                                            if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DATE))
                                            {
                                                Thread.CurrentThread.CurrentCulture = new CultureInfo("fr-FR");
                                                DateTime datetime = DateTime.Parse(dr[dc.ColumnName].ToString());
                                                String day = datetime.ToString("dd");
                                                String month = datetime.ToString("MM");
                                                String year = datetime.ToString("yyyy");
                                                cell.Value = day + "/" + month + "/" + year;
                                                strValue = cell.Value.ToString();
                                            }
                                            else
                                            {
                                                if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.TIME))
                                                {
                                                    DateTime datetime = DateTime.Parse(dr[dc.ColumnName].ToString());
                                                    String hora = datetime.ToString("hh");
                                                    String minuto = datetime.ToString("mm");
                                                    cell.Value = hora + ":" + minuto;
                                                    strValue = cell.Value.ToString();
                                                }
                                                else
                                                {
                                                    if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DATETIME))
                                                    {
                                                        cell.Value = dr[dc.ColumnName].ToString();
                                                        strValue = cell.Value.ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            var border = cell.Style.Border;

                            if (contCol == 1)
                            {
                                border.Left.Style = ExcelBorderStyle.Thin;
                                border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            if (contCol < dt.Columns.Count)
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(System.Drawing.Color.White);
                            }
                            else
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            if (contRow == dt.Rows.Count)
                            {
                                border.Bottom.Style = ExcelBorderStyle.Thin;
                                border.Bottom.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            var font = cell.Style.Font;
                            font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            font.Bold = false;

                            double cellSize = ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width;
                            double proposedCellSize = strValue.Trim().Length * 1.3;
                            if (cellSize <= proposedCellSize)
                            { ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize; }

                            colIndex++;
                            contCol++;
                            cellIndex++;

                        }
                        contRow++;
                    }
                    if (showtotal)
                    {
var celda_total = ws.Cells[rowIndex+2, dt.Columns.Count+1];
                    celda_total.Value = total;
                      ws.Cells[rowIndex + 2, dt.Columns.Count ].Value = "TOTAL";
                    
                    }
                    
                    // Agregamos las imagenes que se incluiran en este Worksheet
                    string codigo_color = System.Configuration.ConfigurationManager.AppSettings["codigocolorcss"].ToString();
                    string tema = "entel-excel-";

                    if (codigo_color == "1")
                    {
                        tema = "nextel-excel-";
                    }
                    else if (codigo_color == "2")
                    {
                        tema = "entel-excel-";
                    }

                    String logoNextelPath = HttpContext.Current.Server.MapPath("~") + "/images/logo/" + tema + "logo.png";
                    Bitmap imageNextel = new Bitmap(logoNextelPath);

                    ExcelPicture pictureNextel = null;

                    //if (imageNextel != null) ACTIVAR LOGO
                    if (imageNextel == null)
                    {
                        pictureNextel = ws.Drawings.AddPicture("logoEntel", imageNextel);
                        pictureNextel.From.Column = initalCol - 1;
                        pictureNextel.From.Row = initialRow + contRow + 2;
                        pictureNextel.SetSize(imageNextel.Width - 20, imageNextel.Height);
                    }

                    contWorksheet++;
                }

                // Generamos el archivo Excel y mostramos el popup de descarga
                Byte[] bin = p.GetAsByteArray();
                DateTime date = DateTime.Now;
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "_" + date.Date.ToString("yyyyMMdd") + "_" + date.Hour + date.Minute + date.Millisecond + ".xlsx");
                context.Response.BinaryWrite(bin);
                context.Response.End();
            }
        }

        public static void ExportToExcel(ExcelFileSpreadsheet spreadsheet, String filename)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                int contWorksheet = 1;
                int initalCol = 2;
                int initialRow = 2;

                // Ingresamos los datos para las propiedades del archivo Excel
                p.Workbook.Properties.Author = spreadsheet.propertyAuthor;
                p.Workbook.Properties.Title = spreadsheet.propertyTitle;

                foreach (ExcelFileWorksheet worksheet in spreadsheet.worksheets)
                {   // Creamos un Worksheet
                    p.Workbook.Worksheets.Add(worksheet.sheetName);

                    ExcelWorksheet ws = p.Workbook.Worksheets[contWorksheet];
                    ws.Cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                    ws.Cells.Style.Font.Size = 10;
                    ws.Cells.Style.Font.Name = "Arial";

                    int colIndex = initalCol;
                    int rowIndex = initialRow;
                    int contHeader = 1;
                    int indexHeader = 0;
                    int contRow = 0;
                    DataTable dt = worksheet.dtSource;

                    if (!worksheet.sheetTitle.Equals(""))
                    {
                        // Unimos celdas para poder colocar el titulo de la grilla a mostrar
                        ws.Cells[colIndex, rowIndex].Value = worksheet.sheetTitle;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Merge = true;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.Font.Size = 12;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.Font.Bold = true;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rowIndex = rowIndex + 2;
                    }

                    // Creamos las cabeceras de la grilla
                    foreach (DataColumn dc in dt.Columns)
                    {
                        var cell = ws.Cells[rowIndex, colIndex];
                        cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                        var font = cell.Style.Font;
                        font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        font.Bold = true;

                        var border = cell.Style.Border;
                        border.Top.Style = ExcelBorderStyle.Thin;
                        border.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));


                        if (contHeader == 1)
                        {
                            border.Left.Style = ExcelBorderStyle.Thin;
                            border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        }

                        if (contHeader < dt.Columns.Count)
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(System.Drawing.Color.White);
                        }
                        else
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        }

                        // Colocamos el nombre de la celda en la cabecera
                        cell.Value = worksheet.columnHeader[indexHeader];

                        double cellSize = ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width;
                        double proposedCellSize = cell.Value.ToString().Trim().Length * 1.3;
                        if (cellSize <= proposedCellSize)
                        {
                            ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize;
                        }

                        colIndex++;
                        contHeader++;
                        indexHeader++;

                    }

                    contRow = 1;

                    // Agregamos el contenido del DataTable en las celdas de la grilla
                    foreach (DataRow dr in dt.Rows)
                    {
                        colIndex = initalCol;
                        rowIndex++;
                        int contCol = 1;
                        int cellIndex = 0;

                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];

                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;

                            if (rowIndex % 2 == 0)
                            {
                                fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ECEEE7"));
                            }

                            String strValue = "";
                            if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.TEXT))
                            {
                                cell.Value = dr[dc.ColumnName].ToString();
                                strValue = cell.Value.ToString();
                            }
                            else
                            {
                                if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.NUMERIC))
                                {
                                    cell.Value = Convert.ToInt32(dr[dc.ColumnName]);
                                    strValue = cell.Value.ToString();
                                }
                                else
                                {
                                    if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.FLOAT))
                                    {
                                        cell.Value = Convert.ToDecimal(dr[dc.ColumnName]);
                                        strValue = cell.Value.ToString();
                                    }
                                    else
                                    {
                                        if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DOUBLE))
                                        {
                                            cell.Value = Convert.ToDouble(dr[dc.ColumnName]);
                                            strValue = cell.Value.ToString();
                                        }
                                        else
                                        {
                                            if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DATE))
                                            {
                                                Thread.CurrentThread.CurrentCulture = new CultureInfo("fr-FR");
                                                DateTime datetime = DateTime.Parse(dr[dc.ColumnName].ToString());
                                                String day = datetime.ToString("dd");
                                                String month = datetime.ToString("MM");
                                                String year = datetime.ToString("yyyy");
                                                cell.Value = day + "/" + month + "/" + year;
                                                strValue = cell.Value.ToString();
                                            }
                                            else
                                            {
                                                if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.TIME))
                                                {
                                                    DateTime datetime = DateTime.Parse(dr[dc.ColumnName].ToString());
                                                    String hora = datetime.ToString("hh");
                                                    String minuto = datetime.ToString("mm");
                                                    cell.Value = hora + ":" + minuto;
                                                    strValue = cell.Value.ToString();
                                                }
                                                else
                                                {
                                                    if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DATETIME))
                                                    {
                                                        cell.Value = dr[dc.ColumnName].ToString();
                                                        strValue = cell.Value.ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            var border = cell.Style.Border;

                            if (contCol == 1)
                            {
                                border.Left.Style = ExcelBorderStyle.Thin;
                                border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            if (contCol < dt.Columns.Count)
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(System.Drawing.Color.White);
                            }
                            else
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            if (contRow == dt.Rows.Count)
                            {
                                border.Bottom.Style = ExcelBorderStyle.Thin;
                                border.Bottom.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            var font = cell.Style.Font;
                            font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            font.Bold = false;

                            double cellSize = ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width;
                            double proposedCellSize = strValue.Trim().Length * 1.3;
                            if (cellSize <= proposedCellSize)
                            { ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize; }

                            colIndex++;
                            contCol++;
                            cellIndex++;

                        }
                        contRow++;
                    }

                    // Agregamos las imagenes que se incluiran en este Worksheet
                    string codigo_color = System.Configuration.ConfigurationManager.AppSettings["codigocolorcss"].ToString();
                    string tema = "entel-excel-";

                    if (codigo_color == "1")
                    {
                        tema = "nextel-excel-";
                    }
                    else if (codigo_color == "2")
                    {
                        tema = "entel-excel-";
                    }

                    String logoNextelPath = HttpContext.Current.Server.MapPath("~") + "/images/logo/" + tema + "logo.png";
                    Bitmap imageNextel = new Bitmap(logoNextelPath);

                    ExcelPicture pictureNextel = null;

                    //if (imageNextel != null) ACTIVAR LOGO
                    if (imageNextel == null)
                    {
                        pictureNextel = ws.Drawings.AddPicture("logoEntel", imageNextel);
                        pictureNextel.From.Column = initalCol - 1;
                        pictureNextel.From.Row = initialRow + contRow + 2;
                        pictureNextel.SetSize(imageNextel.Width - 20, imageNextel.Height);
                    }

                    contWorksheet++;
                }

                // Generamos el archivo Excel y mostramos el popup de descarga
                Byte[] bin = p.GetAsByteArray();
                DateTime date = DateTime.Now;
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "_" + date.Date.ToString("yyyyMMdd") + "_" + date.Hour + date.Minute + date.Millisecond + ".xlsx");
                context.Response.BinaryWrite(bin);
                context.Response.End();
            }
        }
        public static void createExcelFile(ExcelFileSpreadsheet spreadsheet, String path, String fileName)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                int contWorksheet = 1;
                int initalCol = 2;
                int initialRow = 2;

                // Ingresamos los datos para las propiedades del archivo Excel
                p.Workbook.Properties.Author = spreadsheet.propertyAuthor;
                p.Workbook.Properties.Title = spreadsheet.propertyTitle;

                foreach (ExcelFileWorksheet worksheet in spreadsheet.worksheets)
                {
                    // Creamos un Worksheet
                    p.Workbook.Worksheets.Add(worksheet.sheetName);

                    ExcelWorksheet ws = p.Workbook.Worksheets[contWorksheet];
                    ws.Cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                    ws.Cells.Style.Font.Size = 10;
                    ws.Cells.Style.Font.Name = "Arial";

                    int colIndex = initalCol;
                    int rowIndex = initialRow;
                    int contHeader = 1;
                    int indexHeader = 0;

                    DataTable dt = worksheet.dtSource;

                    if (!worksheet.sheetTitle.Equals(""))
                    {   // Unimos celdas para poder colocar el titulo de la grilla a mostrar
                        ws.Cells[colIndex, rowIndex].Value = worksheet.sheetTitle;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Merge = true;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.Font.Size = 12;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.Font.Bold = true;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        rowIndex = rowIndex + 2;
                    }

                    // Creamos las cabeceras de la grilla
                    foreach (DataColumn dc in dt.Columns)
                    {
                        var cell = ws.Cells[rowIndex, colIndex];
                        cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                        var font = cell.Style.Font;
                        font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        font.Bold = true;

                        var border = cell.Style.Border;
                        border.Top.Style = ExcelBorderStyle.Thin;
                        border.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));


                        if (contHeader == 1)
                        {
                            border.Left.Style = ExcelBorderStyle.Thin;
                            border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        }

                        if (contHeader < dt.Columns.Count)
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(System.Drawing.Color.White);
                        }
                        else
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        }

                        // Colocamos el nombre de la celda en la cabecera
                        cell.Value = worksheet.columnHeader[indexHeader];

                        double cellSize = ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width;
                        double proposedCellSize = cell.Value.ToString().Trim().Length * 1.3;

                        if (cellSize <= proposedCellSize)
                        {
                            ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize;
                        }

                        colIndex++;
                        contHeader++;
                        indexHeader++;

                    }

                    int contRow = 1;

                    // Agregamos el contenido del DataTable en las celdas de la grilla
                    foreach (DataRow dr in dt.Rows)
                    {
                        colIndex = initalCol;
                        rowIndex++;
                        int contCol = 1;
                        int cellIndex = 0;

                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];

                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;

                            if (rowIndex % 2 == 0)
                            { fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ECEEE7")); }

                            String strValue = "";
                            if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.TEXT))
                            {
                                cell.Value = dr[dc.ColumnName].ToString();
                                strValue = cell.Value.ToString();
                            }
                            else
                            {
                                if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.NUMERIC))
                                {
                                    cell.Value = Convert.ToInt32(dr[dc.ColumnName]);
                                    strValue = cell.Value.ToString();
                                }
                                else
                                {
                                    if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.FLOAT))
                                    {
                                        cell.Value = Convert.ToDecimal(dr[dc.ColumnName]);
                                        strValue = cell.Value.ToString();
                                    }
                                    else
                                    {
                                        if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DOUBLE))
                                        {
                                            cell.Value = Convert.ToDouble(dr[dc.ColumnName]);
                                            strValue = cell.Value.ToString();
                                        }
                                        else
                                        {
                                            if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DATE))
                                            {
                                                DateTime datetime = DateTime.Parse(dr[dc.ColumnName].ToString());
                                                String day = datetime.ToString("dd");
                                                String month = datetime.ToString("MM");
                                                String year = datetime.ToString("yyyy");
                                                cell.Value = day + "/" + month + "/" + year;
                                                strValue = cell.Value.ToString();
                                            }
                                            else
                                            {
                                                if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.TIME))
                                                {
                                                    DateTime datetime = DateTime.Parse(dr[dc.ColumnName].ToString());
                                                    String hora = datetime.ToString("hh");
                                                    String minuto = datetime.ToString("mm");
                                                    cell.Value = hora + ":" + minuto;
                                                    strValue = cell.Value.ToString();
                                                }
                                                else
                                                {
                                                    if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DATETIME))
                                                    {
                                                        cell.Value = dr[dc.ColumnName].ToString();
                                                        strValue = cell.Value.ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            var border = cell.Style.Border;

                            if (contCol == 1)
                            {
                                border.Left.Style = ExcelBorderStyle.Thin;
                                border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            if (contCol < dt.Columns.Count)
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(System.Drawing.Color.White);
                            }
                            else
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            if (contRow == dt.Rows.Count)
                            {
                                border.Bottom.Style = ExcelBorderStyle.Thin;
                                border.Bottom.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            var font = cell.Style.Font;
                            font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            font.Bold = false;

                            double cellSize = ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width;
                            double proposedCellSize = strValue.Trim().Length * 1.3;
                            if (cellSize <= proposedCellSize)
                            { ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize; }

                            colIndex++;
                            contCol++;
                            cellIndex++;

                        }
                        contRow++;

                    }

                    //How to Add a Image using EP Plus

                    string codigo_color = System.Configuration.ConfigurationManager.AppSettings["codigocolorcss"].ToString();
                    string tema = "entel-excel-";

                    if (codigo_color == "1")
                    {
                        tema = "nextel-excel-";
                    }
                    else if (codigo_color == "2")
                    {
                        tema = "entel-excel-";
                    }

                    String logoNextelPath = HttpContext.Current.Server.MapPath("~") + "/images/logo/" + tema + "logo.png";
                    String logoDescargaPath = HttpContext.Current.Server.MapPath("~") + "/images/logo/" + tema + "descarga.png";
                    Bitmap imageNextel = new Bitmap(logoNextelPath);
                    Bitmap imageDescarga = new Bitmap(logoDescargaPath);

                    ExcelPicture pictureNextel = null;
                    ExcelPicture pictureDescarga = null;

                    if (imageNextel != null)
                    {
                        pictureNextel = ws.Drawings.AddPicture("logoNextel", imageNextel);
                        pictureNextel.From.Column = initalCol - 1;
                        pictureNextel.From.Row = initialRow + contRow + 2;
                        pictureNextel.SetSize(imageNextel.Width - 20, imageNextel.Height);
                    }
                    if (imageDescarga != null)
                    {
                        int imgRow = initialRow + contRow + 2;
                        int imgCol = initalCol + (dt.Columns.Count - 1);

                        int cellwidth = ExcelHelper.ColumnWidth2Pixel(ws, ws.Cells[imgRow, imgCol].Worksheet.Column(imgCol).Width);
                        int imagewidth = imageDescarga.Width;
                        int diff = cellwidth - imagewidth;
                        pictureDescarga = ws.Drawings.AddPicture("logoDescarga", imageDescarga);
                        pictureDescarga.SetPosition(imgRow, 0, imgCol - 1, diff);
                        pictureDescarga.SetSize(imageDescarga.Width, imageDescarga.Height);

                    }
                    contWorksheet++;
                }

                // Generamos el archivo Excel a ser copiado en el servidor
                Byte[] bin = p.GetAsByteArray();
                DateTime date = DateTime.Now;
                String file = path + "/" + fileName + ".xlsx";
                File.WriteAllBytes(file, bin);
            }
        }

        public static ExcelFileSpreadsheet prepararExportacionExcel(DataTable dt, String SheetTitle, String SheetName)
        {
            //custom AlexPG
            ExcelFileSpreadsheet ss = new ExcelFileSpreadsheet();
            ss.propertyAuthor = "Entel";
            ss.propertyTitle = ConfigurationSettings.AppSettings["APP_NAME"] + " - AVANCE METRICAS";

            ExcelFileWorksheet ws = new ExcelFileWorksheet();
            ws.sheetTitle = SheetTitle;
            ws.sheetName = SheetName;
            ws.dtSource = dt;
            foreach (DataColumn col in dt.Columns)
            {
                String tipo = col.DataType.Name;
                String Nombre = col.ColumnName;
                Nombre = Nombre.Replace('_', ' ');
                if (tipo == "Int32" || tipo == "Int64" || tipo == "Decimal")
                {
                    ws.columnHeader.Add(Nombre);
                    ws.columnFormat.Add(ExcelFileCellFormat.DOUBLE);
                }
                else if (tipo == "Date" || tipo == "DateTime")
                {
                    ws.columnHeader.Add(Nombre);
                    ws.columnFormat.Add(ExcelFileCellFormat.DATETIME);
                }
                else
                {
                    ws.columnHeader.Add(Nombre);
                    ws.columnFormat.Add(ExcelFileCellFormat.TEXT);
                }
            }
            ss.worksheets.Add(ws);

            return ss;
        }

        //Custom Alex PG
        public static ExcelFileSpreadsheet prepararExportacionExcel_custom(DataTable dt, String SheetTitle, String SheetName)
        {
            //custom AlexPG
            ExcelFileSpreadsheet ss = new ExcelFileSpreadsheet();
            ss.propertyAuthor = "Entel";
            ss.propertyTitle = ConfigurationSettings.AppSettings["APP_NAME"] + " - AVANCE METRICAS";

            ExcelFileWorksheet ws = new ExcelFileWorksheet();
            ws.sheetTitle = SheetTitle;
            ws.sheetName = SheetName;
            ws.dtSource = dt;
            foreach (DataColumn col in dt.Columns)
            {
                String tipo = col.DataType.Name;
                String Nombre = col.ColumnName;
                Nombre = Nombre.Replace('_', ' ').Replace('3', ' ').Replace('9', ' ').Replace('1', ' ');
                if (tipo == "Int32" || tipo == "Int64" || tipo == "Decimal")
                {
                    ws.columnHeader.Add(Nombre);
                    ws.columnFormat.Add(ExcelFileCellFormat.DOUBLE);
                }
                else if (tipo == "Date" || tipo == "DateTime")
                {
                    ws.columnHeader.Add(Nombre);
                    ws.columnFormat.Add(ExcelFileCellFormat.DATETIME);
                }
                else
                {
                    ws.columnHeader.Add(Nombre);
                    ws.columnFormat.Add(ExcelFileCellFormat.TEXT);
                }
            }
            ss.worksheets.Add(ws);

            return ss;
        }
        public static void ExportToExcel_Custom(ExcelFileSpreadsheet spreadsheet, String filename)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                int contWorksheet = 1;
                int initalCol = 2;
                int initialRow = 2;

                // Ingresamos los datos para las propiedades del archivo Excel
                p.Workbook.Properties.Author = spreadsheet.propertyAuthor;
                p.Workbook.Properties.Title = spreadsheet.propertyTitle;

                foreach (ExcelFileWorksheet worksheet in spreadsheet.worksheets)
                {   // Creamos un Worksheet
                    p.Workbook.Worksheets.Add(worksheet.sheetName);

                    ExcelWorksheet ws = p.Workbook.Worksheets[contWorksheet];
                    ws.Cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                    ws.Cells.Style.Font.Size = 10;
                    ws.Cells.Style.Font.Name = "Arial";

                    int colIndex = initalCol;
                    int rowIndex = initialRow;
                    int contHeader = 1;
                    int indexHeader = 0;
                    int contRow = 0;
                    DataTable dt = worksheet.dtSource;

                    if (!worksheet.sheetTitle.Equals(""))
                    {
                        // Unimos celdas para poder colocar el titulo de la grilla a mostrar
                        ws.Cells[colIndex, rowIndex].Value = worksheet.sheetTitle;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Merge = true;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.Font.Size = 12;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.Font.Bold = true;
                        ws.Cells[colIndex, rowIndex, colIndex, rowIndex + (dt.Columns.Count - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rowIndex = rowIndex + 2;
                    }

                    // Creamos las cabeceras de la grilla
                    foreach (DataColumn dc in dt.Columns)
                    {
                        var cell = ws.Cells[rowIndex, colIndex];
                        cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                        var font = cell.Style.Font;
                        font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        font.Bold = true;

                        var border = cell.Style.Border;
                        border.Top.Style = ExcelBorderStyle.Thin;
                        border.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));


                        if (contHeader == 1)
                        {
                            border.Left.Style = ExcelBorderStyle.Thin;
                            border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        }

                        if (contHeader < dt.Columns.Count)
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(System.Drawing.Color.White);
                        }
                        else
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        }

                        // Colocamos el nombre de la celda en la cabecera
                        cell.Value = worksheet.columnHeader[indexHeader];

                        double cellSize = ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width;
                        double proposedCellSize = cell.Value.ToString().Trim().Length * 1.3;
                        if (cellSize <= proposedCellSize)
                        {
                            ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize;
                        }

                        colIndex++;
                        contHeader++;
                        indexHeader++;

                    }

                    contRow = 1;

                    // Agregamos el contenido del DataTable en las celdas de la grilla
                    foreach (DataRow dr in dt.Rows)
                    {
                        colIndex = initalCol;
                        rowIndex++;
                        int contCol = 1;
                        int cellIndex = 0;

                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];

                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;

                            if (rowIndex % 2 == 0)
                            {
                                fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ECEEE7"));
                            }

                            String strValue = "";
                            if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.TEXT))
                            {
                                cell.Value = dr[dc.ColumnName].ToString();
                                strValue = cell.Value.ToString();
                            }
                            else
                            {
                                if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.NUMERIC))
                                {
                                    cell.Value = Convert.ToInt32(dr[dc.ColumnName]);
                                    strValue = cell.Value.ToString();
                                }
                                else
                                {
                                    if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.FLOAT))
                                    {
                                        cell.Value = Convert.ToDecimal(dr[dc.ColumnName]);
                                        strValue = cell.Value.ToString();
                                    }
                                    else
                                    {
                                        if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DOUBLE))
                                        {
                                            cell.Value = Convert.ToDouble(dr[dc.ColumnName]);
                                            strValue = cell.Value.ToString();
                                        }
                                        else
                                        {
                                            if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DATE))
                                            {
                                                Thread.CurrentThread.CurrentCulture = new CultureInfo("fr-FR");
                                                DateTime datetime = DateTime.Parse(dr[dc.ColumnName].ToString());
                                                String day = datetime.ToString("dd");
                                                String month = datetime.ToString("MM");
                                                String year = datetime.ToString("yyyy");
                                                cell.Value = day + "/" + month + "/" + year;
                                                strValue = cell.Value.ToString();
                                            }
                                            else
                                            {
                                                if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.TIME))
                                                {
                                                    DateTime datetime = DateTime.Parse(dr[dc.ColumnName].ToString());
                                                    String hora = datetime.ToString("hh");
                                                    String minuto = datetime.ToString("mm");
                                                    cell.Value = hora + ":" + minuto;
                                                    strValue = cell.Value.ToString();
                                                }
                                                else
                                                {
                                                    if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DATETIME))
                                                    {
                                                        cell.Value = dr[dc.ColumnName].ToString();
                                                        strValue = cell.Value.ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            var border = cell.Style.Border;

                            if (contCol == 1)
                            {
                                border.Left.Style = ExcelBorderStyle.Thin;
                                border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            if (contCol < dt.Columns.Count)
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(System.Drawing.Color.White);
                            }
                            else
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            if (contRow == dt.Rows.Count)
                            {
                                border.Bottom.Style = ExcelBorderStyle.Thin;
                                border.Bottom.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            var font = cell.Style.Font;
                            font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            font.Bold = false;

                            double cellSize = ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width;
                            double proposedCellSize = strValue.Trim().Length * 1.3;
                            if (cellSize <= proposedCellSize)
                            { ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize; }

                            colIndex++;
                            contCol++;
                            cellIndex++;

                        }
                        contRow++;
                    }

                    // Agregamos las imagenes que se incluiran en este Worksheet
                    string codigo_color = System.Configuration.ConfigurationManager.AppSettings["codigocolorcss"].ToString();
                    string tema = "entel-excel-";

                    if (codigo_color == "1")
                    {
                        tema = "nextel-excel-";
                    }
                    else if (codigo_color == "2")
                    {
                        tema = "entel-excel-";
                    }

                    //String logoNextelPath = HttpContext.Current.Server.MapPath("~") + "/images/logo/" + tema + "logo.png";
                    //String logoDescargaPath = HttpContext.Current.Server.MapPath("~") + "/images/logo/" + tema + "descarga.png";
                    //Bitmap imageNextel = new Bitmap(logoNextelPath);
                    //Bitmap imageDescarga = new Bitmap(logoDescargaPath);

                    //ExcelPicture pictureNextel = null;
                    //ExcelPicture pictureDescarga = null;

                    //if (imageNextel != null)
                    //{
                    //    pictureNextel = ws.Drawings.AddPicture("logoEntel", imageNextel);
                    //    pictureNextel.From.Column = initalCol - 1;
                    //    pictureNextel.From.Row = initialRow + contRow + 2;
                    //    pictureNextel.SetSize(imageNextel.Width - 20, imageNextel.Height);
                    //}
                    //if (imageDescarga != null)
                    //{
                    //    int imgRow = initialRow + contRow + 2;
                    //    int imgCol = initalCol + (dt.Columns.Count - 1);

                    //    int cellwidth = ExcelHelper.ColumnWidth2Pixel(ws, ws.Cells[imgRow, imgCol].Worksheet.Column(imgCol).Width);
                    //    int imagewidth = imageDescarga.Width;
                    //    int diff = cellwidth - imagewidth;
                    //    pictureDescarga = ws.Drawings.AddPicture("logoDescarga", imageDescarga);
                    //    pictureDescarga.SetPosition(imgRow, 0, imgCol - 1, diff);
                    //    pictureDescarga.SetSize(imageDescarga.Width, imageDescarga.Height);
                    //}

                    contWorksheet++;
                }

                // Generamos el archivo Excel y mostramos el popup de descarga
                Byte[] bin = p.GetAsByteArray();
                DateTime date = DateTime.Now;
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "_" + date.Date.ToString("yyyyMMdd") + "_" + date.Hour + date.Minute + date.Millisecond + ".xlsx");
                context.Response.BinaryWrite(bin);
                context.Response.End();
            }
        }

        public static void ExportToExcel_AvanceMetricas(ExcelFileSpreadsheet spreadsheet, String filename)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                int contWorksheet = 1;
                int initalCol = 2;
                int initialRow = 2;

                // Ingresamos los datos para las propiedades del archivo Excel
                p.Workbook.Properties.Author = spreadsheet.propertyAuthor;
                p.Workbook.Properties.Title = spreadsheet.propertyTitle;

                foreach (ExcelFileWorksheet worksheet in spreadsheet.worksheets)
                {   // Creamos un Worksheet
                    p.Workbook.Worksheets.Add(worksheet.sheetName);

                    ExcelWorksheet ws = p.Workbook.Worksheets[contWorksheet];
                    ws.Cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                    ws.Cells.Style.Font.Size = 10;
                    ws.Cells.Style.Font.Name = "Arial";

                    int colIndex = initalCol;
                    int rowIndex = initialRow;
                    int contHeader = 1;
                    int indexHeader = 0;
                    int contRow = 0;
                    DataTable dt = worksheet.dtSource;

                    if (!worksheet.sheetTitle.Equals(""))
                    {
                        // Unimos celdas para poder colocar el titulo de la grilla a mostrar
                        ws.Cells[rowIndex, colIndex].Value = worksheet.sheetTitle;
                        ws.Cells[rowIndex, colIndex, rowIndex, colIndex + (dt.Columns.Count - 1)].Merge = true;
                        ws.Cells[rowIndex, colIndex, rowIndex, colIndex + (dt.Columns.Count - 1)].Style.Font.Size = 12;
                        ws.Cells[rowIndex, colIndex, rowIndex, colIndex + (dt.Columns.Count - 1)].Style.Font.Bold = true;
                        ws.Cells[rowIndex, colIndex, rowIndex, colIndex + (dt.Columns.Count - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rowIndex = rowIndex + 2;
                    }
                    // Creamos las cabeceras de la grilla
                    int colRep = 3;
                    // Colocamos el nombre de la celda en la cabecera
                    var cellCab3 = ws.Cells[rowIndex, colRep, rowIndex, colRep + 2];
                    cellCab3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    var fillCab3 = cellCab3.Style.Fill;
                    fillCab3.PatternType = ExcelFillStyle.Solid;
                    fillCab3.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                    var fontCab3 = cellCab3.Style.Font;
                    fontCab3.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    fontCab3.Bold = true;

                    var borderCab3 = cellCab3.Style.Border;
                    borderCab3.Top.Style = ExcelBorderStyle.Thin;
                    borderCab3.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));

                    borderCab3.Left.Style = ExcelBorderStyle.Thin;
                    borderCab3.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    borderCab3.Right.Style = ExcelBorderStyle.Thin;
                    borderCab3.Right.Color.SetColor(System.Drawing.Color.White);

                    ws.Cells[rowIndex, colRep].Value = "Ticket 3";
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Merge = true;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.Font.Size = 12;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.Font.Bold = true;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colRep = colRep + 3;
                    var cellCab2 = ws.Cells[rowIndex, colRep, rowIndex, colRep + 2];
                    cellCab2.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    var fillCab2 = cellCab2.Style.Fill;
                    fillCab2.PatternType = ExcelFillStyle.Solid;
                    fillCab2.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                    var fontCab2 = cellCab2.Style.Font;
                    fontCab2.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    fontCab2.Bold = true;

                    var borderCab2 = cellCab2.Style.Border;
                    borderCab2.Top.Style = ExcelBorderStyle.Thin;
                    borderCab2.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    borderCab2.Right.Style = ExcelBorderStyle.Thin;
                    borderCab2.Right.Color.SetColor(System.Drawing.Color.White);

                    ws.Cells[rowIndex, colRep].Value = "Ticket 2";
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Merge = true;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.Font.Size = 12;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.Font.Bold = true;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colRep = colRep + 3;
                    var cellCab1 = ws.Cells[rowIndex, colRep, rowIndex, colRep + 2];
                    cellCab1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    var fillCab1 = cellCab1.Style.Fill;
                    fillCab1.PatternType = ExcelFillStyle.Solid;
                    fillCab1.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                    var fontCab1 = cellCab1.Style.Font;
                    fontCab1.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    fontCab1.Bold = true;

                    var borderCab1 = cellCab1.Style.Border;
                    borderCab1.Top.Style = ExcelBorderStyle.Thin;
                    borderCab1.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    borderCab1.Right.Style = ExcelBorderStyle.Thin;
                    borderCab1.Right.Color.SetColor(System.Drawing.Color.White);
                    ws.Cells[rowIndex, colRep].Value = "Ticket 1";
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Merge = true;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.Font.Size = 12;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.Font.Bold = true;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colRep = colRep + 3;
                    var cellCabf = ws.Cells[rowIndex, colRep, rowIndex, colRep + 2];
                    cellCabf.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    var fillCabf = cellCabf.Style.Fill;
                    fillCabf.PatternType = ExcelFillStyle.Solid;
                    fillCabf.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                    var fontCabf = cellCabf.Style.Font;
                    fontCabf.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    fontCabf.Bold = true;

                    var borderCabf = cellCabf.Style.Border;
                    borderCabf.Top.Style = ExcelBorderStyle.Thin;
                    borderCabf.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    borderCabf.Right.Style = ExcelBorderStyle.Thin;
                    borderCabf.Right.Color.SetColor(System.Drawing.Color.White);
                    ws.Cells[rowIndex, colRep].Value = "Fuentes";
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Merge = true;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.Font.Size = 12;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.Font.Bold = true;
                    ws.Cells[rowIndex, colRep, rowIndex, colRep + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colRep = colRep + 3;

                    var cellCabb = ws.Cells[rowIndex, colRep];
                    cellCabb.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    var fillCabb = cellCabb.Style.Fill;
                    fillCabb.PatternType = ExcelFillStyle.Solid;
                    fillCabb.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                    var fontCabb = cellCabb.Style.Font;
                    fontCabb.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    fontCabb.Bold = true;

                    var borderCabb = cellCabb.Style.Border;
                    borderCabb.Top.Style = ExcelBorderStyle.Thin;
                    borderCabb.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    borderCabb.Right.Style = ExcelBorderStyle.Thin;
                    borderCabb.Right.Color.SetColor(System.Drawing.Color.White);
                    ws.Cells[rowIndex, colRep].Value = "Bono(B)";
                    //ws.Cells[rowIndex, colRep, rowIndex, colRep].Merge = true;
                    ws.Cells[rowIndex, colRep].Style.Font.Size = 12;
                    ws.Cells[rowIndex, colRep].Style.Font.Bold = true;
                    ws.Cells[rowIndex, colRep].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colRep = colRep + 1;

                    var cellCabt = ws.Cells[rowIndex, colRep];
                    cellCabt.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    var fillCabt = cellCabt.Style.Fill;
                    fillCabt.PatternType = ExcelFillStyle.Solid;
                    fillCabt.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                    var fontCabt = cellCabt.Style.Font;
                    fontCabt.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    fontCabt.Bold = true;

                    var borderCabt = cellCabt.Style.Border;
                    borderCabt.Top.Style = ExcelBorderStyle.Thin;
                    borderCabt.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    borderCabt.Right.Style = ExcelBorderStyle.Thin;
                    borderCabt.Right.Color.SetColor(System.Drawing.Color.White);
                    ws.Cells[rowIndex, colRep].Value = "Tardanza(T)";
                    //ws.Cells[rowIndex, colRep, rowIndex, colRep].Merge = true;
                    ws.Cells[rowIndex, colRep].Style.Font.Size = 12;
                    ws.Cells[rowIndex, colRep].Style.Font.Bold = true;
                    ws.Cells[rowIndex, colRep].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colRep = colRep + 1;

                    var cellCabp = ws.Cells[rowIndex, colRep];
                    cellCabp.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    var fillCabp = cellCabp.Style.Fill;
                    fillCabp.PatternType = ExcelFillStyle.Solid;
                    fillCabp.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                    var fontCabp = cellCabp.Style.Font;
                    fontCabp.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    fontCabp.Bold = true;

                    var borderCabp = cellCabp.Style.Border;
                    borderCabp.Top.Style = ExcelBorderStyle.Thin;
                    borderCabp.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    borderCabp.Right.Style = ExcelBorderStyle.Thin;
                    borderCabp.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                    ws.Cells[rowIndex, colRep].Value = "Penalidad(P)";
                    //ws.Cells[rowIndex, colRep, rowIndex, colRep].Merge = true;
                    ws.Cells[rowIndex, colRep].Style.Font.Size = 12;
                    ws.Cells[rowIndex, colRep].Style.Font.Bold = true;
                    ws.Cells[rowIndex, colRep].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colRep = colRep + 1;
                    //Siguientes Linea
                    rowIndex = rowIndex + 1;

                    // Creamos Sub cabeceras de la grilla
                    foreach (DataColumn dc in dt.Columns)
                    {
                        var cell = ws.Cells[rowIndex, colIndex];
                        cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D7D2CB"));

                        var font = cell.Style.Font;
                        font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        font.Bold = true;

                        var border = cell.Style.Border;
                        border.Top.Style = ExcelBorderStyle.Thin;
                        border.Top.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));


                        if (contHeader == 1)
                        {
                            border.Left.Style = ExcelBorderStyle.Thin;
                            border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        }

                        if (contHeader < dt.Columns.Count)
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(System.Drawing.Color.White);
                        }
                        else
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                        }

                        // Colocamos el nombre de la celda en la cabecera
                        cell.Value = worksheet.columnHeader[indexHeader];

                        double cellSize = ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width;
                        double proposedCellSize = cell.Value.ToString().Trim().Length * 1.3;
                        if (cellSize <= proposedCellSize)
                        {
                            ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize;
                        }

                        colIndex++;
                        contHeader++;
                        indexHeader++;

                    }

                    contRow = 1;

                    // Agregamos el contenido del DataTable en las celdas de la grilla
                    foreach (DataRow dr in dt.Rows)
                    {
                        colIndex = initalCol;
                        rowIndex++;
                        int contCol = 1;
                        int cellIndex = 0;

                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];

                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;

                            if (rowIndex % 2 == 0)
                            {
                                fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#ECEEE7"));
                            }

                            String strValue = "";
                            if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.TEXT))
                            {
                                cell.Value = dr[dc.ColumnName].ToString();
                                strValue = cell.Value.ToString();
                            }
                            else
                            {
                                if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.NUMERIC))
                                {
                                    cell.Value = Convert.ToInt32(dr[dc.ColumnName]);
                                    strValue = cell.Value.ToString();
                                }
                                else
                                {
                                    if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.FLOAT))
                                    {
                                        cell.Value = Convert.ToDecimal(dr[dc.ColumnName]);
                                        strValue = cell.Value.ToString();
                                    }
                                    else
                                    {
                                        if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DOUBLE))
                                        {
                                            cell.Value = Convert.ToDouble(dr[dc.ColumnName]);
                                            strValue = cell.Value.ToString();
                                        }
                                        else
                                        {
                                            if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DATE))
                                            {
                                                Thread.CurrentThread.CurrentCulture = new CultureInfo("fr-FR");
                                                DateTime datetime = DateTime.Parse(dr[dc.ColumnName].ToString());
                                                String day = datetime.ToString("dd");
                                                String month = datetime.ToString("MM");
                                                String year = datetime.ToString("yyyy");
                                                cell.Value = day + "/" + month + "/" + year;
                                                strValue = cell.Value.ToString();
                                            }
                                            else
                                            {
                                                if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.TIME))
                                                {
                                                    DateTime datetime = DateTime.Parse(dr[dc.ColumnName].ToString());
                                                    String hora = datetime.ToString("hh");
                                                    String minuto = datetime.ToString("mm");
                                                    cell.Value = hora + ":" + minuto;
                                                    strValue = cell.Value.ToString();
                                                }
                                                else
                                                {
                                                    if (worksheet.columnFormat[cellIndex].Equals(ExcelFileCellFormat.DATETIME))
                                                    {
                                                        cell.Value = dr[dc.ColumnName].ToString();
                                                        strValue = cell.Value.ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            var border = cell.Style.Border;

                            if (contCol == 1)
                            {
                                border.Left.Style = ExcelBorderStyle.Thin;
                                border.Left.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            if (contCol < dt.Columns.Count)
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(System.Drawing.Color.White);
                            }
                            else
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            if (contRow == dt.Rows.Count)
                            {
                                border.Bottom.Style = ExcelBorderStyle.Thin;
                                border.Bottom.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            }

                            var font = cell.Style.Font;
                            font.Color.SetColor(System.Drawing.ColorTranslator.FromHtml("#766A87"));
                            font.Bold = false;

                            double cellSize = ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width;
                            double proposedCellSize = strValue.Trim().Length * 1.3;
                            if (cellSize <= proposedCellSize)
                            { ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize; }

                            colIndex++;
                            contCol++;
                            cellIndex++;

                        }
                        contRow++;
                    }

                    // Agregamos las imagenes que se incluiran en este Worksheet
                    string codigo_color = System.Configuration.ConfigurationManager.AppSettings["codigocolorcss"].ToString();
                    string tema = "entel-excel-";

                    if (codigo_color == "1")
                    {
                        tema = "nextel-excel-";
                    }
                    else if (codigo_color == "2")
                    {
                        tema = "entel-excel-";
                    }

                    //String logoNextelPath = HttpContext.Current.Server.MapPath("~") + "/images/logo/" + tema + "logo.png";
                    //String logoDescargaPath = HttpContext.Current.Server.MapPath("~") + "/images/logo/" + tema + "descarga.png";
                    //Bitmap imageNextel = new Bitmap(logoNextelPath);
                    //Bitmap imageDescarga = new Bitmap(logoDescargaPath);

                    //ExcelPicture pictureNextel = null;
                    //ExcelPicture pictureDescarga = null;

                    //if (imageNextel != null)
                    //{
                    //    pictureNextel = ws.Drawings.AddPicture("logoEntel", imageNextel);
                    //    pictureNextel.From.Column = initalCol - 1;
                    //    pictureNextel.From.Row = initialRow + contRow + 2;
                    //    pictureNextel.SetSize(imageNextel.Width - 20, imageNextel.Height);
                    //}
                    //if (imageDescarga != null)
                    //{
                    //    int imgRow = initialRow + contRow + 2;
                    //    int imgCol = initalCol + (dt.Columns.Count - 1);

                    //    int cellwidth = ExcelHelper.ColumnWidth2Pixel(ws, ws.Cells[imgRow, imgCol].Worksheet.Column(imgCol).Width);
                    //    int imagewidth = imageDescarga.Width;
                    //    int diff = cellwidth - imagewidth;
                    //    pictureDescarga = ws.Drawings.AddPicture("logoDescarga", imageDescarga);
                    //    pictureDescarga.SetPosition(imgRow, 0, imgCol - 1, diff);
                    //    pictureDescarga.SetSize(imageDescarga.Width, imageDescarga.Height);
                    //}

                    contWorksheet++;
                }

                // Generamos el archivo Excel y mostramos el popup de descarga
                Byte[] bin = p.GetAsByteArray();
                DateTime date = DateTime.Now;
                HttpContext context = HttpContext.Current;
                context.Response.Clear();
                context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "_" + date.Date.ToString("yyyyMMdd") + "_" + date.Hour + date.Minute + date.Millisecond + ".xlsx");
                context.Response.BinaryWrite(bin);
                context.Response.End();
            }
        }

        public static void ExportToExcel2(ExcelFileSpreadsheet spreadsheet, string filename)
        {
            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                int index1 = 1;
                int num1 = 2;
                int num2 = 2;
                excelPackage.Workbook.Properties.Author = spreadsheet.propertyAuthor;
                excelPackage.Workbook.Properties.Title = spreadsheet.propertyTitle;
                foreach (ExcelFileWorksheet worksheet in spreadsheet.worksheets)
                {
                    excelPackage.Workbook.Worksheets.Add(worksheet.sheetName);
                    ExcelWorksheet ws = excelPackage.Workbook.Worksheets[index1];
                    ws.Cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells.Style.Fill.BackgroundColor.SetColor(Color.White);
                    ws.Cells.Style.Font.Size = 10f;
                    ws.Cells.Style.Font.Name = "Arial";
                    int col1 = num1;
                    int index2 = num2;
                    int num3 = 1;
                    int index3 = 0;
                    DataTable dtSource = worksheet.dtSource;
                    if (!worksheet.sheetTitle.Equals(""))
                    {
                        ws.Cells[col1, index2].Value = (object)worksheet.sheetTitle;
                        ws.Cells[col1, index2, col1, index2 + (dtSource.Columns.Count - 1)].Merge = true;
                        ws.Cells[col1, index2, col1, index2 + (dtSource.Columns.Count - 1)].Style.Font.Size = 12f;
                        ws.Cells[col1, index2, col1, index2 + (dtSource.Columns.Count - 1)].Style.Font.Bold = true;
                        ws.Cells[col1, index2, col1, index2 + (dtSource.Columns.Count - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        index2 += 2;
                    }
                    foreach (DataColumn column in (InternalDataCollectionBase)dtSource.Columns)
                    {
                        ExcelRange excelRange = ws.Cells[index2, col1];
                        excelRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ExcelFill fill = excelRange.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#D7D2CB"));
                        ExcelFont font = excelRange.Style.Font;
                        font.Color.SetColor(ColorTranslator.FromHtml("#766A87"));
                        font.Bold = true;
                        Border border = excelRange.Style.Border;
                        border.Top.Style = ExcelBorderStyle.Thin;
                        border.Top.Color.SetColor(ColorTranslator.FromHtml("#766A87"));
                        if (num3 == 1)
                        {
                            border.Left.Style = ExcelBorderStyle.Thin;
                            border.Left.Color.SetColor(ColorTranslator.FromHtml("#766A87"));
                        }
                        if (num3 < dtSource.Columns.Count)
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(Color.White);
                        }
                        else
                        {
                            border.Right.Style = ExcelBorderStyle.Thin;
                            border.Right.Color.SetColor(ColorTranslator.FromHtml("#766A87"));
                        }
                        excelRange.Value = (object)worksheet.columnHeader[index3];
                        double width = ws.Cells[index2, col1].Worksheet.Column(col1).Width;
                        double num4 = (double)excelRange.Value.ToString().Trim().Length * 1.3;
                        if (width <= num4)
                            ws.Cells[index2, col1].Worksheet.Column(col1).Width = num4;
                        ++col1;
                        ++num3;
                        ++index3;
                    }
                    int num5 = 1;
                    foreach (DataRow row in (InternalDataCollectionBase)dtSource.Rows)
                    {
                        int col2 = num1;
                        ++index2;
                        int num4 = 1;
                        int index4 = 0;
                        foreach (DataColumn column in (InternalDataCollectionBase)dtSource.Columns)
                        {
                            ExcelRange excelRange = ws.Cells[index2, col2];
                            ExcelFill fill = excelRange.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            if (index2 % 2 == 0)
                                fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ECEEE7"));
                            string str = "";
                            if (worksheet.columnFormat[index4].Equals(ExcelFileCellFormat.TEXT))
                            {
                                excelRange.Value = (object)row[column.ColumnName].ToString();
                                str = excelRange.Value.ToString();
                            }
                            else if (worksheet.columnFormat[index4].Equals(ExcelFileCellFormat.NUMERIC))
                            {
                                excelRange.Value = (object)Convert.ToInt32(row[column.ColumnName]);
                                str = excelRange.Value.ToString();
                            }
                            else if (worksheet.columnFormat[index4].Equals(ExcelFileCellFormat.FLOAT))
                            {
                                excelRange.Value = (object)Convert.ToDecimal(row[column.ColumnName]);
                                str = excelRange.Value.ToString();
                            }
                            else if (worksheet.columnFormat[index4].Equals(ExcelFileCellFormat.DOUBLE))
                            {
                                excelRange.Value = (object)Convert.ToDouble(row[column.ColumnName]);
                                str = excelRange.Value.ToString();
                            }
                            else if (worksheet.columnFormat[index4].Equals(ExcelFileCellFormat.DATE))
                            {
                                Thread.CurrentThread.CurrentCulture = new CultureInfo("fr-FR");
                                DateTime dateTime = DateTime.Parse(row[column.ColumnName].ToString());
                                string string1 = dateTime.ToString("dd");
                                string string2 = dateTime.ToString("MM");
                                string string3 = dateTime.ToString("yyyy");
                                excelRange.Value = (object)(string1 + "/" + string2 + "/" + string3);
                                str = excelRange.Value.ToString();
                            }
                            else if (worksheet.columnFormat[index4].Equals(ExcelFileCellFormat.TIME))
                            {
                                DateTime dateTime = DateTime.Parse(row[column.ColumnName].ToString());
                                string string1 = dateTime.ToString("hh");
                                string string2 = dateTime.ToString("mm");
                                excelRange.Value = (object)(string1 + ":" + string2);
                                str = excelRange.Value.ToString();
                            }
                            else if (worksheet.columnFormat[index4].Equals(ExcelFileCellFormat.DATETIME))
                            {
                                excelRange.Value = (object)row[column.ColumnName].ToString();
                                str = excelRange.Value.ToString();
                            }
                            excelRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            Border border = excelRange.Style.Border;
                            if (num4 == 1)
                            {
                                border.Left.Style = ExcelBorderStyle.Thin;
                                border.Left.Color.SetColor(ColorTranslator.FromHtml("#766A87"));
                            }
                            if (num4 < dtSource.Columns.Count)
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(Color.White);
                            }
                            else
                            {
                                border.Right.Style = ExcelBorderStyle.Thin;
                                border.Right.Color.SetColor(ColorTranslator.FromHtml("#766A87"));
                            }
                            if (num5 == dtSource.Rows.Count)
                            {
                                border.Bottom.Style = ExcelBorderStyle.Thin;
                                border.Bottom.Color.SetColor(ColorTranslator.FromHtml("#766A87"));
                            }
                            ExcelFont font = excelRange.Style.Font;
                            font.Color.SetColor(ColorTranslator.FromHtml("#766A87"));
                            font.Bold = false;
                            double width = ws.Cells[index2, col2].Worksheet.Column(col2).Width;
                            double num6 = (double)str.Trim().Length * 1.3;
                            if (width <= num6)
                                ws.Cells[index2, col2].Worksheet.Column(col2).Width = num6;
                            ++col2;
                            ++num4;
                            ++index4;
                        }
                        ++num5;
                    }
                    string filename1 = HttpContext.Current.Server.MapPath("~") + "/images/logo/entel-excel-logo.png";
                    string filename2 = HttpContext.Current.Server.MapPath("~") + "/images/logo/excel_logoDescarga.png";
                    Bitmap bitmap1 = new Bitmap(filename1);
                    Bitmap bitmap2 = null;
                    if (bitmap1 != null)
                    {
                        ExcelPicture excelPicture = ws.Drawings.AddPicture("logoNextel", (System.Drawing.Image)bitmap1);
                        excelPicture.From.Column = num1 - 1;
                        excelPicture.From.Row = num2 + num5 + 2;
                        excelPicture.SetSize(bitmap1.Width - 20, bitmap1.Height);
                    }
                    if (bitmap2 != null)
                    {
                        int Row = num2 + num5 + 2;
                        int col2 = num1 + (dtSource.Columns.Count - 1);
                        int ColumnOffsetPixels = ExcelHelper.ColumnWidth2Pixel(ws, ws.Cells[Row, col2].Worksheet.Column(col2).Width) - bitmap2.Width;
                        ExcelPicture excelPicture = ws.Drawings.AddPicture("logoDescarga", (System.Drawing.Image)bitmap2);
                        excelPicture.SetPosition(Row, 0, col2 - 1, ColumnOffsetPixels);
                        excelPicture.SetSize(bitmap2.Width, bitmap2.Height);
                    }
                    ++index1;
                }
                byte[] asByteArray = excelPackage.GetAsByteArray();
                DateTime now = DateTime.Now;
                HttpContext current = HttpContext.Current;
                current.Response.Clear();
                current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "_" + now.Date.ToString("yyyyMMdd") + "_" + (object)now.Hour + (object)now.Minute + (object)now.Millisecond + ".xlsx");
                current.Response.BinaryWrite(asByteArray);
                current.Response.End();
            }
        }

    }
}

