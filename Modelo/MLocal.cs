﻿using Modelo.BEANS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MLocal
    {
        public static DataTable BuscarLocalM(string cod_local, string nom_local, string page, string rows, Int32 cod_empresa)
        {
            ArrayList AllParameters = new ArrayList();

            if (cod_local.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@cod_local", SqlDbType.Int);
                sqlParameter.Value = (object)cod_local;
                AllParameters.Add((object)sqlParameter);
            }

            if (nom_local.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@nom_local", SqlDbType.VarChar, 50);
                sqlParameter.Value = (object)nom_local;
                AllParameters.Add((object)sqlParameter);
            }

            SqlParameter sqlParameter1 = new SqlParameter("@page", SqlDbType.Int);
            sqlParameter1.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.Int);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)sqlParameter2);

            SqlParameter sqlParameter3 = new SqlParameter("@cod_empresa", SqlDbType.Int);
            sqlParameter3.Value = (object)Convert.ToInt32(cod_empresa);
            AllParameters.Add((object)sqlParameter3);

            return SqlConnector.getDataTable("SPU_LOCAL_BUSCAR", AllParameters);
        }

        public static int AgregarLocalM(BeanLocal bean)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter1 = new SqlParameter("@nom_local", SqlDbType.VarChar, 50);
            sqlParameter1.Value = (object)bean.nom_local;
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@direccion", SqlDbType.VarChar, 1000);
            sqlParameter2.Value = (object)bean.direccion;
            alParametros.Add((object)sqlParameter2);
            SqlParameter sqlParameter3 = new SqlParameter("@referencia", SqlDbType.VarChar, 1000);
            sqlParameter3.Value = (object)bean.referencia;
            alParametros.Add((object)sqlParameter3);
            SqlParameter sqlParameter4 = new SqlParameter("@telf_fijo", SqlDbType.Int);
            sqlParameter4.Value = (object)bean.telf_fijo;
            alParametros.Add((object)sqlParameter4);
            SqlParameter sqlParameter5 = new SqlParameter("@telf_movil", SqlDbType.Int);
            sqlParameter5.Value = (object)bean.telf_movil;
            alParametros.Add((object)sqlParameter5);
            SqlParameter sqlParameter6 = new SqlParameter("@cod_empresa", SqlDbType.Int);
            sqlParameter6.Value = (object)bean.cod_empresa;
            alParametros.Add((object)sqlParameter6);

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_LOCAL_AGREGAR", alParametros));
        }

        public static DataTable ObtenerLocalM(string cod_local)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@cod_local", SqlDbType.Int);
            sqlParameter.Value = (object)int.Parse(cod_local);
            alParametros.Add((object)sqlParameter);
            return SqlConnector.getDataTable("SPU_LOCAL_INFO", alParametros);
        }

        public static int EditarLocalM(BeanLocal bean)
        {
            ArrayList alParametros = new ArrayList();

            SqlParameter sqlParameter = new SqlParameter("@cod_local", SqlDbType.Int);
            sqlParameter.Value = (object)bean.cod_local;
            alParametros.Add((object)sqlParameter);
            SqlParameter sqlParameter1 = new SqlParameter("@nom_local", SqlDbType.VarChar, 50);
            sqlParameter1.Value = (object)bean.nom_local;
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@direccion", SqlDbType.VarChar, 1000);
            sqlParameter2.Value = (object)bean.direccion;
            alParametros.Add((object)sqlParameter2);
            SqlParameter sqlParameter3 = new SqlParameter("@referencia", SqlDbType.VarChar, 1000);
            sqlParameter3.Value = (object)bean.referencia;
            alParametros.Add((object)sqlParameter3);
            SqlParameter sqlParameter4 = new SqlParameter("@telf_fijo", SqlDbType.Int);
            sqlParameter4.Value = (object)bean.telf_fijo;
            alParametros.Add((object)sqlParameter4);
            SqlParameter sqlParameter5 = new SqlParameter("@telf_movil", SqlDbType.Int);
            sqlParameter5.Value = (object)bean.telf_movil;
            alParametros.Add((object)sqlParameter5);
            return Convert.ToInt32(SqlConnector.executeScalar("SPU_LOCAL_EDITAR", alParametros));
        }

        public static void EliminarLocalM(string cod_local)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@cod_local", SqlDbType.VarChar, 4000);
            sqlParameter.Value = (object)cod_local;
            alParametros.Add((object)sqlParameter);
            SqlConnector.getDataTable("SPU_LOCAL_BORRAR", alParametros);
        }
    }
}
