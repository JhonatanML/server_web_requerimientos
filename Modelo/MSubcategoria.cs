﻿using Modelo.BEANS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MSubcategoria
    {
        public static DataTable BuscarSubcategoriaM(string linea, string nom_subcategoria, string id_categoria, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter parameter;

            if (linea.Trim().Length != 0)
            {
                parameter = new SqlParameter("@linea", SqlDbType.VarChar, 20);
                parameter.Value = (object)linea;
                AllParameters.Add((object)parameter);
            }

            if (nom_subcategoria.Trim().Length != 0)
            {
                parameter = new SqlParameter("@nom_subcategoria", SqlDbType.VarChar, 50);
                parameter.Value = (object)nom_subcategoria;
                AllParameters.Add((object)parameter);
            }

            if (id_categoria.Trim() != "-1")
            {
                parameter = new SqlParameter("@id_categoria", SqlDbType.Int);
                parameter.Value = (object)id_categoria;
                AllParameters.Add((object)parameter);
            }

            parameter = new SqlParameter("@page", SqlDbType.Int);
            parameter.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@rows", SqlDbType.Int);
            parameter.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)parameter);

            return SqlConnector.getDataTable("SPU_SUBCATEGORIA_BUSCAR", AllParameters);
        }

        public static void EliminarSubcategoriaM(string id_subcategoria)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter parameter;
            parameter = new SqlParameter("@id_subcategoria", SqlDbType.VarChar, 4000);
            parameter.Value = (object)id_subcategoria;
            alParametros.Add((object)parameter);
            SqlConnector.getDataTable("SPU_SUBCATEGORIA_BORRAR", alParametros);
        }

        public static int AgregarSubcategoriaM(BeanSubCategoria bean)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter;

            parameter = new SqlParameter("@id_categoria", SqlDbType.Int);
            parameter.Value = (object)bean.id_categoria;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@nom_subcategoria", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.nom_subcategoria;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@linea", SqlDbType.VarChar, 20);
            parameter.Value = (object)bean.linea;
            AllParameters.Add((object)parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_SUBCATEGORIA_AGREGAR", AllParameters));
        }

        public static DataTable ObtenerSubcategoriaM(string id_subcategoria)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter parameter;
            parameter = new SqlParameter("@id_subcategoria", SqlDbType.Int);
            parameter.Value = (object)int.Parse(id_subcategoria);
            AllParameters.Add((object)parameter);
            return SqlConnector.getDataTable("SPU_SUBCATEGORIA_INFO", AllParameters);
        }

        public static int EditarSubcategoriaM(BeanSubCategoria bean)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter;

            parameter = new SqlParameter("@id_categoria", SqlDbType.Int);
            parameter.Value = (object)bean.id_categoria;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@nom_subcategoria", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.nom_subcategoria;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@linea", SqlDbType.VarChar, 20);
            parameter.Value = (object)bean.linea;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_subcategoria", SqlDbType.Int);
            parameter.Value = (object)bean.id_subcategoria;
            AllParameters.Add((object)parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_SUBCATEGORIA_EDITAR", AllParameters));
        }
    }
}
