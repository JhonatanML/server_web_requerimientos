﻿using Modelo.BEANS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MReserva
    {
        public static DataTable ObtenerSalasM(Int32 cod_local)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@cod_local", SqlDbType.Int);
            parameter.Value = cod_local;
            AllParameters.Add(parameter);
            return SqlConnector.getDataTable("SPS_OBTENER_SALAS", AllParameters);
        }

        public static DataTable ObtenerReservasM(Int32 id_usuario, Int32 cod_local, Int32 cod_filtro)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter1 = new SqlParameter("@id_usuario", SqlDbType.Int);
            parameter1.Value = id_usuario;
            AllParameters.Add(parameter1);
            SqlParameter parameter2 = new SqlParameter("@cod_local", SqlDbType.Int);
            parameter2.Value = cod_local;
            AllParameters.Add(parameter2);
            SqlParameter parameter3 = new SqlParameter("@cod_filtro", SqlDbType.Int);
            parameter3.Value = cod_filtro;
            AllParameters.Add(parameter3);

            return SqlConnector.getDataTable("SPS_OBTENER_RESERVAS", AllParameters);
        }

        public static DataTable ObtenerReporteReservasM(string fecha_inicio_reserva, string cod_local, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();

            if (fecha_inicio_reserva.Trim().Length != 0)
            {
                SqlParameter parameter2 = new SqlParameter("@fecha_inicio_reserva", SqlDbType.Date);
                parameter2.Value = (object)fecha_inicio_reserva;
                AllParameters.Add((object)parameter2);
            }

            if (cod_local.Trim() != "-1")
            {
                SqlParameter parameter3 = new SqlParameter("@cod_local", SqlDbType.Int);
                parameter3.Value = (object)cod_local;
                AllParameters.Add((object)parameter3);
            }

            SqlParameter parameter4 = new SqlParameter("@page", SqlDbType.Int);
            parameter4.Value = (object)page;
            AllParameters.Add((object)parameter4);
            SqlParameter parameter = new SqlParameter("@rows", SqlDbType.Int);
            parameter.Value = (object)rows;
            AllParameters.Add((object)parameter);

            return SqlConnector.getDataTable("SPS_OBTENER_REPORTE_RESERVAS", AllParameters);
        }

        public static DataTable Combo_LocalM(Int32 cod_empresa)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@cod_empresa", SqlDbType.Int);
            parameter.Value = cod_empresa;
            AllParameters.Add(parameter);
            return SqlConnector.getDataTable("SPS_COMBO_LOCAL", AllParameters);
        }

        public static DataTable Combo_ColoresM()
        {
            return SqlConnector.getDataTable("SPS_OBTENER_COLORES");
        }

        public static DataTable RegistrarReservaM(BeanReserva bean)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter2 = new SqlParameter("@titulo_reserva", SqlDbType.VarChar, 200);
            sqlParameter2.Value = (object)bean.titulo_reserva;
            alParametros.Add((object)sqlParameter2);
            SqlParameter sqlParameter3 = new SqlParameter("@fecha_reserva", SqlDbType.DateTime);
            sqlParameter3.Value = (object)bean.fecha_reserva;
            alParametros.Add((object)sqlParameter3);
            SqlParameter sqlParameter4 = new SqlParameter("@fecha_inicio_reserva", SqlDbType.Date);
            sqlParameter4.Value = (object)bean.fecha_inicio_reserva;
            alParametros.Add((object)sqlParameter4);
            SqlParameter sqlParameter9 = new SqlParameter("@hora_inicio_reserva", SqlDbType.Time);
            sqlParameter9.Value = (object)bean.hora_inicio_reserva;
            alParametros.Add((object)sqlParameter9);
            SqlParameter sqlParameter10 = new SqlParameter("@hora_termino_reserva", SqlDbType.Time);
            sqlParameter10.Value = (object)bean.hora_termino_reserva;
            alParametros.Add((object)sqlParameter10);
            SqlParameter sqlParameter7 = new SqlParameter("@cod_sala", SqlDbType.Int);
            sqlParameter7.Value = (object)bean.cod_sala;
            alParametros.Add((object)sqlParameter7);
            SqlParameter sqlParameter1 = new SqlParameter("@cod_color", SqlDbType.Int);
            sqlParameter1.Value = (object)bean.cod_color;
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter6 = new SqlParameter("@invitados", SqlDbType.VarChar, 1000);
            sqlParameter6.Value = (object)bean.invitados;
            alParametros.Add((object)sqlParameter6);
            SqlParameter sqlParameter0 = new SqlParameter("@detalle_reserva", SqlDbType.VarChar, 1000);
            sqlParameter0.Value = (object)bean.detalle_reserva;
            alParametros.Add((object)sqlParameter0);
            SqlParameter sqlParameter8 = new SqlParameter("@id_usuario", SqlDbType.Int);
            sqlParameter8.Value = (object)bean.id_usuario;
            alParametros.Add((object)sqlParameter8);

            DataSet ds = new DataSet();
            ds =  SqlConnector.getDataset("SPU_RESERVA_REGISTRAR", alParametros);
            return ds.Tables[1];
        }

        public static DataTable ObtenerUsuarioSalaM(Int32 id_usuario, Int32 cod_sala)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter1 = new SqlParameter("@id_usuario", SqlDbType.Int);
            parameter1.Value = id_usuario;
            AllParameters.Add(parameter1);
            SqlParameter parameter2 = new SqlParameter("@cod_sala", SqlDbType.Int);
            parameter2.Value = cod_sala;
            AllParameters.Add(parameter2);

            return SqlConnector.getDataTable("SPS_OBTENER_USUARIO_SALA", AllParameters);
        }

        public static DataTable ObtenerInfoReservaM(Int32 cod_reserva)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@cod_reserva", SqlDbType.Int);
            parameter.Value = cod_reserva;
            AllParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_RESERVA_INFO", AllParameters);
        }
        public static DataTable ObtenerInfoDetalleReservaM(Int32 cod_reserva)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@cod_reserva", SqlDbType.Int);
            parameter.Value = cod_reserva;
            AllParameters.Add(parameter);

            return SqlConnector.getDataTable("SPS_OBTENER_DETALLE_RESERVAS", AllParameters);
        }

        public static DataTable EditarReservaM(BeanReserva bean)
        {
            ArrayList alParametros = new ArrayList();

            SqlParameter sqlParameter1 = new SqlParameter("@cod_reserva", SqlDbType.Int);
            sqlParameter1.Value = (object)bean.cod_reserva;
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@titulo_reserva", SqlDbType.VarChar, 200);
            sqlParameter2.Value = (object)bean.titulo_reserva;
            alParametros.Add((object)sqlParameter2);
            SqlParameter sqlParameter4 = new SqlParameter("@fecha_inicio_reserva", SqlDbType.Date);
            sqlParameter4.Value = (object)bean.fecha_inicio_reserva;
            alParametros.Add((object)sqlParameter4);
            SqlParameter sqlParameter9 = new SqlParameter("@hora_inicio_reserva", SqlDbType.Time);
            sqlParameter9.Value = (object)bean.hora_inicio_reserva;
            alParametros.Add((object)sqlParameter9);
            SqlParameter sqlParameter10 = new SqlParameter("@hora_termino_reserva", SqlDbType.Time);
            sqlParameter10.Value = (object)bean.hora_termino_reserva;
            alParametros.Add((object)sqlParameter10);
            SqlParameter sqlParameter7 = new SqlParameter("@cod_sala", SqlDbType.Int);
            sqlParameter7.Value = (object)bean.cod_sala;
            alParametros.Add((object)sqlParameter7);
            SqlParameter sqlParameter8 = new SqlParameter("@cod_color", SqlDbType.Int);
            sqlParameter8.Value = (object)bean.cod_color;
            alParametros.Add((object)sqlParameter8);
            SqlParameter sqlParameter6 = new SqlParameter("@invitados", SqlDbType.VarChar, 1000);
            sqlParameter6.Value = (object)bean.invitados;
            alParametros.Add((object)sqlParameter6);
            SqlParameter sqlParameter11 = new SqlParameter("@detalle_reserva", SqlDbType.VarChar, 1000);
            sqlParameter11.Value = (object)bean.detalle_reserva;
            alParametros.Add((object)sqlParameter11);

            DataSet ds = new DataSet();
            ds = SqlConnector.getDataset("SPU_RESERVA_EDITAR", alParametros);
            return ds.Tables[1];
        }
    }
}
