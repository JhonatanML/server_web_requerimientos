﻿using Modelo.BEANS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MServicio
    {
        public static DataTable Lista_ServicioM(string nom_serviciosT)
        {
            ArrayList AllParameters = new ArrayList();

            if (nom_serviciosT.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@nom_serviciosT", SqlDbType.VarChar, 50);
                sqlParameter.Value = (object)nom_serviciosT;
                AllParameters.Add((object)sqlParameter);
            }
            return SqlConnector.getDataTable("SPS_LISTA_SERVICIOS", AllParameters);
        }

        public static DataTable Combo_ServicioM(string cod_subcategoria, string cod_categoria, string cod_servicio)
        {

            ArrayList AllParameters = new ArrayList();

            SqlParameter sqlParameter = new SqlParameter("@id_subcategoria", SqlDbType.VarChar, 50);
            sqlParameter.Value = (object)Convert.ToInt32(cod_subcategoria);
            AllParameters.Add((object)sqlParameter);

            SqlParameter sqlParameter1 = new SqlParameter("@id_categoria", SqlDbType.VarChar, 50);
            sqlParameter1.Value = (object)Convert.ToInt32(cod_categoria);
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter2 = new SqlParameter("@id_servicio", SqlDbType.VarChar, 50);
            sqlParameter2.Value = (object)Convert.ToInt32(cod_servicio);
            AllParameters.Add((object)sqlParameter2);

            return SqlConnector.getDataTable("SPS_COMBO_SERVICIOS", AllParameters);
        }

        public static DataTable BuscarServicioM(string nom_servicio, string codigo_sap, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();

            if (codigo_sap.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@codigo_sap", SqlDbType.VarChar, 20);
                sqlParameter.Value = (object)codigo_sap;
                AllParameters.Add((object)sqlParameter);
            }

            if (nom_servicio.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@nom_servicio", SqlDbType.VarChar, 50);
                sqlParameter.Value = (object)nom_servicio;
                AllParameters.Add((object)sqlParameter);
            }

            SqlParameter sqlParameter1 = new SqlParameter("@page", SqlDbType.Int);
            sqlParameter1.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.Int);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)sqlParameter2);

            return SqlConnector.getDataTable("SPU_SERVICIO_BUSCAR", AllParameters);
        }

        public static void EliminarServicioM(string id_servicio)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@id_servicio", SqlDbType.VarChar, 4000);
            sqlParameter.Value = (object)id_servicio;
            alParametros.Add((object)sqlParameter);
            SqlConnector.getDataTable("SPU_SERVICIO_BORRAR", alParametros);
        }

        public static int AgregarServicioM(BeanServicio bean)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@cod_categoria", SqlDbType.Int);
            parameter.Value = (object)bean.cod_categoria;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@cod_subcategoria", SqlDbType.Int);
            parameter.Value = (object)bean.cod_subcategoria;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@linea", SqlDbType.VarChar, 20);
            parameter.Value = (object)bean.linea;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@codigo_sap", SqlDbType.VarChar, 20);
            parameter.Value = (object)bean.codigo_sap;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@nom_servicio", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.nom_servicio;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@descripcion", SqlDbType.VarChar, 1000);
            parameter.Value = (object)bean.descripcion;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@precio", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.precio;
            AllParameters.Add((object)parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_SERVICIO_AGREGAR", AllParameters));
        }

        public static DataTable ObtenerServicioM(string id_servicio)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@id_servicio", SqlDbType.Int);
            sqlParameter.Value = (object)int.Parse(id_servicio);
            AllParameters.Add((object)sqlParameter);
            return SqlConnector.getDataTable("SPU_SERVICIO_INFO", AllParameters);
        }

        public static int EditarServicioM(BeanServicio bean)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@cod_categoria", SqlDbType.Int);
            parameter.Value = (object)bean.cod_categoria;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@cod_subcategoria", SqlDbType.Int);
            parameter.Value = (object)bean.cod_subcategoria;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@linea", SqlDbType.VarChar, 20);
            parameter.Value = (object)bean.linea;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@codigo_sap", SqlDbType.VarChar, 20);
            parameter.Value = (object)bean.codigo_sap;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@nom_servicio", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.nom_servicio;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@descripcion", SqlDbType.VarChar, 1000);
            parameter.Value = (object)bean.descripcion;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@precio", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.precio;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_servicio", SqlDbType.Int);
            parameter.Value = (object)bean.id_servicio;
            AllParameters.Add((object)parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_SERVICIO_EDITAR", AllParameters));
        }
    }
}
