﻿using Modelo.BEANS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MRequerimiento
    {
        public static DataTable ListarRequerimientosM(string visualizar, string id_requerimiento, string fecha_requerimiento, string cod_estado_requerimiento, Int32 id_usuario, Int32 id_perfil, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();

            if (id_requerimiento.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@id_requerimiento", SqlDbType.VarChar, 50);
                sqlParameter.Value = (object)id_requerimiento;
                AllParameters.Add((object)sqlParameter);
            }

            if (fecha_requerimiento.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@fecha_requerimiento", SqlDbType.VarChar);
                sqlParameter.Value = (object)fecha_requerimiento;
                AllParameters.Add((object)sqlParameter);
            }

            if (cod_estado_requerimiento.Trim() != "-1")
            {
                SqlParameter parameter3 = new SqlParameter("@cod_estado_requerimiento", SqlDbType.Int);
                parameter3.Value = (object)cod_estado_requerimiento;
                AllParameters.Add((object)parameter3);
            }

            SqlParameter sqlParameter4 = new SqlParameter("@id_usuario", SqlDbType.Int);
            sqlParameter4.Value = (object)Convert.ToInt32(id_usuario);
            AllParameters.Add((object)sqlParameter4);

            SqlParameter sqlParameter5 = new SqlParameter("@id_perfil", SqlDbType.Int);
            sqlParameter5.Value = (object)Convert.ToInt32(id_perfil);
            AllParameters.Add((object)sqlParameter5);

            SqlParameter sqlParameter6 = new SqlParameter("@visualizar", SqlDbType.Int);
            sqlParameter6.Value = (object)Convert.ToInt32(visualizar);
            AllParameters.Add((object)sqlParameter6);

            SqlParameter sqlParameter1 = new SqlParameter("@page", SqlDbType.Int);
            sqlParameter1.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.Int);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)sqlParameter2);

            return SqlConnector.getDataTable("SPU_REQUERIMIENTO_LISTAR", AllParameters);
        }

        public static DataTable ListarRequerimientosPendientesM(string cod_requerimiento, string fecha_requerimiento, string cod_prioridad, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();

            if (cod_requerimiento.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@cod_requerimiento", SqlDbType.Int);
                sqlParameter.Value = (object)cod_requerimiento;
                AllParameters.Add((object)sqlParameter);
            }

            if (fecha_requerimiento.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@fecha_requerimiento", SqlDbType.Date);
                sqlParameter.Value = (object)fecha_requerimiento;
                AllParameters.Add((object)sqlParameter);
            }

            if (cod_prioridad.Trim() != "-1")
            {
                SqlParameter parameter3 = new SqlParameter("@cod_prioridad", SqlDbType.Int);
                parameter3.Value = (object)cod_prioridad;
                AllParameters.Add((object)parameter3);
            }

            SqlParameter sqlParameter1 = new SqlParameter("@page", SqlDbType.Int);
            sqlParameter1.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.Int);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)sqlParameter2);

            return SqlConnector.getDataTable("SPU_REQUERIMIENTO_PENDIENTES_LISTAR", AllParameters);
        }

        public static DataTable ListarRequerimientosM(string id_requerimiento, string fecha_requerimiento, string cod_estado_requerimiento, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();

            if (id_requerimiento.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@id_requerimiento", SqlDbType.VarChar, 50);
                sqlParameter.Value = (object)id_requerimiento;
                AllParameters.Add((object)sqlParameter);
            }

            if (fecha_requerimiento.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@fecha_requerimiento", SqlDbType.Date);
                sqlParameter.Value = (object)fecha_requerimiento;
                AllParameters.Add((object)sqlParameter);
            }

            if (cod_estado_requerimiento.Trim() != "-1")
            {
                SqlParameter parameter3 = new SqlParameter("@cod_estado_requerimiento", SqlDbType.Int);
                parameter3.Value = (object)cod_estado_requerimiento;
                AllParameters.Add((object)parameter3);
            }

            SqlParameter sqlParameter1 = new SqlParameter("@page", SqlDbType.Int);
            sqlParameter1.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.Int);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)sqlParameter2);

            return SqlConnector.getDataTable("SPU_REQUERIMIENTOS_LISTAR", AllParameters);
        }

        public static DataTable ListarRequerimientosAprobadosM(string cod_requerimiento, string fecha_requerimiento, string cod_prioridad, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();

            if (cod_requerimiento.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@cod_requerimiento", SqlDbType.Int);
                sqlParameter.Value = (object)cod_requerimiento;
                AllParameters.Add((object)sqlParameter);
            }

            if (fecha_requerimiento.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@fecha_requerimiento", SqlDbType.Date);
                sqlParameter.Value = (object)fecha_requerimiento;
                AllParameters.Add((object)sqlParameter);
            }

            if (cod_prioridad.Trim() != "-1")
            {
                SqlParameter parameter3 = new SqlParameter("@cod_prioridad", SqlDbType.Int);
                parameter3.Value = (object)cod_prioridad;
                AllParameters.Add((object)parameter3);
            }

            SqlParameter sqlParameter1 = new SqlParameter("@page", SqlDbType.Int);
            sqlParameter1.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.Int);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)sqlParameter2);

            return SqlConnector.getDataTable("SPU_REQUERIMIENTO_APROBADOS_LISTAR", AllParameters);
        }

        public static DataTable ListarRequerimientosAsignadosM(string cod_requerimiento, string fecha_requerimiento, string cod_prioridad, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();

            if (cod_requerimiento.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@cod_requerimiento", SqlDbType.Int);
                sqlParameter.Value = (object)cod_requerimiento;
                AllParameters.Add((object)sqlParameter);
            }

            if (fecha_requerimiento.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@fecha_requerimiento", SqlDbType.Date);
                sqlParameter.Value = (object)fecha_requerimiento;
                AllParameters.Add((object)sqlParameter);
            }

            if (cod_prioridad.Trim() != "-1")
            {
                SqlParameter parameter3 = new SqlParameter("@cod_prioridad", SqlDbType.Int);
                parameter3.Value = (object)cod_prioridad;
                AllParameters.Add((object)parameter3);
            }

            SqlParameter sqlParameter1 = new SqlParameter("@page", SqlDbType.Int);
            sqlParameter1.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.Int);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)sqlParameter2);

            return SqlConnector.getDataTable("SPU_REQUERIMIENTO_ASIGNADOS_LISTAR", AllParameters);
        }

        public static DataTable Combo_EstadoRequerimientoM(string id_perfil)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter sqlParameter = new SqlParameter("@id_perfil", SqlDbType.Int);
            sqlParameter.Value = (object)id_perfil;
            AllParameters.Add((object)sqlParameter);

            return SqlConnector.getDataTable("SPS_COMBO_ESTADO_REQUERIMIENTO", AllParameters);
        }

        public static DataTable Combo_Prioridad()
        {
            return SqlConnector.getDataTable("SPS_COMBO_PRIORIDAD");
        }

        public static DataTable Combo_CategoriaM()
        {
            return SqlConnector.getDataTable("SPS_COMBO_CATEGORIA");
        }

        public static DataTable Combo_SubCategoriaM(string cod_categoria)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter sqlParameter = new SqlParameter("@cod_categoria", SqlDbType.Int);
            sqlParameter.Value = (object)Convert.ToInt32(cod_categoria);
            AllParameters.Add((object)sqlParameter);

            return SqlConnector.getDataTable("SPS_COMBO_SUBCATEGORIA", AllParameters);
        }

        public static DataTable ListarComentarioM(string cod_requerimiento)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter sqlParameter = new SqlParameter("@id_requerimiento", SqlDbType.Int);
            sqlParameter.Value = (object)Convert.ToInt32(cod_requerimiento);
            AllParameters.Add((object)sqlParameter);

            return SqlConnector.getDataTable("SPS_COMENTARIO_LISTAR", AllParameters);
        }

        public static DataTable Combo_PrioridadM()
        {
            return SqlConnector.getDataTable("SPS_COMBO_PRIORIDAD");
        }

        public static DataTable Lista_ServiciosM()
        {
            return SqlConnector.getDataTable("SPS_LISTA_SERVICIOS");
        }

        public static DataTable RegistrarRequerimientoM(BeanRequerimiento bean)
        {
            //fecha_requerimiento, cod_categoria, codigoServicio, tipoServicio, detalle, cod_prioridad, id_usuario

            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;
            /*
            {"cod_requerimiento":0,
            "fecha_requerimiento":"2016/10/26",
            "fecha_cierre":null,
            "descripcion":null,
            "detalle":"PRUEBA",
            "cod_categoria":0,
            "cod_subcategoria":0,
            "cod_servicio":"01.00.00",
            "tipoServicio":"C",
            "cod_prioridad":1,
            "id_usuario":5,
            "cod_estado_requerimiento":0,
            "nom_categoria":"",
            "nom_subcategoria":"",
            "nom_prioridad":"","nombre_completo":"","nom_estado_requerimiento":"","rutaImg":null,"tituloImg":null,"codigo_categoria":"01.00.00","precio":"0","nom_servicio":"","id_servicio":"","linea":"","codigo_sap":"","cantidad":"","precio_unitario":"","precio_parcial":""}
            */
            parameter = new SqlParameter("@FECHA_REQUERIMIENTO", SqlDbType.VarChar, 50);
            parameter.Value = bean.fecha_requerimiento;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@COD_CATEGORIA", SqlDbType.VarChar, 50);
            parameter.Value = bean.codigo_categoria;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@COD_SERVICIO", SqlDbType.VarChar, 50);
            parameter.Value = bean.cod_servicio;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@TIPO_SERVICIO", SqlDbType.VarChar, 50);
            parameter.Value = bean.tipoServicio;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@DETALLE", SqlDbType.VarChar, 200);
            parameter.Value = bean.detalle;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@COD_PRIORIDAD", SqlDbType.Int);
            parameter.Value = bean.cod_prioridad;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@ID_USUARIO", SqlDbType.VarChar, 50);
            parameter.Value = bean.id_usuario;
            Allparameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_REQUERIMIENTO_REGISTRAR", Allparameters);
        }

        public static DataTable ObtenerDetalleRequerimientoM(Int32 id_usuario, Int32 cod_prioridad, Int32 id_perfil)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@id_usuario", SqlDbType.Int);
            parameter.Value = id_usuario;
            AllParameters.Add(parameter);
            SqlParameter parameter3 = new SqlParameter("@cod_prioridad", SqlDbType.Int);
            parameter3.Value = cod_prioridad;
            AllParameters.Add(parameter3);
            SqlParameter parameter4 = new SqlParameter("@id_perfil", SqlDbType.Int);
            parameter4.Value = id_perfil;
            AllParameters.Add(parameter4);

            return SqlConnector.getDataTable("SPS_OBTENER_DETALLE_REQUERIMIENTO", AllParameters);
        }

        public static DataTable ObtenerDetalleRequerimientosM(Int32 cod_requerimiento)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@cod_requerimiento", SqlDbType.Int);
            parameter.Value = cod_requerimiento;
            AllParameters.Add(parameter);

            return SqlConnector.getDataTable("SPS_OBTENER_DETALLE_REQUERIMIENTOS", AllParameters);
        }

        public static DataTable ObtenerInfoDetallesRequerimientoM(Int32 cod_requerimiento)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@cod_requerimiento", SqlDbType.Int);
            parameter.Value = cod_requerimiento;
            AllParameters.Add(parameter);

            return SqlConnector.getDataTable("SPS_OBTENER_DETALLES_REQUERIMIENTO", AllParameters);
        }

        public static void AprobarRequerimientoM(Int32 cod_requerimiento)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@cod_requerimiento", SqlDbType.Int);
            sqlParameter.Value = (object)cod_requerimiento;
            alParametros.Add((object)sqlParameter);
            SqlConnector.getDataTable("SPU_REQUERIMIENTO_APROBAR", alParametros);
        }

        public static void CerrarRequerimientoM(Int32 cod_requerimiento)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@cod_requerimiento", SqlDbType.Int);
            sqlParameter.Value = (object)cod_requerimiento;
            alParametros.Add((object)sqlParameter);
            SqlConnector.getDataTable("SPU_REQUERIMIENTO_APROBAR", alParametros);
        }

        public static DataTable ObtenerCategoriaSubcategoriaM(String cod_requerimiento)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@cod_requerimiento", SqlDbType.Int);
            parameter.Value = cod_requerimiento;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPS_OBTENER_CATEGORIA_SUBCATEGORIA", alParameters);
        }

        //SIEDEVS - JM
        public static DataTable ObtenerCategoriaCombo(String codigoServicio, String tipoServicio)
        {
            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@codigoServicio", SqlDbType.VarChar, 50);
            parameter.Value = codigoServicio;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@tipo", SqlDbType.VarChar, 10);
            parameter.Value = tipoServicio;
            Allparameters.Add(parameter);

            return SqlConnector.getDataTable("SPS_OBTENER_CATEGORIA_COMBO", Allparameters);
        }

        public static DataTable ListarServiciosM(Int32 cod_categoria, Int32 cod_subcategoria)
        {
            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@cod_categoria", SqlDbType.Int);
            parameter.Value = cod_categoria;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@cod_subcategoria", SqlDbType.Int);
            parameter.Value = cod_subcategoria;
            Allparameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_SERVICIO_LISTAR", Allparameters);
        }

        public static void GuardarRequerimiento(BeanDetalleRequerimiento bean)
        {
            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@CODIGO_REQUERIMIENTO", SqlDbType.Int);
            parameter.Value = bean.cod_requerimiento;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@SERVICIO_CLIENTE", SqlDbType.VarChar, 200);
            parameter.Value = bean.servicio_cliente;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@ESTADO", SqlDbType.VarChar, 50);
            parameter.Value = bean.nom_estado_requerimiento;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@CATEGORIA", SqlDbType.Int);
            parameter.Value = bean.id_categoria;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@SUBCATEGORIA", SqlDbType.Int);
            parameter.Value = bean.id_subcategoria;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@SERVICIO", SqlDbType.Int);
            parameter.Value = bean.id_servicio;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@DETALLE", SqlDbType.VarChar, 200);
            parameter.Value = bean.detalle;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@OBSERVACION", SqlDbType.VarChar, 200);
            parameter.Value = bean.observacion;
            Allparameters.Add(parameter);

            //parameter = new SqlParameter("@id_usuario", SqlDbType.Int);
            //parameter.Value = bean.idUsuario;
            //Allparameters.Add(parameter);

            parameter = new SqlParameter("@idUsuarioSesion", SqlDbType.Int);
            parameter.Value = bean.idUsuarioSesion;
            Allparameters.Add(parameter);

            SqlConnector.executeNonQuery("SPU_REQUERIMIENTO_MODIFICAR", Allparameters);
        }

        public static void AprobarRequerimiento(BeanDetalleRequerimiento bean)
        {
            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@CODIGO_REQUERIMIENTO", SqlDbType.Int);
            parameter.Value = bean.cod_requerimiento;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@SERVICIO_CLIENTE", SqlDbType.VarChar, 200);
            parameter.Value = bean.servicio_cliente;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@ESTADO", SqlDbType.VarChar, 50);
            parameter.Value = bean.nom_estado_requerimiento;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@CATEGORIA", SqlDbType.Int);
            parameter.Value = bean.id_categoria;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@SUBCATEGORIA", SqlDbType.Int);
            parameter.Value = bean.id_subcategoria;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@SERVICIO", SqlDbType.Int);
            parameter.Value = bean.id_servicio;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@DETALLE", SqlDbType.VarChar, 200);
            parameter.Value = bean.detalle;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@OBSERVACION", SqlDbType.VarChar, 200);
            parameter.Value = bean.observacion;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@PERSONAL", SqlDbType.VarChar, 300);
            parameter.Value = bean.personal;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@ID_USUARIO", SqlDbType.Int);
            parameter.Value = bean.idUsuario;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@idUsuarioSesion", SqlDbType.Int);
            parameter.Value = bean.idUsuarioSesion;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@fechaAsignacion", SqlDbType.VarChar, 25);
            parameter.Value = bean.fecha_asignacion;
            Allparameters.Add(parameter);

            SqlConnector.executeNonQuery("SPI_APROBAR_REQUERIMIENTO", Allparameters);
        }

        public static void IngresarDetalleRequerimiento(BeanDetalleRequerimiento bean)
        {
            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@CODIGO_REQUERIMIENTO", SqlDbType.Int);
            parameter.Value = bean.cod_requerimiento;
            Allparameters.Add(parameter);


            parameter = new SqlParameter("@COD_SAP", SqlDbType.VarChar, 20);
            parameter.Value = bean.servicioCurrent;
            Allparameters.Add(parameter);

            //parameter = new SqlParameter("@ID_SERVICIO_ACTUAL", SqlDbType.Int);
            //parameter.Value = bean.id_servicio;
            //Allparameters.Add(parameter);

            SqlConnector.executeNonQuery("SPS_SERVICIOS_FINALES", Allparameters);
        }

        public static void CerrarRequerimiento(BeanDetalleRequerimiento bean)
        {
            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@CODIGO_REQUERIMIENTO", SqlDbType.Int);
            parameter.Value = bean.cod_requerimiento;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@SERVICIO_CLIENTE", SqlDbType.VarChar, 200);
            parameter.Value = bean.servicio_cliente;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@ESTADO", SqlDbType.VarChar, 50);
            parameter.Value = bean.nom_estado_requerimiento;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@CATEGORIA", SqlDbType.Int);
            parameter.Value = bean.id_categoria;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@SUBCATEGORIA", SqlDbType.Int);
            parameter.Value = bean.id_subcategoria;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@SERVICIO", SqlDbType.Int);
            parameter.Value = bean.id_servicio;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@DETALLE", SqlDbType.VarChar, 200);
            parameter.Value = bean.detalle;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@OBSERVACION", SqlDbType.VarChar, 200);
            parameter.Value = bean.observacion;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@FOTO", SqlDbType.VarChar, 100);
            parameter.Value = bean.foto1;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@FOTO1", SqlDbType.VarChar, 100);
            parameter.Value = bean.foto2;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@FOTO2", SqlDbType.VarChar, 100);
            parameter.Value = bean.foto3;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@idUsuarioSesion", SqlDbType.Int);
            parameter.Value = bean.idUsuarioSesion;
            Allparameters.Add(parameter);

            //parameter = new SqlParameter("@ID_USUARIO", SqlDbType.Int);
            //parameter.Value = bean.idUsuarioC;
            //Allparameters.Add(parameter);

            SqlConnector.executeNonQuery("SPU_CERRAR_REQUERIMIENTO", Allparameters);
        }

        public static DataTable ObtenerComentarioM(Int32 cod_requerimiento, Int32 cod_estado_requerimiento)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@cod_requerimiento", SqlDbType.Int);
            parameter.Value = cod_requerimiento;
            AllParameters.Add(parameter);

            SqlParameter parameter1 = new SqlParameter("@cod_estado_requerimiento", SqlDbType.Int);
            parameter1.Value = cod_estado_requerimiento;
            AllParameters.Add(parameter1);

            return SqlConnector.getDataTable("SPU_OBTENER_COMENTARIO", AllParameters);
        }

        public static DataSet ExportaReporteRequerimientosM(string fechaIN, string fechaFI, string id_linea_negocio, string id_centro_coste, string id_perfil, string id_usuario)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter sqlParameter4 = new SqlParameter("@id_usuario", SqlDbType.Int);
            sqlParameter4.Value = (object)id_usuario;
            AllParameters.Add((object)sqlParameter4);

            SqlParameter sqlParameter5 = new SqlParameter("@id_perfil", SqlDbType.Int);
            sqlParameter5.Value = (object)id_perfil;
            AllParameters.Add((object)sqlParameter5);

            if (id_linea_negocio.Trim() != "-1")
            {
                SqlParameter sqlParameter2 = new SqlParameter("@id_linea_negocio", SqlDbType.VarChar, 20);
                sqlParameter2.Value = (object)id_linea_negocio;
                AllParameters.Add((object)sqlParameter2);
            }

            if (id_centro_coste.Trim() != "-1")
            {
                SqlParameter sqlParameter3 = new SqlParameter("@id_centro_coste", SqlDbType.VarChar, 20);
                sqlParameter3.Value = (object)id_centro_coste;
                AllParameters.Add((object)sqlParameter3);
            }

            SqlParameter sqlParameter1 = new SqlParameter("@fechaIN", SqlDbType.VarChar, 20);
            sqlParameter1.Value = (object)fechaIN;
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter = new SqlParameter("@fechaFI", SqlDbType.VarChar, 20);
            sqlParameter.Value = (object)fechaFI;
            AllParameters.Add((object)sqlParameter);

            return SqlConnector.getDataset("SPU_REPORTE_EXPORTAR", AllParameters);
        }

        public static DataTable ReporteRequerimientosM(string fechaIN, string fechaFI, string id_linea_negocio, string id_centro_coste, string id_perfil, string id_usuario, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter sqlParameter1 = new SqlParameter("@fechaIN", SqlDbType.VarChar, 20);
            sqlParameter1.Value = (object)fechaIN;
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter = new SqlParameter("@fechaFI", SqlDbType.VarChar, 20);
            sqlParameter.Value = (object)fechaFI;
            AllParameters.Add((object)sqlParameter);

            if (id_linea_negocio.Trim() != "-1")
            {
                SqlParameter sqlParameter4 = new SqlParameter("@id_linea_negocio", SqlDbType.VarChar, 20);
                sqlParameter4.Value = (object)id_linea_negocio;
                AllParameters.Add((object)sqlParameter4);
            }

            if (id_centro_coste.Trim() != "-1")
            {
                SqlParameter sqlParameter5 = new SqlParameter("@id_centro_coste", SqlDbType.VarChar, 20);
                sqlParameter5.Value = (object)id_centro_coste;
                AllParameters.Add((object)sqlParameter5);
            }

            SqlParameter sqlParameter6 = new SqlParameter("@id_perfil", SqlDbType.Int);
            sqlParameter6.Value = (object)Convert.ToInt32(id_perfil);
            AllParameters.Add((object)sqlParameter6);

            SqlParameter sqlParameter7 = new SqlParameter("@id_usuario", SqlDbType.Int);
            sqlParameter7.Value = (object)Convert.ToInt32(id_usuario);
            AllParameters.Add((object)sqlParameter7);

            SqlParameter sqlParameter3 = new SqlParameter("@page", SqlDbType.Int);
            sqlParameter3.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)sqlParameter3);

            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.Int);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)sqlParameter2);

            return SqlConnector.getDataTable("SPU_REPORTE_LISTAR", AllParameters);
        }

        //validarCodigoServicio
        public static DataTable validarCodigoServicio(String codigoServicio, String codigoServicioActual)
        {
            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@COD_SERVICIO", SqlDbType.VarChar, 20);
            parameter.Value = codigoServicio;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@ID_SERVICIO_ACTUAL", SqlDbType.Int);
            parameter.Value = codigoServicioActual;
            Allparameters.Add(parameter);

            return SqlConnector.getDataTable("SPS_VALIDAR_SERVICIO", Allparameters);
        }

        public static DataTable obtenerRutaFoto(String codigoRequerimiento)
        {
            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@COD_REQUERIMIENTO", SqlDbType.VarChar, 50);
            parameter.Value = codigoRequerimiento;
            Allparameters.Add(parameter);

            return SqlConnector.getDataTable("SPS_OBTENER_FOTO", Allparameters);
        }

        public static DataTable ObtenerSupervisores()
        {
            ArrayList AllParameters = new ArrayList();

            return SqlConnector.getDataTable("SPS_OBTENER_SUPERVISORES", AllParameters);
        }


        public static DataTable EnviarComentarioM(String comentarios, String cod_requerimiento, String idUsuario)
        {
            //fecha_requerimiento, cod_categoria, codigoServicio, tipoServicio, detalle, cod_prioridad, id_usuario

            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@id_requerimiento", SqlDbType.Int);
            parameter.Value = cod_requerimiento;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@comentarios", SqlDbType.VarChar, 2000);
            parameter.Value = comentarios;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@id_usuario", SqlDbType.Int);
            parameter.Value = idUsuario;
            Allparameters.Add(parameter);

            return SqlConnector.getDataTable("SPI_REGISTRAR_COMENTARIO", Allparameters);
        }
    }
}
