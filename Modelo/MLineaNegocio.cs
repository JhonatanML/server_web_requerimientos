﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MLineaNegocio
    {
        public static DataTable CargarComboLineaNegocioM(string id_perfil, string id_usuario)
        {
            ArrayList Allparameters = new ArrayList();
            SqlParameter parameter;

            parameter = new SqlParameter("@id_perfil", SqlDbType.Int);
            parameter.Value = id_perfil;
            Allparameters.Add(parameter);

            parameter = new SqlParameter("@id_usuario", SqlDbType.Int);
            parameter.Value = id_usuario;
            Allparameters.Add(parameter);

            return SqlConnector.getDataTable("SPS_COMBO_LINEA_NEGOCIO", Allparameters);
        }
    }
}
