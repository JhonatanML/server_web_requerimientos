﻿using Modelo.BEANS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MCategoria
    {
        public static DataTable BuscarCategoriaM(string linea, string nom_categoria, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter parameter;

            if (linea.Trim().Length != 0)
            {
                parameter = new SqlParameter("@linea", SqlDbType.VarChar, 20);
                parameter.Value = (object)linea;
                AllParameters.Add((object)parameter);
            }

            if (nom_categoria.Trim().Length != 0)
            {
                parameter = new SqlParameter("@nom_categoria", SqlDbType.VarChar, 50);
                parameter.Value = (object)nom_categoria;
                AllParameters.Add((object)parameter);
            }

            parameter = new SqlParameter("@page", SqlDbType.Int);
            parameter.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@rows", SqlDbType.Int);
            parameter.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)parameter);

            return SqlConnector.getDataTable("SPU_CATEGORIA_BUSCAR", AllParameters);
        }

        public static void EliminarCategoriaM(string id_categoria)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter parameter;
            parameter = new SqlParameter("@id_categoria", SqlDbType.VarChar, 4000);
            parameter.Value = (object)id_categoria;
            alParametros.Add((object)parameter);
            SqlConnector.getDataTable("SPU_CATEGORIA_BORRAR", alParametros);
        }

        public static int AgregarCategoriaM(BeanCategoria bean)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter;

            parameter = new SqlParameter("@nom_categoria", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.nom_categoria;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@linea", SqlDbType.VarChar, 20);
            parameter.Value = (object)bean.linea;
            AllParameters.Add((object)parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_CATEGORIA_AGREGAR", AllParameters));
        }

        public static DataTable ObtenerCategoriaM(string id_categoria)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter parameter;
            parameter = new SqlParameter("@id_categoria", SqlDbType.Int);
            parameter.Value = (object)int.Parse(id_categoria);
            AllParameters.Add((object)parameter);
            return SqlConnector.getDataTable("SPU_CATEGORIA_INFO", AllParameters);
        }

        public static int EditarCategoriaM(BeanCategoria bean)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter;

            parameter = new SqlParameter("@nom_categoria", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.nom_categoria;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@linea", SqlDbType.VarChar, 20);
            parameter.Value = (object)bean.linea;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_categoria", SqlDbType.Int);
            parameter.Value = (object)bean.id_categoria;
            AllParameters.Add((object)parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_CATEGORIA_EDITAR", AllParameters));
        }
    }
}
