﻿using Modelo.BEANS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MReporte
    {
        public static DataTable Sucursales_dt_combo()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("SPU_RPT_BUSQUEDAS_SUCURSALES", alParameters);
        }

        public static DataTable Sucursales_dt_combo_bycodigo(Int32 id, Int32 idperfil)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@CODIGO", SqlDbType.BigInt);
            parameter.Value = id;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@CODIGOPERFIL", SqlDbType.BigInt);
            parameter.Value = idperfil;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("SPU_RPT_BUSQUEDAS_SUCURSALES_USU", alParameters);
        }

        public static DataTable Usuarios_dt_combo(String codSucursal)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@SUCURSAL", SqlDbType.VarChar, 50);
            parameter.Value = codSucursal;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("SPU_RPT_BUSQUEDAS_USUARIOS", alParameters);
        }

        public static DataTable consultaReporteBusqueda(BeanBusqueda bean)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@page", SqlDbType.Int);
            parameter.Value = bean.pagina;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@rows", SqlDbType.Int);
            parameter.Value = bean.filas;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@USUARIO", SqlDbType.Int);
            parameter.Value = bean.usuario;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@SUCURSAL", SqlDbType.VarChar, 10);
            parameter.Value = bean.surcursal;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@DESDE", SqlDbType.VarChar, 10);
            parameter.Value = bean.fecInicio;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@HASTA", SqlDbType.VarChar, 10);
            parameter.Value = bean.fecFin;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("SPU_WEB_RPT_BUSQUEDAS", alParameters);
        }

        public static DataTable reporteBUSQUEDASxls(string pFechaini, string pFechaFin, string pSucursal, string pUsuario)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@DESDE", SqlDbType.VarChar, 10);
            parameter.Value = pFechaini;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@HASTA", SqlDbType.VarChar, 10);
            parameter.Value = pFechaFin;
            alParameters.Add(parameter);
            if (pUsuario.Trim() != "-1")
            {
                parameter = new SqlParameter("@USUARIO", SqlDbType.Int);
                parameter.Value = pUsuario;
                alParameters.Add(parameter);
            }
            if (pSucursal.Trim() != "-1")
            {
                parameter = new SqlParameter("@SUCURSAL", SqlDbType.VarChar, 10);
                parameter.Value = pSucursal;
                alParameters.Add(parameter);
            }
            return SqlConnector.getDataTable("SPU_RPT_BUSQUEDAS_XLS", alParameters);
        }

        public static DataTable reporteBUSQUEDASdet(string codigo, string desde, string hasta)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter1 = new SqlParameter("@USUARIO", SqlDbType.BigInt);
            sqlParameter1.Value = (object)Convert.ToInt32(codigo);
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@DESDE", SqlDbType.VarChar, 10);
            sqlParameter2.Value = (object)desde;
            alParametros.Add((object)sqlParameter2);
            SqlParameter sqlParameter3 = new SqlParameter("@HASTA", SqlDbType.VarChar, 10);
            sqlParameter3.Value = (object)hasta;
            alParametros.Add((object)sqlParameter3);
            return SqlConnector.getDataTable("SPU_RPT_BUSQUEDAS_DET", alParametros);
        }


        public static DataTable reporteDetDescarga(string pFechaini, string pFechaFin, string pSucursal, string pUsuario)
        {
           
            if (ConfigurationManager.AppSettings["DEBUG"].Equals("1"))
            {
                return SqlConnector.getDataTable("SPS_INSERT_DATA");
            }
            else
            {
                ArrayList alParameters = new ArrayList();
                SqlParameter parameter = new SqlParameter("@DESDE", SqlDbType.VarChar, 20);
                parameter.Value = pFechaini;
                alParameters.Add(parameter);
                parameter = new SqlParameter("@HASTA", SqlDbType.VarChar, 20);
                parameter.Value = pFechaFin;
                alParameters.Add(parameter);
                parameter = new SqlParameter("@USUARIO", SqlDbType.VarChar, 20);
                parameter.Value = pUsuario;
                alParameters.Add(parameter);
                parameter = new SqlParameter("@SUCURSAL", SqlDbType.VarChar, 20);
                parameter.Value = pSucursal;
                alParameters.Add(parameter);
                return SqlConnector.getDataTable("SPS_WEB_DESCARGA_CONSULTA_DET", alParameters);
            }
        }
    }


}
