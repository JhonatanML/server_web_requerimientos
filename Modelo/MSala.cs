﻿using Modelo.BEANS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MSala
    {
        public static DataTable BuscarSalaM(string cod_sala, string nom_sala, string cod_local, string page, string rows, Int32 cod_empresa)
        {
            ArrayList AllParameters = new ArrayList();

            if (cod_sala.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@cod_sala", SqlDbType.Int);
                sqlParameter.Value = (object)cod_local;
                AllParameters.Add((object)sqlParameter);
            }

            if (nom_sala.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@nom_sala", SqlDbType.VarChar, 50);
                sqlParameter.Value = (object)nom_sala;
                AllParameters.Add((object)sqlParameter);
            }

            if (cod_local.Trim() != "-1")
            {
                SqlParameter sqlParameter3 = new SqlParameter("@cod_local", SqlDbType.Int);
                sqlParameter3.Value = (object)cod_local;
                AllParameters.Add((object)sqlParameter3);
            }

            SqlParameter sqlParameter1 = new SqlParameter("@page", SqlDbType.Int);
            sqlParameter1.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)sqlParameter1);

            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.Int);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)sqlParameter2);

            SqlParameter sqlParameter4 = new SqlParameter("@cod_empresa", SqlDbType.Int);
            sqlParameter4.Value = (object)cod_empresa;
            AllParameters.Add((object)sqlParameter4);

            return SqlConnector.getDataTable("SPU_SALA_BUSCAR", AllParameters);
        }

        public static int AgregarSalaM(BeanSala bean)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter sqlParameter1 = new SqlParameter("@nom_sala", SqlDbType.VarChar, 50);
            sqlParameter1.Value = (object)bean.nom_sala;
            AllParameters.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@capacidad_personas", SqlDbType.Int);
            sqlParameter2.Value = (object)bean.capacidad_personas;
            AllParameters.Add((object)sqlParameter2);
            SqlParameter sqlParameter3 = new SqlParameter("@cod_local", SqlDbType.Int);
            sqlParameter3.Value = (object)bean.cod_local;
            AllParameters.Add((object)sqlParameter3);
            SqlParameter sqlParameter4 = new SqlParameter("@descripcion", SqlDbType.VarChar, 800);
            sqlParameter4.Value = (object)bean.descripcion;
            AllParameters.Add((object)sqlParameter4);            

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_SALA_AGREGAR", AllParameters));
        }

        public static DataTable ObtenerSalaM(string cod_sala)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@cod_sala", SqlDbType.Int);
            sqlParameter.Value = (object)int.Parse(cod_sala);
            AllParameters.Add((object)sqlParameter);
            return SqlConnector.getDataTable("SPU_SALA_INFO", AllParameters);
        }

        public static int EditarSalaM(BeanSala bean)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter sqlParameter = new SqlParameter("@cod_sala", SqlDbType.Int);
            sqlParameter.Value = (object)bean.cod_sala;
            AllParameters.Add((object)sqlParameter);
            SqlParameter sqlParameter1 = new SqlParameter("@nom_sala", SqlDbType.VarChar, 50);
            sqlParameter1.Value = (object)bean.nom_sala;
            AllParameters.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@capacidad_personas", SqlDbType.Int);
            sqlParameter2.Value = (object)bean.capacidad_personas;
            AllParameters.Add((object)sqlParameter2);
            SqlParameter sqlParameter3 = new SqlParameter("@cod_local", SqlDbType.Int);
            sqlParameter3.Value = (object)bean.cod_local;
            AllParameters.Add((object)sqlParameter3);
            SqlParameter sqlParameter4 = new SqlParameter("@descripcion", SqlDbType.VarChar, 800);
            sqlParameter4.Value = (object)bean.descripcion;
            AllParameters.Add((object)sqlParameter4);            

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_SALA_EDITAR", AllParameters));
        }

        public static void EliminarSalaM(string cod_sala)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@cod_sala", SqlDbType.VarChar, 4000);
            sqlParameter.Value = (object)cod_sala;
            alParametros.Add((object)sqlParameter);
            SqlConnector.getDataTable("SPU_SALA_BORRAR", alParametros);
        }
    }
}
