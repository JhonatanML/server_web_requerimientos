﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MCorreo
    {
        public static void EnviarCorreoM(string email, string id_requerimiento, string email_jefe, string nombre_completo, string nom_estado_requerimiento, string fecha_generada, string linea, string nom_prioridad, string detalle, string id_perfil)
        {
            string estado_requerimiento = "";
            switch (nom_estado_requerimiento)
            {
                case "PENDIENTE":
                    estado_requerimiento = "REGISTRADO";
                    break;
                case "APROBADO":
                    estado_requerimiento = "APROBADO";
                    break;
                case "ASIGNADO":
                    estado_requerimiento = "ASIGNADO";
                    break;
                case "CERRADO":
                    estado_requerimiento = "CERRADO";
                    break;
                    //default:
                    //    Console.WriteLine("Default case");
                    //    break;
            }

            /*-------------------------MENSAJE DE CORREO----------------------*/

            //Creamos un nuevo Objeto de mensaje
            System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

            //Direccion de correo electronico a la que queremos enviar el mensaje
            //CORREO DEL USUARIO QUE REGISTRA EL REQUERIMIENTO DEL SERVICIO
            mmsg.To.Add(email);

            //Nota: La propiedad To es una colección que permite enviar el mensaje a más de un destinatario

            //Asunto
            mmsg.Subject = "SETERZA RQ" + " " + id_requerimiento + " - " + estado_requerimiento;
            //mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

            //Direccion de correo electronico que queremos que reciba una copia del mensaje
            if (id_perfil == "3")
            {
                //CORREO DEL SUPERVISOR AL QUE SE LE ENVIA UNA COPIA DEL MENSAJE            
                mmsg.Bcc.Add(email_jefe); //Opcional
            }

            //CORREO DEL PROVEEDOR AL QUE SE LE ENVIA UNA COPIA DEL MENSAJE
            String email_proveedor = ConfigurationManager.AppSettings["EMAIL_PROVEEDOR"].ToString();
            mmsg.Bcc.Add(email_proveedor); //Opcional

            //Cuerpo del Mensaje
            mmsg.Body = "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'/></head><body><table width='750' border='7px' align='center' cellpadding='0' cellspacing='0'><tr><td align='center' valign='top' bgcolor='#000000' style='background-color:#FFFFFF; font-size:13px; color:#000000;'><table width='100%' border='0' cellspacing='0' cellpadding='0' style='margin-top:-35px;'><tr><td align='center' valign='top'><br><br><br><br><br><div style='font-size:45px; color:#000000 ;'><u><b> MENSAJE INFORMATIVO </b></u></div><div style='font-size:30px; color:#000000;'><br> Sr.(a) " + nombre_completo + ", su requerimiento de servicio ha sido " + estado_requerimiento + " correctamente.</div><div align='left'  style='font-size:17px; color:#34495E; padding-left:100px'><br><b><u>Datos sobre el requerimiento</u></b><br><br><b>Estado: </b>" + nom_estado_requerimiento + "<br><b>Fecha generada: </b>" + fecha_generada + "<br><b>Línea / Nombre del servicio: </b>" + linea + "<br><b>Prioridad: </b>" + nom_prioridad + "<br><b>Detalle: </b>" + detalle + "</b></div><br><br><br><br><br><br></td></tr></table></td></tr><tr></tr></table></body></html>";
            mmsg.BodyEncoding = System.Text.Encoding.UTF8;
            mmsg.IsBodyHtml = true; //Si no queremos que se envíe como HTML

            //CORREO DEL SERVIDOR (EL MISMO DE LAS CREDENCIALES)
            //Correo electronico desde la que enviamos el mensaje
            String email_empresa = ConfigurationManager.AppSettings["EMAIL_EMPRESA"].ToString();
            mmsg.From = new System.Net.Mail.MailAddress(email_empresa);


            /*-------------------------CLIENTE DE CORREO----------------------*/

            //Creamos un objeto de cliente de correo
            System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();

            //Hay que crear las credenciales del correo emisor                
            String password_email_empresa = ConfigurationManager.AppSettings["PASSWORD_EMAIL_EMPRESA"].ToString();
            cliente.Credentials =
                new System.Net.NetworkCredential(email_empresa, password_email_empresa);

            //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail

            cliente.Port = 587;
            cliente.EnableSsl = true;


            cliente.Host = "smtp-mail.outlook.com"; //Para Gmail "smtp.gmail.com";
                                                    //cliente.Host = "smtp.gmail.com";


            /*-------------------------ENVIO DE CORREO----------------------*/

            try
            {
                //Enviamos el mensaje      
                cliente.Send(mmsg);
            }
            catch (Exception ex)
            {
                /*Utility.registrarLog("Enviar correo :" + ex.Message)*/;
                //Utility.registrarLog(ex.StackTrace);
            }
        }

        public static DataTable ObtenerDetalleCorreoM(Int32 id_requerimiento, Int32 id_usuario)
        {
            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter = new SqlParameter("@id_requerimiento", SqlDbType.Int);
            parameter.Value = id_requerimiento;
            AllParameters.Add(parameter);

            SqlParameter parameter2 = new SqlParameter("@id_usuario", SqlDbType.Int);
            parameter2.Value = id_usuario;
            AllParameters.Add(parameter2);

            return SqlConnector.getDataTable("SPS_OBTENER_DETALLE_CORREO", AllParameters);
        }
    }
}