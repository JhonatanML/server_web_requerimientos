﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilidad;

namespace Modelo
{
    public class MConfiguracion
    {
        public static DataTable configuracion_dt(String grupo)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@grupo", SqlDbType.VarChar, 3);
            parameter.Value = grupo;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("EVA_CRED_SPS_CFCONFIG", alParameters);
        }
    }
}
