﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilidad;

namespace Modelo
{
    public class MPerfil
    {
        public static DataTable Perfil_dt_Combo()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("SPS_PERFIL_LIST_COMBO", alParameters);
        }

        public static DataTable Combo_PerfilM(Int32 id_perfil)
        {
            ArrayList alParameters = new ArrayList();

            SqlParameter sqlParameter = new SqlParameter("@id_perfil", SqlDbType.Int);
            sqlParameter.Value = id_perfil;
            alParameters.Add(sqlParameter);


            return SqlConnector.getDataTable("SPS_COMBO_PERFIL", alParameters);
        }
    }
}
