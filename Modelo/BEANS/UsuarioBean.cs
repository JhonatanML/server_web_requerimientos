﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class UsuarioBean
    {
        public string id { get; set; }

        public string login { get; set; }

        public string clave { get; set; }

        public string estado { get; set; }

        public string codigo { get; set; }

        public string nombre { get; set; }

        public string codSucursal { get; set; }

        public string sucursal { get; set; }

        public string supUsuario { get; set; }

        public UsuarioBean()
        {
            this.id = "";
            this.codigo = "";
            this.nombre = "";
            this.login = "";
            this.sucursal = "";
            this.clave = "";
            this.estado = "F";
            this.codSucursal = "";
            this.supUsuario = "";
        }
    }
}
