﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanComentario
    {
        public String comentarios { get; set; }
        public String fecha { get; set; }
        public String descripcionUsuario { get; set; }

        public String idPerfil { get; set; }

        public BeanComentario() { }
    }
}
