﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanConfiguracion
    {
        public Int32 idconfiguracion { get; set; }
        public String codigo { get; set; }
        public String nombre { get; set; }
        public String descripcion { get; set; }
        public String grupo { get; set; }
        public String estado { get; set; }

    }
}
