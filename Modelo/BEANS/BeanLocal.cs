﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanLocal
    {
        public Int32 cod_local { get; set; }
        public string nom_local { get; set; }
        public Int32 cod_empresa { get; set; }
        public string direccion { get; set; }
        public string referencia { get; set; }
        public Int32 telf_movil { get; set; }
        public Int32 telf_fijo { get; set; }

        public BeanLocal()
        {
            this.cod_local = 0;
            this.nom_local = "";
            this.cod_empresa = 0;
            this.direccion = "";
            this.referencia = "";
            this.telf_movil = 0;
            this.telf_fijo = 0;

        }
    }
}
