﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class SucursalBean
    {
        public string id { get; set; }

        public string codigo { get; set; }

        public string nombre { get; set; }

        public SucursalBean()
        {
            this.id = "";
            this.codigo = "";
            this.nombre = "";
        }
    }
}
