﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanRequerimiento
    {
        //fecha_requerimiento, cod_categoria, codigoServicio, tipoServicio, detalle, cod_prioridad, id_usuario
        public Int32 cod_requerimiento { get; set; }
        public String fecha_requerimiento { get; set; }
        public String fecha_cierre { get; set; }
        public String descripcion { get; set; }
        public String detalle { get; set; }
        public Int32 cod_categoria { get; set; }
        public Int32 cod_subcategoria { get; set; }
        public String cod_servicio { get; set; }
        public String tipoServicio { get; set; }
        public Int32 cod_prioridad { get; set; }
        public Int32 id_usuario { get; set; }
        public Int32 id_perfil { get; set; }
        public Int32 cod_estado_requerimiento { get; set; }

        public String nom_categoria { get; set; }
        public String nom_subcategoria { get; set; }
        public String nom_prioridad { get; set; }
        public String nombre_completo { get; set; }
        public String nom_estado_requerimiento { get; set; }

        public String rutaImg { get; set; }

        public String tituloImg { get; set; }

        //Nuevo
        public String codigo_categoria { get; set; }

        public String precio { get; set; }

        public String nom_servicio { get; set; }

        //REPORTE
        public String id_servicio { get; set; }
        public String linea { get; set; }
        public String codigo_sap { get; set; }
        public String cantidad { get; set; }
        public String precio_unitario { get; set; }
        public String precio_parcial { get; set; }

        public String fecha_asignacion { get; set; }
        public String hora_asignacion { get; set; }

        public String nombre_linea_negocio { get; set; }
        public String nombre_centro_coste { get; set; }
        public String id_centro_coste { get; set; }

        public BeanRequerimiento()
        {
            this.cod_requerimiento = 0;
            this.fecha_requerimiento = "";
            this.cod_categoria = 0;
            this.cod_prioridad = 0;
            this.id_usuario = 0;
            this.cod_estado_requerimiento = 0;
            this.nom_categoria = "";
            this.nom_subcategoria = "";
            this.nom_prioridad = "";
            this.nombre_completo = "";
            this.nom_estado_requerimiento = "";
            this.precio = "0";
            this.nom_servicio = "";
            this.id_servicio = "";
            this.linea = "";
            this.codigo_sap = "";
            this.cantidad = "";
            this.precio_unitario = "";
            this.precio_parcial = "";
            this.fecha_asignacion = "";
            this.hora_asignacion = "";
            this.nombre_linea_negocio = "";
            this.nombre_centro_coste = "";
            this.id_perfil = 0;
            this.id_centro_coste = "";
        }
    }
}