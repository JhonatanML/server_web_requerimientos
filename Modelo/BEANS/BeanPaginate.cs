﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanPaginate
    {
        //public List<BeanIncidencias> lstBeanIncidencias { get; set; }
        //public List<BeanUsuarioBono> lstResultadoBeanUsuarioBono { get; set; }
        //public List<BeanAplicacion> lstResultadoBeanAplicacion { get; set; }

        public List<BeanComentario> lstResultadoBeanComentario { get; set; }
        public List<BeanUsuario> lstResultadoBeanUsuario { get; set; }
        public List<BeanCategoria> lstResultadoBeanCategoria { get; set; }
        public List<BeanReserva> lstResultadoBeanReserva { get; set; }
        public List<BeanSubCategoria> lstResultadoBeanSubcategoria { get; set; }
        public List<BeanRequerimiento> lstResultadoBeanRequerimiento { get; set; }
        public List<BeanServicio> lstResultadoBeanServicio { get; set; }
        public List<BeanSala> lstResultadoBeanSala { get; set; }
        public List<BeanConsulta> lstResultadoBeanConsulta { get; set; }
        public List<BeanUsuario> lstResultadoBeanConsultaUsuario { get; set; }
        public List<BeanReporte> lstResultadoBeanReporte { get; set; }
        public List<ReporteInfoBean> lstResultadoReporteInfoBean { get; set; }
        public List<UsuarioBean> lstResultadoUsuarioBean { get; set; }
        public List<SucursalBean> lstResultadoSucursalBean { get; set; }

        public List<BeanLocal> lstResultadoBeanLocal { get; set; }
        public Int32 totalPages { get; set; }
        public Int32 totalRegistros { get; set; }

        public Double precio_total { get; set; }
        public BeanPaginate()
        {
            //lstBeanIncidencias = new List<BeanIncidencias>();
            //lstResultadoBeanAplicacion = new List<BeanAplicacion>();
            lstResultadoBeanSubcategoria = new List<BeanSubCategoria>();
            lstResultadoBeanCategoria = new List<BeanCategoria>();

            lstResultadoBeanUsuario = new List<BeanUsuario>();

            lstResultadoBeanRequerimiento = new List<BeanRequerimiento>();
            lstResultadoBeanServicio = new List<BeanServicio>();
            lstResultadoBeanReserva = new List<BeanReserva>();
            lstResultadoBeanLocal = new List<BeanLocal>();
            lstResultadoBeanSala = new List<BeanSala>();
            lstResultadoBeanReporte = new List<BeanReporte>();
            lstResultadoBeanConsulta = new List<BeanConsulta>();
            lstResultadoBeanConsultaUsuario = new List<BeanUsuario>();
            lstResultadoReporteInfoBean = new List<ReporteInfoBean>();
            lstResultadoUsuarioBean = new List<UsuarioBean>();
            lstResultadoSucursalBean = new List<SucursalBean>();
            lstResultadoBeanComentario = new List<BeanComentario>();
            totalPages = 0;
            totalRegistros = 0;
            precio_total = 0.0;
        }
    }
}

