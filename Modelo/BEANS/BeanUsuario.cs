﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanUsuario
    {
        public Int32 id_usuario { get; set; }
        public String usuario { get; set; }
        public String clave { get; set; }
        public String apellido_paterno { get; set; }
        public String apellido_materno { get; set; }
        public String nombres { get; set; }
        public String dni { get; set; }
        public String email { get; set; }
        public String telf_movil { get; set; }
        public String telf_fijo { get; set; }
        public String direccion { get; set; }
        public String distrito { get; set; }
        public String ciudad { get; set; }
        public Int32 id_perfil { get; set; }
        public Int32 cod_empresa { get; set; }
        public Int32 habilitado { get; set; }

        public String nombre_completo { get; set; }
        public String nom_perfil { get; set; }
        public Int32 estado_perfil { get; set; }
        public String nom_empresa { get; set; }

        public String codigo_supervisor { get; set; }

        public Int32 id_jefe { get; set; }
        public String nombres_jefe { get; set; }

        public Int32 id_admin_generalA { get; set; }
        public Int32 id_admin_generalC { get; set; }
        public String nombres_admin_general { get; set; }
        public Int32 id_perfil_admin { get; set; }

        public String idLineaNegocio { get; set; }

        public String idCentroCosto { get; set; }
        public Int32 primeraSesion { get; set; }

        public BeanUsuario()
        {
            this.id_usuario = 0;
            //this.usuario = "";
            //this.clave = "";
            this.apellido_paterno = "";
            this.apellido_materno = "";
            this.nombres = "";
            this.dni = "";
            this.email = "";
            this.telf_movil = "";
            this.telf_fijo = "";
            this.direccion = "0";
            this.distrito = "";
            this.ciudad = "";
            this.id_perfil = 0;
            this.nom_perfil = "";
            this.cod_empresa = 0;
            this.nom_empresa = "";
            this.habilitado = 0;

            this.id_jefe = 0;
            this.nombres_jefe = "";
            this.id_admin_generalA = 0;
            this.id_admin_generalC = 0;
            this.id_perfil_admin = 0;
            this.nombres_admin_general = "";            
        }
    }
}
