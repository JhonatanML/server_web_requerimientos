﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanLineaNegocio
    {
        public string id_linea_negocio { get; set; }
        public string nombre_linea_negocio { get; set; }
        public string descripcion { get; set; }

        public BeanLineaNegocio()
        {
            this.id_linea_negocio = "";
            this.nombre_linea_negocio = "";
            this.descripcion = "";
        }
    }
}
