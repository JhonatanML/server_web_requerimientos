﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanSala
    {
        public Int32 cod_sala { get; set; }
        public string nom_sala { get; set; }
        public string color_sala { get; set; }
        public Int32 capacidad_personas { get; set; }
        public string descripcion { get; set; }
        public string nom_local { get; set; }
        public Int32 cod_local { get; set; }
        public Int32 estado { get; set; }

        public BeanSala()
        {
            this.cod_sala = 0;
            this.nom_sala = "";
            this.color_sala = "";
            this.capacidad_personas = 0;
            this.descripcion = "";
            this.nom_local = "";
            this.cod_local = 0;
            this.estado = 0;

        }
    }
}
