﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class ReporteInfoBean
    {
        public string nrodocto { get; set; }

        public string tipodocto { get; set; }

        public string cbase { get; set; }

        public string fecha { get; set; }

        public string encontrado { get; set; }

        public string afiliado { get; set; }

        public string codUsuario { get; set; }

        public string codSucursal { get; set; }

        public ReporteInfoBean()
        {
            this.nrodocto = string.Empty;
            this.tipodocto = string.Empty;
            this.cbase = string.Empty;
            this.fecha = string.Empty;
            this.encontrado = string.Empty;
            this.afiliado = string.Empty;
            this.codUsuario = string.Empty;
            this.codSucursal = string.Empty;
        }
    }
}
