﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanCorreo
    {
        public String email { get; set; }
        public Int32 id_requerimiento { get; set; }
        public String email_jefe { get; set; }
        public String nombre_completo { get; set; }
        public String nom_estado_requerimiento { get; set; }
        public String fecha_requerimiento { get; set; }
        public String nom_servicio { get; set; }
        public String nom_prioridad { get; set; }
        public String detalle { get; set; }
        public String id_perfil { get; set; }

        public BeanCorreo()
        {
            this.email = "";
            this.id_requerimiento = 0;
            this.email_jefe = "";
            this.nombre_completo = "";
            this.nom_estado_requerimiento = "";
            this.fecha_requerimiento = "";
            this.nom_servicio = "";
            this.nom_prioridad = "";
            this.detalle = "";
            this.id_perfil = "";
        }
    }
}
