﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanBusqueda
    {
        public String surcursal { get; set; }
        public String usuario { get; set; }
        public String fecInicio { get; set; }
        public String fecFin { get; set; }
        public String filas { get; set; }
        public String pagina { get; set; }

        public BeanBusqueda() { }
    }
}
