﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanDocumento
    {
        public Int32 IdDocumento { get; set; }
        public String NumeroDocumento { get; set; }
        public String FechaCarga { get; set; }
        public Int32 Estado { get; set; }
        public Int32 IdTipoDoc { get; set; }
        public String TipoDocumento { get; set; }
        public String RazonSocial { get; set; }
    }
}
