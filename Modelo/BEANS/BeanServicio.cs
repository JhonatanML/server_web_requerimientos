﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanServicio
    {
        public String nom_servicios { get; set; }
        public String cod_servicios { get; set; }

        public String tipo_servicios { get; set; }

        public String cod_sap { get; set; }

        //GRID SERVICIO
        public String nom_servicio { get; set; }
        public Int32 id_servicio { get; set; }
        public String cod_servicio { get; set; }
        public String codigo_sap { get; set; }
        public String detalles { get; set; }
        public String costo { get; set; }
        public String nom_categoria { get; set; }
        public String nom_subcategoria { get; set; }

        public String cod_categoria { get; set; }
        public String cod_subcategoria { get; set; }
        public String linea { get; set; }
        public String precio { get; set; }
        public String descripcion { get; set; }

        public BeanServicio()
        {
            this.nom_servicios = "";
            this.cod_servicios = "0";
            this.cod_sap = "0";
            this.tipo_servicios = "";


            this.nom_servicio = "";
            this.id_servicio = 0;
            this.cod_servicio = "";
            this.codigo_sap = "";
            this.detalles = "";
            this.costo = "";
            this.nom_categoria = "";
            this.nom_subcategoria = "";

            this.cod_categoria = "0";
            this.cod_subcategoria = "0";
            this.linea = "";
            this.precio = "0";
            this.descripcion = "";
        }
    }
}
