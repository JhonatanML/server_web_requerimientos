﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class Combo
    {
        public String Codigo { get; set; }
        public String Nombre { get; set; }

        public Combo()
        {
            Codigo = "";
            Nombre = "";
        }
    }
}
