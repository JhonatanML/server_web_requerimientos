﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanUsuarioR
    {
        public Int32 id_usuario { get; set; }
        public String nombres { get; set; }
        public String usuario { get; set; }
        public String clave { get; set; }
        public Int32 cod_empresa { get; set; }
        public Int32 id_perfil { get; set; }
        public Int32 estado_perfil { get; set; }
        public Int32 estado { get; set; }
    }
}
