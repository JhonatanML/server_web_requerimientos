﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanCategoria
    {
        public String codigoCategoria { get; set; }
        public String nombreCategoria { get; set; }
        public String detalleCategoria { get; set; }

        //GRID CATEGORIA
        public Int32 id_categoria { get; set; }
        public String nom_categoria { get; set; }
        public String linea { get; set; }

        public BeanCategoria()
        {
            this.id_categoria = 0;
            this.nom_categoria = "";
            this.linea = "";
        }
    }
}
