﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.BEANS
{
    public class BeanConsulta
    {
        public String IdConsulta { get; set; }
        public Int32 IdUsuario { get; set; }
        public Int32 IdDocumento { get; set; }
        public String NumeroDocumento { get; set; }
        public String Fecha { get; set; }
        public String Hora { get; set; }
        public String idResultado { get; set; }
        public String nomResultado { get; set; }
        public String nomUsuario { get; set; }
        public String TipoDoc { get; set; }
        public String RazonSocial { get; set; }
    }
}
