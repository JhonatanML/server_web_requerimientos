﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanReserva
    {
        public Int32 cod_reserva { get; set; }
        public String titulo_reserva { get; set; }
        public String fecha_reserva { get; set; }
        public String fecha_inicio_reserva { get; set; }
        public String hora_inicio_reserva { get; set; }
        public String hora_termino_reserva { get; set; }
        public String invitados { get; set; }
        public Int32 cod_sala { get; set; }
        public Int32 id_usuario { get; set; }
        public String detalle_reserva { get; set; }
        public Int32 cod_color { get; set; }
        public String color { get; set; }
        public String nom_sala { get; set; }
        public String nom_color { get; set; }
        public String nombres { get; set; }
        public String nom_local { get; set; }
        public string nombre_completo { get; set; }

        public BeanReserva() {

            this.cod_reserva = 0;
            this.titulo_reserva = "";
            this.fecha_reserva = "";
            this.fecha_inicio_reserva = "";
            this.hora_inicio_reserva = "";
            this.hora_termino_reserva = "";
            this.invitados = "";
            this.cod_sala = 0;
            this.id_usuario = 0;
            this.detalle_reserva = "";
            this.cod_color = 0;
            this.color = "";
            this.nombres = "";
            this.nom_sala = "";
            this.nom_local = "";
            this.nom_color = "";
            this.nombre_completo = "";

        }
    }
}
