﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanReporte
    {
        public String usuario { get; set; }
        public String sucursal { get; set; }
        public String consultas { get; set; }
        public String encontrados { get; set; }
        public String afiliados { get; set; }
        public String noafiliados { get; set; }
        public String ide { get; set; }

        public String fecInicio { get; set; }
        public String fecFin { get; set; }

        public BeanReporte() { }
    }
}
