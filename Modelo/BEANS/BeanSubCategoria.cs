﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanSubCategoria
    {
        public Int32 cod_subcategoria { get; set; }
        public String descripcion { get; set; }
        public Int32 id_subcategoria { get; set; }
        public String linea { get; set; }
        public String nom_subcategoria { get; set; }
        public Int32 id_categoria { get; set; }

        public String nom_categoria { get; set; }
        public BeanSubCategoria()
        {
            this.id_subcategoria = 0;
            this.cod_subcategoria = 0;
            this.linea = "";
            this.nom_subcategoria = "";
            this.id_categoria = 0;

            this.nom_categoria = "";
        }
    }
}
