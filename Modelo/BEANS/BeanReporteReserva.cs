﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanReporteReserva
    {
        public String titulo_reserva { get; set; }
        public String fecha_Reserva { get; set; }
        public String fecha_inicio_reserva { get; set; }
        public String hora_inicio_reserva { get; set; }
        public String hora_termino_reserva { get; set; }
        public String invitados { get; set; }
        public String cod_sala { get; set; }
        public String id_usuario { get; set; }

        public String ide { get; set; }

        public BeanReporteReserva() { }
    }
}
