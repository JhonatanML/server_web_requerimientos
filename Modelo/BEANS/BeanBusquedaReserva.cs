﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanBusquedaReserva
    {
        public String fecha_inicio_reserva { get; set; }
        public String cod_local { get; set; }
        public String id_usuario { get; set; }       
        public String filas { get; set; }
        public String pagina { get; set; }

        public BeanBusquedaReserva() { }
    }
}
