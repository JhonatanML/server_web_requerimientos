﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanDetalleRequerimiento
    {
        public String cod_requerimiento { get; set; }
        public String id_categoria { get; set; }
        public String id_subcategoria { get; set; }
        public String id_servicio { get; set; }
        public String nombre_completo { get; set; }
        public String servicio_cliente { get; set; }
        public String nom_categoria { get; set; }
        public String nom_subcategoria { get; set; }
        public String nom_servicio { get; set; }
        public String nom_prioridad { get; set; }

        public String cod_prioridad { get; set; }
        public String nom_estado_requerimiento { get; set; }

        public String personal { get; set; }

        public String idUsuario { get; set; }
        public String idUsuarioC { get; set; }
        public String email { get; set; }
        public String email_jefe { get; set; }

        public String serviciosAdicionales { get; set; }

        public String servicioCurrent { get; set; }

        public String servicioActual { get; set; }

        public string fecha_requerimiento { get; set; }
        public string fecha_cierre { get; set; }
        public string descripcion { get; set; }
        public string detalle { get; set; }

        public String fecha_comentario { get; set; }

        public String fecha_asignacion { get; set; }

        public String comentario { get; set; }

        public string observacion { get; set; }

        public String foto1 { get; set; }
        public String foto2 { get; set; }
        public String foto3 { get; set; }
        public String id_perfil { get; set; }

        public String idUsuarioSesion { get; set; }
        public BeanDetalleRequerimiento()
        {
            this.cod_requerimiento = "";
            this.id_categoria = "";
            this.id_subcategoria = "";
            this.id_servicio = "";
            this.nombre_completo = "";
            this.nom_categoria = "";
            this.nom_subcategoria = "";
            this.nom_servicio = "";
            this.email = "";
            this.email_jefe = "";
            this.nom_prioridad = "";
            this.nom_estado_requerimiento = "";
            this.servicio_cliente ="";

            this.fecha_requerimiento = "";
            this.fecha_cierre = "";
            this.descripcion = "";
            this.detalle = "";
            this.personal = "";
            this.id_perfil = "";
        }
    }
}
