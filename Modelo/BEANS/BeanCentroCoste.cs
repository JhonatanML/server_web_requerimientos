﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.BEANS
{
    public class BeanCentroCoste
    {
        public string id_centro_coste { get; set; }
        public string nombre_centro_coste { get; set; }
        public string descripcion { get; set; }

        public BeanCentroCoste()
        {
            this.id_centro_coste = "";
            this.nombre_centro_coste = "";
            this.descripcion = "";
        }
    }
}
