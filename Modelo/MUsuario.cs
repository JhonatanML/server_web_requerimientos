﻿using Modelo.BEANS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilidad;

namespace Modelo
{
    public class MUsuario
    {
        public static DataTable tablaUsuarios()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("SPS_TABLA_USUARIO", alParameters);
        }

        public static DataTable permisosUsuario(Int32 idPerfil)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Id_Perfil", SqlDbType.Int);
            parameter.Value = idPerfil;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPS_MENU_PERFIL", alParameters);
        }
        
        public static DataTable permisosUsuarioR(Int32 idPerfil)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Id_Perfil", SqlDbType.Int);
            parameter.Value = idPerfil;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPS_WEB_MENU_PERFIL", alParameters);
        }

        public static DataTable operador_dt_combo()
        {
            ArrayList alParameters = new ArrayList();
            return SqlConnector.getDataTable("SPS_USUARIO_LIST_COMBO", alParameters);
        }

        //SD
        public static DataTable validarUsuario(String lgn, String pwd)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usuario", SqlDbType.VarChar, 20);
            parameter.Value = lgn;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@clave", SqlDbType.VarChar, 20);
            parameter.Value = pwd;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("SPS_VALIDAR_USUARIO", alParameters);

        }

        public static DataTable validarUsuarioR(String lgn, String pwd)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@usuario", SqlDbType.VarChar, 30);
            parameter.Value = lgn;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@clave", SqlDbType.VarChar, 30);
            parameter.Value = pwd;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("SPS_WEB_VALIDAR_USUARIOR", alParameters);

        }

        public static DataTable actualizarClave(String login, String oldClave, String newClave)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Login", SqlDbType.VarChar, 20);
            parameter.Value = login;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@oldClave", SqlDbType.VarChar, 20);
            parameter.Value = oldClave;
            alParameters.Add(parameter);
            parameter = new SqlParameter("@newClave", SqlDbType.VarChar, 20);
            parameter.Value = newClave;
            alParameters.Add(parameter);

            return SqlConnector.getDataTable("SPU_ACTUALIZAR_CLAVE", alParameters);
        }

        public static void actualizarDatos(String login, String clave, String newClave)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@Login", SqlDbType.VarChar, 20);
            parameter.Value = login;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Clave", SqlDbType.VarChar, 20);
            parameter.Value = clave;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@NewClave", SqlDbType.VarChar, 20);
            parameter.Value = newClave;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("SPU_ACTUALIZAR_DATOS_USER", alParameters);

        }

        public static void usuario_Borrar(String codigo, String estado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@codigo", SqlDbType.VarChar, 250);
            parameter.Value = codigo;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@estado", SqlDbType.Int);
            parameter.Value = estado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("SPU_ACTUALIZAR_ESTADO", alParameters);

        }

        public static void usuario_Nuevo(String login, String nombres, String perfil, String contrasena, String estado)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@login", SqlDbType.VarChar, 20);
            parameter.Value = login;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@nombres", SqlDbType.VarChar, 100);
            parameter.Value = nombres;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@perfil", SqlDbType.Int);
            parameter.Value = perfil;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@contrasena", SqlDbType.VarChar, 20);
            parameter.Value = contrasena;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@estado", SqlDbType.Int);
            parameter.Value = estado;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("SPI_NUEVO_USUARIO", alParameters);

        }
        

        public static DataTable obtenerDatos(String codigo)
        {
                ArrayList alParameters = new ArrayList();
                SqlParameter parameter = new SqlParameter("@IdUsuario", SqlDbType.Int);
                parameter.Value = codigo;
                alParameters.Add(parameter);

                return SqlConnector.getDataTable("SPS_USUARIO_GET_DATOS", alParameters);
            
        }

        public static void Usuario_actualizar(String codigo, String login, String nombres, String perfil, String clave)
        {
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@codigo", SqlDbType.Int);
            parameter.Value = int.Parse(codigo);
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Username", SqlDbType.VarChar, 20);
            parameter.Value = login;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Nombres", SqlDbType.VarChar, 100);
            parameter.Value = nombres;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@IdPerfil", SqlDbType.Int);
            parameter.Value = perfil;
            alParameters.Add(parameter);

            parameter = new SqlParameter("@Clave", SqlDbType.VarChar, 20);
            parameter.Value = clave;
            alParameters.Add(parameter);

            SqlConnector.getDataTable("SPU_EDITAR_USUARIO", alParameters);

        }

        //JM
        public static DataTable autoUsuario()
        {
            int codigo = 1;
            ArrayList alParameters = new ArrayList();
            SqlParameter parameter = new SqlParameter("@IdUsuario", SqlDbType.Int);
            parameter.Value = codigo;
            alParameters.Add(parameter);
            return SqlConnector.getDataTable("SPS_AUTO_USUARIOS", alParameters);
        }

        //SAGA
        public static DataTable paginarBuscarUsuarios(string cod, string nom, string suc, string page, string rows)
        {
            ArrayList alParametros = new ArrayList();
            if (cod.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 10);
                sqlParameter.Value = (object)cod;
                alParametros.Add((object)sqlParameter);
            }
            if (nom.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@USUARIO", SqlDbType.VarChar, 100);
                sqlParameter.Value = (object)nom;
                alParametros.Add((object)sqlParameter);
            }
            if (suc.Trim() != "-1")
            {
                SqlParameter sqlParameter = new SqlParameter("@SUCURSAL", SqlDbType.VarChar, 10);
                sqlParameter.Value = (object)suc;
                alParametros.Add((object)sqlParameter);
            }
            SqlParameter sqlParameter1 = new SqlParameter("@page", SqlDbType.VarChar, 20);
            sqlParameter1.Value = (object)Convert.ToInt32(page);
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.VarChar, 20);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            alParametros.Add((object)sqlParameter2);
            return SqlConnector.getDataTable("SPU_WEB_USUARIO_BUSCAR", alParametros);
        }

        public static int crearUsuario(UsuarioBean bean)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter1 = new SqlParameter("@CODIGO", SqlDbType.VarChar, 10);
            sqlParameter1.Value = (object)bean.codigo;
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 100);
            sqlParameter2.Value = (object)bean.nombre;
            alParametros.Add((object)sqlParameter2);
            SqlParameter sqlParameter3 = new SqlParameter("@CLAVE", SqlDbType.VarChar, 10);
            sqlParameter3.Value = (object)bean.clave;
            alParametros.Add((object)sqlParameter3);
            SqlParameter sqlParameter4 = new SqlParameter("@SUCURSAL", SqlDbType.VarChar, 10);
            sqlParameter4.Value = (object)bean.codSucursal;
            alParametros.Add((object)sqlParameter4);
            return Convert.ToInt32(SqlConnector.executeScalar("SPU_WEB_USUARIO_CREAR", alParametros));
        }

        public static int editarUsuario(UsuarioBean bean)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter1 = new SqlParameter("@ID", SqlDbType.BigInt);
            sqlParameter1.Value = (object)int.Parse(bean.id);
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@CODIGO", SqlDbType.VarChar, 10);
            sqlParameter2.Value = (object)bean.codigo;
            alParametros.Add((object)sqlParameter2);
            SqlParameter sqlParameter3 = new SqlParameter("@NOMBRE", SqlDbType.VarChar, 100);
            sqlParameter3.Value = (object)bean.nombre;
            alParametros.Add((object)sqlParameter3);
            SqlParameter sqlParameter4 = new SqlParameter("@CLAVE", SqlDbType.VarChar, 10);
            sqlParameter4.Value = (object)bean.clave;
            alParametros.Add((object)sqlParameter4);
            SqlParameter sqlParameter5 = new SqlParameter("@SUCURSAL", SqlDbType.VarChar, 10);
            sqlParameter5.Value = (object)bean.codSucursal;
            alParametros.Add((object)sqlParameter5);
            return Convert.ToInt32(SqlConnector.executeScalar("SPU_WEB_USUARIO_EDITAR", alParametros));
        }

        public static DataTable infoUsuario(string id)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@ID", SqlDbType.BigInt);
            sqlParameter.Value = (object)int.Parse(id);
            alParametros.Add((object)sqlParameter);
            return SqlConnector.getDataTable("SPU_WEB_USUARIO_INFO", alParametros);
        }

        public static void borrarUsuario(string id)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@COD", SqlDbType.VarChar, 4000);
            sqlParameter.Value = (object)id;
            alParametros.Add((object)sqlParameter);
            SqlConnector.getDataTable("SPU_WEB_USUARIO_BORRAR", alParametros);
        }

        public static DataTable BuscarUsuarioM(string id_usuario, string nom_usuario, string usuario, string idPerfil, string id_perfil, string page, string rows)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter parameter;

            if (id_usuario.Trim().Length != 0)
            {
                parameter = new SqlParameter("@id_usuario", SqlDbType.VarChar, 50);
                parameter.Value = (object)id_usuario;
                AllParameters.Add((object)parameter);
            }

            if (nom_usuario.Trim().Length != 0)
            {
                parameter = new SqlParameter("@nom_usuario", SqlDbType.VarChar, 50);
                parameter.Value = (object)nom_usuario;
                AllParameters.Add((object)parameter);
            }

            if (usuario.Trim().Length != 0)
            {
                parameter = new SqlParameter("@usuario", SqlDbType.VarChar, 15);
                parameter.Value = (object)usuario;
                AllParameters.Add((object)parameter);
            }

            if (idPerfil.Trim() != "-1")
            {
                parameter = new SqlParameter("@idPerfil", SqlDbType.Int);
                parameter.Value = (object)idPerfil;
                AllParameters.Add((object)parameter);
            }
            //parameter = new SqlParameter("@id_admin_general", SqlDbType.Int);
            //parameter.Value = (object)int.Parse(id_admin_general);
            //AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_perfil", SqlDbType.Int);
            parameter.Value = (object)int.Parse(id_perfil);
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@page", SqlDbType.Int);
            parameter.Value = (object)Convert.ToInt32(page);
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@rows", SqlDbType.Int);
            parameter.Value = (object)Convert.ToInt32(rows);
            AllParameters.Add((object)parameter);

            return SqlConnector.getDataTable("SPU_USUARIO_BUSCAR", AllParameters);
        }

        public static void EliminarUsuarioM(string id_usuario)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter;
            sqlParameter = new SqlParameter("@id_usuario", SqlDbType.VarChar, 4000);
            sqlParameter.Value = (object)id_usuario;
            alParametros.Add((object)sqlParameter);
            SqlConnector.getDataTable("SPU_USUARIO_BORRAR", alParametros);
        }

        public static int AgregarUsuarioM(BeanUsuario bean)
        {

            if (bean.codigo_supervisor == null)
            {
                bean.codigo_supervisor = "0";
            }
            else if (bean.codigo_supervisor == "-1")
            {
                bean.codigo_supervisor = "0";
            }

            if (bean.idLineaNegocio == null)
            {
                bean.idLineaNegocio = "0";
            }
            else if (bean.idLineaNegocio == "-1")
            {
                bean.idLineaNegocio = "0";
            }

            if (bean.idCentroCosto == null)
            {
                bean.idCentroCosto = "0";
            }
            else if (bean.idCentroCosto == "-1")
            {
                bean.idCentroCosto = "0";
            }

            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter;

            parameter = new SqlParameter("@apellido_paterno", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.apellido_paterno;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@apellido_materno", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.apellido_materno;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@nombres", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.nombres;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@dni", SqlDbType.Char, 10);
            parameter.Value = (object)bean.dni;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@email", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.email;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@telf_movil", SqlDbType.Char, 10);
            parameter.Value = (object)bean.telf_movil;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@telf_fijo", SqlDbType.Char, 10);
            parameter.Value = (object)bean.telf_fijo;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@direccion", SqlDbType.VarChar, 800);
            parameter.Value = (object)bean.direccion;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_perfil", SqlDbType.Int);
            parameter.Value = (object)bean.id_perfil;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_linea_negocio", SqlDbType.Int);
            parameter.Value = (object)bean.idLineaNegocio;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_centro_coste", SqlDbType.Int);
            parameter.Value = (object)bean.idCentroCosto;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@usuario", SqlDbType.Char, 15);
            parameter.Value = (object)bean.usuario;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@clave", SqlDbType.Char, 15);
            parameter.Value = (object)bean.clave;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@cod_supervisor", SqlDbType.VarChar, 20);
            parameter.Value = (object)bean.codigo_supervisor;
            AllParameters.Add((object)parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_USUARIO_AGREGAR", AllParameters));
        }

        public static DataTable ObtenerUsuarioM(string id_usuario)
        {
            ArrayList AllParameters = new ArrayList();
            SqlParameter parameter;
            parameter = new SqlParameter("@id_usuario", SqlDbType.Int);
            parameter.Value = (object)int.Parse(id_usuario);
            AllParameters.Add((object)parameter);

            return SqlConnector.getDataTable("SPU_USUARIO_INFO", AllParameters);
        }

        public static int EditarUsuarioM(BeanUsuario bean)
        {
            if (bean.codigo_supervisor == null)
            {
                bean.codigo_supervisor = "0";
            }
            else if (bean.codigo_supervisor == "-1")
            {
                bean.codigo_supervisor = "0";
            }

            ArrayList AllParameters = new ArrayList();

            SqlParameter parameter;

            parameter = new SqlParameter("@id_usuario", SqlDbType.Int);
            parameter.Value = (object)bean.id_usuario;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@apellido_paterno", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.apellido_paterno;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@apellido_materno", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.apellido_materno;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@nombres", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.nombres;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@dni", SqlDbType.Char, 10);
            parameter.Value = (object)bean.dni;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@email", SqlDbType.VarChar, 50);
            parameter.Value = (object)bean.email;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@telf_movil", SqlDbType.Char, 10);
            parameter.Value = (object)bean.telf_movil;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@telf_fijo", SqlDbType.Char, 10);
            parameter.Value = (object)bean.telf_fijo;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@direccion", SqlDbType.VarChar, 800);
            parameter.Value = (object)bean.direccion;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_perfil", SqlDbType.Int);
            parameter.Value = (object)bean.id_perfil;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_linea_negocio", SqlDbType.Int);
            parameter.Value = (object)bean.idLineaNegocio;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_centro_coste", SqlDbType.Int);
            parameter.Value = (object)bean.idCentroCosto;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@usuario", SqlDbType.Char, 15);
            parameter.Value = (object)bean.usuario;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@clave", SqlDbType.Char, 15);
            parameter.Value = (object)bean.clave;
            AllParameters.Add((object)parameter);

            parameter = new SqlParameter("@id_supervisor", SqlDbType.VarChar, 20);
            parameter.Value = (object)bean.codigo_supervisor;
            AllParameters.Add((object)parameter);

            return Convert.ToInt32(SqlConnector.executeScalar("SPU_USUARIO_EDITAR", AllParameters));
        }
    }
}
