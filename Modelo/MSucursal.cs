﻿using Modelo.BEANS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Utilidad;

namespace Modelo
{
    public class MSucursal
    {
        public static DataTable paginarBuscarSucursales(string cod, string nom, string page, string rows)
        {
            ArrayList alParametros = new ArrayList();
            if (cod.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@CODIGO", SqlDbType.VarChar, 10);
                sqlParameter.Value = (object)cod;
                alParametros.Add((object)sqlParameter);
            }
            if (nom.Trim().Length != 0)
            {
                SqlParameter sqlParameter = new SqlParameter("@SUCURSAL", SqlDbType.VarChar, 100);
                sqlParameter.Value = (object)nom;
                alParametros.Add((object)sqlParameter);
            }
            SqlParameter sqlParameter1 = new SqlParameter("@page", SqlDbType.VarChar, 20);
            sqlParameter1.Value = (object)Convert.ToInt32(page);
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@rows", SqlDbType.VarChar, 20);
            sqlParameter2.Value = (object)Convert.ToInt32(rows);
            alParametros.Add((object)sqlParameter2);
            return SqlConnector.getDataTable("SPU_WEB_SUCURSAL_BUSCAR", alParametros);
        }

        public static DataTable infoSucursal(string id)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@ID", SqlDbType.BigInt);
            sqlParameter.Value = (object)int.Parse(id);
            alParametros.Add((object)sqlParameter);
            return SqlConnector.getDataTable("SPU_WEB_SUCURSAL_INFO", alParametros);
        }

        public static void borrarSucursal(string id)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter = new SqlParameter("@COD", SqlDbType.VarChar, 4000);
            sqlParameter.Value = (object)id;
            alParametros.Add((object)sqlParameter);
            SqlConnector.getDataTable("SPU_WEB_SUCURSAL_BORRAR", alParametros);
        }

        public static int crearSucursal(SucursalBean bean)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter1 = new SqlParameter("@CODIGO", SqlDbType.VarChar, 10);
            sqlParameter1.Value = (object)bean.codigo;
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@SUCURSAL", SqlDbType.VarChar, 100);
            sqlParameter2.Value = (object)bean.nombre;
            alParametros.Add((object)sqlParameter2);
            return Convert.ToInt32(SqlConnector.executeScalar("SPU_WEB_SUCURSAL_CREAR", alParametros));
        }

        public static int editarSucursal(SucursalBean bean)
        {
            ArrayList alParametros = new ArrayList();
            SqlParameter sqlParameter1 = new SqlParameter("@ID", SqlDbType.BigInt);
            sqlParameter1.Value = (object)int.Parse(bean.id);
            alParametros.Add((object)sqlParameter1);
            SqlParameter sqlParameter2 = new SqlParameter("@CODIGO", SqlDbType.VarChar, 10);
            sqlParameter2.Value = (object)bean.codigo;
            alParametros.Add((object)sqlParameter2);
            SqlParameter sqlParameter3 = new SqlParameter("@SUCURSAL", SqlDbType.VarChar, 100);
            sqlParameter3.Value = (object)bean.nombre;
            alParametros.Add((object)sqlParameter3);
            return Convert.ToInt32(SqlConnector.executeScalar("SPU_WEB_SUCURSAL_EDITAR", alParametros));
        }

    }
}
